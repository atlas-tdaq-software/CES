# CES

Package: [CES](https://gitlab.cern.ch/atlas-tdaq-software/CES)  
Jira: [ATDAQCCCES](https://its.cern.ch/jira/browse/ATDAQCCCES)  

Live documentation about recoveries and procedure can be found [here](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltErrorRecoverySystem).

**Changes in recoveries and/or procedures**:    

- When the `TDAQ_CES_FORCE_REMOVAL_AUTOMATIC` environment variable is set to `true`, then no acknowledgment is asked by the operator
  for a stop-less removal action to be executed (regardless of the machine or beam mode);
- Stop-less removal involving the SwRod: the reporting application now always receives the initial list of components back (and not only the valid
  components, as it was before);
- After each clock switch, a new command is sent to AFP and ZDC;
- The `BeamSpotArchiver_PerBunchLiveMon` application it not started at warm stop anymore;
- The `DCM` is now notified when a `SwROD` dies or is restarted (in the same way as it happens with a ROS or a SFO).

**Internal changes**:       

- Following changes in the `MasterTrigger` interface;
- Fixed bug not allowing the auto-pilot to be disabled in some conditions;
- Fixed bug causing the ERS `HoldingTriggerAction` message to not be sent.

## tdaq-09-03-00

**Changes in recoveries and/or procedures**:    

-   Fixed `maxEvents` procedure using wrong IS server name;
-   Handling root controller failures ([ATDAQCCCES-155](https://its.cern.ch/jira/browse/ATDAQCCCES-155));
-   Introducing synchronous actions when a transition is done ([ATDAQCCCES-156](https://its.cern.ch/jira/browse/ATDAQCCCES-156)).

**Internal changes**:       

-   OKS configuration parsing updated to follow changes in the `SwRod` schema;
-   Not sending an event to `ESPER` when its name cannot be determined;
-   Proper exception handling from the `config` layer;
-   Following changes in `PMG` Java lib;
-   EPL: using parameters in modules ([ATDAQCCCES-157](https://its.cern.ch/jira/browse/ATDAQCCCES-157));
-   EPL: the name of the root controller is no more hard-coded;
-   EPL: using hashed contexts for `PerRCApplication` and `SegmentedByProblemPerApplication`;
-   IS flag used to enable `ESPER` metrics moved to `RunParams`.

## tdaq-09-02-01

**Changes in recoveries and/or procedures**:    

-   HLT recoveries: sending `FORK_AFTER_CRASH` only if `RUNNING` ([ADHI-4780](https://its.cern.ch/jira/browse/ADHI-4780)).      

**Internal changes**:       

-   The `ESPER CEP` engine has been updated to version **8.5**;      
-   Fixed memory leak in the `Metaspace` ([ATDAQCCCES-154](https://its.cern.ch/jira/browse/ATDAQCCCES-154));    
-   Added sTGC as a possible source of trigger on hold; 
-   Avoid concurrent executions of the `ESPER` compiler.  

## tdaq-09-00-00

**Changes in recoveries and/or procedures**:

-   Stopless recovery: handle failures on the detector side ([ATDAQCCCES-149](https://its.cern.ch/jira/browse/ATDAQCCCES-149)):

    In case no channels can be recovered, the `rc::HardwareRecovered` message can be sent with an empty list of recovered channels. In that case, `CHIP` will notify of the failure in recovery but the trigger will be released.     
    
-   Stop-less removal and recovery adapted to the new `SWROD` ([ATDAQCCCES-137](https://its.cern.ch/jira/browse/ATDAQCCCES-137));   
-   `HLT` recoveries adapted to the new `HLT` framework ([ATDAQCCCES-138](https://its.cern.ch/jira/browse/ATDAQCCCES-138)). 

**Internal changes**:

-   The `ESPER CEP` engine has been updated to version 8 ([ATDAQCCCES-133](https://its.cern.ch/jira/browse/ATDAQCCCES-133));
-   Fixed scalability issue observed after the introduction of `ESPER` 8 ([ATDAQCCCES-150](https://its.cern.ch/jira/browse/ATDAQCCCES-150));
-   Publishing information about the status of the Java Virtual Machine ([ATDAQCCCES-144](https://its.cern.ch/jira/browse/ATDAQCCCES-144));
-   Proper handling of `ERS` fatal messages ([ATDAQCCCES-147](https://its.cern.ch/jira/browse/ATDAQCCCES-147));
-   Fixing an issue with the `BAD_HOST` problem not being cleared ([ATDAQCCCES-143](https://its.cern.ch/jira/browse/ATDAQCCCES-143));
-   Executing tests in case of transition timeout ([ATDAQCCCES-140](https://its.cern.ch/jira/browse/ATDAQCCCES-140));
-   Adapting to changes in `DAL` (new implementation of template applications and segments);
-   Adapting to changes in `DVS/TestManager` (new implementations).