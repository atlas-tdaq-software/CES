package chip.test;

import java.util.EnumSet;
import java.util.Properties;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.espertech.esper.common.client.scopetest.EPAssertionUtil;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.scopetest.SupportUpdateListener;

import chip.CEPService;
import chip.DAQConfigReader.APP_TYPE;
import chip.dao.EsperDAO;
import chip.event.en.FSM;
import chip.event.en.IFDIES;
import chip.event.en.IFFAILED;
import chip.event.en.MEMBERSHIP;
import chip.event.en.STARTAT;
import chip.event.en.STATUS;
import chip.event.en.STOPAT;
import chip.event.en.TRCOMMAND;
import chip.event.issue.core.Problem;
import chip.event.meta.RCApplication;
import chip.executor.ExecutorRC;
import chip.listener.PrintDebug;
import chip.listener.PrintErs;
import chip.listener.PrintInfo;
import chip.listener.PrintXml;
import chip.subscriber.core.ProblemExecutorAsynch;
import chip.subscriber.core.ProblemExecutorSynch;
import chip.subscriber.meta.SynchActionExecutor;
import chip.utils.ExecutorFactory;
import daq.rc.CommandSender;


/**
 * Unit tests for the validation of the Application event generation, as union of static configuration events and dynamic information events
 * about application status and, in case, state information.
 * <p>
 * This test uses external time source to define time passing in Esper, for a better testing of out of order event injection
 * <p>
 * Tests environment is build using Spring Java Config, as from here:
 * http://blog.springsource.org/2011/06/21/spring-3-1-m2-testing-with-configuration-classes-and-profiles/
 * 
 * @author lmagnoni
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ApplicationDeadTest.TestConfiguration.class)
public class ApplicationDeadTest {

    @Autowired
    private CEPService cep;

    @Autowired
    private EsperDAO dao;

    @Before
    public void setUp() throws Exception {
        Utils.setup(this.cep);
    }

    @After
    public void tearDown() throws Exception {
        Utils.reset(this.cep);
    }

    /**
     * Test to validate the detection of a dead application and the error reset when the application is up again C_ok A1_ok A1_bad
     * 
     * @throws InterruptedException
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void applicationDeadDetectedAndReset()
        throws InterruptedException,
            EPRuntimeDestroyedException,
            EPCompileException,
            EPDeployException
    {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller = new RCApplication.DefaultBuilder("mycontroller").isController(Boolean.TRUE).build();
        this.cep.injectEventSynch(controller);

        final RCApplication app1_ok =
                                    new RCApplication.DefaultBuilder("app1").controller("mycontroller").status(STATUS.UP).startAtState(FSM.INITIAL).stopAtState(FSM.NONE).parentState(FSM.CONNECTED).build();
        this.cep.injectEventSynch(app1_ok);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Create a new event from app1_ok and change status to absent
        final RCApplication app1_bad = new RCApplication.CopyBuilder(app1_ok).status(STATUS.ABSENT).build();
        this.cep.injectEventSynch(app1_bad);

        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "mycontroller"});

        // Check Problem events in Esper working memory
        // Be careful, Problem event creation is asynch and may take a while
        Thread.sleep(500);
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()), CoreMatchers.is(Integer.valueOf(1)));

        // Check error reset
        this.cep.injectEventSynch(app1_ok);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "mycontroller"});

        // Check Problem events is esper working memory
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()), CoreMatchers.is(Integer.valueOf(0)));

    }

    /**
     * Test to validate the detection of a dead application and the error reset when the application is up again C_ok A1_ok A1_bad
     * 
     * @throws InterruptedException
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void applicationDeadAndIgnore() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller = new RCApplication.DefaultBuilder("mycontroller").isController(Boolean.TRUE).build();
        this.cep.injectEventSynch(controller);

        final RCApplication app1_ok =
                                    new RCApplication.DefaultBuilder("app1").controller("mycontroller").status(STATUS.UP).startAtState(FSM.NONE).stopAtState(FSM.ABSENT).parentState(FSM.NONE).build();
        this.cep.injectEventSynch(app1_ok);

        final RCApplication app2_ok =
                                    new RCApplication.DefaultBuilder("app2").controller("mycontroller").status(STATUS.UP).ifDies(IFDIES.IGNORE).startAtState(FSM.NONE).stopAtState(FSM.ABSENT).parentState(FSM.NONE).build();
        this.cep.injectEventSynch(app2_ok);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Create a new event from app1_ok and change status to absent
        final RCApplication app2_bad = new RCApplication.CopyBuilder(app2_ok).status(STATUS.ABSENT).build();
        this.cep.injectEventSynch(app2_bad);

        Thread.sleep(600);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.IGNORE, "mycontroller"});

        // Create a new event from app1_ok and change status to absent
        final RCApplication app1_bad = new RCApplication.CopyBuilder(app1_ok).status(STATUS.ABSENT).build();
        this.cep.injectEventSynch(app1_bad);

        Thread.sleep(600);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "mycontroller"});

        // Check Problem events in Esper working memory
        // Be careful, Problem event creation is asynch and may take a while
        Thread.sleep(100);
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()), CoreMatchers.is(Integer.valueOf(1)));

        // Check error reset
        this.cep.injectEventSynch(app1_ok);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "mycontroller"});

        // Check Problem events is esper working memory
        // assertThat(dao.getAllHandlingProblemByController("mycontroller").size(), is(0));

    }
    
    @Test
    public void applicationDeadAndRestart() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        // Use an external clock in order to test the automatic restart afte some time is elapsed
        this.cep.getEsper().getEventService().clockExternal();
        this.cep.getEsper().getEventService().advanceTime(0);
        
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication root = new RCApplication.DefaultBuilder("RootController").isController(Boolean.TRUE)
                                       .state(FSM.INITIAL).controller("RootController").parentState(FSM.INITIAL).build();
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(root);
        
        final RCApplication controller = new RCApplication.DefaultBuilder("mycontroller").isController(Boolean.TRUE)
                                       .state(FSM.INITIAL).parentState(FSM.INITIAL).controller("RootController")
                                       .parentIsExiting(Boolean.FALSE).parentIsInitializing(Boolean.FALSE).build();
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(controller);

        final RCApplication app2_ok =
                                    new RCApplication.DefaultBuilder("app2").controller("mycontroller").status(STATUS.UP).state(FSM.INITIAL)
                                        .ifFailed(IFFAILED.IGNORE).ifDies(IFDIES.RESTART).startAtState(FSM.NONE).stopAtState(FSM.ABSENT)
                                        .parentState(FSM.INITIAL).parentIsExiting(Boolean.FALSE).parentIsInitializing(Boolean.FALSE).build();
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(app2_ok);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        final RCApplication app2_bad = new RCApplication.CopyBuilder(app2_ok).status(STATUS.EXITED).build();
        
        // Check that the app is restarted up to a certain number of times (MAX_RESTARTS in problems.epl)
        for(int nRestarts = 0; nRestarts < 6; nRestarts++) {
            // Create a new event from app1_ok and change status to absent
            this.cep.getEsper().getEventService().advanceTime(100);
            this.cep.injectEventSynch(app2_bad);
    
            Thread.sleep(500);
           
            EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                        new String[] {"application", "action", "controller"},
                                        new Object[] {"app2", Problem.ACTION.RESTART, "mycontroller"});
            
            this.cep.getEsper().getEventService().advanceTime(100);
            this.cep.injectEventSynch(app2_ok);
    
            Thread.sleep(1000);
            
            EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                        new String[] {"application", "action", "controller"},
                                        new Object[] {"app2", Problem.ACTION.REMOVE_ERROR, "mycontroller"});
        }
        
        // Next failure should cause the application to be ignored
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(app2_bad);
        
        Thread.sleep(500);
       
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.IGNORE, "mycontroller"});
        
        // Put the app out of membership
        final RCApplication app2_out = new RCApplication.CopyBuilder(app2_bad).membership(MEMBERSHIP._OUT).build();
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(app2_out);

        // Reset the listener for any event caused by the change of membership
        listener.reset();
        
        // Advance the time in order to let the events leave the time window (length of the window is LENGTH_ACTION_HISTORY in problems.epl)
        // That should cause the application to be put back in membership (and hence restarted)
        this.cep.getEsper().getEventService().advanceTime(18000000);
        
        Thread.sleep(1000);
        
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.INCLUDE, "mycontroller"});
    }    

    // Like applicationDeadAndRestart but in NONE
    @Test
    public void applicationDeadAndRestart_2() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        // Use an external clock in order to test the automatic restart afte some time is elapsed
        this.cep.getEsper().getEventService().clockExternal();
        this.cep.getEsper().getEventService().advanceTime(0);
        
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication root = new RCApplication.DefaultBuilder("RootController").isController(Boolean.TRUE)
                                       .state(FSM.NONE).controller("RootController").parentState(FSM.NONE).build();
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(root);
        
        final RCApplication controller = new RCApplication.DefaultBuilder("mycontroller").isController(Boolean.TRUE)
                                       .state(FSM.NONE).parentState(FSM.NONE).controller("RootController")
                                       .parentIsExiting(Boolean.FALSE).parentIsInitializing(Boolean.FALSE).build();
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(controller);

        final RCApplication app2_ok =
                                    new RCApplication.DefaultBuilder("app2").controller("mycontroller").status(STATUS.UP).state(FSM.NONE)
                                        .ifFailed(IFFAILED.IGNORE).ifDies(IFDIES.RESTART).startAtState(FSM.NONE).stopAtState(FSM.ABSENT)
                                        .parentState(FSM.NONE).parentIsExiting(Boolean.FALSE).parentIsInitializing(Boolean.FALSE).build();
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(app2_ok);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        final RCApplication app2_bad = new RCApplication.CopyBuilder(app2_ok).status(STATUS.EXITED).build();
        
        // Check that the app is restarted up to a certain number of times (MAX_RESTARTS in problems.epl)
        for(int nRestarts = 0; nRestarts < 6; nRestarts++) {
            // Create a new event from app1_ok and change status to absent
            this.cep.getEsper().getEventService().advanceTime(100);
            this.cep.injectEventSynch(app2_bad);
    
            Thread.sleep(500);
           
            EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                        new String[] {"application", "action", "controller"},
                                        new Object[] {"app2", Problem.ACTION.RESTART, "mycontroller"});
            
            this.cep.getEsper().getEventService().advanceTime(100);
            this.cep.injectEventSynch(app2_ok);
    
            Thread.sleep(1000);
            
            EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                        new String[] {"application", "action", "controller"},
                                        new Object[] {"app2", Problem.ACTION.REMOVE_ERROR, "mycontroller"});
        }
        
        // Next failure should cause the application to be ignored
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(app2_bad);
        
        Thread.sleep(500);
       
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.IGNORE, "mycontroller"});
        
        // Put the app out of membership
        final RCApplication app2_out = new RCApplication.CopyBuilder(app2_bad).membership(MEMBERSHIP._OUT).build();
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(app2_out);

        // Reset the listener for any event caused by the change of membership
        listener.reset();
        
        // Advance the time in order to let the events leave the time window (length of the window is LENGTH_ACTION_HISTORY in problems.epl)
        // That should NOT cause the application to be put back in membership (and hence restarted) because we are in NONE
        this.cep.getEsper().getEventService().advanceTime(18000000);
        
        Thread.sleep(1000);
        
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));        
    }    
    
    // Like applicationDeadAndRestart_2 but with the app being a child of the RootController
    @Test
    public void applicationDeadAndRestart_3() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        // Use an external clock in order to test the automatic restart afte some time is elapsed
        this.cep.getEsper().getEventService().clockExternal();
        this.cep.getEsper().getEventService().advanceTime(0);
        
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication root = new RCApplication.DefaultBuilder("RootController").isController(Boolean.TRUE)
                                       .state(FSM.NONE).controller("RootController").parentState(FSM.NONE).build();
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(root);
        
        final RCApplication app2_ok =
                                    new RCApplication.DefaultBuilder("app2").controller("RootController").status(STATUS.UP).state(FSM.NONE)
                                        .ifFailed(IFFAILED.IGNORE).ifDies(IFDIES.RESTART).startAtState(FSM.NONE).stopAtState(FSM.ABSENT)
                                        .parentState(FSM.NONE).parentIsExiting(Boolean.FALSE).parentIsInitializing(Boolean.FALSE).build();
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(app2_ok);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        final RCApplication app2_bad = new RCApplication.CopyBuilder(app2_ok).status(STATUS.EXITED).build();
        
        // Check that the app is restarted up to a certain number of times (MAX_RESTARTS in problems.epl)
        for(int nRestarts = 0; nRestarts < 6; nRestarts++) {
            // Create a new event from app1_ok and change status to absent
            this.cep.getEsper().getEventService().advanceTime(100);
            this.cep.injectEventSynch(app2_bad);
    
            Thread.sleep(500);
           
            EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                        new String[] {"application", "action", "controller"},
                                        new Object[] {"app2", Problem.ACTION.RESTART, "RootController"});
            
            this.cep.getEsper().getEventService().advanceTime(100);
            this.cep.injectEventSynch(app2_ok);
    
            Thread.sleep(1000);
            
            EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                        new String[] {"application", "action", "controller"},
                                        new Object[] {"app2", Problem.ACTION.REMOVE_ERROR, "RootController"});
        }
        
        // Next failure should cause the application to be ignored
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(app2_bad);
        
        Thread.sleep(500);
       
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.IGNORE, "RootController"});
        
        // Put the app out of membership
        final RCApplication app2_out = new RCApplication.CopyBuilder(app2_bad).membership(MEMBERSHIP._OUT).build();
        this.cep.getEsper().getEventService().advanceTime(100);
        this.cep.injectEventSynch(app2_out);

        // Reset the listener for any event caused by the change of membership
        listener.reset();
        
        // Advance the time in order to let the events leave the time window (length of the window is LENGTH_ACTION_HISTORY in problems.epl)
        // That should NOT cause the application to be put back in membership (and hence restarted) because we are in NONE
        this.cep.getEsper().getEventService().advanceTime(18000000);
        
        Thread.sleep(1000);
        
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.INCLUDE, "RootController"});
    }    
    
    @Test
    public void startAtCONFIGURE_stopAtUNCONFIGURE() throws Exception {
        final STARTAT startAt = STARTAT.CONFIGURE;
        final STOPAT stopAt = STOPAT.UNCONFIGURE;

        final FSM startAtFSM = startAt.toFSM(APP_TYPE.OTHER);
        final FSM stopAtFSM = stopAt.toFSM(APP_TYPE.OTHER);

        Assert.assertEquals(FSM.CONFIGURED, startAtFSM);
        Assert.assertEquals(FSM.INITIAL, stopAtFSM);

        this.tearDown();

        final EnumSet<FSM> shouldBeUp = EnumSet.of(FSM.RUNNING,
                                                   FSM.ROIBSTOPPED,
                                                   FSM.DCSTOPPED,
                                                   FSM.HLTSTOPPED,
                                                   FSM.SFOSTOPPED,
                                                   FSM.GTHSTOPPED,
                                                   FSM.CONNECTED,
                                                   FSM.CONFIGURED);
        for(final FSM state : shouldBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, true);
            this.tearDown();
        }

        final EnumSet<FSM> shouldNotBeUp = EnumSet.of(FSM.INITIAL, FSM.NONE, FSM.ABSENT);
        for(final FSM state : shouldNotBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, false);
            this.tearDown();
        }
    }

    @Test
    public void startAtSOR_stopAtEOR() throws Exception {
        final STARTAT startAt = STARTAT.SOR;
        final STOPAT stopAt = STOPAT.EOR;

        final FSM startAtFSM = startAt.toFSM(APP_TYPE.OTHER);
        final FSM stopAtFSM = stopAt.toFSM(APP_TYPE.OTHER);

        Assert.assertEquals(FSM.RUNNING, startAtFSM);
        Assert.assertEquals(FSM.CONNECTED, stopAtFSM);

        this.tearDown();

        final EnumSet<FSM> shouldBeUp = EnumSet.of(FSM.RUNNING,
                                                   FSM.ROIBSTOPPED,
                                                   FSM.DCSTOPPED,
                                                   FSM.HLTSTOPPED,
                                                   FSM.SFOSTOPPED,
                                                   FSM.GTHSTOPPED);
        for(final FSM state : shouldBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, true);
            this.tearDown();
        }

        final EnumSet<FSM> shouldNotBeUp = EnumSet.of(FSM.CONNECTED, FSM.CONFIGURED, FSM.INITIAL, FSM.NONE, FSM.ABSENT);
        for(final FSM state : shouldNotBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, false);
            this.tearDown();
        }
    }

    @Test
    public void startAtEOR_stopAtSOR() throws Exception {
        final STARTAT startAt = STARTAT.EOR;
        final STOPAT stopAt = STOPAT.SOR;

        final FSM startAtFSM = startAt.toFSM(APP_TYPE.OTHER);
        final FSM stopAtFSM = stopAt.toFSM(APP_TYPE.OTHER);

        Assert.assertEquals(FSM.CONNECTED, startAtFSM);
        Assert.assertEquals(FSM.RUNNING, stopAtFSM);

        this.tearDown();

        final EnumSet<FSM> shouldBeUp = EnumSet.of(FSM.CONNECTED, FSM.CONFIGURED, FSM.INITIAL, FSM.NONE);
        for(final FSM state : shouldBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, true);
            this.tearDown();
        }

        final EnumSet<FSM> shouldNotBeUp = EnumSet.of(FSM.RUNNING,
                                                      FSM.ROIBSTOPPED,
                                                      FSM.DCSTOPPED,
                                                      FSM.HLTSTOPPED,
                                                      FSM.SFOSTOPPED,
                                                      FSM.GTHSTOPPED);
        for(final FSM state : shouldNotBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, false);
            this.tearDown();
        }
    }

    @Test
    public void startAtUSER_stopAtSHUTDOWN() throws Exception {
        final STARTAT startAt = STARTAT.USERDEFINED;
        final STOPAT stopAt = STOPAT.SHUTDOWN;

        final FSM startAtFSM = startAt.toFSM(APP_TYPE.OTHER);
        final FSM stopAtFSM = stopAt.toFSM(APP_TYPE.OTHER);

        Assert.assertEquals(FSM._EMPTY, startAtFSM);
        Assert.assertEquals(FSM.NONE, stopAtFSM);

        this.tearDown();

        final EnumSet<FSM> shouldBeUp = EnumSet.of(FSM.INITIAL,
                                                   FSM.CONFIGURED,
                                                   FSM.CONNECTED,
                                                   FSM.GTHSTOPPED,
                                                   FSM.SFOSTOPPED,
                                                   FSM.HLTSTOPPED,
                                                   FSM.DCSTOPPED,
                                                   FSM.ROIBSTOPPED,
                                                   FSM.RUNNING);
        for(final FSM state : shouldBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, true);
            this.tearDown();
        }

        final EnumSet<FSM> shouldNotBeUp = EnumSet.of(FSM.ABSENT, FSM.NONE);
        for(final FSM state : shouldNotBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, false);
            this.tearDown();
        }
    }

    @Test
    public void startAt_stopAt_OsInfra() throws Exception {
        final STARTAT startAt = STARTAT.BOOT;
        final STOPAT stopAt = STOPAT.SHUTDOWN;

        final FSM startAtFSM = startAt.toFSM(APP_TYPE.OS_INFRASTRUCTURE);
        final FSM stopAtFSM = stopAt.toFSM(APP_TYPE.OS_INFRASTRUCTURE);

        Assert.assertEquals(FSM.NONE, startAtFSM);
        Assert.assertEquals(FSM.ABSENT, stopAtFSM);

        this.tearDown();

        final EnumSet<FSM> shouldBeUp = EnumSet.of(FSM.NONE,
                                                   FSM.INITIAL,
                                                   FSM.CONFIGURED,
                                                   FSM.CONNECTED,
                                                   FSM.GTHSTOPPED,
                                                   FSM.SFOSTOPPED,
                                                   FSM.HLTSTOPPED,
                                                   FSM.DCSTOPPED,
                                                   FSM.ROIBSTOPPED,
                                                   FSM.RUNNING);
        for(final FSM state : shouldBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, true);
            this.tearDown();
        }
    }

    @Test
    public void startAt_stopAt_OnlSeg() throws Exception {
        final STARTAT startAt = STARTAT.BOOT;
        final STOPAT stopAt = STOPAT.SHUTDOWN;

        final FSM startAtFSM = startAt.toFSM(APP_TYPE.ONLINE_SEGMENT);
        final FSM stopAtFSM = stopAt.toFSM(APP_TYPE.ONLINE_SEGMENT);

        Assert.assertEquals(FSM.INITIAL, startAtFSM);
        Assert.assertEquals(FSM.NONE, stopAtFSM);

        this.tearDown();

        final EnumSet<FSM> shouldBeUp = EnumSet.of(FSM.INITIAL,
                                                   FSM.CONFIGURED,
                                                   FSM.CONNECTED,
                                                   FSM.GTHSTOPPED,
                                                   FSM.SFOSTOPPED,
                                                   FSM.HLTSTOPPED,
                                                   FSM.DCSTOPPED,
                                                   FSM.ROIBSTOPPED,
                                                   FSM.RUNNING);
        for(final FSM state : shouldBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, true);
            this.tearDown();
        }

        final EnumSet<FSM> shouldNotBeUp = EnumSet.of(FSM.NONE, FSM.ABSENT);
        for(final FSM state : shouldNotBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, false);
            this.tearDown();
        }
    }

    @Test
    public void startAt_stopAt_Infr() throws Exception {
        final STARTAT startAt = STARTAT.BOOT;
        final STOPAT stopAt = STOPAT.SHUTDOWN;

        final FSM startAtFSM = startAt.toFSM(APP_TYPE.INFRASTRUCTURE);
        final FSM stopAtFSM = stopAt.toFSM(APP_TYPE.INFRASTRUCTURE);

        Assert.assertEquals(FSM.INITIAL, startAtFSM);
        Assert.assertEquals(FSM.NONE, stopAtFSM);

        this.tearDown();

        final EnumSet<FSM> shouldBeUp = EnumSet.of(FSM.INITIAL,
                                                   FSM.CONFIGURED,
                                                   FSM.CONNECTED,
                                                   FSM.GTHSTOPPED,
                                                   FSM.SFOSTOPPED,
                                                   FSM.HLTSTOPPED,
                                                   FSM.DCSTOPPED,
                                                   FSM.ROIBSTOPPED,
                                                   FSM.RUNNING);
        for(final FSM state : shouldBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, true);
            this.tearDown();
        }

        final EnumSet<FSM> shouldNotBeUp = EnumSet.of(FSM.NONE, FSM.ABSENT);
        for(final FSM state : shouldNotBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, false);
            this.tearDown();
        }
    }

    @Test
    public void startAt_stopAt_RunControl() throws Exception {
        final STARTAT startAt = STARTAT.BOOT;
        final STOPAT stopAt = STOPAT.SHUTDOWN;

        final FSM startAtFSM = startAt.toFSM(APP_TYPE.RUNCONTROL);
        final FSM stopAtFSM = stopAt.toFSM(APP_TYPE.RUNCONTROL);

        Assert.assertEquals(FSM.INITIAL, startAtFSM);
        Assert.assertEquals(FSM.NONE, stopAtFSM);

        this.tearDown();

        final EnumSet<FSM> shouldBeUp = EnumSet.of(FSM.INITIAL,
                                                   FSM.CONFIGURED,
                                                   FSM.CONNECTED,
                                                   FSM.GTHSTOPPED,
                                                   FSM.SFOSTOPPED,
                                                   FSM.HLTSTOPPED,
                                                   FSM.DCSTOPPED,
                                                   FSM.ROIBSTOPPED,
                                                   FSM.RUNNING);
        for(final FSM state : shouldBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, true);
            this.tearDown();
        }

        final EnumSet<FSM> shouldNotBeUp = EnumSet.of(FSM.NONE, FSM.ABSENT);
        for(final FSM state : shouldNotBeUp) {
            this.setUp();
            this.startAt_stopAt_impl(startAtFSM, stopAtFSM, startAt, stopAt, state, false);
            this.tearDown();
        }
    }

    private void startAt_stopAt_impl(final FSM startAtFSM,
                                     final FSM stopAtFSM,
                                     final STARTAT startAt,
                                     final STOPAT stopAt,
                                     final FSM parentState,
                                     final boolean expectError)
        throws InterruptedException,
            EPRuntimeDestroyedException,
            EPCompileException,
            EPDeployException
    {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action = chip.event.issue.core.Problem$ACTION.SET_ERROR");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller = new RCApplication.DefaultBuilder("mycontroller").isController(Boolean.TRUE).build();
        this.cep.injectEventSynch(controller);

        final RCApplication app1_ok =
                                    new RCApplication.DefaultBuilder("app1").controller("mycontroller").status(STATUS.UP).allowSpontaneousExit(Boolean.FALSE).startAtState(startAtFSM).stopAtState(stopAtFSM).startAt(startAt).stopAt(stopAt).parentIsTransitioning(Boolean.FALSE).parentIsExiting(Boolean.FALSE).parentState(parentState).ifDies(IFDIES.ERROR).build();
        this.cep.injectEventSynch(app1_ok);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Create a new event from app1_ok and change status to absent
        final RCApplication app1_absent = new RCApplication.CopyBuilder(app1_ok).status(STATUS.EXITED).build();
        this.cep.injectEventSynch(app1_absent);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.valueOf(expectError)));

        // Check Problem events in Esper working memory
        // Be careful, Problem event creation is asynch and may take a while
        Thread.sleep(100);
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()),
                          CoreMatchers.is(expectError ? Integer.valueOf(1) : Integer.valueOf(0)));
    }

    /**
     * Test to validate the application event creation given a config and info events C_ok A1_ok A2_ok A1_bad A2_bad A1_ok A2_ok
     * 
     * @throws InterruptedException
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void manyDeadApplications() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        // Inject config and info events for application "app1", with TS=TS+1 (in millisecond)
        final RCApplication controller = new RCApplication.DefaultBuilder("mycontroller").isController(Boolean.TRUE).build();
        this.cep.injectEventSynch(controller);
        final RCApplication app1_ok =
                                    new RCApplication.DefaultBuilder("app1").controller("mycontroller").status(STATUS.UP).startAtState(FSM.RUNNING).stopAtState(FSM.CONNECTED).parentState(FSM.ROIBSTOPPED).build();
        this.cep.injectEventSynch(app1_ok);
        final RCApplication app2_ok =
                                    new RCApplication.DefaultBuilder("app2").controller("mycontroller").status(STATUS.UP).startAtState(FSM.INITIAL).stopAtState(FSM.NONE).parentState(FSM.ROIBSTOPPED).build();
        // Now the time of the buffer window is gone, I should see an event
        this.cep.injectEventSynch(app2_ok);

        // Create a new event from app1_ok and change status to absent
        final RCApplication app1_bad = new RCApplication.CopyBuilder(app1_ok).status(STATUS.ABSENT).build();
        this.cep.injectEventSynch(app1_bad);

        Thread.sleep(600);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "mycontroller"});

        final RCApplication app2_bad = new RCApplication.CopyBuilder(app2_ok).status(STATUS.ABSENT).build();
        this.cep.injectEventSynch(app2_bad);

        Thread.sleep(600);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.SET_ERROR, "mycontroller"});

        // Give the time to inject the problem event
        Thread.sleep(100);

        // Check error reset
        this.cep.injectEventSynch(app1_ok);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "mycontroller"});

        this.cep.injectEventSynch(app2_ok);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.REMOVE_ERROR, "mycontroller"});

    }

    /**
     * Test to validate the application event creation given a config and info events
     * 
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void applicationDeadControllerStopping() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        // Inject config and info events for application "app1", with TS=TS+1 (in millisecond)
        final RCApplication controller = new RCApplication.DefaultBuilder("mycontroller").isController(Boolean.TRUE).build();
        final RCApplication app1_ok = new RCApplication.DefaultBuilder("app1").controller("mycontroller").status(STATUS.UP).build();

        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(app1_ok);

        final RCApplication controller_transitioning =
                                                     new RCApplication.CopyBuilder(controller).isTransitioning(Boolean.TRUE).currentTransitionCommand(TRCOMMAND.STOP).build();

        this.cep.injectEvent(controller_transitioning);
        // Create a new event from app1_ok and change status to absent
        final RCApplication app1_bad =
                                     new RCApplication.CopyBuilder(app1_ok).status(STATUS.ABSENT).parentIsTransitioning(Boolean.TRUE).build();

        this.cep.injectEventSynch(app1_bad);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // EPAssertionUtil.assertProps(listener.assertOneGetNew(),new String[]{"application"}, new String[]{"app1"});

    }

    /**
     * Test to validate the application event creation given a config and info events
     * 
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void applicationDeadControllerShutdown() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        // Inject config and info events for application "app1", with TS=TS+1 (in millisecond)
        // Inject config and info events for application "app1", with TS=TS+1 (in millisecond)
        final RCApplication controller = new RCApplication.DefaultBuilder("mycontroller").build();
        final RCApplication app1_ok = new RCApplication.DefaultBuilder("app1").controller("mycontroller").status(STATUS.UP).build();

        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(app1_ok);

        final RCApplication controller_shutdown =
                                                new RCApplication.CopyBuilder(controller).isTransitioning(Boolean.TRUE).currentTransitionCommand(TRCOMMAND.SHUTDOWN).build();

        this.cep.injectEventSynch(controller_shutdown);

        final RCApplication app1_bad =
                                     new RCApplication.CopyBuilder(app1_ok).status(STATUS.ABSENT).parentIsTransitioning(Boolean.TRUE).build();
        this.cep.injectEventSynch(app1_bad);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

    }

    /**
     * Spring Java-Configuration class for the test. Define instantiation, configuration, and initialization logic for objects to be managed
     * by the Spring IoC container.
     * 
     * @author lmagnoni
     */
    @Configuration
    static class TestConfiguration {

        @Bean
        public CEPService cep() {
            return new CEPService("TEST2", Boolean.TRUE, Boolean.TRUE, ExecutorFactory.newThreadPool("Processing", 10, false));
        }

        @Bean
        public EsperDAO dao() {
            return new EsperDAO(this.cep());
        }

        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }

        @Bean
        public PrintDebug printDebug() {
            return new PrintDebug(this.cep());
        }

        @Bean
        public PrintErs printErs() {
            return new PrintErs();
        }

        @Bean
        public PrintXml printXml() {
            return new PrintXml(this.cep(), "file.txt");
        }

        @Bean
        public CommandSender commandSender() {
            return new StubCommandSender("part_ef");
        }

        @Bean
        public ExecutorRC execRC() {
            return new ExecutorRC("ATLAS", this.commandSender());
        }

        @Bean
        public SynchActionExecutor synchActionExecutor() {
            return new SynchActionExecutor();
        }
        
        @Bean
        public ProblemExecutorAsynch problemExecutorAsynch() {
            return new ProblemExecutorAsynch(this.execRC(), this.dao());
        }

        @Bean
        public ProblemExecutorSynch problemExecutorSynch() {
            return new ProblemExecutorSynch(this.execRC(), this.dao());
        }

        /**
         * This is kind of hack to overcome limitation of JavaConfig to load directly a Property object from a property file, equivalent to
         * <util:properties id="configuration" location="classpath:ces.properties" />
         * 
         * @return Properties
         */
        @Bean
        public Properties configuration() {

            /**
             * Set system properties
             */
            System.getProperties().setProperty("tdaq.partition", "cippa");

            // PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
            // propertiesFactoryBean.setLocation(new ClassPathResource("ces.properties"));
            // Properties properties = null;
            // try {
            // propertiesFactoryBean.afterPropertiesSet();
            // properties = propertiesFactoryBean.getObject();
            //
            // } catch (IOException e) {
            // log.warn("Cannot load properties file.");
            // }
            // return properties;

            final Properties p = new Properties();
            p.setProperty("epl.files", "epl/problems.epl,epl/print.epl,epl/core.epl");

            return p;
        }
    }

}
