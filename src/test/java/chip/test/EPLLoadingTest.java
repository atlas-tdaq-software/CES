package chip.test;

import java.util.Properties;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import chip.CEPService;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.config.PixelConfig;
import chip.executor.ExecutorRC;
import chip.listener.PrintDebug;
import chip.listener.PrintErs;
import chip.listener.PrintInfo;
import chip.listener.PrintXml;
import chip.subscriber.auto.AutoPilotExecutor;
import chip.subscriber.auto.BCMActionExecutor;
import chip.subscriber.auto.ClockSwitchExecutor;
import chip.subscriber.auto.EventDisplayExecutor;
import chip.subscriber.auto.MaxEventsExecutor;
import chip.subscriber.auto.OnDemandExecutor;
import chip.subscriber.auto.PixelActionExecutor;
import chip.subscriber.auto.ResubscribeExecutor;
import chip.subscriber.auto.SubscriberEnableCHIPMetricsIs;
import chip.subscriber.auto.UnexpectedStopExecutor;
import chip.subscriber.auto.WarmStartExecutor;
import chip.subscriber.core.ProblemExecutorAsynch;
import chip.subscriber.core.ProblemExecutorSynch;
import chip.subscriber.is.SubscriberISEngineMetrics;
import chip.subscriber.is.SubscriberISStatementMetrics;
import chip.subscriber.meta.SynchActionExecutor;
import chip.subscriber.recovery.DCMExecutor;
import chip.subscriber.recovery.HltErsRecoveryExecutor;
import chip.subscriber.recovery.HltRecoveryExecutor;
import chip.subscriber.recovery.HltSvExecutor;
import chip.subscriber.recovery.L1CaloExecutor;
import chip.subscriber.recovery.ModuleExecutor;
import chip.subscriber.recovery.ResynchExecutor;
import chip.subscriber.recovery.RootCtrlExecutor;
import chip.subscriber.recovery.StoplessExecutor;
import chip.subscriber.recovery.TTCRestartExecutor;
import chip.subscriber.test.TestFollowUpExecutor;
import chip.utils.ExecutorFactory;
import daq.rc.CommandSender;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = EPLLoadingTest.TestConfiguration.class)
public class EPLLoadingTest {

    @Autowired
    private CEPService cep;

    @Autowired
    private EsperDAO dao;

    @Before
    public void setUp() throws Exception {
        Utils.setup(this.cep);
    }

    @After
    public void tearDown() throws Exception {
        Utils.reset(this.cep);
    }

    @Test
    public void dummy() {
        // Load also statements from EsperDAO
        this.dao.getAllRCApplication();
        this.dao.getChildrenOfController("dummy");
        this.dao.getRCApplication("dummy");
        this.dao.getAllErrorsOfApplication("dummy");
        this.dao.hasHostBeenDisabled("dummy");
        this.dao.getAllErrorsOfController("dummy");
    }

    @Test
    public void loadFromCache() {
        // This should be executed always after "dummy"
        // In this second test all the modules should be executed from cache
    }
    
    /**
     * Spring Java-Configuration class for the test. Define instantiation, configuration, and initialization logic for objects to be managed
     * by the Spring IoC container.
     * 
     * @author lmagnoni
     */
    @Configuration
    static class TestConfiguration {

        @Bean
        public CEPService cep() {
            return new CEPService("TEST2", Boolean.TRUE, Boolean.FALSE, ExecutorFactory.newThreadPool("Processing", 10, false));
        }

        @Bean
        public EsperDAO esperDAO() {
            return new EsperDAO(this.cep());
        }
        
        @Bean
        public ProblemExecutorAsynch problemExecutorAsynch() {
            return new ProblemExecutorAsynch(null, null);
        }

        @Bean
        public ProblemExecutorSynch problemExecutorSynch() {
            return new ProblemExecutorSynch(null, null);
        }

        @Bean
        public SynchActionExecutor synchActionExecutor() {
            return new SynchActionExecutor();
        }
        
        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }

        @Bean
        public PrintErs printErs() {
            return new PrintErs();
        }

        @Bean
        public PrintXml printXml() {
            return new PrintXml(this.cep(), "");
        }
        
        @Bean
        public TestFollowUpExecutor testFollowUpExecutor() {
            return new TestFollowUpExecutor(null, null, null, null);
        }

        @Bean
        public PrintDebug printDebug() {
            return new PrintDebug(this.cep());
        }

        @Bean
        public ClockSwitchExecutor clockSwitchExecutor() {
            return new ClockSwitchExecutor(null, null, null, null, null, null);
        }

        @Bean
        public HltRecoveryExecutor hltRecoveryExecutor() {
            return new HltRecoveryExecutor(null, null);
        }

        @Bean
        public HltErsRecoveryExecutor hltErsRecoveryExecutor() {
            return new HltErsRecoveryExecutor(null, null);
        }

        @Bean
        public StoplessExecutor stoplessExecutor() {
            return new StoplessExecutor(null, null, null, null, null);
        }

        @Bean
        public ResynchExecutor resynchExecutor() {
            return new ResynchExecutor(null, null, null, null, null);
        }

        @Bean
        public ModuleExecutor moduleExecutor() {
            return new ModuleExecutor(null, null, null, null, null);
        }

        @Bean
        public TTCRestartExecutor getTTCRestartExecutor() {
            return new TTCRestartExecutor(null, null, null, null, null);
        }

        @Bean
        public RootCtrlExecutor getRootCtrlExecutor() {
            return new RootCtrlExecutor(null, null, null);
        }
        
        @Bean
        public WarmStartExecutor warmStartExecutor() {
            return new WarmStartExecutor(null, null, null, null);
        }

        @Bean
        public L1CaloExecutor getL1CaloExecutor() {
            return new L1CaloExecutor(null, null, null, null, null);
        }

        @Bean
        public UnexpectedStopExecutor unexpectedStopExecutor() {
            return new UnexpectedStopExecutor(null);
        }

        @Bean
        public MaxEventsExecutor maxEventsExecutor() {
            return new MaxEventsExecutor(null, null);
        }

        @Bean
        public ResubscribeExecutor resubscribeExecutor() {
            return new ResubscribeExecutor(null);
        }

        @Bean
        public HltSvExecutor hltSvExecutor() {
            return new HltSvExecutor(null, null, null, null, this.daqConfiguration());
        }

        @Bean
        public PixelActionExecutor pixelActionExecutor() {
            return new PixelActionExecutor(null, null, null, null);
        }

        @Bean
        public PixelConfig pixelConfig() {
            return new PixelConfig(null, null, null, null, null);
        }

        @Bean
        public BCMActionExecutor getBCMActionExecutor() {
            return new BCMActionExecutor(null, this.daqConfiguration());
        }

        @Bean
        public DCMExecutor getDCMExecutor() {
            return new DCMExecutor(null, null);
        }

        @Bean
        public DAQConfigReader daqConfiguration() {
            return new DAQConfigReader("part_hlt_avolio",
                                       "oksconfig:/afs/cern.ch/user/a/avolio/work/public/partitions/tdaq-10-00-00/part_hlt_avolio_merged.data.xml",
                                       "CHIP");
        }

        @Bean
        public EventDisplayExecutor eventDisplayExecutor() {
            return new EventDisplayExecutor(this.execRC());
        }

        @Bean
        public CommandSender commandSender() {
            return new StubCommandSender(null);
        }

        @Bean
        public ExecutorRC execRC() {
            return new ExecutorRC(null, this.commandSender());
        }

        @Bean
        public SubscriberEnableCHIPMetricsIs subscriberEnableCHIPMetricsIs() {
            return new SubscriberEnableCHIPMetricsIs(this.cep());
        }

        @Bean
        public SubscriberISEngineMetrics subscriberISEngineMetrics() {
            return new SubscriberISEngineMetrics(this.daqConfiguration());
        }

        @Bean
        public SubscriberISStatementMetrics subscriberISStatementMetrics() {
            return new SubscriberISStatementMetrics(this.daqConfiguration());
        }

        @Bean
        public OnDemandExecutor onDemandExecutor() {
            return new OnDemandExecutor(null, null, null, null);
        }

        @Bean
        public AutoPilotExecutor autoPilotExecutor() {
            return new AutoPilotExecutor(null, null, null, null);
        }

        @Bean
        public Properties configuration() {
            final Properties p = new Properties();
            p.setProperty("epl.files",
                          "epl/core.epl,epl/problems.epl,epl/print.epl,epl/test.epl,epl/hlt.epl,epl/rulesErs.epl,"
                                       + "epl/stopless.epl,epl/resynch.epl,epl/module.epl,epl/ttcRestart.epl,epl/lhc.epl,"
                                       + "epl/warmStart.epl,epl/clockSwitch.epl,epl/l1Calo.epl,epl/unexpectedStop.epl,"
                                       + "epl/maxEvents.epl,epl/resubscribe.epl,epl/hltSv.epl,"
                                       + "epl/pixel.epl,epl/dcm.epl,epl/bcm.epl,epl/eventDisplay.epl,epl/chipMetricsIs.epl,epl/onDemand.epl,"
                                       + "epl/autoPilot.epl,epl/rootCtrl.epl,epl/k8s.epl");
            return p;
        }
    }

}
