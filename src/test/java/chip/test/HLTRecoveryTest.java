package chip.test;

import java.util.Collections;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.common.client.scopetest.EPAssertionUtil;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.scopetest.SupportUpdateListener;

import chip.CEPService;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.config.RCApplicationConfig;
import chip.event.en.FSM;
import chip.event.en.IFDIES;
import chip.event.en.IFERROR;
import chip.event.en.IFFAILED;
import chip.event.en.MEMBERSHIP;
import chip.event.en.STARTAT;
import chip.event.en.STATUS;
import chip.event.en.STOPAT;
import chip.event.issue.recovery.HltErsRecovery;
import chip.event.issue.recovery.HltRecovery;
import chip.event.meta.RCApplication;
import chip.executor.ExecutorPMG;
import chip.executor.ExecutorRC;
import chip.listener.PrintDebug;
import chip.listener.PrintErs;
import chip.listener.PrintInfo;
import chip.listener.PrintXml;
import chip.subscriber.core.ProblemExecutorAsynch;
import chip.subscriber.core.ProblemExecutorSynch;
import chip.subscriber.meta.SynchActionExecutor;
import chip.subscriber.recovery.HltErsRecoveryExecutor;
import chip.subscriber.recovery.HltRecoveryExecutor;
import chip.subscriber.test.TestFollowUpExecutor;
import chip.utils.ExecutorFactory;
import daq.EsperUtils.ERSEvent;
import daq.rc.CommandSender;
import ers.Severity;


/**
 * Unit tests for the validation of the Application event generation, as union of static configuration events and dynamic information events
 * about application status and, in case, state information.
 * <p>
 * This test uses external time source to define time passing in Esper, for a better testing of out of order event injection
 * <p>
 * Tests environment is build using Spring Java Config, as from here:
 * http://blog.springsource.org/2011/06/21/spring-3-1-m2-testing-with-configuration-classes-and-profiles/
 * 
 * @author lmagnoni
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = HLTRecoveryTest.TestConfiguration.class)
public class HLTRecoveryTest {

    @Autowired
    private CEPService cep;

    @Before
    public void setUp() throws Exception {
        Utils.setup(this.cep);
    }

    @After
    public void tearDown() throws Exception {
        Utils.reset(this.cep);
    }

    /**
     * Test to validate the detection of a dead HLT application and to verify the recovery procedure DCM application dies -> DCM, HLTMPPU
     * and HLTRC need to terminated and restarted
     * 
     * @throws InterruptedException
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void DcmDeadDetectedAndReset() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        // Use the module name in order to avoid flagging the HltRecoveryTable as public
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "module chip.hlt; select * from HltRecoveryTable");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication rootController =
                                           new RCApplication.DefaultBuilder("RootController").isController(Boolean.TRUE).state(FSM.RUNNING).build();
        this.cep.injectEventSynch(rootController);

        final RCApplication controller = new RCApplication.DefaultBuilder("myController").isController(Boolean.TRUE).build();
        this.cep.injectEventSynch(controller);

        // Configuration
        final RCApplicationConfig dcm_conf = new RCApplicationConfig("myDcm",
                                                                     "DcmApplication",
                                                                     Boolean.TRUE,
                                                                     IFFAILED.IGNORE,
                                                                     IFDIES.HANDLE,
                                                                     STARTAT.BOOT,
                                                                     STOPAT.SHUTDOWN,
                                                                     FSM.INITIAL,
                                                                     FSM.NONE,
                                                                     "myHost1",
                                                                     Collections.emptySet(),
                                                                     "myController",
                                                                     "mySegment",
                                                                     Boolean.FALSE,
                                                                     Boolean.FALSE,
                                                                     IFERROR.ERROR,
                                                                     Boolean.FALSE);
        final RCApplicationConfig hltmppu_conf = new RCApplicationConfig("myHltmppu",
                                                                         "HLTMPPUApplication",
                                                                         Boolean.TRUE,
                                                                         IFFAILED.IGNORE,
                                                                         IFDIES.HANDLE,
                                                                         STARTAT.BOOT,
                                                                         STOPAT.SHUTDOWN,
                                                                         FSM.INITIAL,
                                                                         FSM.NONE,
                                                                         "myHost1",
                                                                         Collections.emptySet(),
                                                                         "myController",
                                                                         "mySegment",
                                                                         Boolean.FALSE,
                                                                         Boolean.FALSE,
                                                                         IFERROR.ERROR,
                                                                         Boolean.FALSE);
        final RCApplicationConfig hltrc_conf = new RCApplicationConfig("myHltrc",
                                                                       "HLTRCApplication",
                                                                       Boolean.TRUE,
                                                                       IFFAILED.IGNORE,
                                                                       IFDIES.HANDLE,
                                                                       STARTAT.BOOT,
                                                                       STOPAT.SHUTDOWN,
                                                                       FSM.INITIAL,
                                                                       FSM.NONE,
                                                                       "myHost1",
                                                                       Collections.emptySet(),
                                                                       "myController",
                                                                       "mySegment",
                                                                       Boolean.FALSE,
                                                                       Boolean.FALSE,
                                                                       IFERROR.ERROR,
                                                                       Boolean.FALSE);

        this.cep.injectEventSynch(dcm_conf);
        this.cep.injectEventSynch(hltmppu_conf);
        this.cep.injectEventSynch(hltrc_conf);

        // DCM, HLTPPU and HLTRC are up
        final RCApplication dcm_ok =
                                   new RCApplication.DefaultBuilder("myDcm").controller("myController").status(STATUS.UP).oksClassName("DcmApplication").runningHost("myHost1").ifDies(IFDIES.HANDLE).parentState(FSM.RUNNING).build();
        final RCApplication hltppu_ok =
                                      new RCApplication.DefaultBuilder("myHltmppu").controller("myController").status(STATUS.UP).oksClassName("HLTMPPUApplication").ifDies(IFDIES.HANDLE).runningHost("myHost1").build();
        final RCApplication hltrc_ok =
                                     new RCApplication.DefaultBuilder("myHltrc").controller("myController").status(STATUS.UP).oksClassName("HLTRCApplication").runningHost("myHost1").ifDies(IFDIES.HANDLE).build();

        final RCApplication dcmRC_ok =
                                     new RCApplication.DefaultBuilder("myDcm").controller("myController").status(STATUS.UP).oksClassName("DcmApplication").runningHost("myHost1").ifDies(IFDIES.HANDLE).build();
        final RCApplication hltrcRC_ok =
                                       new RCApplication.DefaultBuilder("myHltrc").controller("myController").status(STATUS.UP).oksClassName("HLTRCApplication").runningHost("myHost1").ifDies(IFDIES.HANDLE).build();

        this.cep.injectEventSynch(dcm_ok);
        this.cep.injectEventSynch(hltppu_ok);
        this.cep.injectEventSynch(hltrc_ok);
        this.cep.injectEventSynch(dcmRC_ok);
        this.cep.injectEventSynch(hltrcRC_ok);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // DCM dies
        final RCApplication dcm_absent = new RCApplication.CopyBuilder(dcm_ok).status(STATUS.EXITED).build();
        this.cep.injectEventSynch(dcm_absent);

        Thread.sleep(5100);

        // CHECK FOR STOP
        final EventBean[] e1 = listener.getNewDataListFlattened();
        listener.reset();
        EPAssertionUtil.assertProps(e1[0],
                                    new String[] {"dcm", "hltpu", "hltrc", "controller", "runningHost", "deadapplication", "type", "status",
                                                  "action"},
                                    new Object[] {"myDcm", "myHltmppu", "myHltrc", "myController", "myHost1", "myDcm", HltRecovery.TYPE.DCM,
                                                  HltRecovery.STATUS._NEW, HltRecovery.ACTION.STOP});
        EPAssertionUtil.assertProps(e1[1],
                                    new String[] {"dcm", "hltpu", "hltrc", "controller", "runningHost", "deadapplication", "type", "status",
                                                  "action"},
                                    new Object[] {"myDcm", "myHltmppu", "myHltrc", "myController", "myHost1", "myDcm", HltRecovery.TYPE.DCM,
                                                  HltRecovery.STATUS.STOP_IN_PROGRESS, HltRecovery.ACTION.NONE});

        // Finally the hltppu and hltrc applications are killed
        this.cep.injectEventSynch(rootController);
        final RCApplication hltppu_term =
                                        new RCApplication.CopyBuilder(hltppu_ok).status(STATUS.TERMINATING).membership(MEMBERSHIP._OUT).build();
        final RCApplication hltrc_term =
                                       new RCApplication.CopyBuilder(hltrc_ok).status(STATUS.TERMINATING).membership(MEMBERSHIP._OUT).build();
        final RCApplication dcm_out = new RCApplication.CopyBuilder(dcm_absent).membership(MEMBERSHIP._OUT).build();

        this.cep.injectEventSynch(dcm_out);
        this.cep.injectEventSynch(hltppu_term);
        this.cep.injectEventSynch(hltrc_term);

        final RCApplication hltppu_absent = new RCApplication.CopyBuilder(hltppu_term).status(STATUS.EXITED).build();
        final RCApplication hltrc_absent = new RCApplication.CopyBuilder(hltrc_term).status(STATUS.EXITED).build();

        this.cep.injectEventSynch(hltrc_absent);
        this.cep.injectEventSynch(hltppu_absent);

        Thread.sleep(1100);

        // CHECK FOR START
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.TRUE));
        final EventBean[] e2 = listener.getNewDataListFlattened();
        listener.reset();

        EPAssertionUtil.assertProps(e2[0],
                                    new String[] {"dcm", "hltpu", "hltrc", "controller", "runningHost", "deadapplication", "type", "status",
                                                  "action"},
                                    new Object[] {"myDcm", "myHltmppu", "myHltrc", "myController", "myHost1", "myDcm", HltRecovery.TYPE.DCM,
                                                  HltRecovery.STATUS.PREPARE_START, HltRecovery.ACTION.NONE});

        EPAssertionUtil.assertProps(e2[1],
                                    new String[] {"dcm", "hltpu", "hltrc", "controller", "runningHost", "deadapplication", "type", "status",
                                                  "action"},
                                    new Object[] {"myDcm", "myHltmppu", "myHltrc", "myController", "myHost1", "myDcm", HltRecovery.TYPE.DCM,
                                                  HltRecovery.STATUS.PREPARE_START, HltRecovery.ACTION._START});

        EPAssertionUtil.assertProps(e2[2],
                                    new String[] {"dcm", "hltpu", "hltrc", "controller", "runningHost", "deadapplication", "type", "status",
                                                  "action"},
                                    new Object[] {"myDcm", "myHltmppu", "myHltrc", "myController", "myHost1", "myDcm", HltRecovery.TYPE.DCM,
                                                  HltRecovery.STATUS.START_IN_PROGRESS, HltRecovery.ACTION.NONE});

        this.cep.injectEventSynch(dcm_ok);
        this.cep.injectEventSynch(hltppu_ok);
        this.cep.injectEventSynch(hltrc_ok);

        Thread.sleep(1100);

        // CHECK FOR start

        final EventBean[] e3 = listener.getNewDataListFlattened();
        listener.reset();
        EPAssertionUtil.assertProps(e3[0],
                                    new String[] {"dcm", "hltpu", "hltrc", "controller", "runningHost", "deadapplication", "type", "status",
                                                  "action"},
                                    new Object[] {"myDcm", "myHltmppu", "myHltrc", "myController", "myHost1", "myDcm", HltRecovery.TYPE.DCM,
                                                  HltRecovery.STATUS.START_IN_PROGRESS, HltRecovery.ACTION.ENABLE});
        EPAssertionUtil.assertProps(e3[1],
                                    new String[] {"dcm", "hltpu", "hltrc", "controller", "runningHost", "deadapplication", "type", "status",
                                                  "action"},
                                    new Object[] {"myDcm", "myHltmppu", "myHltrc", "myController", "myHost1", "myDcm", HltRecovery.TYPE.DCM,
                                                  HltRecovery.STATUS.DONE, HltRecovery.ACTION.NONE});

    }

    @Test
    public void HltpuCrashRecovery() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from HltErsRecovery");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller = new RCApplication.DefaultBuilder("myController").isController(Boolean.TRUE).build();
        this.cep.injectEventSynch(controller);

        // Configuration
        final RCApplicationConfig dcm_conf = new RCApplicationConfig("myDcm",
                                                                     "DcmApplication",
                                                                     Boolean.TRUE,
                                                                     IFFAILED.IGNORE,
                                                                     IFDIES.HANDLE,
                                                                     STARTAT.BOOT,
                                                                     STOPAT.SHUTDOWN,
                                                                     FSM.INITIAL,
                                                                     FSM.NONE,
                                                                     "myHost1",
                                                                     Collections.emptySet(),
                                                                     "myController",
                                                                     "mySegment",
                                                                     Boolean.FALSE,
                                                                     Boolean.FALSE,
                                                                     IFERROR.ERROR,
                                                                     Boolean.FALSE);
        final RCApplicationConfig hltmppu_conf = new RCApplicationConfig("myHltmppu",
                                                                         "HLTMPPUApplication",
                                                                         Boolean.TRUE,
                                                                         IFFAILED.IGNORE,
                                                                         IFDIES.HANDLE,
                                                                         STARTAT.BOOT,
                                                                         STOPAT.SHUTDOWN,
                                                                         FSM.INITIAL,
                                                                         FSM.NONE,
                                                                         "myHost1",
                                                                         Collections.emptySet(),
                                                                         "myController",
                                                                         "mySegment",
                                                                         Boolean.FALSE,
                                                                         Boolean.FALSE,
                                                                         IFERROR.ERROR,
                                                                         Boolean.FALSE);
        final RCApplicationConfig hltrc_conf = new RCApplicationConfig("myHltrc",
                                                                       "HLTRCApplication",
                                                                       Boolean.TRUE,
                                                                       IFFAILED.IGNORE,
                                                                       IFDIES.HANDLE,
                                                                       STARTAT.BOOT,
                                                                       STOPAT.SHUTDOWN,
                                                                       FSM.INITIAL,
                                                                       FSM.NONE,
                                                                       "myHost1",
                                                                       Collections.emptySet(),
                                                                       "myController",
                                                                       "mySegment",
                                                                       Boolean.FALSE,
                                                                       Boolean.FALSE,
                                                                       IFERROR.ERROR,
                                                                       Boolean.FALSE);

        this.cep.injectEventSynch(dcm_conf);
        this.cep.injectEventSynch(hltmppu_conf);
        this.cep.injectEventSynch(hltrc_conf);

        // DCM, HLTPPU and HLTRC are up
        final RCApplication dcm_ok =
                                   new RCApplication.DefaultBuilder("myDcm").controller("myController").status(STATUS.UP).oksClassName("DcmApplication").runningHost("myHost1").ifDies(IFDIES.HANDLE).build();
        final RCApplication hltppu_ok =
                                      new RCApplication.DefaultBuilder("myHltmppu").controller("myController").status(STATUS.UP).oksClassName("HLTMPPUApplication").ifDies(IFDIES.HANDLE).runningHost("myHost1").build();
        final RCApplication hltrc_ok =
                                     new RCApplication.DefaultBuilder("myHltrc").state(FSM.RUNNING).isTransitioning(Boolean.FALSE).controller("myController").status(STATUS.UP).oksClassName("HLTRCApplication").runningHost("myHost1").ifDies(IFDIES.HANDLE).build();
        this.cep.injectEventSynch(dcm_ok);
        this.cep.injectEventSynch(hltppu_ok);
        this.cep.injectEventSynch(hltrc_ok);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Now inject DCM::HltpuTimeoutIssue message via ERS
        final String[] qualifiers = {""};
        final ERSEvent ersMsg = new ERSEvent();
        ersMsg.setPartitionName("ATLAS");
        ersMsg.setSeverity(Severity.level.Warning.toString());
        ersMsg.setMessageID("dcm::HltpuCrashIssue");
        ersMsg.setMachineName("myHost1");
        ersMsg.setApplicationName("myDcm");
        ersMsg.setIssuedDate(Long.valueOf(Long.MAX_VALUE));
        ersMsg.setFormattedDate("2013-01-01 00:00");
        ersMsg.setQualifiers(qualifiers);
        
        final Map<String, String> params = new TreeMap<>();
        params.put("from", "HltpuNo1");
        ersMsg.setParameters(params);
        
        this.cep.injectEventSynch(ersMsg);

        // Sleep for some time: the EPL statement batches ERS events in a time window
        Thread.sleep(4000);
        
        final EventBean[] e = listener.getNewDataListFlattened();
        listener.reset();
        EPAssertionUtil.assertProps(e[0],
                                    new String[] {"reportingApp", "hltrc", "runningHost", "hltpu", "type", "status", "action"},
                                    new Object[] {"myDcm", "myHltrc", "myHost1", "HltpuNo1",
                                                  HltErsRecovery.TYPE.HLTPU_CRASH, HltErsRecovery.STATUS._NEW,
                                                  HltErsRecovery.ACTION.NONE});
        EPAssertionUtil.assertProps(e[1],
                                    new String[] {"reportingApp", "hltrc", "runningHost", "hltpu", "type", "status", "action"},
                                    new Object[] {"myDcm", "myHltrc", "myHost1", "HltpuNo1",
                                                  HltErsRecovery.TYPE.HLTPU_CRASH, HltErsRecovery.STATUS._NEW,
                                                  HltErsRecovery.ACTION.FORK});
        
        // The statement should not trigger if the app is transitioning
        final RCApplication hltrc_nok =
            new RCApplication.DefaultBuilder("myHltrc").state(FSM.RUNNING).isTransitioning(Boolean.TRUE).controller("myController").status(STATUS.UP).oksClassName("HLTRCApplication").runningHost("myHost1").ifDies(IFDIES.HANDLE).build();

        this.cep.injectEventSynch(hltrc_nok);
        this.cep.injectEventSynch(ersMsg);

        Thread.sleep(4000);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));
    }

    @Test
    public void HltpuTimeoutRecovery() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from HltErsRecovery");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication rootController =
                                           new RCApplication.DefaultBuilder("RootController").isController(Boolean.TRUE).state(FSM.RUNNING).build();
        this.cep.injectEventSynch(rootController);

        final RCApplication controller = new RCApplication.DefaultBuilder("myController").isController(Boolean.TRUE).build();
        this.cep.injectEventSynch(controller);

        // Configuration
        final RCApplicationConfig dcm_conf = new RCApplicationConfig("myDcm",
                                                                     "DcmApplication",
                                                                     Boolean.TRUE,
                                                                     IFFAILED.IGNORE,
                                                                     IFDIES.HANDLE,
                                                                     STARTAT.BOOT,
                                                                     STOPAT.SHUTDOWN,
                                                                     FSM.INITIAL,
                                                                     FSM.NONE,
                                                                     "myHost1",
                                                                     Collections.emptySet(),
                                                                     "myController",
                                                                     "mySegment",
                                                                     Boolean.FALSE,
                                                                     Boolean.FALSE,
                                                                     IFERROR.ERROR,
                                                                     Boolean.FALSE);
        final RCApplicationConfig hltmppu_conf = new RCApplicationConfig("myHltmppu",
                                                                         "HLTMPPUApplication",
                                                                         Boolean.TRUE,
                                                                         IFFAILED.IGNORE,
                                                                         IFDIES.HANDLE,
                                                                         STARTAT.BOOT,
                                                                         STOPAT.SHUTDOWN,
                                                                         FSM.INITIAL,
                                                                         FSM.NONE,
                                                                         "myHost1",
                                                                         Collections.emptySet(),
                                                                         "myController",
                                                                         "mySegment",
                                                                         Boolean.FALSE,
                                                                         Boolean.FALSE,
                                                                         IFERROR.ERROR,
                                                                         Boolean.FALSE);
        final RCApplicationConfig hltrc_conf = new RCApplicationConfig("myHltrc",
                                                                       "HLTRCApplication",
                                                                       Boolean.TRUE,
                                                                       IFFAILED.IGNORE,
                                                                       IFDIES.HANDLE,
                                                                       STARTAT.BOOT,
                                                                       STOPAT.SHUTDOWN,
                                                                       FSM.INITIAL,
                                                                       FSM.NONE,
                                                                       "myHost1",
                                                                       Collections.emptySet(),
                                                                       "myController",
                                                                       "mySegment",
                                                                       Boolean.FALSE,
                                                                       Boolean.FALSE,
                                                                       IFERROR.ERROR,
                                                                       Boolean.FALSE);

        this.cep.injectEventSynch(dcm_conf);
        this.cep.injectEventSynch(hltmppu_conf);
        this.cep.injectEventSynch(hltrc_conf);

        // DCM, HLTPPU and HLTRC are up
        final RCApplication dcm_1 =
                                  new RCApplication.DefaultBuilder("myDcm").controller("myController").status(STATUS.UP).oksClassName("DcmApplication").runningHost("myHost1").ifDies(IFDIES.HANDLE).build();
        final RCApplication hltppu_1 =
                                     new RCApplication.DefaultBuilder("myHltmppu").controller("myController").status(STATUS.UP).oksClassName("HLTMPPUApplication").ifDies(IFDIES.HANDLE).runningHost("myHost1").build();
        final RCApplication hltrc_1 =
                                    new RCApplication.DefaultBuilder("myHltrc").controller("myController").status(STATUS.UP).oksClassName("HLTRCApplication").runningHost("myHost1").ifDies(IFDIES.HANDLE).build();
        this.cep.injectEventSynch(dcm_1);
        this.cep.injectEventSynch(hltppu_1);
        this.cep.injectEventSynch(hltrc_1);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Now inject DCM::HltpuTimeoutIssue message via ERS
        final String[] qualifiers = {""};
        final ERSEvent ersMsg = new ERSEvent();
        ersMsg.setPartitionName("ATLAS");
        ersMsg.setSeverity(Severity.level.Warning.toString());
        ersMsg.setMessageID("dcm::HltpuTimeoutIssue");
        ersMsg.setMachineName("myHost1");
        ersMsg.setApplicationName("myDcm");
        ersMsg.setIssuedDate(Long.valueOf(Long.MAX_VALUE));
        ersMsg.setFormattedDate("2013-01-01 00:00");
        ersMsg.setQualifiers(qualifiers);
        
        final Map<String, String> params = new TreeMap<>();
        params.put("from", "HltpuNo1");
        ersMsg.setParameters(params);
        
        this.cep.injectEventSynch(ersMsg);

        // List<String> qualifiers = new ArrayList<String>();
        // ERSMessage ersMsg = new ERSMessage("ATLAS",
        // Severity.level.Warning,"dcm::HltpuTimeoutIssue","myHost1","myDcm",Long.MAX_VALUE,"2013-01-01 00:00",
        // "HltpuTimeoutIssue: some Cause from HltpuNo1",qualifiers,null);
        // cep.injectEventSynch(ersMsg);

        Thread.sleep(600);

        EventBean[] e = listener.getNewDataListFlattened();
        listener.reset();
        EPAssertionUtil.assertProps(e[0],
                                    new String[] {"reportingApp", "hltrc", "runningHost", "hltpu", "type", "status", "action"},
                                    new Object[] {"myDcm", "myHltrc", "myHost1", "HltpuNo1",
                                                  HltErsRecovery.TYPE.HLTPU_TIMEOUT, HltErsRecovery.STATUS._NEW,
                                                  HltErsRecovery.ACTION.STOP});
    }

    /**
     * Spring Java-Configuration class for the test. Define instantiation, configuration, and initialization logic for objects to be managed
     * by the Spring IoC container.
     * 
     * @author lmagnoni
     */
    @Configuration
    static class TestConfiguration {

        @Bean
        public CEPService cep() {
            return new CEPService("TEST2", Boolean.TRUE, Boolean.TRUE, ExecutorFactory.newThreadPool("Processing", 10, false));
        }

        @Bean
        public EsperDAO dao() {
            return new EsperDAO(this.cep());
        }

        @Bean
        public PrintErs printErs() {
            return new PrintErs();
        }

        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }

        @Bean
        public PrintDebug printDebug() {
            return new PrintDebug(this.cep());
        }

        @Bean
        public PrintXml printXml() {
            return new PrintXml(this.cep(), "file.txt");
        }

        @Bean
        public ExecutorRC execRC() {
            return new ExecutorRC("ATLAS", this.commandSender());
        }

        @Bean
        public TestFollowUpExecutor testFollowUpExecutor() {
            return new TestFollowUpExecutor(this.execRC(), this.executorPMG(), this.dao(), this.daqConfiguration());
        }

        @Bean
        public DAQConfigReader daqConfiguration() {
            return new DAQConfigReader("part_hlt_avolio",
                                       "oksconfig:/afs/cern.ch/user/a/avolio/work/public/partitions/tdaq-06-01-01/part_hlt_avolio_merged.data.xml",
                                       "CHIP");
        }

        @Bean
        public ExecutorPMG executorPMG() {
            return null;
        }

        @Bean
        public SynchActionExecutor synchActionExecutor() {
            return new SynchActionExecutor();
        }
        
        @Bean
        public ProblemExecutorAsynch problemExecutorAsynch() {
            return new ProblemExecutorAsynch(this.execRC(), this.dao());
        }

        @Bean
        public ProblemExecutorSynch problemExecutorSynch() {
            return new ProblemExecutorSynch(this.execRC(), this.dao());
        }

        @Bean
        public CommandSender commandSender() {
            return new StubCommandSender("part_ef");
        }

        @Bean
        public HltRecoveryExecutor hltExecutor() {
            return new HltRecoveryExecutor(this.commandSender(), this.dao());
        }

        @Bean
        public HltErsRecoveryExecutor hltErsExecutor() {
            return new HltErsRecoveryExecutor(this.commandSender(), this.dao());
        }

        /**
         * This is kind of hack to overcome limitation of JavaConfig to load directly a Property object from a property file, equivalent to
         * <util:properties id="configuration" location="classpath:ces.properties" />
         * 
         * @return Properties
         */
        @Bean
        public Properties configuration() {

            /**
             * Set system properties
             */
            System.getProperties().setProperty("tdaq.partition", "cippa");

            // PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
            // propertiesFactoryBean.setLocation(new ClassPathResource("ces.properties"));
            // Properties properties = null;
            // try {
            // propertiesFactoryBean.afterPropertiesSet();
            // properties = propertiesFactoryBean.getObject();
            //
            // } catch (IOException e) {
            // log.warn("Cannot load properties file.");
            // }
            // return properties;

            final Properties p = new Properties();
            p.setProperty("epl.files", "epl/core.epl,epl/print.epl,epl/problems.epl,epl/test.epl,epl/hlt.epl");

            return p;
        }
    }

}
