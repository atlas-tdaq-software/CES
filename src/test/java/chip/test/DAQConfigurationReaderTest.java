package chip.test;

import java.util.Map;
import java.util.Properties;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import chip.CEPService;
import chip.CHIP;
import chip.DAQConfigReader;
import chip.event.config.PixelConfig;
import chip.event.config.RCApplicationConfig;
import chip.executor.ExecutorRC;
import chip.executor.ExecutorTrigger;
import chip.injector.Injectors;
import chip.listener.PrintDebug;
import chip.listener.PrintInfo;
import chip.listener.PrintXml;
import chip.subscriber.meta.SynchActionExecutor;
import chip.utils.ExecutorFactory;


/**
 * Unit tests for the DAQConfigurationReader.
 * <p>
 * Tests environment is build using Spring Java Config, as from here:
 * http://blog.springsource.org/2011/06/21/spring-3-1-m2-testing-with-configuration-classes-and-profiles/
 * 
 * @author lmagnoni
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DAQConfigurationReaderTest.TestConfiguration.class)
public class DAQConfigurationReaderTest {

    @Autowired
    private CHIP ces;

    @Autowired
    private DAQConfigReader reader;

    private static boolean setupdone = false;

    @Before
    public void setUp() throws Exception {
        if(!DAQConfigurationReaderTest.setupdone) {
            this.ces.configure();
            DAQConfigurationReaderTest.setupdone = true;
        }
    }

    @After
    public void tearDown() throws Exception {
        // ces.shutdown();
    }

    /**
     * This tests validate the list of applications build by the DAQConfigReader.
     * <p>
     * It read the configuration file 'part_ef' build using partition maker cli tool.
     * <p>
     */
    @Test
    public void validateApplicationTree() {

        /**
         * Check RootController
         */
        RCApplicationConfig rootController = null;
        int controllerFound = 0;
        for(final Map.Entry<String, RCApplicationConfig> app : this.reader.getRCApplications().entrySet()) {
            if(app.getKey().equals("RootController")) {
                rootController = app.getValue();
                controllerFound = +1;
            }
        }
        Assert.assertNotNull("RootController not found!", rootController);
        Assert.assertEquals("Too many RootController...", 1, controllerFound);
        Assert.assertEquals("Wrong controller ", "RootController", rootController.getController());
        Assert.assertEquals("Wrong segment ", "setup", rootController.getSegment());

        /**
         * Check Online applications
         * 
         * @TODO
         */

        /**
         * Check all the applications
         * 
         * @TODO add more logic tests
         */
        // Verify number of all applications
        Assert.assertEquals("Wrong number of applications detected!", 195, this.reader.getAppConfiguration().size());

        /**
         * Check State Aware Applications
         * 
         * @TODO expand tests with more logic
         */
        // Verify number of State Aware applications
        Assert.assertEquals("Wrong number of state-aware applications detected!", 115, this.reader.getRCApplications().size());

    }

    /**
     * Spring Java-Configuration class for the test. Define instantiation, configuration, and initialization logic for objects to be managed
     * by the Spring IoC container.
     * 
     * @author lmagnoni
     */
    @Configuration
    static class TestConfiguration {

        @Bean
        public CEPService cep() {
            return new CEPService("TEST2", Boolean.TRUE, Boolean.TRUE, ExecutorFactory.newThreadPool("Processing", 10, false));
        }

        @Bean
        public SynchActionExecutor synchActionExecutor() {
            return new SynchActionExecutor();
        }
        
        @Bean
        public DAQConfigReader daqConfiguration() {
            return new DAQConfigReader("part_hlt_avolio",
                                       "oksconfig:/afs/cern.ch/user/a/avolio/work/public/partitions/tdaq-07-01-00/part_hlt_avolio_merged.data.xml",
                                       "CHIP");
            // return new DAQConfigReader("part_l2ef","oksconfig:/home/lmagnoni/partitions/part_l2ef_merged.data.xml");

        }

        @Bean
        public PixelConfig pixelConfig() {
            return new PixelConfig("IBL", "BLayer", "Barrel", "Disk", "Pixel");
        }

        @Bean
        public Injectors injectorHelper() {
            return new Injectors();
        }

        @Bean
        public ExecutorTrigger executorTrigger() {
            return new ExecutorTrigger("part_hlt_avolio", new ExecutorRC("part_hlt_avolio", 
                                                                         new StubCommandSender("part_hlt_avolio")));
        }

        @Bean
        public CHIP ces() {

            /**
             * Set system properties
             */
            // System.getProperties().setProperty("tdaq.partition", "part_ef");
            // System.getProperties().setProperty("tdaq.dbconfig", "oksconfig:/home/lmagnoni/partitions/part_ef.data.xml");

            final CHIP ces = new CHIP("part_hlt_avolio", this.cep(), this.daqConfiguration(), this.executorTrigger());
            ces.setInjectorHelper(this.injectorHelper());
            return ces;
        }

        @Bean
        public PrintXml printXml() {
            return new PrintXml(this.cep(), "file.txt");
        }

        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }

        @Bean
        public PrintDebug printDebug() {
            return new PrintDebug(this.cep());
        }

        /**
         * This is kind of hack to overcome limitation of JavaConfig to load directly a Property object from a property file, equivalent to
         * <util:properties id="configuration" location="classpath:ces.properties" />
         * 
         * @return Properties
         */
        @Bean
        public Properties configuration() {

            // PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
            // propertiesFactoryBean.setLocation(new ClassPathResource("ces.properties"));
            // Properties properties = null;
            // try {
            // propertiesFactoryBean.afterPropertiesSet();
            // properties = propertiesFactoryBean.getObject();
            //
            // } catch (IOException e) {
            // log.warn("Cannot load properties file.");
            // }
            // return properties;

            final Properties p = new Properties();
            p.setProperty("chip.runInTestMode", "true");
            p.setProperty("epl.files", "epl/core.epl,epl/print.epl");
            return p;
        }
    }

}
