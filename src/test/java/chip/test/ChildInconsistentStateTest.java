package chip.test;

import java.util.Properties;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.espertech.esper.common.client.scopetest.EPAssertionUtil;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.scopetest.SupportUpdateListener;

import chip.CEPService;
import chip.dao.EsperDAO;
import chip.event.en.FSM;
import chip.event.en.TRCOMMAND;
import chip.event.issue.core.Problem;
import chip.event.meta.RCApplication;
import chip.executor.ExecutorRC;
import chip.listener.PrintDebug;
import chip.listener.PrintErs;
import chip.listener.PrintInfo;
import chip.listener.PrintXml;
import chip.subscriber.core.ProblemExecutorAsynch;
import chip.subscriber.core.ProblemExecutorSynch;
import chip.subscriber.meta.SynchActionExecutor;
import chip.utils.ExecutorFactory;
import daq.rc.CommandSender;


/**
 * Unit tests for the validation of the Application event generation, as union of static configuration events and dynamic information events
 * about application status and, in case, state information.
 * <p>
 * This test uses external time source to define time passing in Esper, for a better testing of out of order event injection
 * <p>
 * Tests environment is build using Spring Java Config, as from here:
 * http://blog.springsource.org/2011/06/21/spring-3-1-m2-testing-with-configuration-classes-and-profiles/
 * 
 * @author lmagnoni
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ChildInconsistentStateTest.TestConfiguration.class)
public class ChildInconsistentStateTest {

    @Autowired
    private CEPService cep;

    @Before
    public void setUp() throws Exception {
        Utils.setup(this.cep);
    }

    @After
    public void tearDown() throws Exception {
        Utils.reset(this.cep);
    }

    /**
     * Test to validate the application event creation given a config and info events
     * 
     * @throws InterruptedException
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void childInconsistentAndReset()
        throws InterruptedException,
            EPRuntimeDestroyedException,
            EPCompileException,
            EPDeployException
    {

        final EPStatement stmt =
                               Utils.deployStatement(this.cep.getEsper(),
                                                     "select * from Problem "
                                                                          + "where status in (chip.event.issue.core.Problem$STATUS._NEW, chip.event.issue.core.Problem$STATUS.RESOLVED) "
                                                                          + "and action = chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        // Inject config and info events for application "app1", with TS=TS+1 (in millisecond)
        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("RootController").state(FSM.RUNNING).isController(Boolean.TRUE).build();
        this.cep.injectEventSynch(controller);
        final RCApplication app1_ok =
                                    new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.RUNNING).parentState(FSM.RUNNING).build();
        this.cep.injectEventSynch(app1_ok);

        // Create a new event from app1_ok and change status to absent

        final RCApplication app1_bad = new RCApplication.CopyBuilder(app1_ok).state(FSM.CONFIGURED).build();
        this.cep.injectEventSynch(app1_bad);

        // Give time to inject Problem event
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(), new String[] {"application"}, new String[] {"app1"});

        this.cep.injectEventSynch(app1_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(), new String[] {"application"}, new String[] {"app1"});

        // Now I should see the action
        // cep.injectEventSynch(new CurrentTimeEvent(TS+6*BUFFER));

        // cep.injectEventSynch(new CurrentTimeEvent(TS+8*BUFFER));

    }

    /**
     * Test to validate the application event creation given a config and info events
     * 
     * @throws InterruptedException
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void childInconsistentManyTransition()
        throws InterruptedException,
            EPRuntimeDestroyedException,
            EPCompileException,
            EPDeployException
    {

        final EPStatement stmt =
                               Utils.deployStatement(this.cep.getEsper(),
                                                     "select * from Problem "
                                                                          + "where status in (chip.event.issue.core.Problem$STATUS._NEW, chip.event.issue.core.Problem$STATUS.RESOLVED, chip.event.issue.core.Problem$STATUS.WAIT_FOR_RESOLVED) "
                                                                          + "and action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        // Inject config and info events for application "app1", with TS=TS+1 (in millisecond)
        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("RootController").controller("RootController").state(FSM.RUNNING).isController(Boolean.TRUE).build();
        this.cep.injectEventSynch(controller);
        final RCApplication app1_ok =
                                    new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.RUNNING).parentState(controller.getParentState()).build();
        this.cep.injectEventSynch(app1_ok);

        // Create a new event from app1_ok and change status to absent

        final RCApplication app1_bad =
                                     new RCApplication.CopyBuilder(app1_ok).state(FSM.CONFIGURED).parentState(controller.getParentState()).build();
        this.cep.injectEventSynch(app1_bad);

        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(), new String[] {"application"}, new String[] {"app1"});

        final RCApplication app1_bad2 =
                                      new RCApplication.CopyBuilder(app1_ok).state(FSM.CONNECTED).parentState(controller.getParentState()).build();
        this.cep.injectEventSynch(app1_bad2);
        Thread.sleep(1000);
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));
        // EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),new String[]{"application"}, new String[]{"app1"});

        this.cep.injectEventSynch(app1_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(), new String[] {"application"}, new String[] {"app1"});

        // Now I should see the action
        // cep.injectEventSynch(new CurrentTimeEvent(TS+6*BUFFER));

        // cep.injectEventSynch(new CurrentTimeEvent(TS+8*BUFFER));

    }

    /**
     * Pattern C1 A1_bad A1_ok
     * 
     * @throws InterruptedException
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void pattern1() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("RootController").state(FSM.RUNNING).isController(Boolean.TRUE).build();

        final RCApplication app1_bad = new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.INITIAL).build();
        final RCApplication app1_ok = new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.RUNNING).build();

        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(app1_bad);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "RootController"});

        this.cep.injectEventSynch(app1_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "RootController"});

    }

    /**
     * Pattern 1) C1 A1_bad A1_bad A1_ok Pattern 2) C1 A1_bad C1 A1_bad A1_ok
     * 
     * @throws InterruptedException
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void pattern2() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("RootController").state(FSM.RUNNING).isController(Boolean.TRUE).build();

        final RCApplication app1_bad = new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.INITIAL).build();
        final RCApplication app1_bad_2 =
                                       new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.CONFIGURED).build();
        final RCApplication app1_ok = new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.RUNNING).build();

        // Case 1

        this.cep.injectEventSynch(controller);

        this.cep.injectEvent(app1_bad);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "RootController"});

        this.cep.injectEvent(app1_bad_2);
        Thread.sleep(1000);
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        this.cep.injectEvent(app1_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "RootController"});

        // Case 2
        this.cep.injectEventSynch(controller);
        Thread.sleep(1000);

        this.cep.injectEventSynch(app1_bad);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "RootController"});

        this.cep.injectEventSynch(controller);
        Thread.sleep(1000);
        this.cep.injectEventSynch(app1_bad_2);
        Thread.sleep(1000);
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        this.cep.injectEventSynch(app1_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "RootController"});

    }

    /**
     * Multiple controller updates Pattern 1 C1 C2 A1_bad A1_ok Pattern 2 C1 A1_bad C1 C2 A1_ok Pattern 3 C1 C1(transitioning) A1_bad A1_ok
     * Pattern 4 C1 C1 A1_bad C1 A1_ok
     * 
     * @throws InterruptedException
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void pattern3() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("RootController").state(FSM.RUNNING).isController(Boolean.TRUE).build();
        final RCApplication controller2 =
                                        new RCApplication.DefaultBuilder("another_controller").state(FSM.RUNNING).isTransitioning(Boolean.TRUE).isController(Boolean.TRUE).build();
        final RCApplication app1_bad =
                                     new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.INITIAL).parentState(controller.getState()).build();
        final RCApplication app1_ok =
                                    new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.RUNNING).parentState(controller.getState()).build();
        final RCApplication controller1_tr =
                                           new RCApplication.DefaultBuilder("RootController").state(FSM.RUNNING).isController(Boolean.TRUE).isTransitioning(Boolean.TRUE).build();
        final RCApplication app1_bad_tr =
                                        new RCApplication.CopyBuilder(app1_bad).parentIsTransitioning(controller1_tr.getIsTransitioning()).build();
        final RCApplication app1_ok_tr =
                                       new RCApplication.CopyBuilder(app1_ok).parentIsTransitioning(controller1_tr.getIsTransitioning()).build();

        // Case 1
        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(controller2);
        this.cep.injectEventSynch(app1_bad);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(), new String[] {"application"}, new String[] {"app1"});
        this.cep.injectEventSynch(app1_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(), new String[] {"application"}, new String[] {"app1"});

        // Case2
        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(app1_bad);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(), new String[] {"application"}, new String[] {"app1"});
        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(controller2);
        this.cep.injectEventSynch(app1_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(), new String[] {"application"}, new String[] {"app1"});

        // Case 3
        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(controller1_tr);
        this.cep.injectEventSynch(app1_bad_tr);
        Thread.sleep(1000);
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));
        this.cep.injectEventSynch(app1_ok_tr);
        Thread.sleep(1000);
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Case 4
        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(app1_bad);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "RootController"});
        this.cep.injectEventSynch(controller);
        Thread.sleep(1000);
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));
        this.cep.injectEventSynch(controller);
        Thread.sleep(1000);
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));
        this.cep.injectEventSynch(app1_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "RootController"});

    }

    /**
     * Multiple applications Pattern 1 C1 A1_bad A2_bad A1_ok A2_ok A1_bad A1_ok Pattern 2 C1 C1 A1_bad A1_bad
     * 
     * @throws InterruptedException
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void pattern4() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("RootController").state(FSM.RUNNING).isController(Boolean.TRUE).build();
        final RCApplication app1_bad = new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.INITIAL).build();
        final RCApplication app1_ok = new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.RUNNING).build();
        final RCApplication app2_bad = new RCApplication.DefaultBuilder("app2").controller("RootController").state(FSM.INITIAL).build();
        final RCApplication app2_ok = new RCApplication.DefaultBuilder("app2").controller("RootController").state(FSM.RUNNING).build();

        // Case 1

        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(app1_bad);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "RootController"});
        this.cep.injectEventSynch(app2_bad);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.SET_ERROR, "RootController"});
        this.cep.injectEventSynch(app1_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "RootController"});
        this.cep.injectEventSynch(app2_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.REMOVE_ERROR, "RootController"});
        this.cep.injectEventSynch(app1_bad);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "RootController"});
        this.cep.injectEventSynch(app1_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "RootController"});

        // Case 2
        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(app1_bad);
        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(app1_bad);
        this.cep.injectEventSynch(controller);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "RootController"});

    }

    @Test
    public void pattern5() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("RootController").state(FSM.RUNNING).isController(Boolean.TRUE).build();
        final RCApplication app1_bad = new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.INITIAL).build();
        final RCApplication app1_ok = new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.RUNNING).build();
        final RCApplication controller1_tr =
                                           new RCApplication.DefaultBuilder("RootController").state(FSM.RUNNING).isController(Boolean.TRUE).isTransitioning(Boolean.TRUE).build();

        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(controller1_tr);
        this.cep.injectEventSynch(controller);

        this.cep.injectEventSynch(app1_bad);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "RootController"});
        this.cep.injectEventSynch(app1_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "RootController"});

        // for(int i=0;i<100000;i++) {
        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(app1_bad);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "RootController"});
        this.cep.injectEventSynch(app1_ok);
        Thread.sleep(1000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "RootController"});

        // }

    }

    @Test
    public void pattern5MultiThread() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("RootController").state(FSM.RUNNING).isController(Boolean.TRUE).build();
        final RCApplication controller2 =
                                        new RCApplication.DefaultBuilder("RootController2").state(FSM.RUNNING).isController(Boolean.TRUE).build();
        final RCApplication app1_bad =
                                     new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.INITIAL).parentState(controller.getParentState()).build();
        final RCApplication app1_bad2 =
                                      new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.CONFIGURED).parentState(controller.getParentState()).build();

        this.cep.injectEvent(controller);
        Thread.sleep(1000);
        this.cep.injectEvent(controller);

        this.cep.injectEvent(controller);
        this.cep.injectEvent(controller2);

        this.cep.injectEvent(app1_bad);
        this.cep.injectEvent(app1_bad2);
        this.cep.injectEvent(app1_bad);
        this.cep.injectEvent(app1_bad2);
        this.cep.injectEvent(app1_bad);
        this.cep.injectEvent(app1_bad2);

        this.cep.injectEvent(controller);

        Thread.sleep(5000);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "RootController"});
    }

    @Test
    public void pattern7() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        // Scenario:
        // - The RootController in in RUNNING and its last transition command is START
        // - The child application is in RUNNING too
        // - The RootController executes an in-state transition (i.e., USERBORDCAST)
        // - The child application moves to ROIBSTOPPED
        // In this case the CHILD_INCONSISTENT_STATE should be triggered
        // For possible issues with in-state transitions, see the comment on statement
        // "INSERT_INTO_Problem_ChildInconState_New" in module "problems.epl"

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("RootController").state(FSM.RUNNING).isController(Boolean.TRUE).build();
        final RCApplication app1_ok =
                                    new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.RUNNING).parentState(FSM.RUNNING).parentPreviousTransitionCommand(TRCOMMAND.START).build();
        final RCApplication app1_bad =
                                     new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.ROIBSTOPPED).parentState(FSM.RUNNING).parentPreviousTransitionCommand(TRCOMMAND.USERBROADCAST).build();

        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(app1_ok);
        this.cep.injectEventSynch(app1_bad);

        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "RootController"});
    }

    @Test
    public void pattern6() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        // Controller is performing the CONFIGURE transition
        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("RootController").state(FSM.INITIAL).isController(Boolean.TRUE).isTransitioning(Boolean.TRUE).currentTransitionCommand(TRCOMMAND.CONFIGURE).previousTransitionCommand(TRCOMMAND.INITIALIZE).parentState(FSM._EMPTY).build();

        // When a SHUTDOWN commands arrive, the current transition is interrupted; the controllers stays in the the previous state
        final RCApplication controller_shut =
                                            new RCApplication.DefaultBuilder("RootController").state(FSM.INITIAL).isController(Boolean.TRUE).isTransitioning(Boolean.FALSE).currentTransitionCommand(TRCOMMAND._EMPTY).previousTransitionCommand(TRCOMMAND.CONFIGURE).parentState(FSM._EMPTY).build();

        // One application completed the CONFIGURE transition
        final RCApplication app1_done =
                                      new RCApplication.DefaultBuilder("app1").controller("RootController").state(FSM.CONFIGURED).isTransitioning(Boolean.FALSE).parentState(FSM.INITIAL).previousTransitionCommand(TRCOMMAND.CONFIGURE).parentCurrentTransitionCommand(TRCOMMAND._EMPTY).parentPreviousTransitionCommand(TRCOMMAND.CONFIGURE).currentTransitionCommand(TRCOMMAND._EMPTY).build();

        // One application did not complete the CONFIGURE transition when the SHUTDOWN command arrived
        final RCApplication app2_not_done =
                                          new RCApplication.DefaultBuilder("app2").controller("RootController").state(FSM.INITIAL).isTransitioning(Boolean.TRUE).parentState(FSM.INITIAL).previousTransitionCommand(TRCOMMAND._EMPTY).parentCurrentTransitionCommand(TRCOMMAND._EMPTY).parentPreviousTransitionCommand(TRCOMMAND.CONFIGURE).currentTransitionCommand(TRCOMMAND.CONFIGURE).build();

        // "app1" is formally in inconsistent state because both its parent and itself are not transitioning but are in two different FSM
        // states
        // at the same time the CHILD_INCONSISTENT_STATE error should not be raised because that is a "normal" action: i.e., the current
        // transition
        // is interrupted in case of shutdown

        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(controller_shut);

        this.cep.injectEventSynch(app1_done);
        Thread.sleep(1000);
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        this.cep.injectEventSynch(app2_not_done);
        Thread.sleep(1000);
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));
    }

    /**
     * Spring Java-Configuration class for the test. Define instantiation, configuration, and initialization logic for objects to be managed
     * by the Spring IoC container.
     * 
     * @author lmagnoni
     */
    @Configuration
    static class TestConfiguration {

        @Bean
        public CEPService cep() {
            return new CEPService("TEST2", Boolean.TRUE, Boolean.TRUE, ExecutorFactory.newThreadPool("Processing", 10, false));
        }

        @Bean
        public EsperDAO dao() {
            return new EsperDAO(this.cep());
        }

        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }

        @Bean
        public PrintDebug printDebug() {
            return new PrintDebug(this.cep());
        }

        @Bean
        public PrintXml printXml() {
            return new PrintXml(this.cep(), "file.txt");
        }

        @Bean
        public CommandSender commandSender() {
            return new StubCommandSender("part_ef");
        }

        @Bean
        public ExecutorRC execRC() {
            return new ExecutorRC("ATLAS", this.commandSender());
        }

        @Bean
        public SynchActionExecutor synchActionExecutor() {
            return new SynchActionExecutor();
        }

        @Bean
        public ProblemExecutorAsynch problemExecutorAsynch() {
            return new ProblemExecutorAsynch(this.execRC(), this.dao());
        }

        @Bean
        public ProblemExecutorSynch problemExecutorSynch() {
            return new ProblemExecutorSynch(this.execRC(), this.dao());
        }

        @Bean
        public PrintErs printErs() {
            return new PrintErs();
        }

        /**
         * This is kind of hack to overcome limitation of JavaConfig to load directly a Property object from a property file, equivalent to
         * <util:properties id="configuration" location="classpath:ces.properties" />
         * 
         * @return Properties
         */
        @Bean
        public Properties configuration() {

            /**
             * Set system properties
             */
            System.getProperties().setProperty("tdaq.partition", "cippa");

            // PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
            // propertiesFactoryBean.setLocation(new ClassPathResource("ces.properties"));
            // Properties properties = null;
            // try {
            // propertiesFactoryBean.afterPropertiesSet();
            // properties = propertiesFactoryBean.getObject();
            //
            // } catch (IOException e) {
            // log.warn("Cannot load properties file.");
            // }
            // return properties;

            final Properties p = new Properties();
            p.setProperty("epl.files", "epl/core.epl,epl/problems.epl,epl/print.epl");

            return p;
        }
    }

}
