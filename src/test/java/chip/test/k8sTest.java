package chip.test;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.espertech.esper.common.client.scopetest.EPAssertionUtil;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.scopetest.SupportUpdateListener;

import chip.CEPService;
import chip.event.k8s.Container;
import chip.event.k8s.Node;
import chip.event.k8s.Pod;
import chip.event.k8s.Workload;
import chip.listener.PrintInfo;
import chip.utils.ExecutorFactory;
import es.k8s.ConditionStatus;
import es.k8s.Label;
import es.k8s.container.ContainerInfo;
import es.k8s.container.State;
import es.k8s.container.Type;
import es.k8s.node.Condition;
import es.k8s.node.ConditionType;
import es.k8s.node.NodeInfo;
import es.k8s.pod.Phase;
import es.k8s.pod.PodInfo;
import es.k8s.workload.ConditionEntry;
import es.k8s.workload.Kind;
import es.k8s.workload.WorkloadInfo;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = k8sTest.TestConfiguration.class)
public class k8sTest {
    private final static String DEFAULT_POD_NAME = "my-pod";
    private final static String DEFAULT_HOST_NAME = "my-host";
    private final static String DEFAULT_NAMESPACE = "my-namespace";
    private final static String DEFAULT_CONTAINER_NAME = "my-container";

    @Autowired
    private CEPService cep;

    @Before
    public void setUp() throws Exception {
        Utils.setup(this.cep);
    }

    @After
    public void tearDown() throws Exception {
        Utils.reset(this.cep);
    }

    @Test
    public void testEnums() {
        k8sTest.verifyEnums(es.k8s.ConditionStatus.class, chip.event.k8s.ConditionStatus.class);
        k8sTest.verifyEnums(es.k8s.node.ConditionType.class, chip.event.k8s.Node.ConditionType.class);
        k8sTest.verifyEnums(es.k8s.workload.Kind.class, chip.event.k8s.Workload.Kind.class);
        k8sTest.verifyEnums(es.k8s.pod.Phase.class, chip.event.k8s.Pod.Phase.class);
        k8sTest.verifyEnums(es.k8s.pod.ConditionType.class, chip.event.k8s.Pod.ConditionType.class);
        k8sTest.verifyEnums(es.k8s.container.Type.class, chip.event.k8s.Container.Type.class);
        k8sTest.verifyEnums(es.k8s.container.State.class, chip.event.k8s.Container.State.class);
    }

    @Test
    public void testJoin() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(),
                                                       "context SegmentedByPod "  
                                                       + "select "
                                                       + "pod.name as name "
                                                       + "from " 
                                                       + "PodTable as pod, "
                                                       + "ContainerTable as container " 
                                                       + "where "
                                                       + "container.belongsTo.name = pod.name "
                                                       + "and container.namespace = pod.namespace");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final Node node = this.createNode(k8sTest.DEFAULT_HOST_NAME);
        this.cep.injectEventSynch(node);

        final Workload workload = this.createWorkload();
        this.cep.injectEventSynch(workload);

        final Pod pod = this.createPod(k8sTest.DEFAULT_POD_NAME, k8sTest.DEFAULT_HOST_NAME, k8sTest.DEFAULT_NAMESPACE);
        this.cep.injectEventSynch(pod);

        final Container container = this.createContainer(k8sTest.DEFAULT_CONTAINER_NAME,
                                                         k8sTest.DEFAULT_POD_NAME,
                                                         k8sTest.DEFAULT_HOST_NAME,
                                                         k8sTest.DEFAULT_NAMESPACE);
        this.cep.injectEventSynch(container);
        
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"name"},
                                    new Object[] {k8sTest.DEFAULT_POD_NAME});
        
        final Pod pod_1 = this.createPod("new-pod", k8sTest.DEFAULT_HOST_NAME, k8sTest.DEFAULT_NAMESPACE);
        this.cep.injectEventSynch(pod_1);
        
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));
    }

    @Test
    public void testNodeEvent() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from NodeTable");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final Node node = this.createNode(k8sTest.DEFAULT_HOST_NAME);
        this.cep.injectEventSynch(node);

        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"name", "isEnabled", "conditions", "infoTime"},
                                    new Object[] {node.getName(), node.getIsEnabled(), node.getConditions(), node.getInfoTime()});
    }

    @Test
    public void testWorkloadEvent() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from WorkloadTable");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final Workload workload = this.createWorkload();
        this.cep.injectEventSynch(workload);

        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"kind", "name", "namespace", "labels", "desiredReplicas", "readyReplicas",
                                                  "availableReplicas", "isConditionAvailable", "conditions", "infoTime"},
                                    new Object[] {workload.getKind(), workload.getName(), workload.getNamespace(), workload.getLabels(),
                                                  workload.getDesiredReplicas(), workload.getReadyReplicas(),
                                                  workload.getAvailableReplicas(), workload.getIsConditionAvailable(),
                                                  workload.getConditions(), workload.getInfoTime()});
    }

    @Test
    public void testPodEvent() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from PodTable");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final Pod pod = this.createPod(k8sTest.DEFAULT_POD_NAME, k8sTest.DEFAULT_HOST_NAME, k8sTest.DEFAULT_NAMESPACE);
        this.cep.injectEventSynch(pod);

        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"name", "host", "namespace", "belongsTo", "labels", "phase", "conditions", "infoTime"},
                                    new Object[] {pod.getName(), pod.getHost(), pod.getNamespace(), pod.getBelongsTo(), pod.getLabels(),
                                                  pod.getPhase(), pod.getConditions(), pod.getInfoTime()});
    }

    @Test
    public void testContainerEvent() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from ContainerTable");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final Container container = this.createContainer(k8sTest.DEFAULT_CONTAINER_NAME,
                                                         k8sTest.DEFAULT_POD_NAME,
                                                         k8sTest.DEFAULT_HOST_NAME,
                                                         k8sTest.DEFAULT_NAMESPACE);
        this.cep.injectEventSynch(container);

        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"type", "belongsTo", "name", "host", "namespace", "state", "isReady", "isStarted",
                                                  "restartCount", "exitCode", "signal", "infoTime"},
                                    new Object[] {container.getType(), container.getBelongsTo(), container.getName(), container.getHost(),
                                                  container.getNamespace(), container.getState(), container.getIsReady(),
                                                  container.getIsStarted(), container.getRestartCount(), container.getExitCode(),
                                                  container.getSignal(), container.getInfoTime()});
    }

    @Configuration
    static class TestConfiguration {
        @Bean
        public CEPService cep() {
            return new CEPService("TEST2", Boolean.TRUE, Boolean.TRUE, ExecutorFactory.newThreadPool("Processing", 1, false));
        }

        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }

        @Bean
        public Properties configuration() {
            System.getProperties().setProperty("tdaq.partition", "cippa");

            final Properties p = new Properties();
            p.setProperty("epl.files", "epl/k8s.epl");

            return p;
        }

    }

    private Node createNode(final String hostName) {
        final long infoTime = System.nanoTime();
        final es.k8s.node.ConditionEntry[] conditions = new es.k8s.node.ConditionEntry[6];

        conditions[0] = new es.k8s.node.ConditionEntry(ConditionType.READY,
                                                       new Condition(ConditionType.READY,
                                                                     ConditionStatus.FALSE_,
                                                                     "reason",
                                                                     "message",
                                                                     infoTime));

        conditions[1] = new es.k8s.node.ConditionEntry(ConditionType.DISK_PRESSURE,
                                                       new Condition(ConditionType.DISK_PRESSURE,
                                                                     ConditionStatus.TRUE_,
                                                                     "reason",
                                                                     "message",
                                                                     infoTime));

        conditions[2] = new es.k8s.node.ConditionEntry(ConditionType.MEMORY_PRESSURE,
                                                       new Condition(ConditionType.MEMORY_PRESSURE,
                                                                     ConditionStatus.UNKNOWN,
                                                                     "reason",
                                                                     "message",
                                                                     infoTime));

        conditions[3] = new es.k8s.node.ConditionEntry(ConditionType.PID_PRESSURE,
                                                       new Condition(ConditionType.PID_PRESSURE,
                                                                     ConditionStatus.OTHER,
                                                                     "reason",
                                                                     "message",
                                                                     infoTime));

        conditions[4] = new es.k8s.node.ConditionEntry(ConditionType.NETWORK_UNAVAILABLE,
                                                       new Condition(ConditionType.NETWORK_UNAVAILABLE,
                                                                     ConditionStatus.FALSE_,
                                                                     "reason",
                                                                     "message",
                                                                     infoTime));

        conditions[5] = new es.k8s.node.ConditionEntry(ConditionType.OTHER,
                                                       new Condition(ConditionType.OTHER,
                                                                     ConditionStatus.FALSE_,
                                                                     "reason",
                                                                     "message",
                                                                     infoTime));

        final NodeInfo nodeInfo = new NodeInfo(hostName, true, conditions, infoTime);
        final Node node = new Node(nodeInfo);

        return node;
    }

    private WorkloadInfo createWorkloadInfo() {
        final long infoTime = System.nanoTime();

        final Label[] labels = new Label[3];
        labels[0] = new Label("key-0", "value-0");
        labels[1] = new Label("key-1", "value-1");
        labels[2] = new Label("key-2", "value-2");

        final ConditionEntry conditions[] = new ConditionEntry[4];
        conditions[0] = new ConditionEntry("Progressing",
                                           new es.k8s.workload.Condition("Progressing",
                                                                         ConditionStatus.TRUE_,
                                                                         "reason",
                                                                         "message",
                                                                         infoTime));

        conditions[1] =
                      new ConditionEntry("Available",
                                         new es.k8s.workload.Condition("Available", ConditionStatus.FALSE_, "reason", "message", infoTime));

        conditions[2] = new ConditionEntry("ReplicaFailure",
                                           new es.k8s.workload.Condition("ReplicaFailure",
                                                                         ConditionStatus.UNKNOWN,
                                                                         "reason",
                                                                         "message",
                                                                         infoTime));

        conditions[3] = new ConditionEntry("AnotherCondition",
                                           new es.k8s.workload.Condition("AnotherCondition",
                                                                         ConditionStatus.TRUE_,
                                                                         "reason",
                                                                         "message",
                                                                         infoTime));

        final WorkloadInfo workloadInfo = new WorkloadInfo(Kind.DEPLOYMENT,
                                                           "my-deployment",
                                                           "my-namespace",
                                                           labels,
                                                           10,
                                                           8,
                                                           9,
                                                           true,
                                                           conditions,
                                                           infoTime);
        return workloadInfo;
    }

    private Workload createWorkload() {
        final Workload workload = new Workload(this.createWorkloadInfo());
        return workload;
    }

    private PodInfo createPodInfo(final String podName, final String hostName, final String namespace) {
        final long infoTime = System.nanoTime();

        final Label[] labels = new Label[3];
        labels[0] = new Label("key-0", "value-0");
        labels[1] = new Label("key-1", "value-1");
        labels[2] = new Label("key-2", "value-2");

        final es.k8s.pod.ConditionEntry[] conditions = new es.k8s.pod.ConditionEntry[6];
        conditions[0] = new es.k8s.pod.ConditionEntry(es.k8s.pod.ConditionType.POD_SCHEDULED,
                                                      new es.k8s.pod.Condition("reason",
                                                                               ConditionStatus.TRUE_,
                                                                               es.k8s.pod.ConditionType.POD_SCHEDULED,
                                                                               "message",
                                                                               infoTime));

        conditions[1] = new es.k8s.pod.ConditionEntry(es.k8s.pod.ConditionType.POD_READY_TO_START_CONTAINERS,
                                                      new es.k8s.pod.Condition("reason",
                                                                               ConditionStatus.TRUE_,
                                                                               es.k8s.pod.ConditionType.POD_READY_TO_START_CONTAINERS,
                                                                               "message",
                                                                               infoTime));

        conditions[2] = new es.k8s.pod.ConditionEntry(es.k8s.pod.ConditionType.CONTAINERS_READY,
                                                      new es.k8s.pod.Condition("reason",
                                                                               ConditionStatus.TRUE_,
                                                                               es.k8s.pod.ConditionType.CONTAINERS_READY,
                                                                               "message",
                                                                               infoTime));

        conditions[3] = new es.k8s.pod.ConditionEntry(es.k8s.pod.ConditionType.INITIALIZED,
                                                      new es.k8s.pod.Condition("reason",
                                                                               ConditionStatus.TRUE_,
                                                                               es.k8s.pod.ConditionType.INITIALIZED,
                                                                               "message",
                                                                               infoTime));

        conditions[4] = new es.k8s.pod.ConditionEntry(es.k8s.pod.ConditionType.READY,
                                                      new es.k8s.pod.Condition("reason",
                                                                               ConditionStatus.TRUE_,
                                                                               es.k8s.pod.ConditionType.READY,
                                                                               "message",
                                                                               infoTime));

        conditions[5] = new es.k8s.pod.ConditionEntry(es.k8s.pod.ConditionType.OTHER,
                                                      new es.k8s.pod.Condition("reason",
                                                                               ConditionStatus.TRUE_,
                                                                               es.k8s.pod.ConditionType.OTHER,
                                                                               "message",
                                                                               infoTime));

        final PodInfo podInfo = new PodInfo(podName,
                                            hostName,
                                            namespace,
                                            this.createWorkloadInfo(),
                                            labels,
                                            Phase.RUNNING,
                                            conditions,
                                            infoTime);
        return podInfo;
    }

    private Pod createPod(final String podName, final String hostName, final String namespace) {
        final Pod pod = new Pod(this.createPodInfo(podName, hostName, namespace));
        return pod;
    }

    private Container createContainer(final String containerName, final String podName, final String hostName, final String namespace) {
        final ContainerInfo containerInfo = new ContainerInfo(Type.MAIN,
                                                              this.createPodInfo(podName, hostName, namespace),
                                                              containerName,
                                                              hostName,
                                                              namespace,
                                                              State.RUNNING,
                                                              false,
                                                              true,
                                                              7,
                                                              -1,
                                                              15,
                                                              System.nanoTime());

        final Container container = new Container(containerInfo);
        return container;
    }

    // Check that enums in CORBA data structures are properly represented in the Java code.
    // This uses reflection to get the CORBA enums using the "from_int" static method.
    // Then the CORBA enum value is translated into the corresponding Java value using the "from"
    // static method.
    // The full list of CORBA enums is retrieved invoking the "from_int" static method until it
    // throws an instance of the org.omg.CORBA.BAD_PARAM exception.
    private static <T, K> void verifyEnums(final Class<T> corba, final Class<K> java) {
        try {
            final Method corbaFromInt = corba.getMethod("from_int", int.class);
            final Method fromCorba = java.getMethod("from", corba);

            for(int i = 0;; ++i) {
                // Get the CORBA enum item corresponding to the "i" integer value
                @SuppressWarnings("unchecked")
                final T corbaEnum = (T) corbaFromInt.invoke(null, Integer.valueOf(i));

                // Convert the CORBA enum to the Java one
                @SuppressWarnings("unchecked")
                final K javaEnum = (K) fromCorba.invoke(null, corbaEnum);

                // Pedantic check: get back the original CORBA enum item from Java and compare the
                // result of "toString()" call
                final Method originalEnumFromJava = java.getMethod("original");
                final Object v = originalEnumFromJava.invoke(javaEnum);

                MatcherAssert.assertThat(v.toString(), CoreMatchers.is(corbaEnum.toString()));
            }
        }
        catch(final NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException ex) {
            // This shall never happen
            ex.printStackTrace();
            Assert.fail(ex.toString());
        }
        catch(final InvocationTargetException ex) {
            // org.omg.CORBA.BAD_PARAM is expected
            final Throwable cause = ex.getCause();
            MatcherAssert.assertThat(cause, CoreMatchers.instanceOf(org.omg.CORBA.BAD_PARAM.class));
        }
    }
}
