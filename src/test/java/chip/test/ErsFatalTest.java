package chip.test;

import java.util.Properties;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.espertech.esper.common.client.scopetest.EPAssertionUtil;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.scopetest.SupportUpdateListener;

import chip.CEPService;
import chip.dao.EsperDAO;
import chip.event.en.MEMBERSHIP;
import chip.event.en.STATUS;
import chip.event.issue.core.Problem;
import chip.event.meta.RCApplication;
import chip.executor.ExecutorRC;
import chip.listener.PrintDebug;
import chip.listener.PrintErs;
import chip.listener.PrintInfo;
import chip.listener.PrintXml;
import chip.subscriber.core.ProblemExecutorAsynch;
import chip.subscriber.core.ProblemExecutorSynch;
import chip.subscriber.meta.SynchActionExecutor;
import chip.utils.ExecutorFactory;
import daq.EsperUtils.ERSEvent;
import daq.rc.CommandSender;
import ers.Severity;


/**
 * Unit tests for ers messages of severity FATAL
 * 
 * @author ganders
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ErsFatalTest.TestConfiguration.class)
public class ErsFatalTest {

    @Autowired
    private CEPService cep;

    @Before
    public void setUp() throws Exception {
        Utils.setup(this.cep);
    }

    @After
    public void tearDown() throws Exception {
        Utils.reset(this.cep);
    }
    
    @Test
    public void ersMessageOfSeverityFatal() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException, InterruptedException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem " + "where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        // Inject controller and application
        final RCApplication parentController = new RCApplication.DefaultBuilder("parentController").isController(Boolean.TRUE).build();
        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("myController").controller("parentController").isController(Boolean.TRUE).build();
        this.cep.injectEventSynch(parentController);
        this.cep.injectEventSynch(controller);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Now inject ers message of severity WARNING
        final String[] qualifiers = {""};
        final ERSEvent ersMsg1 = new ERSEvent();
        ersMsg1.setPartitionName("ATLAS");
        ersMsg1.setSeverity(Severity.level.Warning.toString());
        ersMsg1.setMachineName("myHost1");
        ersMsg1.setApplicationName("myController");
        ersMsg1.setIssuedDate(Long.valueOf(Long.MAX_VALUE));
        ersMsg1.setFormattedDate("2013-01-01 00:00");
        ersMsg1.setMessageTxt("HltpuCrashIssue: some Cause from HltpuNo1");
        ersMsg1.setQualifiers(qualifiers);
        this.cep.injectEventSynch(ersMsg1);

        // Assert that no action is triggered
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Inject message of severity FATAL
        final ERSEvent ersMsg2 = new ERSEvent();
        ersMsg2.setPartitionName("ATLAS");
        ersMsg2.setSeverity(Severity.level.Fatal.toString());
        ersMsg2.setMachineName("myHost1");
        ersMsg2.setApplicationName("myController");
        ersMsg2.setIssuedDate(Long.valueOf(Long.MAX_VALUE));
        ersMsg2.setFormattedDate("2013-01-01 00:00");
        ersMsg2.setMessageTxt("HltpuCrashIssue: some Cause from HltpuNo1");
        ersMsg2.setQualifiers(qualifiers);
        this.cep.injectEventSynch(ersMsg2);

        // Assert that correct action is performed
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"myController", Problem.ACTION.SET_ERROR, "myController"});

        // No event triggered, Problem for this application and this type already created
        this.cep.injectEventSynch(ersMsg2);
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Simulating a kill and start of the controller
        final RCApplication controller_ex =
                                          new RCApplication.DefaultBuilder("myController").controller("parentController").isController(Boolean.TRUE).membership(MEMBERSHIP._OUT).status(STATUS.EXITED).build();
        this.cep.injectEventSynch(controller_ex);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        final RCApplication controller_up =
                                          new RCApplication.DefaultBuilder("myController").controller("parentController").isController(Boolean.TRUE).membership(MEMBERSHIP._IN).status(STATUS.UP).build();
        this.cep.injectEventSynch(controller_up);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Event should be triggered: when the controller exited, all the related problems and errors have been cleaned up
        this.cep.injectEventSynch(ersMsg2);

        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"myController", Problem.ACTION.SET_ERROR, "myController"});
        
        // Just wait to let the engine to send the command to set the error
        // That is done in a different thread and without waiting the engine is destroyed in the while
        Thread.sleep(3000);
    }

    @Test
    public void ersMessageOfSeverityFatal_RootController() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException, InterruptedException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem " + "where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        // Inject controller and application
        final RCApplication parentController = new RCApplication.DefaultBuilder("RootController").isController(Boolean.TRUE).build();
        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("RootController").controller("RootController").isController(Boolean.TRUE).build();
        this.cep.injectEventSynch(parentController);
        this.cep.injectEventSynch(controller);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Now inject ers message of severity WARNING
        final String[] qualifiers = {""};
        final ERSEvent ersMsg1 = new ERSEvent();
        ersMsg1.setPartitionName("ATLAS");
        ersMsg1.setSeverity(Severity.level.Warning.toString());
        ersMsg1.setMachineName("myHost1");
        ersMsg1.setApplicationName("RootController");
        ersMsg1.setIssuedDate(Long.valueOf(Long.MAX_VALUE));
        ersMsg1.setFormattedDate("2013-01-01 00:00");
        ersMsg1.setMessageTxt("HltpuCrashIssue: some Cause from HltpuNo1");
        ersMsg1.setQualifiers(qualifiers);
        this.cep.injectEventSynch(ersMsg1);

        // Assert that no action is triggered
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Inject message of severity FATAL
        final ERSEvent ersMsg2 = new ERSEvent();
        ersMsg2.setPartitionName("ATLAS");
        ersMsg2.setSeverity(Severity.level.Fatal.toString());
        ersMsg2.setMachineName("myHost1");
        ersMsg2.setApplicationName("RootController");
        ersMsg2.setIssuedDate(Long.valueOf(Long.MAX_VALUE));
        ersMsg2.setFormattedDate("2013-01-01 00:00");
        ersMsg2.setMessageTxt("HltpuCrashIssue: some Cause from HltpuNo1");
        ersMsg2.setQualifiers(qualifiers);
        this.cep.injectEventSynch(ersMsg2);

        // Assert that correct action is performed
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"RootController", Problem.ACTION.SET_ERROR, "RootController"});
        
        // Just wait to let the engine to send the command to set the error
        // That is done in a different thread and without waiting the engine is destroyed in the while
        Thread.sleep(3000);
    }

    @Test
    public void ersMessageOfSeverityFatal_Leaf() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from Problem " + "where action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        // Inject controller and application
        final RCApplication parentController = new RCApplication.DefaultBuilder("parentController").isController(Boolean.TRUE).build();
        final RCApplication app = new RCApplication.DefaultBuilder("myApp").controller("parentController").isController(Boolean.FALSE).build();
        this.cep.injectEventSynch(parentController);
        this.cep.injectEventSynch(app);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Now inject ers message of severity FATAL
        final String[] qualifiers = {""};
        final ERSEvent ersMsg1 = new ERSEvent();
        ersMsg1.setPartitionName("ATLAS");
        ersMsg1.setSeverity(Severity.level.Fatal.toString());
        ersMsg1.setMachineName("myHost1");
        ersMsg1.setApplicationName("myApp");
        ersMsg1.setIssuedDate(Long.valueOf(Long.MAX_VALUE));
        ersMsg1.setFormattedDate("2013-01-01 00:00");
        ersMsg1.setMessageTxt("HltpuCrashIssue: some Cause from HltpuNo1");
        ersMsg1.setQualifiers(qualifiers);
        this.cep.injectEventSynch(ersMsg1);

        // Assert that no action is triggered
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));
    }

    /**
     * Spring Java-Configuration class for the test. Define instantiation, configuration, and initialization logic for objects to be managed
     * by the Spring IoC container.
     * 
     * @author lmagnoni
     */
    @Configuration
    static class TestConfiguration {

        @Bean
        public CEPService cep() {
            return new CEPService("TEST2", Boolean.TRUE, Boolean.TRUE, ExecutorFactory.newThreadPool("Processing", 10, false));
        }

        @Bean
        public EsperDAO dao() {
            return new EsperDAO(this.cep());
        }

        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }

        @Bean
        public PrintDebug printDebug() {
            return new PrintDebug(this.cep());
        }

        @Bean
        public PrintXml printXml() {
            return new PrintXml(this.cep(), "file.txt");
        }

        @Bean
        public PrintErs printErs() {
            return new PrintErs();
        }

        @Bean
        public CommandSender commandSender() {
            return new StubCommandSender("part_ef");
        }

        @Bean
        public ExecutorRC execRC() {
            return new ExecutorRC("ATLAS", this.commandSender());
        }

        @Bean
        public SynchActionExecutor synchActionExecutor() {
            return new SynchActionExecutor();
        }
        
        @Bean
        public ProblemExecutorAsynch problemExecutorAsynch() {
            return new ProblemExecutorAsynch(this.execRC(), this.dao());
        }

        @Bean
        public ProblemExecutorSynch problemExecutorSynch() {
            return new ProblemExecutorSynch(this.execRC(), this.dao());
        }

        /**
         * This is kind of hack to overcome limitation of JavaConfig to load directly a Property object from a property file, equivalent to
         * <util:properties id="configuration" location="classpath:ces.properties" />
         * 
         * @return Properties
         */
        @Bean
        public Properties configuration() {

            /**
             * Set system properties
             */
            System.getProperties().setProperty("tdaq.partition", "cippa");

            // PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
            // propertiesFactoryBean.setLocation(new ClassPathResource("ces.properties"));
            // Properties properties = null;
            // try {
            // propertiesFactoryBean.afterPropertiesSet();
            // properties = propertiesFactoryBean.getObject();
            //
            // } catch (IOException e) {
            // log.warn("Cannot load properties file.");
            // }
            // return properties;

            final Properties p = new Properties();
            p.setProperty("epl.files", "epl/core.epl,epl/problems.epl,epl/print.epl,epl/rulesErs.epl");

            return p;
        }
    }

}
