package chip.test;

import java.util.Collections;
import java.util.Date;
import java.util.Properties;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import chip.CEPService;
import chip.dao.EsperDAO;
import chip.event.config.ApplicationConfig;
import chip.event.dynamic.ApplicationInfo;
import chip.event.en.FSM;
import chip.event.en.IFDIES;
import chip.event.en.IFFAILED;
import chip.event.en.MEMBERSHIP;
import chip.event.en.STARTAT;
import chip.event.en.STATUS;
import chip.event.en.STOPAT;
import chip.event.en.TRCOMMAND;
import chip.event.meta.RCApplication;
import chip.listener.PrintDebug;
import chip.listener.PrintErs;
import chip.listener.PrintInfo;
import chip.listener.PrintXml;
import chip.subscriber.meta.SynchActionExecutor;
import chip.utils.ExecutorFactory;


/**
 * Unit tests for the validation of the Application event generation, as union of static configuration events and dynamic information events
 * about application status and, in case, state information.
 * <p>
 * Tests environment is build using Spring Java Config, as from here:
 * http://blog.springsource.org/2011/06/21/spring-3-1-m2-testing-with-configuration-classes-and-profiles/
 * 
 * @author lmagnoni
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CoreStatementsTest.TestConfiguration.class)
public class CoreStatementsTest {

    /**
     * Size of the time-order buffer in millisecond
     */
    private static Long ORDER_BUFFER = Long.valueOf(100L);

    @Autowired
    private CEPService cep;

    @Autowired
    private EsperDAO dao;

    @Before
    public void setUp() throws Exception {
        Utils.setup(this.cep);
    }

    @After
    public void tearDown() throws Exception {
        Utils.reset(this.cep);
    }

    /**
     * Test to validate the application event creation given a config and info events
     * 
     * @throws InterruptedException
     */
    @Test
    public void validateApplicationCreation() throws InterruptedException {

        // Check that no application exists yet
        MatcherAssert.assertThat("Wrong number of applications returned",
                          Integer.valueOf(this.dao.getAllRCApplication().size()),
                          CoreMatchers.is(Integer.valueOf(0)));
        // Inject config and info events for application "app1"
        this.cep.injectEventSynch(CoreStatementsTest.app1config);
        this.cep.injectEventSynch(CoreStatementsTest.app1info);
        // Check that now the application is retrieved via EsperDAO

        // Wait the size of the time buffer
        Thread.sleep(CoreStatementsTest.ORDER_BUFFER.longValue());
        MatcherAssert.assertThat("Wrong number of applications returned",
                          Integer.valueOf(this.dao.getAllRCApplication().size()),
                          CoreMatchers.is(Integer.valueOf(1)));

        // Inject config and info events for application "app2"
        this.cep.injectEventSynch(CoreStatementsTest.app2config);
        this.cep.injectEventSynch(CoreStatementsTest.app2info);
        // Now I should get 2 applications

        // Wait the size of the time buffer
        Thread.sleep(CoreStatementsTest.ORDER_BUFFER.longValue());
        MatcherAssert.assertThat("Wrong number of applications returned",
                          Integer.valueOf(this.dao.getAllRCApplication().size()),
                          CoreMatchers.is(Integer.valueOf(2)));

        // Validate Applications
        for(final RCApplication app : this.dao.getAllRCApplication()) {
            if(app.getName().equals("app1")) {
                this.validateApplication(app, CoreStatementsTest.app1config, CoreStatementsTest.app1info);
            } else {
                this.validateApplication(app, CoreStatementsTest.app2config, CoreStatementsTest.app2info);
            }
        }

    }

    /**
     * This tests validate the application update
     * <p>
     * It read the configuration file 'part_ef' build using partition maker cli tool.
     * <p>
     * 
     * @throws InterruptedException
     */
    @Test
    public void validateApplicationUpdate() throws InterruptedException {

        // Inject config and info events for application "app1"
        this.cep.injectEventSynch(CoreStatementsTest.app1config);
        this.cep.injectEventSynch(CoreStatementsTest.app1info);
        this.cep.injectEventSynch(CoreStatementsTest.app2config);
        this.cep.injectEventSynch(CoreStatementsTest.app2info);

        // Update app1 dynamic info with membership out.
        // This should create a new app1 event,
        // and the old one should be discharged. Check that:
        // - the total app retrieved should remains 2 and
        // - app1 has membership out

        // Wait the size of the time buffer
        Thread.sleep(CoreStatementsTest.ORDER_BUFFER.longValue()); // Size of the ordering buffer

        final ApplicationInfo app1info_bis = new ApplicationInfo("app1",
                                                                 Long.valueOf(new Date().getTime()),
                                                                 Long.valueOf(new Date().getTime()),
                                                                 MEMBERSHIP._OUT,
                                                                 "realhost",
                                                                 STATUS.UP,
                                                                 Boolean.FALSE,
                                                                 Boolean.FALSE,
                                                                 Long.valueOf(123),
                                                                 Long.valueOf(456),
                                                                 FSM.RUNNING,
                                                                 Boolean.FALSE,
                                                                 TRCOMMAND._EMPTY,
                                                                 TRCOMMAND.START,
                                                                 Boolean.FALSE,
                                                                 Boolean.FALSE);
        this.cep.injectEventSynch(app1info_bis);

        // Wait the size of the time buffer + some margin
        Thread.sleep(CoreStatementsTest.ORDER_BUFFER.longValue() * 2);

        MatcherAssert.assertThat("Wrong number of applications returned",
                          Integer.valueOf(this.dao.getAllRCApplication().size()),
                          CoreMatchers.is(Integer.valueOf(2)));
        RCApplication app1 = null;
        for(final RCApplication app : this.dao.getAllRCApplication()) {
            if(app.getName().equals("app1")) {
                app1 = app;
            }
        }
        this.validateApplication(app1, CoreStatementsTest.app1config, app1info_bis);

    }

    /**
     * Verify the correctness of the Application event given the building input events. This verify the EPL logic in common.epl
     * 
     * @todo move asserEquals to assertThat, more expressive
     * @param result
     * @param capp1
     * @param dapp1
     */

    private void validateApplication(final RCApplication result, final ApplicationConfig capp1, final ApplicationInfo dapp1) {

        MatcherAssert.assertThat("Wrong application name", capp1.getName(), CoreMatchers.equalTo(result.getName()));
        MatcherAssert.assertThat("Wrong oks class name", capp1.getOksClassName(), CoreMatchers.equalTo(result.getOksClassName()));
        MatcherAssert.assertThat("Wrong restartable during run flag",
                          capp1.getRestartableDuringRun(),
                          CoreMatchers.equalTo(result.getRestartableDuringRun()));
        MatcherAssert.assertThat("Wrong ifFailed attribute", capp1.getIfFailed(), CoreMatchers.equalTo(result.getIfFailed()));
        MatcherAssert.assertThat("Wrong ifDies attribute", capp1.getIfDies(), CoreMatchers.equalTo(result.getIfDies()));
        MatcherAssert.assertThat("Wrong startAt attribute", capp1.getStartAt(), CoreMatchers.equalTo(result.getStartAt()));
        MatcherAssert.assertThat("Wrong stopAt attribute", capp1.getStopAt(), CoreMatchers.equalTo(result.getStopAt()));
        MatcherAssert.assertThat("Wrong startAtState attribute", capp1.getStartAtState(), CoreMatchers.equalTo(result.getStartAtState()));
        MatcherAssert.assertThat("Wrong stopAtState attribute", capp1.getStopAtState(), CoreMatchers.equalTo(result.getStopAtState()));
        MatcherAssert.assertThat("Wrong getAllowSpontaneousExit attribute",
                          capp1.getAllowSpontaneousExit(),
                          CoreMatchers.equalTo(result.getAllowSpontaneousExit()));

        Assert.assertEquals("BackupHost", capp1.getBackupHosts(), result.getBackupHosts());
        Assert.assertEquals("RunningHost", dapp1.getRunningHost(), result.getRunningHost());
        Assert.assertEquals("Status", dapp1.getStatus(), result.getStatus());
        Assert.assertEquals("NotResponding", dapp1.getNotResponding(), result.getNotResponding());
        Assert.assertEquals("Membership", dapp1.getMembership(), result.getMembership());
        Assert.assertEquals("Restarting", dapp1.getRestarting(), result.getRestarting());
        Assert.assertEquals("exitCode", dapp1.getExitCode(), result.getExitCode());
        Assert.assertEquals("exitSignal", dapp1.getExitSignal(), result.getExitSignal());
        Assert.assertEquals("IsControlledBy", capp1.getController(), result.getController());
        Assert.assertEquals("Segment", capp1.getSegment(), result.getSegment());
        Assert.assertEquals("AllowSpontaneousExit", capp1.getAllowSpontaneousExit(), result.getAllowSpontaneousExit());

    }

    /**
     * Spring Java-Configuration class for the test. Define instantiation, configuration, and initialization logic for objects to be managed
     * by the Spring IoC container.
     * 
     * @author lmagnoni
     */
    @Configuration
    static class TestConfiguration {

        @Bean
        public CEPService cep() {
            return new CEPService("TEST2", Boolean.TRUE, Boolean.TRUE, ExecutorFactory.newThreadPool("Processing", 10, false));
        }

        @Bean
        public EsperDAO dao() {
            return new EsperDAO(this.cep());
        }

        @Bean
        public SynchActionExecutor synchActionExecutor() {
            return new SynchActionExecutor();
        }
        
        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }
        
        @Bean
        public PrintErs printErs() {
            return new PrintErs();
        }

        @Bean
        public PrintDebug printDebug() {
            return new PrintDebug(this.cep());
        }

        @Bean
        public PrintXml printXml() {
            return new PrintXml(this.cep(), "file.txt");
        }

        /**
         * This is kind of hack to overcome limitation of JavaConfig to load directly a Property object from a property file, equivalent to
         * <util:properties id="configuration" location="classpath:ces.properties" />
         * 
         * @return Properties
         */
        @Bean
        public Properties configuration() {
            // PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
            // propertiesFactoryBean.setLocation(new ClassPathResource("ces.properties"));
            // Properties properties = null;
            // try {
            // propertiesFactoryBean.afterPropertiesSet();
            // properties = propertiesFactoryBean.getObject();
            //
            // } catch (IOException e) {
            // log.warn("Cannot load properties file.");
            // }
            // return properties;

            final Properties p = new Properties();
            p.setProperty("epl.files", "epl/core.epl,epl/print.epl");
            return p;
        }
    }

    /**
     * Utilities events used for testing
     */

    private final static ApplicationConfig app1config = new ApplicationConfig("app1",
                                                                              "oksClassName",
                                                                              Boolean.FALSE,
                                                                              IFFAILED.RESTART,
                                                                              IFDIES.IGNORE,
                                                                              STARTAT.BOOT,
                                                                              STOPAT.SOR,
                                                                              FSM.INITIAL,
                                                                              FSM.GTHSTOPPED,
                                                                              "myhost",
                                                                              Collections.emptySet(),
                                                                              "mycontroller",
                                                                              "mysegment",
                                                                              Boolean.FALSE);

    private final static ApplicationConfig app2config = new ApplicationConfig("app2",
                                                                              "oksClassName",
                                                                              Boolean.FALSE,
                                                                              IFFAILED.RESTART,
                                                                              IFDIES.IGNORE,
                                                                              STARTAT.BOOT,
                                                                              STOPAT.SOR,
                                                                              FSM.INITIAL,
                                                                              FSM.GTHSTOPPED,
                                                                              "fakehost",
                                                                              Collections.emptySet(),
                                                                              "mycontroller",
                                                                              "mysegment",
                                                                              Boolean.FALSE);

    private final static ApplicationInfo app1info = new ApplicationInfo("app1",
                                                                        Long.valueOf(new Date().getTime()),
                                                                        Long.valueOf(new Date().getTime()),
                                                                        MEMBERSHIP._IN,
                                                                        "realhost",
                                                                        STATUS.UP,
                                                                        Boolean.FALSE,
                                                                        Boolean.FALSE,
                                                                        Long.valueOf(123),
                                                                        Long.valueOf(456),
                                                                        FSM.RUNNING,
                                                                        Boolean.FALSE,
                                                                        TRCOMMAND._EMPTY,
                                                                        TRCOMMAND.START,
                                                                        Boolean.FALSE,
                                                                        Boolean.FALSE);

    private final static ApplicationInfo app2info = new ApplicationInfo("app2",
                                                                        Long.valueOf(new Date().getTime()),
                                                                        Long.valueOf(new Date().getTime()),
                                                                        MEMBERSHIP._IN,
                                                                        "realhost",
                                                                        STATUS.UP,
                                                                        Boolean.FALSE,
                                                                        Boolean.FALSE,
                                                                        Long.valueOf(123),
                                                                        Long.valueOf(456),
                                                                        FSM.RUNNING,
                                                                        Boolean.FALSE,
                                                                        TRCOMMAND._EMPTY,
                                                                        TRCOMMAND.START,
                                                                        Boolean.FALSE,
                                                                        Boolean.FALSE);

}
