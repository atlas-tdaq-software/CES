package chip.test;

import java.util.Properties;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import chip.CEPService;
import chip.dao.EsperDAO;
import chip.event.issue.IdentifiableIssue;
import chip.event.issue.core.Problem;
import chip.executor.ExecutorRC;
import chip.listener.PrintDebug;
import chip.listener.PrintErs;
import chip.listener.PrintInfo;
import chip.listener.PrintXml;
import chip.subscriber.core.ProblemExecutorAsynch;
import chip.subscriber.core.ProblemExecutorSynch;
import chip.subscriber.meta.SynchActionExecutor;
import chip.utils.ExecutorFactory;
import daq.rc.CommandSender;


/**
 * Unit tests for the validation of the Application event generation, as union of static configuration events and dynamic information events
 * about application status and, in case, state information.
 * <p>
 * This test uses external time source to define time passing in Esper, for a better testing of out of order event injection
 * <p>
 * Tests environment is build using Spring Java Config, as from here:
 * http://blog.springsource.org/2011/06/21/spring-3-1-m2-testing-with-configuration-classes-and-profiles/
 * 
 * @author lmagnoni
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ProblemTest.TestConfiguration.class)
public class ProblemTest {

    /**
     * Initial Time event in millisecond
     */
    private static final Long TS = Long.valueOf(1000L);

    /**
     * Size of the ordered window buffer in milliseconds
     */
    private static final long BUFFER = 100L;

    @Autowired
    private CEPService cep;

    @Autowired
    private EsperDAO dao;

    @Before
    public void setUp() throws Exception {
        Utils.setup(this.cep);
        // Set Esper initial time
        this.cep.getEsper().getEventService().advanceTime(ProblemTest.TS.longValue());
    }

    @After
    public void tearDown() throws Exception {
        Utils.reset(this.cep);
    }

    /**
     * Validate error stream using DAO for on-demand query execution
     */
    @Test
    public void validateErrorStream() {

        final Problem e1 = new Problem("mycontroller",
                                       "app1",
                                       Problem.TYPE.APPLICATION_DEAD,
                                       Problem.STATUS._NEW,
                                       Problem.ACTION.NONE,
                                       IdentifiableIssue.generateUUID(""));
        this.cep.injectEventSynch(e1);
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()), CoreMatchers.is(Integer.valueOf(0)));

        final Problem e2 = new Problem("mycontroller",
                                       "app2",
                                       Problem.TYPE.APPLICATION_DEAD,
                                       Problem.STATUS._NEW,
                                       Problem.ACTION.NONE,
                                       IdentifiableIssue.generateUUID(""));
        this.cep.injectEventSynch(e2);
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()), CoreMatchers.is(Integer.valueOf(0)));

        final Problem e1_hdl = new Problem("mycontroller",
                                           "app1",
                                           Problem.TYPE.APPLICATION_DEAD,
                                           Problem.STATUS.WAIT_FOR_RESOLVED,
                                           Problem.ACTION.SET_ERROR,
                                           e1.getId());
        this.cep.injectEventSynch(e1_hdl);
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()), CoreMatchers.is(Integer.valueOf(1)));

        final Problem e1_bis = new Problem("mycontroller",
                                           "app1",
                                           Problem.TYPE.APPLICATION_DEAD,
                                           Problem.STATUS.WAIT_FOR_RESOLVED,
                                           Problem.ACTION.NONE,
                                           e1.getId());
        this.cep.injectEventSynch(e1_bis);
        // Now the time of the buffer window is gone, I should see an event
        this.cep.getEsper().getEventService().advanceTime(ProblemTest.TS.longValue() + (4 * ProblemTest.BUFFER));
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()), CoreMatchers.is(Integer.valueOf(1)));

    }

    /**
     * Spring Java-Configuration class for the test. Define instantiation, configuration, and initialization logic for objects to be managed
     * by the Spring IoC container.
     * 
     * @author lmagnoni
     */
    @Configuration
    static class TestConfiguration {

        @Bean
        public CEPService cep() {
            return new CEPService("TEST2", Boolean.TRUE, Boolean.TRUE, ExecutorFactory.newThreadPool("Processing", 10, false));
        }

        @Bean
        public EsperDAO dao() {
            return new EsperDAO(this.cep());
        }

        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }

        @Bean
        public PrintDebug printDebug() {
            return new PrintDebug(this.cep());
        }

        @Bean
        public PrintXml printXml() {
            return new PrintXml(this.cep(), "file.txt");
        }

        @Bean
        public CommandSender commandSender() {
            return new StubCommandSender("part_ef");
        }

        @Bean
        public PrintErs printErs() {
            return new PrintErs();
        }

        @Bean
        public ExecutorRC execRC() {
            return new ExecutorRC("ATLAS", this.commandSender());
        }
        
        @Bean
        public SynchActionExecutor synchActionExecutor() {
            return new SynchActionExecutor();
        }

        @Bean
        public ProblemExecutorAsynch problemExecutorAsynch() {
            return new ProblemExecutorAsynch(this.execRC(), this.dao());
        }

        @Bean
        public ProblemExecutorSynch problemExecutorSynch() {
            return new ProblemExecutorSynch(this.execRC(), this.dao());
        }

        /**
         * This is kind of hack to overcome limitation of JavaConfig to load directly a Property object from a property file, equivalent to
         * <util:properties id="configuration" location="classpath:ces.properties" />
         * 
         * @return Properties
         */
        @Bean
        public Properties configuration() {

            /**
             * Set system properties
             */
            System.getProperties().setProperty("tdaq.partition", "cippa");

            // PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
            // propertiesFactoryBean.setLocation(new ClassPathResource("ces.properties"));
            // Properties properties = null;
            // try {
            // propertiesFactoryBean.afterPropertiesSet();
            // properties = propertiesFactoryBean.getObject();
            //
            // } catch (IOException e) {
            // log.warn("Cannot load properties file.");
            // }
            // return properties;

            final Properties p = new Properties();
            p.setProperty("epl.files", "epl/core.epl,epl/problems.epl,epl/print.epl");

            return p;
        }
    }

}
