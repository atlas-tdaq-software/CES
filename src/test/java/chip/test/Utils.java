package chip.test;

import com.espertech.esper.common.client.EPCompiled;
import com.espertech.esper.compiler.client.CompilerArguments;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.compiler.client.EPCompilerProvider;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;
import com.espertech.esper.runtime.client.EPStatement;

import chip.CEPService;
import chip.CEPService.EPLReplacementString;
import chip.CHIPException.CEPConfigurationError;


class Utils {
    private Utils() {
    }

    public static EPStatement deployStatement(final EPRuntime esper, final String epl)
        throws EPCompileException,
            EPRuntimeDestroyedException,
            EPDeployException
    {
        final CompilerArguments compilerArgs = new CompilerArguments(esper.getConfigurationDeepCopy());
        compilerArgs.getPath().add(esper.getRuntimePath());
        final EPCompiled compiled = EPCompilerProvider.getCompiler().compile(epl, compilerArgs);
        return esper.getDeploymentService().deploy(compiled).getStatements()[0];
    }

    public static void setup(final CEPService cep) throws CEPConfigurationError {
        cep.setEPLReplacementValue(EPLReplacementString.ROOT_CONTROLLER_NAME, "RootController");
        cep.setEPLReplacementValue(EPLReplacementString.DO_RECOVERY, "true");
        cep.setEPLReplacementValue(EPLReplacementString.DO_AUTO_PROCEDURE, "true");
        cep.setEPLReplacementValue(EPLReplacementString.MAX_EVENT_IS_SERVER_NAME, "");
        cep.setEPLReplacementValue(EPLReplacementString.MAX_EVENT_IS_INFO_NAME, "");
        cep.setEPLReplacementValue(EPLReplacementString.MAX_EVENT_IS_ATTRIBUTE_NAME, "");            
        
        cep.initAndConfigure();
    }
    
    public static void reset(final CEPService cep) {
        cep.stop();
    }
}
