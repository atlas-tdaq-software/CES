package chip.test;

import chip.CHIPException.ExecutorRCException;
import chip.executor.ExecutorRC;
import daq.rc.CommandSender;

public class StubRCExecutor extends ExecutorRC {
    public StubRCExecutor(final String partitionName, final CommandSender commandSender) {
        super(partitionName, commandSender);
    }

    @Override
    public void pingApplication(String application) throws ExecutorRCException {
    }
}
