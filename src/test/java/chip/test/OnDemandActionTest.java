package chip.test;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.scopetest.SupportUpdateListener;

import chip.CEPService;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.meta.RCApplication;
import chip.listener.PrintErs;
import chip.listener.PrintInfo;
import chip.subscriber.auto.OnDemandExecutor;
import chip.subscriber.meta.SynchActionExecutor;
import chip.utils.ExecutorFactory;
import daq.EsperUtils.ERSEvent;
import daq.EsperUtils.ISEvent;
import daq.EsperUtils.ISEvent.OP;
import daq.EsperUtils.TypedObject;
import ers.Severity;
import pmgClient.ConfigurationException;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = OnDemandActionTest.TestConfiguration.class)
public class OnDemandActionTest {
    private final static String PART_NAME = "part_hlt_avolio";
    private final static String ERS_MSG_ID = "tdaq::sa::CHIPAction";
    private final static String OKS_DB =
                                       "oksconfig:/afs/cern.ch/user/a/avolio/work/public/partitions/tdaq-07-01-00/part_hlt_avolio_merged.data.xml";

    @Autowired
    private CEPService cep;

    @Before
    public void setUp() throws Exception {
        Utils.setup(this.cep);
    }

    @After
    public void tearDown() throws Exception {
        Utils.reset(this.cep);
    }

    @Test
    public void startAppsCommand() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(),
                                                       "select * from OnDemandAction " + "where action = chip.event.issue.auto.OnDemandAction$ACTION.START_APPS");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication app_1 = new RCApplication.Builder("app_1").controller("ctrl_1").runningHost("host_1").build();
        final RCApplication app_2 = new RCApplication.Builder("app_2").controller("ctrl_2").runningHost("host_2").build();
        final RCApplication app_3 = new RCApplication.Builder("app_3").controller("ctrl_3").runningHost("host_3").build();

        this.cep.injectEventSynch(app_1);
        this.cep.injectEventSynch(app_2);
        this.cep.injectEventSynch(app_3);

        this.cep.injectEventSynch(this.createERSEvent("START_APPS", "app_1, app_2, app_3"));

        Thread.sleep(1000);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.TRUE));

        this.cep.injectEventSynch(this.createISEvent("START_APPS", "app_1, app_2, app_3"));

        Thread.sleep(1000);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.TRUE));
    }

    @Test
    public void restartAppsCommand() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt =
                               Utils.deployStatement(this.cep.getEsper(),
                                                     "select * from OnDemandAction " + "where action = chip.event.issue.auto.OnDemandAction$ACTION.RESTART_APPS");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication app_1 = new RCApplication.Builder("app_1").controller("ctrl_1").runningHost("host_1").build();
        final RCApplication app_2 = new RCApplication.Builder("app_2").controller("ctrl_2").runningHost("host_2").build();
        final RCApplication app_3 = new RCApplication.Builder("app_3").controller("ctrl_3").runningHost("host_3").build();

        this.cep.injectEventSynch(app_1);
        this.cep.injectEventSynch(app_2);
        this.cep.injectEventSynch(app_3);

        this.cep.injectEventSynch(this.createERSEvent("RESTART_APPS", "app_1, app_2, app_3"));

        Thread.sleep(1000);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.TRUE));

        this.cep.injectEventSynch(this.createISEvent("RESTART_APPS", "app_1, app_2, app_3"));

        Thread.sleep(1000);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.TRUE));
    }

    @Test
    public void killAppsCommand() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(),
                                                       "select * from OnDemandAction " + "where action = chip.event.issue.auto.OnDemandAction$ACTION.KILL_APPS");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication app_1 = new RCApplication.Builder("app_1").controller("ctrl_1").runningHost("host_1").build();
        final RCApplication app_2 = new RCApplication.Builder("app_2").controller("ctrl_2").runningHost("host_2").build();
        final RCApplication app_3 = new RCApplication.Builder("app_3").controller("ctrl_3").runningHost("host_3").build();

        this.cep.injectEventSynch(app_1);
        this.cep.injectEventSynch(app_2);
        this.cep.injectEventSynch(app_3);

        this.cep.injectEventSynch(this.createERSEvent("KILL_APPS", "app_1, app_2, app_3"));

        Thread.sleep(1000);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.TRUE));

        this.cep.injectEventSynch(this.createISEvent("KILL_APPS", "app_1, app_2, app_3"));

        Thread.sleep(1000);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.TRUE));
    }

    @Test
    public void unknownCommand() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(),
                                                       "select * from OnDemandAction " + "where action = chip.event.issue.auto.OnDemandAction$ACTION.UNKNOWN");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        this.cep.injectEventSynch(this.createERSEvent("FAKE_COMMAND", "1, 2, 3"));

        Thread.sleep(1000);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.TRUE));

        this.cep.injectEventSynch(this.createISEvent("FAKE_COMMAND", "1, 2, 3"));

        Thread.sleep(1000);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.TRUE));
    }

    private ERSEvent createERSEvent(final String command, final String args) {
        final ERSEvent ersMsg = new ERSEvent();

        ersMsg.setMessageID(OnDemandActionTest.ERS_MSG_ID);
        ersMsg.setPartitionName(OnDemandActionTest.PART_NAME);
        ersMsg.setSeverity(Severity.level.Information.toString());
        ersMsg.setMachineName("localhost.localdomain");
        ersMsg.setApplicationName("some_app");
        ersMsg.setMessageTxt("There is something to do");

        final Map<String, String> params = new HashMap<>();
        params.put("command", command);
        params.put("arguments", args);
        ersMsg.setParameters(params);

        return ersMsg;
    }

    private ISEvent createISEvent(final String command, final String args) {
        final Map<String, TypedObject> attrs = new TreeMap<>();
        attrs.put("command", new TypedObject(command));
        attrs.put("arguments", new TypedObject(args));

        final ISEvent isEvnt = new ISEvent(OP.UPDATE,
                                           "RunCtrlStatistics",
                                           "RunCtrlStatistics.CHIPAction",
                                           "OnDemandAction",
                                           new java.util.Date().getTime() * 1000,
                                           attrs,
                                           "myPartition");
        return isEvnt;
    }

    @Configuration
    static class TestConfiguration {
        @Bean
        public CEPService cep() {
            return new CEPService("ON_DEMAND_TEST", Boolean.TRUE, Boolean.TRUE, ExecutorFactory.newThreadPool("Processing", 10, false));
        }

        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }

        @Bean
        public PrintErs printErs() {
            return new PrintErs();
        }

        @Bean
        public SynchActionExecutor synchActionExecutor() {
            return new SynchActionExecutor();
        }
        
        @Bean
        public OnDemandExecutor onDemandExecutor() throws ConfigurationException {
            return new OnDemandExecutor(new StubCommandSender(OnDemandActionTest.PART_NAME),
                                        new StubPMGExecutor(),
                                        new DAQConfigReader(OnDemandActionTest.PART_NAME, OnDemandActionTest.OKS_DB, "CHIP"),
                                        new EsperDAO(this.cep()));
        }

        @Bean
        public Properties configuration() {
            System.getProperties().setProperty("tdaq.partition", OnDemandActionTest.PART_NAME);

            final Properties p = new Properties();
            p.setProperty("epl.files", "epl/core.epl,epl/onDemand.epl");

            return p;
        }
    }

}
