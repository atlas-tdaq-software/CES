package chip.test;

import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.scopetest.SupportUpdateListener;

import chip.CEPService;
import chip.DAQConfigReader;
import chip.event.en.FSM;
import chip.event.meta.RCApplication;
import chip.listener.PrintErs;
import chip.listener.PrintInfo;
import chip.subscriber.meta.SynchActionExecutor;
import chip.subscriber.recovery.RootCtrlExecutor;
import chip.utils.ExecutorFactory;
import daq.EsperUtils.ISEvent;
import daq.EsperUtils.ISEvent.OP;
import daq.EsperUtils.TypedObject;
import pmgClient.ConfigurationException;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RootCtrlActionTest.TestConfiguration.class)
public class RootCtrlActionTest {
    private final static String PART_NAME = "part_hlt_avolio";
    private final static String OKS_DB =
                                       "oksconfig:/afs/cern.ch/user/a/avolio/work/public/partitions/tdaq-07-01-00/part_hlt_avolio_merged.data.xml";

    @Autowired
    private CEPService cep;

    @Before
    public void setUp() throws Exception {
        Utils.setup(this.cep);
    }

    @After
    public void tearDown() throws Exception {
        Utils.reset(this.cep);
    }

    @Test
    public void restartRootOnHostProblem() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(),
                                                       "select * from RootCtrlActionTable " + 
                                                        "where type = chip.event.issue.recovery.RootCtrl$TYPE.HOST_PROBLEM " +
                                                        " and action = chip.event.issue.recovery.RootCtrl$ACTION.RESTART");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        this.cep.getEsper().getEventService().advanceTime(0); // Advance time
                
        this.cep.getEsper().getEventService().advanceTime(10000); // Advance time
        
        final RCApplication root = new RCApplication.Builder("RootController").controller("RootController")
                                                                              .runningHost("host_1")
                                                                              .isTransitioning(Boolean.FALSE)
                                                                              .parentIsInitializing(Boolean.FALSE)
                                                                              .state(FSM.RUNNING).build();
        this.cep.injectEventSynch(root); 
        this.cep.injectEventSynch(this.createPMGISEvent("running"));

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));
        
        this.cep.getEsper().getEventService().advanceTime(150000); // Advance time

        Thread.sleep(1000);
        
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.TRUE));
    }
    
    @Test
    public void restartRootOnHealthCheck() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(),
                                                       "select * from RootCtrlActionTable " + 
                                                        "where type = chip.event.issue.recovery.RootCtrl$TYPE.NOT_HEALTHY " +
                                                        " and action = chip.event.issue.recovery.RootCtrl$ACTION.RESTART");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        this.cep.getEsper().getEventService().advanceTime(0); // Advance time
                
        this.cep.getEsper().getEventService().advanceTime(10000); // Advance time
        
        final RCApplication root = new RCApplication.Builder("RootController").controller("RootController")
                                                                              .runningHost("host_1")
                                                                              .isTransitioning(Boolean.FALSE)
                                                                              .parentIsInitializing(Boolean.FALSE)
                                                                              .state(FSM.RUNNING).build();
        this.cep.injectEventSynch(root); 
        this.cep.injectEventSynch(this.createRunCtrlISEvent());

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));
        
        this.cep.getEsper().getEventService().advanceTime(50000); // Advance time

        Thread.sleep(1000);
        
        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.TRUE));
    }
    
    @Test
    public void restartRooOnCrash() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {
        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(),
                                                       "select * from RootCtrlActionTable " + 
                                                        "where type = chip.event.issue.recovery.RootCtrl$TYPE.CRASH " +
                                                        " and action = chip.event.issue.recovery.RootCtrl$ACTION.RESTART");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);
        
        this.cep.injectEventSynch(this.createPMGISEvent("failure"));

        Thread.sleep(1000);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.TRUE));
    }

    private ISEvent createRunCtrlISEvent() {
        final Map<String, TypedObject> attrs = new TreeMap<>();

        final ISEvent isEvnt = new ISEvent(OP.UPDATE,
                                           "RunCtrl",
                                           "RunCtrl.RootController",
                                           "RCStateInfo",
                                           new java.util.Date().getTime() * 1000,
                                           attrs,
                                           RootCtrlActionTest.PART_NAME);
        return isEvnt;
    }
    
    private ISEvent createPMGISEvent(final String state) {
        final Map<String, TypedObject> attrs = new TreeMap<>();
        attrs.put("host", new TypedObject("some_host"));
        attrs.put("app_name", new TypedObject("RootController"));
        attrs.put("state", new TypedObject(state));

        final ISEvent isEvnt = new ISEvent(OP.UPDATE,
                                           "PMG",
                                           "PMG.some_host|RootController",
                                           "PMGPublishedProcessData",
                                           new java.util.Date().getTime() * 1000,
                                           attrs,
                                           RootCtrlActionTest.PART_NAME);
        return isEvnt;
    }

    @Configuration
    static class TestConfiguration {
        @Bean
        public CEPService cep() {
            return new CEPService("RC_RESTART_TEST", Boolean.FALSE, Boolean.FALSE, ExecutorFactory.newThreadPool("Processing", 10, false));
        }

        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }

        @Bean
        public PrintErs printErs() {
            return new PrintErs();
        }
        
        @Bean
        public SynchActionExecutor synchActionExecutor() {
            return new SynchActionExecutor();
        }

        @Bean
        public RootCtrlExecutor rootCtrlExecutor() throws ConfigurationException {
            return new RootCtrlExecutor(new StubRCExecutor(RootCtrlActionTest.PART_NAME, new StubCommandSender(RootCtrlActionTest.PART_NAME)),
                                        new StubPMGExecutor(),
                                        new DAQConfigReader(RootCtrlActionTest.PART_NAME, RootCtrlActionTest.OKS_DB, "CHIP"));
        }

        @Bean
        public Properties configuration() {
            System.getProperties().setProperty("tdaq.partition", RootCtrlActionTest.PART_NAME);

            final Properties p = new Properties();
            p.setProperty("epl.files", "epl/core.epl,epl/rootCtrl.epl");

            return p;
        }
    }

}
