package chip.test;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import daq.rc.ErrorElement;


public class StubCommandSender extends daq.rc.CommandSender {

    private static final Log log = LogFactory.getLog(StubCommandSender.class);

    public StubCommandSender(final String arg0) {
        super(arg0, "CES");
    }

    @Override
    public void restartApplications(final String arg0, final String[] arg1) {
        StubCommandSender.log.info("Sending restart command to controller " + arg0 + " for application: " + arg1[0]);

    }

    @Override
    public void startApplications(final String arg0, final String[] arg1) {
        StubCommandSender.log.info("Sending start command to controller " + arg0 + " for application: " + arg1[0]);

    }

    @Override
    public void stopApplications(final String arg0, final String[] arg1) {
        StubCommandSender.log.info("Sending stop command to controller " + arg0 + " for application: " + arg1[0]);

    }

    @Override
    public void userCommand(final String arg0, final String arg1, final String[] arg2) {
        StubCommandSender.log.info("Sending user command " + arg1 + " " + Arrays.deepToString(arg2) + " to application: " + arg0);
    }

    @Override
    public void setError(final String arg0, final List<? extends ErrorElement> arg1) {

        StubCommandSender.log.info("Sending set error to controller " + arg0 + " for # application: " + arg1.size());
        if(!arg1.isEmpty()) {
            for(final ErrorElement e : arg1) {
                StubCommandSender.log.debug("Application " + e.getApplicationName() + ", error " + e.getErrorType());
            }
        }
    }

    @Override
    public void changeMembership(final String arg0, final boolean arg1, final String[] arg2) {
        StubCommandSender.log.info("Sending change of membership (" + arg1 + ") to controller " + arg0 + " for # application: "
                                   + arg2.length);
        for(final String e : arg2) {
            StubCommandSender.log.debug("Application " + e);
        }
    }

}
