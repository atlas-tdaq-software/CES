package chip.test;

import java.util.Properties;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.espertech.esper.common.client.scopetest.EPAssertionUtil;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.scopetest.SupportUpdateListener;

import chip.CEPService;
import chip.dao.EsperDAO;
import chip.event.en.FSM;
import chip.event.en.STATUS;
import chip.event.issue.core.Problem;
import chip.event.meta.RCApplication;
import chip.executor.ExecutorRC;
import chip.listener.PrintDebug;
import chip.listener.PrintErs;
import chip.listener.PrintInfo;
import chip.listener.PrintXml;
import chip.subscriber.core.ProblemExecutorAsynch;
import chip.subscriber.core.ProblemExecutorSynch;
import chip.subscriber.meta.SynchActionExecutor;
import chip.utils.ExecutorFactory;
import daq.rc.CommandSender;


/**
 * Unit tests for the validation of the Application event generation, as union of static configuration events and dynamic information events
 * about application status and, in case, state information.
 * <p>
 * This test uses external time source to define time passing in Esper, for a better testing of out of order event injection
 * <p>
 * Tests environment is build using Spring Java Config, as from here:
 * http://blog.springsource.org/2011/06/21/spring-3-1-m2-testing-with-configuration-classes-and-profiles/
 * 
 * @author lmagnoni
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = ControllerTest.TestConfiguration.class)
public class ControllerTest {

    @Autowired
    private CEPService cep;

    @Autowired
    private EsperDAO dao;

    @Before
    public void setUp() throws Exception {
        Utils.setup(this.cep);
    }

    @After
    public void tearDown() throws Exception {
        Utils.reset(this.cep);
    }

    /**
     * Test to validate the application event creation given a config and info events C_ok A1_ok A1_bad
     * 
     * @throws InterruptedException
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void basic() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt =
                               Utils.deployStatement(this.cep.getEsper(),
                                                     "select * from Problem "
                                                                          + "where status in (chip.event.issue.core.Problem$STATUS._NEW, chip.event.issue.core.Problem$STATUS.RESOLVED, chip.event.issue.core.Problem$STATUS.WAIT_FOR_RESOLVED) "
                                                                          + "and action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("mycontroller").isController(Boolean.TRUE).state(FSM.RUNNING).build();
        this.cep.injectEventSynch(controller);

        final RCApplication app1_ok =
                                    new RCApplication.DefaultBuilder("app1").controller("mycontroller").status(STATUS.UP).parentState(controller.getState()).startAtState(FSM.INITIAL).stopAtState(FSM.NONE).build();
        this.cep.injectEventSynch(app1_ok);

        final RCApplication app2_ok =
                                    new RCApplication.DefaultBuilder("app2").controller("mycontroller").state(FSM.RUNNING).parentState(controller.getState()).startAtState(FSM.INITIAL).stopAtState(FSM.NONE).build();
        this.cep.injectEventSynch(app2_ok);

        MatcherAssert.assertThat(Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        // Create a new event from app1_ok and change status to absent
        final RCApplication app1_bad =
                                     new RCApplication.CopyBuilder(app1_ok).status(STATUS.ABSENT).parentState(controller.getState()).build();
        this.cep.injectEventSynch(app1_bad);

        Thread.sleep(600);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "mycontroller"});

        // Create a new event from app1_ok and change status to absent
        final RCApplication app2_bad =
                                     new RCApplication.CopyBuilder(app2_ok).state(FSM.SFOSTOPPED).parentState(controller.getState()).build();
        this.cep.injectEventSynch(app2_bad);

        Thread.sleep(600);

        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.SET_ERROR, "mycontroller"});

        // Recovery app1
        this.cep.injectEventSynch(app1_ok);

        Thread.sleep(600);

        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "mycontroller"});
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()), CoreMatchers.is(Integer.valueOf(1)));

        this.cep.injectEventSynch(app1_bad);
        Thread.sleep(600);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "mycontroller"});

        this.cep.injectEventSynch(app2_ok);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.REMOVE_ERROR, "mycontroller"});
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()), CoreMatchers.is(Integer.valueOf(1)));

        this.cep.injectEventSynch(app1_ok);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "mycontroller"});
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()), CoreMatchers.is(Integer.valueOf(0)));

    }

    /**
     * Test to validate the application event creation given a config and info events C_ok A1_bad_state A2_bad_notrunning A1_ok A2_ok
     * 
     * @throws InterruptedException
     * @throws EPDeployException
     * @throws EPCompileException
     * @throws EPRuntimeDestroyedException
     */
    @Test
    public void multiple() throws InterruptedException, EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt =
                               Utils.deployStatement(this.cep.getEsper(),
                                                     "select * from Problem "
                                                                          + "where status in (chip.event.issue.core.Problem$STATUS._NEW, chip.event.issue.core.Problem$STATUS.RESOLVED, chip.event.issue.core.Problem$STATUS.WAIT_FOR_RESOLVED) "
                                                                          + "and action != chip.event.issue.core.Problem$ACTION.NONE");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        final RCApplication controller =
                                       new RCApplication.DefaultBuilder("mycontroller").isController(Boolean.TRUE).state(FSM.RUNNING).build();
        final RCApplication app1_ok =
                                    new RCApplication.DefaultBuilder("app1").controller("mycontroller").status(STATUS.UP).parentState(controller.getState()).startAtState(FSM.INITIAL).stopAtState(FSM.NONE).build();
        final RCApplication app2_ok =
                                    new RCApplication.DefaultBuilder("app2").controller("mycontroller").state(FSM.RUNNING).parentState(controller.getState()).startAtState(FSM.INITIAL).stopAtState(FSM.NONE).build();

        final RCApplication app1_bad =
                                     new RCApplication.CopyBuilder(app1_ok).status(STATUS.ABSENT).parentState(controller.getState()).build();
        final RCApplication app2_bad =
                                     new RCApplication.CopyBuilder(app2_ok).state(FSM.SFOSTOPPED).parentState(controller.getState()).build();

        this.cep.injectEventSynch(controller);
        this.cep.injectEventSynch(app1_ok);
        this.cep.injectEventSynch(app2_ok);

        this.cep.injectEventSynch(app1_bad);
        Thread.sleep(600);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.SET_ERROR, "mycontroller"});

        this.cep.injectEventSynch(app2_bad);
        Thread.sleep(600);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.SET_ERROR, "mycontroller"});

        this.cep.injectEventSynch(app1_ok);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app1", Problem.ACTION.REMOVE_ERROR, "mycontroller"});
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()), CoreMatchers.is(Integer.valueOf(1)));

        this.cep.injectEventSynch(app2_ok);
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(),
                                    new String[] {"application", "action", "controller"},
                                    new Object[] {"app2", Problem.ACTION.REMOVE_ERROR, "mycontroller"});
        MatcherAssert.assertThat(Integer.valueOf(this.dao.getAllErrorsOfController("mycontroller").size()), CoreMatchers.is(Integer.valueOf(0)));

    }

    /**
     * Spring Java-Configuration class for the test. Define instantiation, configuration, and initialization logic for objects to be managed
     * by the Spring IoC container.
     * 
     * @author lmagnoni
     */
    @Configuration
    static class TestConfiguration {

        @Bean
        public CEPService cep() {
            return new CEPService("TEST2", Boolean.TRUE, Boolean.TRUE, ExecutorFactory.newThreadPool("Processing", 10, false));
        }

        @Bean
        public EsperDAO dao() {
            return new EsperDAO(this.cep());
        }

        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }

        @Bean
        public PrintDebug printDebug() {
            return new PrintDebug(this.cep());
        }

        @Bean
        public PrintXml printXml() {
            return new PrintXml(this.cep(), "file.txt");
        }

        @Bean
        public CommandSender commandSender() {
            return new StubCommandSender("part_ef");
        }

        @Bean
        public ExecutorRC execRC() {
            return new ExecutorRC("ATLAS", this.commandSender());
        }

        @Bean
        public SynchActionExecutor synchActionExecutor() {
            return new SynchActionExecutor();
        }
        
        @Bean
        public ProblemExecutorAsynch problemExecutorAsynch() {
            return new ProblemExecutorAsynch(this.execRC(), this.dao());
        }

        @Bean
        public ProblemExecutorSynch problemExecutorSynch() {
            return new ProblemExecutorSynch(this.execRC(), this.dao());
        }

        @Bean
        public PrintErs printErs() {
            return new PrintErs();
        }

        /**
         * This is kind of hack to overcome limitation of JavaConfig to load directly a Property object from a property file, equivalent to
         * <util:properties id="configuration" location="classpath:ces.properties" />
         * 
         * @return Properties
         */
        @Bean
        public Properties configuration() {

            /**
             * Set system properties
             */
            System.getProperties().setProperty("tdaq.partition", "cippa");

            // PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
            // propertiesFactoryBean.setLocation(new ClassPathResource("ces.properties"));
            // Properties properties = null;
            // try {
            // propertiesFactoryBean.afterPropertiesSet();
            // properties = propertiesFactoryBean.getObject();
            //
            // } catch (IOException e) {
            // log.warn("Cannot load properties file.");
            // }
            // return properties;

            final Properties p = new Properties();
            p.setProperty("epl.files", "epl/core.epl,epl/problems.epl,epl/print.epl");

            return p;
        }
    }

}
