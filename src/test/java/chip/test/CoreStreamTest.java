package chip.test;

import java.util.Collections;
import java.util.Properties;

import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.espertech.esper.common.client.scopetest.EPAssertionUtil;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.scopetest.SupportUpdateListener;

import chip.CEPService;
import chip.dao.EsperDAO;
import chip.event.config.RCApplicationConfig;
import chip.event.dynamic.ApplicationInfo;
import chip.event.dynamic.RCApplicationInfo;
import chip.event.en.FSM;
import chip.event.en.IFDIES;
import chip.event.en.IFERROR;
import chip.event.en.IFFAILED;
import chip.event.en.MEMBERSHIP;
import chip.event.en.RCCOMMAND;
import chip.event.en.STARTAT;
import chip.event.en.STATUS;
import chip.event.en.STOPAT;
import chip.event.en.TRCOMMAND;
import chip.event.meta.RCApplication;
import chip.listener.PrintDebug;
import chip.listener.PrintInfo;
import chip.subscriber.meta.SynchActionExecutor;
import chip.utils.ExecutorFactory;
import daq.rc.CommandSender;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = CoreStreamTest.TestConfiguration.class)
public class CoreStreamTest {

    @Autowired
    private CEPService cep;

    @Autowired
    private EsperDAO dao;

    @Before
    public void setUp() throws Exception {
        Utils.setup(this.cep);
    }

    @After
    public void tearDown() throws Exception {
        Utils.reset(this.cep);
    }

    @Test
    public void validateRCApplicationCreation() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from RCApplication");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        /**
         * Validation input sequence and application event correctness
         */
        final RCApplicationConfig capp1 = new RCApplicationConfig("rcapp1",
                                                                  "oksClassName",
                                                                  Boolean.FALSE,
                                                                  IFFAILED.IGNORE,
                                                                  IFDIES.ERROR,
                                                                  STARTAT.BOOT,
                                                                  STOPAT.EOR,
                                                                  FSM.INITIAL,
                                                                  FSM.ROIBSTOPPED,
                                                                  "myhost.cern.ch",
                                                                  Collections.<String> emptySet(),
                                                                  "mycontroller",
                                                                  "mysegment",
                                                                  Boolean.FALSE,
                                                                  Boolean.FALSE,
                                                                  IFERROR.HANDLE,
                                                                  Boolean.FALSE);

        final ApplicationInfo dapp1 = new ApplicationInfo("rcapp1",
                                                          Long.valueOf(10L),
                                                          Long.valueOf(10L),
                                                          MEMBERSHIP._IN,
                                                          "realhost",
                                                          STATUS.UP,
                                                          Boolean.FALSE,
                                                          Boolean.FALSE,
                                                          Long.valueOf(0),
                                                          Long.valueOf(0),
                                                          FSM.RUNNING,
                                                          Boolean.FALSE,
                                                          TRCOMMAND._EMPTY,
                                                          TRCOMMAND.START,
                                                          Boolean.FALSE,
                                                          Boolean.FALSE);

        final RCApplicationInfo rcapp1 = new RCApplicationInfo("rcapp1",
                                                               Long.valueOf(11L),
                                                               Long.valueOf(11L),
                                                               FSM.ABSENT,
                                                               Boolean.FALSE,
                                                               TRCOMMAND.CONFIG,
                                                               "uid",
                                                               TRCOMMAND._EMPTY,
                                                               Long.valueOf(10L),
                                                               Boolean.FALSE,
                                                               RCCOMMAND.DISABLE,
                                                               new String[] {""},
                                                               "uid",
                                                               Boolean.FALSE,
                                                               FSM.RUNNING,
                                                               Boolean.FALSE,
                                                               TRCOMMAND._EMPTY,
                                                               TRCOMMAND.START,
                                                               Boolean.FALSE,
                                                               Boolean.FALSE);

        this.cep.injectEventSynch(capp1);
        MatcherAssert.assertThat("Wrong listener invocation", Boolean.valueOf(listener.isInvoked()), CoreMatchers.is(Boolean.FALSE));

        this.cep.injectEventSynch(dapp1);
        listener.assertOneGetNewAndReset();

        this.cep.injectEventSynch(rcapp1);
        this.validateRCApplication(this.dao.createRCApplicationFromEventBean(listener.assertOneGetNewAndReset()), capp1, dapp1, rcapp1);

        /**
         * Update dynamic application information
         */
        final ApplicationInfo dapp1_2 = new ApplicationInfo("rcapp1",
                                                            Long.valueOf(10L),
                                                            Long.valueOf(10L),
                                                            MEMBERSHIP._IN,
                                                            "realhost",
                                                            STATUS.EXITED,
                                                            Boolean.FALSE,
                                                            Boolean.FALSE,
                                                            Long.valueOf(0),
                                                            Long.valueOf(0),
                                                            FSM.RUNNING,
                                                            Boolean.FALSE,
                                                            TRCOMMAND._EMPTY,
                                                            TRCOMMAND.START,
                                                            Boolean.FALSE,
                                                            Boolean.FALSE);
        this.cep.injectEventSynch(dapp1_2);
        this.validateRCApplication(this.dao.createRCApplicationFromEventBean(listener.assertOneGetNewAndReset()), capp1, dapp1_2, rcapp1);

        /**
         * Update dynamic rcapplication information
         */
        final RCApplicationInfo rcapp1_2 = new RCApplicationInfo("rcapp1",
                                                                 Long.valueOf(11L),
                                                                 Long.valueOf(10L),
                                                                 FSM.ABSENT,
                                                                 Boolean.FALSE,
                                                                 TRCOMMAND.SHUTDOWN,
                                                                 "uid",
                                                                 TRCOMMAND._EMPTY,
                                                                 Long.valueOf(10L),
                                                                 Boolean.FALSE,
                                                                 RCCOMMAND.DISABLE,
                                                                 new String[] {""},
                                                                 "uid",
                                                                 Boolean.FALSE,
                                                                 FSM.RUNNING,
                                                                 Boolean.FALSE,
                                                                 TRCOMMAND._EMPTY,
                                                                 TRCOMMAND.START,
                                                                 Boolean.FALSE,
                                                                 Boolean.FALSE);

        this.cep.injectEventSynch(rcapp1_2);
        this.validateRCApplication(this.dao.createRCApplicationFromEventBean(listener.assertOneGetNewAndReset()), capp1, dapp1_2, rcapp1_2);

    }

    @Test
    public void validateControllerCreation() throws EPRuntimeDestroyedException, EPCompileException, EPDeployException {

        final EPStatement stmt = Utils.deployStatement(this.cep.getEsper(), "select * from RCApplicationTable(isController = true)");
        final SupportUpdateListener listener = new SupportUpdateListener();
        stmt.addListener(listener);

        this.cep.injectEventSynch(new RCApplication.DefaultBuilder("mycontroller").isController(Boolean.TRUE).build());
        this.cep.injectEventSynch(new RCApplication.DefaultBuilder("app1").controller("mycontroller").build());
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(), new String[] {"name"}, new Object[] {"mycontroller"});

        this.cep.injectEventSynch(new RCApplication.DefaultBuilder("mycontroller2").isController(Boolean.TRUE).build());
        this.cep.injectEventSynch(new RCApplication.DefaultBuilder("app2").controller("mycontroller2").build());
        EPAssertionUtil.assertProps(listener.assertOneGetNewAndReset(), new String[] {"name"}, new Object[] {"mycontroller2"});

    }

    /**
     * Verify the correctness of the Application event given the building input events. This verify the EPL logic in common.epl
     * 
     * @param result
     * @param capp1
     * @param dapp1
     */

    private void validateRCApplication(final RCApplication result,
                                       final RCApplicationConfig capp1,
                                       final ApplicationInfo dapp1,
                                       final RCApplicationInfo rcinfo)
    {

        Assert.assertEquals("Application Name", capp1.getName(), result.getName());
        Assert.assertEquals("OksClassName", capp1.getOksClassName(), result.getOksClassName());
        Assert.assertEquals("RestartableDuringRuns", capp1.getRestartableDuringRun(), result.getRestartableDuringRun());
        Assert.assertEquals("IfFailed", capp1.getIfFailed(), result.getIfFailed());
        Assert.assertEquals("IfDies", capp1.getIfDies(), result.getIfDies());
        Assert.assertEquals("StartAt", capp1.getStartAt(), result.getStartAt());
        Assert.assertEquals("StopAt", capp1.getStopAt(), result.getStopAt());
        Assert.assertEquals("BackupHost", capp1.getBackupHosts(), result.getBackupHosts());
        Assert.assertEquals("ControlsTTCPartiotions", capp1.getControlsTTCPartitions(), result.getControlsTTCPartitions());
        Assert.assertEquals("IfError", capp1.getIfError(), result.getIfError());
        Assert.assertEquals("RunningHost", dapp1.getRunningHost(), result.getRunningHost());
        Assert.assertEquals("Status", dapp1.getStatus(), result.getStatus());
        Assert.assertEquals("NotResponding", dapp1.getNotResponding(), result.getNotResponding());
        Assert.assertEquals("Membership", dapp1.getMembership(), result.getMembership());
        Assert.assertEquals("Restarting", dapp1.getRestarting(), result.getRestarting());
        Assert.assertEquals("exitCode", dapp1.getExitCode(), result.getExitCode());
        Assert.assertEquals("exitSignal", dapp1.getExitSignal(), result.getExitSignal());
        Assert.assertEquals("IsControlledBy", capp1.getController(), result.getController());
        Assert.assertEquals("Segment", capp1.getSegment(), result.getSegment());
        Assert.assertEquals("ApplicationState", rcinfo.getState(), result.getState());
        Assert.assertEquals("Transitioning", rcinfo.getIsTransitioning(), result.getIsTransitioning());
        Assert.assertEquals("CurrentTransitionCommand", rcinfo.getCurrentTransitionCommand(), result.getCurrentTransitionCommand());
        Assert.assertEquals("TransitionTime", rcinfo.getTransitionTime(), result.getTransitionTime());
        Assert.assertEquals("InternalError", rcinfo.getInternalError(), result.getInternalError());
    }

    /**
     * Spring Java-Configuration class for the test. Define instantiation, configuration, and initialization logic for objects to be managed
     * by the Spring IoC container.
     * 
     * @author lmagnoni
     */
    @Configuration
    static class TestConfiguration {

        @Bean
        public CEPService cep() {
            return new CEPService("TEST2", Boolean.TRUE, Boolean.TRUE, ExecutorFactory.newThreadPool("Processing", 10, false));
        }

        @Bean
        public EsperDAO dao() {
            return new EsperDAO(this.cep());
        }

        @Bean
        public SynchActionExecutor synchActionExecutor() {
            return new SynchActionExecutor();
        }
        
        @Bean
        public PrintInfo printInfo() {
            return new PrintInfo(this.cep());
        }

        @Bean
        public PrintDebug printDebug() {
            return new PrintDebug(this.cep());
        }

        @Bean
        public CommandSender commandSender() {
            return new StubCommandSender("part_ef");
        }

        /**
         * This is kind of hack to overcome limitation of JavaConfig to load directly a Property object from a property file, equivalent to
         * <util:properties id="configuration" location="classpath:ces.properties" />
         * 
         * @return Properties
         */
        @Bean
        public Properties configuration() {
            // PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
            // propertiesFactoryBean.setLocation(new ClassPathResource("ces.properties"));
            // Properties properties = null;
            // try {
            // propertiesFactoryBean.afterPropertiesSet();
            // properties = propertiesFactoryBean.getObject();
            //
            // } catch (IOException e) {
            // log.warn("Cannot load properties file.");
            // }
            // return properties;

            final Properties p = new Properties();
            p.setProperty("epl.files", "epl/core.epl");
            return p;
        }
    }

}
