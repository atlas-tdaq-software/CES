package chip.test;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import chip.CHIPException.ExecutorPMGException;
import chip.executor.ExecutorPMG;
import pmgClient.ConfigurationException;
import pmgClient.PmgClient;
import pmgClient.Process;


public class StubPMGExecutor extends ExecutorPMG {
    private static final Log log = LogFactory.getLog(StubPMGExecutor.class);

    public StubPMGExecutor() throws ConfigurationException {
        super(new PmgClient());
    }

    @Override
    public void killApplication(final String applicationName, final String partitionName, final String host) {
        StubPMGExecutor.log.info("Killing " + applicationName + " of partition " + partitionName + " on host " + host);
    }
    
    @Override
    public Process startApplication(final String hostName,
                                    final String partitionName,
                                    final String progName,
                                    final String executables,
                                    final Map<String, String> env,
                                    final String workingDir,
                                    final String startArgs,
                                    final String logPath,
                                    final String rmSwobject,
                                    final String inputDevice,
                                    final int initTimeout,
                                    final int autoKillTimeout) throws ExecutorPMGException
    {
        StubPMGExecutor.log.info("Starting " + progName + " of partition " + partitionName + " on host " + hostName);
        return null;
    }
}
