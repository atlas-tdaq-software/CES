//Module name
module chip.resynch;

// Declare other module(s) that this module depends on
uses chip.core;
uses chip.problems;

import chip.event.issue.IdentifiableIssue;
import chip.event.issue.recovery.*;
import chip.event.en.FSM;

@Name('CREATE_SCHEMA_Resynch')
@public
@buseventtype
create schema Resynch as chip.event.issue.recovery.Resynch;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Resynch recovery
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Detect when an ers message of type rc::HardwareSynchronization is thrown
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@Name('INSERT_INTO_Resynch_New')
insert into Resynch
select 
ers.parameters('branchController') as controller,
Resynch$TYPE.RESYNCH as type,
Resynch$STATUS._NEW as status,
case when ers.getQualifiersContains("RESYNCH_FORCE_TRG_RELEASE")
    then true
    else false
end as forceTriggerRelease,
IdentifiableIssue.generateUUID(rootController.name)
from ERSEvent(messageID = 'rc::HardwareSynchronization') as ers unidirectional,
RCApplication(name = root_controller_name).std:lastevent() as rootController
where
rootController.state = FSM.RUNNING;


@Name('SUBSCRIBER_Resynch')
@Subscriber(className='chip.subscriber.recovery.ResynchExecutor')
select * from Resynch(status = Resynch$STATUS._NEW);

@Name('PRINT_Info_GetAllResynch')
@Listeners(classNames={'chip.listener.PrintInfo'})
select * from Resynch;

@Name('PRINT_Ers_GetAllResynch_Ers')
@Listeners(classNames={'chip.listener.PrintErs'})
select * from Resynch;