package chip.annotation;

/**
 * Custom annotation to attach Subscriber to 
 * EPL statements.
 * <p>
 * The className is the fqdn name of the Class
 * to be used as Subscriber.
 * 
 * @author lucamag
 *
 */
public @interface Subscriber {

	String className();
}
