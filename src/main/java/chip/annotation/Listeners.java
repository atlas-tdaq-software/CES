package chip.annotation;

/**
* Custom annotation to attach Listeners to 
* EPL statements.
* <p>
* The classNames is a comma separated list
* of fqdn names of Classes to be used as Subscriber.
* 
* @author lucamag
*
*/
public @interface Listeners {

	String[] classNames();
}
