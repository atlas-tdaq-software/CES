package chip.executor;

import java.util.Date;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;

import ers.BadStreamConfiguration;
import ers.ReceiverNotFound;
import ers.StreamManager;


/**
 * Utility class to wait for a specific ERS message to be received.
 * <p>
 * The ERS subscription is executed in the constructor.
 * Be sure that the {@link #close()} method is executed in order to remove the ERS subscription.
 */
public class ExecutorERS implements Executor, AutoCloseable {
    private final ERSIssueReceiver ersReciever;

    private static class ERSIssueReceiver implements ers.IssueReceiver {
        private final ReentrantLock lock = new ReentrantLock();
        private final Condition cond = this.lock.newCondition();
        private final Predicate<? super ers.Issue> testPredicate;
        private ers.Issue receivedIssue = null;

        public ERSIssueReceiver(final Predicate<? super ers.Issue> condition) {
            this.testPredicate = condition;
        }

        @Override
        public void receive(final ers.Issue issue) {
            this.lock.lock();
            try {
                if(this.testPredicate.test(issue) == true) {
                    this.receivedIssue = issue;
                    this.cond.signalAll();
                }
            }
            finally {
                this.lock.unlock();
            }
        }

        public ers.Issue getIssue() {
            this.lock.lock();
            try {
                return this.receivedIssue;
            }
            finally {
                this.lock.unlock();
            }
        }

        public boolean waitForMessage(final long seconds) throws InterruptedException {
            final Date now = new Date();
            boolean stillWaiting = true;

            this.lock.lock();
            try {
                while((this.receivedIssue == null) && (stillWaiting == true)) {
                    // That returns false if the timeout is elapsed
                    stillWaiting = this.cond.awaitUntil(new Date(now.getTime() + (seconds * 1000)));
                }

                return stillWaiting;
            }
            finally {
                this.lock.unlock();
            }
        }
    }

    /**
     * Constructor.
     * <p>
     * The ERS subscription is executed. This will wait for <em>timeout</em> milliseconds for the subscription to
     * be done. If the timeout elapses, then an exception is thrown.
     * 
     * @param partName The name of the partition the ERS message is coming from
     * @param filterExpression The filter to be used for the ERS subscription
     * @param condition The received message will be accepted if the condition is passed
     * @param timeout Time (in milliseconds) to wait for the subscription to happen
     * @throws BadStreamConfiguration Failed to executed the ERS subscription
     */
    public ExecutorERS(final String partName, final String filterExpression, final Predicate<? super ers.Issue> condition, final Integer timeout)
        throws BadStreamConfiguration
    {
        this.ersReciever = new ERSIssueReceiver(condition);
        StreamManager.instance().add_receiver(this.ersReciever, "mts", partName, filterExpression, timeout);
    }

    
    /**
     * Constructor.
     * <p>
     * The ERS subscription is executed.
     * 
     * @param partName The name of the partition the ERS message is coming from
     * @param filterExpression The filter to be used for the ERS subscription
     * @param condition The received message will be accepted if the condition is passed
     * @throws BadStreamConfiguration Failed to executed the ERS subscription
     */
    public ExecutorERS(final String partName, final String filterExpression, final Predicate<? super ers.Issue> condition)
        throws BadStreamConfiguration
    {
        this.ersReciever = new ERSIssueReceiver(condition);
        StreamManager.instance().add_receiver(this.ersReciever, "mts", partName, filterExpression);
    }
    
    /**
     * Constructor.
     * <p>
     * The ERS subscription is executed.
     * 
     * @param partName The name of the partition the ERS message is coming from
     * @param filterExpression The filter to be used for the ERS subscription
     * @throws BadStreamConfiguration Failed to executed the ERS subscription
     */
    public ExecutorERS(final String partName, final String filterExpression) throws BadStreamConfiguration {
        this(partName, filterExpression, (final ers.Issue e) -> { return true; });
    }

    /**
     * It waits for the ERS message to be received.
     * 
     * @param seconds The time in seconds to wait for the message to come
     * @return <em>false</em> if the timeout elapsed
     * @throws InterruptedException The thread has been interrupted while waiting
     */
    public boolean waitForMessage(final long seconds) throws InterruptedException {
        return this.ersReciever.waitForMessage(seconds);
    }

    /**
     * It returns the received ERS message.
     * <p>
     * It is guaranteed to return a not <em>null</em> value if {@link #waitForMessage(long)} returns <em>true</em>.
     * 
     * @return <em>null</em> if no message has been received.
     */
    public ers.Issue getIssue() {
        return this.ersReciever.getIssue();
    }

    @Override
    public Category getCategory() {
        return Executor.Category.ERS;
    }

    /**
     * It removes the ERS subscription.
     */
    @Override
    public void close() {
        try {
            StreamManager.instance().remove_receiver(this.ersReciever);
        }
        catch(final ReceiverNotFound ex) {
            ex.printStackTrace();
        }
    }
}
