package chip.executor;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import rc.RunParamsNamed;
import TRIGGER.CommandExecutionFailed;
import TRIGGER.TriggerCommander;
import TRIGGER.TriggerCommander.HoldTriggerInfo;
import chip.CEPService;
import chip.CHIPException;
import chip.CHIPException.ExecutorRCException;
import chip.CHIPException.ExecutorTriggerException;
import daq.eformat.DetectorMask;

// TODO: all the commands to the MasterTrigger are synchronized because of an issue
// with concurrent calls to the same CORBA servant. The synchronization should be
// removed once the issue is understood and fixed

public class ExecutorTrigger implements Executor {

    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(ExecutorTrigger.class);

    private final String partName;
    private volatile String controllerName = null;
    
    /**
     * Commander used to send the hold and resume trigger commands to the master trigger
     */
    private volatile TriggerCommander trgCommander = null;
    private final ExecutorRC rcCommander;
    
    public ExecutorTrigger(final String partitionName, final ExecutorRC rcCommander) {        
        this.partName = partitionName;
        this.rcCommander = rcCommander;
    }

    @Override
    public Category getCategory() {
        return Executor.Category.Trigger;
    }
    
    public void init(final String controller) throws ExecutorTriggerException {
        this.controllerName = controller;
        
        try {
            this.trgCommander = new TriggerCommander(this.partName, controller);
        }
        catch(final Exception e) {
            throw new CHIPException.ExecutorTriggerException("Init trigger commander", e);
        }
    }

    public void sendRCUserCommand(final String commandName, 
                                  final String[] args, 
                                  final Boolean sync, 
                                  final int timeout) throws ExecutorRCException
    {
        this.rcCommander.sendUserCommand(this.controllerName, commandName, args, sync, timeout);
    }
    
    // This can return the current ECR and the last valid extended L1 ID
    public synchronized HoldTriggerInfo hold() throws ExecutorTriggerException {
        final HoldTriggerInfo trgInfo = this.hold(new DetectorMask());
        return trgInfo;
    }
    
    // This can return the current ECR and the last valid extended L1 ID
    public synchronized HoldTriggerInfo hold(final DetectorMask dmask) throws ExecutorTriggerException {
        final HoldTriggerInfo value;

        // hold trigger
        try {
            if(dmask.get().isEmpty() == true) {
                value = this.trgCommander.hold("");
                ExecutorTrigger.log.debug("Holding the trigger, ecr=" + String.valueOf(value.getEcrCounter())  
                                          + ", extended L1ID=" + String.valueOf(value.getExendedL1ID()));
            } else {
                value = this.trgCommander.hold(dmask.toString());
                ExecutorTrigger.log.debug("Holding the trigger for " + StringUtils.join(dmask.getSubdetectors().iterator(), ", ") + ", "
                                          + "ecr=" + String.valueOf(value.getEcrCounter()) 
                                          + ", extended L1ID=" + String.valueOf(value.getExendedL1ID()));
            }
        }
        catch(final CommandExecutionFailed e) {
            throw new CHIPException.ExecutorTriggerException("Hold trigger: " + e.reason, e);
        }
        catch(final Exception e) {
            throw new CHIPException.ExecutorTriggerException("Hold trigger", e);
        }

        return value;
    }

    public synchronized void resume() throws ExecutorTriggerException {
        // resume trigger
        try {
            this.trgCommander.resume();
        }
        catch(final CommandExecutionFailed e) {
            throw new CHIPException.ExecutorTriggerException("Resume trigger: " + e.reason, e);
        }
        catch(final Exception e) {
            throw new CHIPException.ExecutorTriggerException("Resume trigger", e);
        }
    }

    public synchronized void resume(final DetectorMask dm) throws ExecutorTriggerException {
        // resume trigger
        try {
            if(dm.get().isEmpty() == false) {
                this.trgCommander.resume(dm.toString());
            } else {
                this.trgCommander.resume();
            }
        }
        catch(final CommandExecutionFailed e) {
            throw new CHIPException.ExecutorTriggerException("Resume trigger: " + e.reason, e);
        }
        catch(final Exception e) {
            throw new CHIPException.ExecutorTriggerException("Resume trigger", e);
        }
    }
    
    public synchronized void setHLTPrescales(final int l1Ps, final int hltPs) throws ExecutorTriggerException {
        // resume trigger
        try {
            this.trgCommander.setPrescales(l1Ps, hltPs);
        }
        catch(final CommandExecutionFailed e) {
            throw new CHIPException.ExecutorTriggerException("Set prescales: " + e.reason, e);
        }
        catch(final Exception e) {
            throw new CHIPException.ExecutorTriggerException("Set prescales", e);
        }
    }

    public synchronized void increaseLumiBlock() throws ExecutorTriggerException {
        try {
            // read run number from IS
            int runNumber = 0;
            final ipc.Partition part = new ipc.Partition(this.partName);
            final RunParamsNamed runParams = new RunParamsNamed(part, "RunParams.RunParams");
            runParams.checkout();
            runNumber = runParams.run_number;
            // increase lumi block
            this.trgCommander.increaseLumiBlock(runNumber);
        }
        catch(final CommandExecutionFailed e) {
            throw new CHIPException.ExecutorTriggerException("Increase luminosity block: " + e.reason, e);
        }
        catch(final Exception e) {
            throw new CHIPException.ExecutorTriggerException("Increase luminosity block", e);
        }
    }
}
