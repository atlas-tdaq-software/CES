package chip.executor;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import pmgClient.Callback;
import pmgClient.Handle;
import pmgClient.PmgClient;
import pmgClient.PmgClientException;
import pmgClient.Process;
import pmgClient.ProcessDescription;
import pmgpub.ProcessState;
import pmgpub.ProcessStatusInfo;
import chip.CEPService;
import chip.CHIPException;
import chip.CHIPException.ExecutorPMGException;


public class ExecutorPMG implements Executor {

    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(ExecutorPMG.class);

    private final PmgClient pmgClient;

    public ExecutorPMG(final PmgClient pmgClient) {
        this.pmgClient = pmgClient;
        this.pmgClient.setSuppressPeriodicLogging(true);
    }

    final class PmgCallback implements Callback {

        final List<? extends Object> succeededEvents;
        final List<? extends Object> failedEvents;

        public PmgCallback(final List<? extends Object> succeededEvents, final List<? extends Object> failedEvents) {
            this.succeededEvents = succeededEvents;
            this.failedEvents = failedEvents;
        }

        @Override
        public void callbackFunction(final pmgClient.Process linkedProcess) {
            final ProcessStatusInfo status = linkedProcess.getProcessStatusInfo();
            final ProcessState state = status.state;

            switch(state.value()) {
                case ProcessState._CREATED:
                    break;
                case ProcessState._EXITED: // ok, clean executable exit, check exit code
                    final int exitCode = status.info.exit_value;
                    if(exitCode == 0) {
                        for(final Object se : this.succeededEvents) {
                            ExecutorPMG.this.cep.injectEvent(se);
                        }
                    } else {
                        for(final Object fe : this.failedEvents) {
                            ExecutorPMG.this.cep.injectEvent(fe);
                        }
                    }
                    break;
                case ProcessState._FAILED: // failed to start executable
                    for(final Object fe : this.failedEvents) {
                        ExecutorPMG.this.cep.injectEvent(fe);
                    }
                    break;
                case ProcessState._LAUNCHING:
                    break;
                case ProcessState._NOTAV:
                    break;
                case ProcessState._REQUESTED:
                    break;
                case ProcessState._RUNNING:
                    break;
                case ProcessState._SIGNALED: // executable exited because of signal (external or internal)
                    for(final Object fe : this.failedEvents) {
                        ExecutorPMG.this.cep.injectEvent(fe);
                    }
                    break;
                case ProcessState._SYNCERROR: // executable not started after init timeout
                    for(final Object fe : this.failedEvents) {
                        ExecutorPMG.this.cep.injectEvent(fe);
                    }
                    break;
                default:
                    break;
            }

            if((state.value() == ProcessState._EXITED) || (state.value() == ProcessState._FAILED)
               || (state.value() == ProcessState._SIGNALED) || (state.value() == ProcessState._SYNCERROR))
            {
                ExecutorPMG.log.debug("-----  Callback from Process :::::  " + linkedProcess.getHandle() + "  -------");
                ExecutorPMG.log.debug("State : " + linkedProcess.getProcessStatusInfo().state.toString());
                if(linkedProcess.getProcessStatusInfo().failure_str_hr.isEmpty() == false) {
                    ExecutorPMG.log.debug("Error : " + linkedProcess.getProcessStatusInfo().failure_str_hr);
                }
                if(state.value() == ProcessState._EXITED) {
                    ExecutorPMG.log.debug("Exit code : " + linkedProcess.getProcessStatusInfo().info.exit_value);
                }
            }
        }
    }
    
    public void executeTestExecAction(final List<? extends Object> executingEvents,
                                      final List<? extends Object> succeededEvents,
                                      final List<? extends Object> failedEvents,
                                      final String hostName,
                                      final String partitionName,
                                      final String progName,
                                      final String executables,
                                      final Map<String, String> env,
                                      final String workingDir,
                                      final String startArgs,
                                      final String logPath,
                                      final String rmSwobject,
                                      final int initTimeout,
                                      final int autoKillTimeout) throws ExecutorPMGException
    {
        this.executeTestExecAction(executingEvents,
                                   succeededEvents, 
                                   failedEvents, 
                                   hostName, 
                                   partitionName, 
                                   progName, 
                                   executables, 
                                   env, 
                                   workingDir, 
                                   startArgs, 
                                   logPath, 
                                   rmSwobject,
                                   "/dev/null",
                                   initTimeout, 
                                   autoKillTimeout);
    }

    public void executeTestExecAction(final List<? extends Object> executingEvents,
                                      final List<? extends Object> succeededEvents,
                                      final List<? extends Object> failedEvents,
                                      final String hostName,
                                      final String partitionName,
                                      final String progName,
                                      final String executables,
                                      final Map<String, String> env,
                                      final String workingDir,
                                      final String startArgs,
                                      final String logPath,
                                      final String rmSwobject,
                                      final String inputDevice,
                                      final int initTimeout,
                                      final int autoKillTimeout) throws ExecutorPMGException
    {

        for(final Object ee : executingEvents) {
            this.cep.injectEventSynch(ee);
        }

        final ProcessDescription desc =
                                      new ProcessDescription.ProcessDescriptionBuilder(this.pmgClient,
                                                                                       hostName,
                                                                                       partitionName,
                                                                                       progName,
                                                                                       executables,
                                                                                       env).wd(workingDir)
                                                                                           .startArgs(startArgs)
                                                                                           .logPath(logPath)
                                                                                           .inputDev(inputDevice)
                                                                                           .rmSwobject(rmSwobject)
                                                                                           .initTimeout(initTimeout)
                                                                                           .autoKillTimeout(autoKillTimeout)
                                                                                           .nullLogs(logPath.trim().isEmpty()).build();

        final PmgCallback arg0 = new PmgCallback(succeededEvents, failedEvents);
        try {
            desc.start(arg0);
            // log.debug(desc.toString());
        }
        catch(final PmgClientException e) {
            ExecutorPMG.log.error(e);
            throw new CHIPException.ExecutorPMGException("Start executable " + progName, e);
        }
    }

    
    public Process startApplication(final String hostName,
                                    final String partitionName,
                                    final String progName,
                                    final String executables,
                                    final Map<String, String> env,
                                    final String workingDir,
                                    final String startArgs,
                                    final String logPath,
                                    final String rmSwobject,
                                    final String inputDevice,
                                    final int initTimeout,
                                    final int autoKillTimeout) throws ExecutorPMGException
    {

        final ProcessDescription desc =
                                      new ProcessDescription.ProcessDescriptionBuilder(this.pmgClient,
                                                                                       hostName,
                                                                                       partitionName,
                                                                                       progName,
                                                                                       executables,
                                                                                       env).wd(workingDir)
                                                                                           .startArgs(startArgs)
                                                                                           .logPath(logPath)
                                                                                           .inputDev(inputDevice)
                                                                                           .rmSwobject(rmSwobject)
                                                                                           .initTimeout(initTimeout)
                                                                                           .autoKillTimeout(autoKillTimeout)
                                                                                           .nullLogs(logPath.trim().isEmpty()).build();

        try {
            return desc.start();
            // log.debug(desc.toString());
        }
        catch(final PmgClientException e) {
            ExecutorPMG.log.error(e);
            throw new CHIPException.ExecutorPMGException("Cannot start application " + progName, e);
        }
    }
    
    public void killApplication(final String applicationName, final String partitionName, final String host) throws ExecutorPMGException {
        try {
            ExecutorPMG.log.debug("Look up handle for application: " + applicationName);
            final Handle handle = this.pmgClient.lookup(applicationName, partitionName, host);
            if(handle == null) {
                throw new CHIPException.ExecutorPMGException("Get handle for application " + applicationName);
            }
            ExecutorPMG.log.debug("Found handle for application: " + applicationName);
            final Process proces = this.pmgClient.getProcess(handle);
            ExecutorPMG.log.debug("Found process for application: " + applicationName);
            proces.kill();
            ExecutorPMG.log.debug("Killed application: " + applicationName);
        }
        catch(final PmgClientException e) {
            ExecutorPMG.log.error(e);
            throw new CHIPException.ExecutorPMGException("Kill application " + applicationName, e);
        }
    }

    @Override
    public Category getCategory() {
        return Category.PMG;
    }

}
