package chip.executor;

import ipc.InvalidPartitionException;
import ipc.ObjectEnumeration;
import ipc.Partition;
import is.AlreadySubscribedException;
import is.InfoEvent;
import is.InfoWatcher;
import is.InvalidCriteriaException;
import is.RepositoryNotFoundException;
import is.SubscriptionNotFoundException;

import java.io.Closeable;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Predicate;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import TRIGGER.autoprescaler;
import chip.CEPService;
import chip.CHIPException;
import chip.CHIPException.ExecutorISException;
import chip.CHIPException.WaitTimeoutException;
import daq.EsperUtils.ISReader;
import daq.EsperUtils.TypedObject;


public class ExecutorIS<T extends is.Info> implements Executor, Closeable {

    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(ExecutorIS.class);
    private final ISInfoMonitor<T> infoMonitor;

    public enum PRESCALE_TYPE {
        STANDBY_L1,
        STANDBY_HLT,
        PHYSICS_L1,
        PHYSICS_HLT
    }

    public ExecutorIS(final Class<T> infoClass, final String partName, final String infoName, final Predicate<T> validator) {
        this.infoMonitor = new ISInfoMonitor<>(infoClass, partName, infoName, validator);
    }

    @Override
    public Category getCategory() {
        return Executor.Category.IS;
    }

    private static class ISInfoMonitor<T extends is.Info> extends InfoWatcher implements Closeable {
        private final Class<T> infoClass;
        private final Predicate<T> validator;
        private final ReentrantLock lock = new ReentrantLock();
        private final Condition condition;
        private boolean infoReceived = false;
        private final String partName;
        private final String infoName;

        private ISInfoMonitor(final Class<T> infoClass, final String partName, final String infoName, final Predicate<T> validator) {
            super(new ipc.Partition(partName), infoName);
            this.infoClass = infoClass;
            this.partName = partName;
            this.infoName = infoName;
            this.validator = validator;
            this.condition = this.lock.newCondition();
        }

        public void start() throws ExecutorISException {
            try {
                this.subscribe();
            }
            catch(final AlreadySubscribedException ex) {
                // Ignore this
                ExecutorIS.log.debug(ex);
            }
            catch(final RepositoryNotFoundException | InvalidCriteriaException ex) {
                throw new ExecutorISException("Cannot subscribe to info \"" + this.infoName + "\" in partition \"" + this.partName + "\": "
                                              + ex,
                                              ex);
            }
        }

        public boolean get(final long time, final TimeUnit unit) throws InterruptedException {
            final Date now = new Date();

            boolean timeout = false;

            this.lock.lock();
            try {
                while((this.infoReceived == false) && (timeout == false)) {
                    timeout = !this.condition.awaitUntil(new Date(now.getTime() + unit.toMillis(time)));
                }
            }
            finally {
                this.lock.unlock();
            }

            return !timeout;
        }

        @Override
        public void close() {
            try {
                this.unsubscribe();
            }
            catch(final SubscriptionNotFoundException ex) {
                // This can be ignored
                ExecutorIS.log.debug(ex);
            }
            catch(final RepositoryNotFoundException ex) {
                ExecutorIS.log.info("Failed to unsubscribe from IS info \"" + this.infoName + "\" in partition \"" + this.partName + "\": "
                                    + ex,
                                    ex);
            }
        }

        protected void processUpdate(final InfoEvent info) {
            try {
                final T i = this.infoClass.newInstance();
                info.getValue(i);
                final boolean success = this.validator.test(i);
                
                this.lock.lock();
                this.infoReceived = success;
                this.condition.signalAll();
            }
            catch(final InstantiationException | IllegalAccessException ex) {
                ExecutorIS.log.error("Failed processing update for IS info \"" + info.getName() + "\"", ex);
            }
            finally {
                if(this.lock.isLocked() == true) {
                    this.lock.unlock();
                }
            }
        }

        @Override
        public void infoSubscribed(final InfoEvent info) {
            this.processUpdate(info);
        }

        @Override
        public void infoCreated(final InfoEvent info) {
            this.processUpdate(info);
        }

        @Override
        public void infoUpdated(final InfoEvent info) {
            this.processUpdate(info);
        }
    }

    public void startWatching() throws ExecutorISException {
        this.infoMonitor.start();
    }

    public void waitForISMessage(final long time, final TimeUnit unit) throws InterruptedException, WaitTimeoutException {
        if(this.infoMonitor.get(time, unit) == false) {
            throw new WaitTimeoutException("Timeout (" + unit.toSeconds(time) + " s) for command execution elapsed");
        }
    }

    @Override
    public void close() {
        this.infoMonitor.close();
    }

    public static TypedObject getPrescales(final String partitionName, final PRESCALE_TYPE type) {
        switch(type) {
            case PHYSICS_L1:
                return ISReader.getInfoByName(partitionName, "RunParams.Physics.L1PsKey", "L1PrescaleKey");
            case PHYSICS_HLT:
                return ISReader.getInfoByName(partitionName, "RunParams.Physics.HltPsKey", "HltPrescaleKey");
            case STANDBY_L1:
                return ISReader.getInfoByName(partitionName, "RunParams.Standby.L1PsKey", "L1PrescaleKey");
            case STANDBY_HLT:
                return ISReader.getInfoByName(partitionName, "RunParams.Standby.HltPsKey", "HltPrescaleKey");
            default:
                return null;
        }
    }

    public static String getAutoPrescalerName(final String partitionName) throws ExecutorISException {
        String autoPrescalerName = null;
        try {

            final ObjectEnumeration<autoprescaler> enumeration = new ObjectEnumeration<autoprescaler>(new Partition(partitionName),
                                                                                                      autoprescaler.class);

            final int cap = enumeration.size();
            if(cap > 1) {
                ExecutorIS.log.error("Found more than one autoprescaler, using the first in list: " + enumeration.firstElement().name);
            }
            if(cap > 0) {
                autoPrescalerName = enumeration.firstElement().name;
            } else {
                ExecutorIS.log.error("No autoprescaler found in partition.");
            }

            return autoPrescalerName;
        }
        catch(final InvalidPartitionException e) {
            throw new CHIPException.ExecutorISException("Get AutoPrescaler", e);
        }
    }
}
