package chip.executor;

import iguiCommander.ClientPOA;
import iguiCommander.ClientRequest;
import iguiCommander.UserAnswer;
import ipc.InvalidObjectException;
import ipc.InvalidPartitionException;

import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.omg.CORBA.SystemException;
import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.CHIPException.ExecutorIGUIException;
import chip.msg.Common;


public class ExecutorIGUI implements Executor {

    @Autowired
    CEPService cep;

    private final String partitionName;

    public ExecutorIGUI(final String partitionName) {
        this.partitionName = partitionName;
    }

    @Override
    public Category getCategory() {
        return Executor.Category.IGUI;
    }

    public UserAnswer askYesOrNo(final String question, final long timeoutMillis) throws ExecutorIGUIException {
        UserAnswer iguiAnswer = UserAnswer.CANCELLED;
        final AtomicInteger answer = new AtomicInteger(UserAnswer._CANCELLED);

        try {
            // Create the partition object
            final ipc.Partition p = new ipc.Partition(this.partitionName);

            // Look for the Igui in IPC
            final iguiCommander.Server s = p.lookup(iguiCommander.Server.class, iguiCommander.servantName.value);

            // Create an object implementing the client interface
            final AtomicBoolean callbackReceived = new AtomicBoolean(false);

            final Lock lk = new ReentrantLock();
            final Condition cond = lk.newCondition();
            
            final ClientPOA cl = new ClientPOA() {
                @Override
                public void done(final UserAnswer ua) {                    
                    answer.set(ua.value());

                    lk.lock();
                    try {
                        callbackReceived.set(true);
                        cond.signalAll();
                    }
                    finally {
                        lk.unlock();
                    }                    
                }
            };

            // Create a client request
            final ClientRequest cr = new ClientRequest("CHIP", question, cl._this(ipc.Core.getORB()));

            // Send the request to the Igui
            s.askUser(cr);

            // Wait for the answer (needed to not let the test application exit before receiving the call-back)
            lk.lock();
            try {
                final Date now = new Date();
                
                while(callbackReceived.get() == false) {
                    if(timeoutMillis != 0) {
                        final boolean withinTimeout = cond.awaitUntil(new Date(now.getTime() + timeoutMillis));
                        if(withinTimeout == false) {
                            ers.Logger.warning(new Common("No answer within " + timeoutMillis + " ms from the operator to question: \"" + 
                                                           question + "\". Sending back a CANCELLED result."));
                            break;
                        }
                    } else {
                        cond.await();
                    }
                }
            }
            finally {
                lk.unlock();
            }
            
            iguiAnswer = UserAnswer.from_int(answer.get());
        }
        catch(final InterruptedException | InvalidPartitionException | InvalidObjectException | SystemException e) {
            throw new ExecutorIGUIException("AskUser", e);
        }        

        return iguiAnswer;
    }

    public void showError(final String message) throws ExecutorIGUIException {
        try {

            // Create the partition object
            final ipc.Partition p = new ipc.Partition(this.partitionName);

            // Look for the Igui in IPC
            final iguiCommander.Server s = p.lookup(iguiCommander.Server.class, iguiCommander.servantName.value);

            s.informUser(message, iguiCommander.MessageSeverity.ERROR);
        }
        catch(final InvalidPartitionException | InvalidObjectException | SystemException e) {
            throw new ExecutorIGUIException("InformUser", e);
        }        
    }

    public void stopAlertCommand() throws ExecutorIGUIException {
        try {
            // Create the partition object
            final ipc.Partition p = new ipc.Partition(this.partitionName);

            // Look for the Igui in IPC
            final iguiCommander.Server s = p.lookup(iguiCommander.Server.class, iguiCommander.servantName.value);
            s.executeStopAlertCommand();

        }
        catch(final InvalidPartitionException | InvalidObjectException | SystemException e) {
            throw new ExecutorIGUIException("ExecuteStopAlertCommand", e);
        }
    }
}
