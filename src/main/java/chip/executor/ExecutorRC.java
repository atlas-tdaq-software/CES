package chip.executor;

import java.util.Arrays;
import java.util.LinkedList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import TRIGGER.TriggerCommander.HoldTriggerInfo;
import chip.CEPService;
import chip.CHIPException;
import chip.CHIPException.ExecutorRCException;
import chip.CHIPException.WaitTimeoutException;
import daq.rc.Command;
import daq.rc.Command.FSMCommands;
import daq.rc.Command.ResynchCmd;
import daq.rc.Command.StopAppsCmd;
import daq.rc.Command.TransitionCmd;
import daq.rc.Command.UserBroadCastCmd;
import daq.rc.Command.UserCmd;
import daq.rc.CommandSender;
import daq.rc.ErrorElement;
import daq.rc.RCException;


public class ExecutorRC implements Executor {
    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(ExecutorRC.class);

    private final String partitionName;
    private final CommandSender commandSender;

    public ExecutorRC(final String partitionName, final CommandSender commandSender) {
        this.partitionName = partitionName;
        this.commandSender = commandSender;
    }

    public String getPartitionName() {
        return this.partitionName;
    }
    
    @Override
    public Category getCategory() {
        return Executor.Category.RC;
    }

    public void restartApps(final String controller, final String[] applications) throws ExecutorRCException {
        ExecutorRC.log.debug("Restart: controller: " + controller + ", apps: " + Arrays.toString(applications));

        try {
            this.commandSender.restartApplications(controller, applications);
        }
        catch(final RCException e) {
            e.printStackTrace();
            throw new CHIPException.ExecutorRCException("Restart app for controller " + controller, e);
        }
    }

    public void stopApps(final String controller, final String[] applications) throws ExecutorRCException {
        this.stopApps(controller, applications, false, 0);
    }

    public void stopApps(final String controller,
                         final String[] applications,
                         final boolean sendSynch,
                         final int timeoutInSeconds) throws ExecutorRCException
    {
        ExecutorRC.log.debug("Stop: controller: " + controller + ", apps: " + Arrays.toString(applications));

        try {
            final StopAppsCmd cmd = new StopAppsCmd(applications);
            this.sendSynchCommand(controller, cmd, Boolean.valueOf(sendSynch), timeoutInSeconds);
        }
        catch(final RCException e) {
            throw new CHIPException.ExecutorRCException("Stop app for controller " + controller, e);
        }
        catch(final WaitTimeoutException e) {
            throw new CHIPException.ExecutorRCException("Stop app for controller " + controller, e);
        }
        catch(final InterruptedException e) {
            throw new CHIPException.ExecutorRCException("Stop app for controller " + controller, e);
        }
    }
    
    public void startApps(final String controller, final String[] applications) throws ExecutorRCException {
        ExecutorRC.log.debug("Start: controller: " + controller + ", apps: " + Arrays.toString(applications));

        try {
            this.commandSender.startApplications(controller, applications);
        }
        catch(final RCException e) {
            throw new CHIPException.ExecutorRCException("Start app for controller " + controller, e);
        }
    }

    public void putAppsOutFsm(final String controller, final String[] applications) throws ExecutorRCException {
        ExecutorRC.log.debug("PutOutFsm: controller " + controller + ", apps: " + Arrays.toString(applications));

        try {
            this.commandSender.changeMembership(controller, false, applications);
        }
        catch(final RCException e) {
            throw new CHIPException.ExecutorRCException("Disable for controller " + controller, e);
        }
    }

    public void putAppsInFsm(final String controller, final String[] applications) throws ExecutorRCException {
        ExecutorRC.log.debug("PutInFsm: controller " + controller + ", apps: " + Arrays.toString(applications));

        try {
            this.commandSender.changeMembership(controller, true, applications);
        }
        catch(final RCException e) {
            throw new CHIPException.ExecutorRCException("Enable for controller " + controller, e);
        }
    }

    public void setControllerErrors(final String controller, final LinkedList<ErrorElement> errorForController) throws ExecutorRCException {
        ExecutorRC.log.debug("SetError: controller " + controller + ", numberErrors: " + errorForController.size());

        try {
            this.commandSender.setError(controller, errorForController);
        }
        catch(final RCException e) {
            throw new CHIPException.ExecutorRCException("SetError for controller " + controller, e);
        }
    }

    public void sendUserCommand(final String application, final String command, final String[] commandArgs) throws ExecutorRCException {
        this.sendUserCommand(application, command, commandArgs, Boolean.FALSE, 0);
    }

    public void sendUserCommand(final String application,
                                final String command,
                                final String[] commandArgs,
                                final Boolean sendSynch,
                                final int timeout) throws ExecutorRCException
    {
        ExecutorRC.log.debug("User command to app " + application + ": command=" + command + ", args=" + Arrays.toString(commandArgs));

        try {
            final UserCmd userCommand = new daq.rc.Command.UserCmd(command, commandArgs);
            this.sendSynchCommand(application, userCommand, sendSynch, timeout);
        }
        catch(final RCException e) {
            throw new CHIPException.ExecutorRCException("Send user command to " + application, e);
        }
        catch(final WaitTimeoutException e) {
            throw new CHIPException.ExecutorRCException("Send user command to " + application, e);
        }
        catch(final InterruptedException e) {
            throw new CHIPException.ExecutorRCException("Send user command to " + application, e);
        }
    }

    public void sendUserBroadcast(final String application,
                                  final String command,
                                  final String[] commandArgs,
                                  final Boolean sendSynch,
                                  final int timeout) throws ExecutorRCException
    {
        ExecutorRC.log.debug("User broadcast to app " + application + ": command=" + command + ", args=" + Arrays.toString(commandArgs));

        try {

            final UserCmd userCommand = new daq.rc.Command.UserCmd(command, commandArgs);
            final UserBroadCastCmd userBroadCast = new daq.rc.Command.UserBroadCastCmd(userCommand);
            this.sendSynchTransCommand(application, userBroadCast, sendSynch, timeout);

        }
        catch(final RCException e) {
            throw new CHIPException.ExecutorRCException("Send user broadcast to " + application, e);
        }
        catch(final WaitTimeoutException e) {
            throw new CHIPException.ExecutorRCException("Send user broadcast to " + application, e);
        }
        catch(final InterruptedException e) {
            throw new CHIPException.ExecutorRCException("Send user broadcast to " + application, e);
        }
    }

    public void sendResynch(final String application, final HoldTriggerInfo trgInfo, final String[] commandArgs, final boolean sendSynch, final int timeout)
        throws ExecutorRCException
    {
        final long ecr = trgInfo.getEcrCounter();
        final long extendedL1ID = trgInfo.getExendedL1ID();
        
        ExecutorRC.log.debug("Send resynch command to app " + application + 
                             ": ecr=" + String.valueOf(ecr) + 
                             " extended L1ID=" + String.valueOf(extendedL1ID) +
                             " args=" + Arrays.toString(commandArgs));

        try {

            final ResynchCmd resynchCmd = new daq.rc.Command.ResynchCmd(ecr, extendedL1ID, commandArgs);
            this.sendSynchTransCommand(application, resynchCmd, Boolean.valueOf(sendSynch), timeout);

        }
        catch(final RCException e) {
            throw new CHIPException.ExecutorRCException("Send resynch to " + application, e);
        }
        catch(final WaitTimeoutException e) {
            throw new CHIPException.ExecutorRCException("Send resynch to " + application, e);
        }
        catch(final InterruptedException e) {
            throw new CHIPException.ExecutorRCException("Send resynch to " + application, e);
        }
    }

    public void sendChangeStatus(final String application, final boolean enable, final String[] commandArgs) throws ExecutorRCException {
        ExecutorRC.log.debug("Send changeStatus command to app " + application + ": enable=" + String.valueOf(enable) + " args="
                             + Arrays.toString(commandArgs));

        try {
            this.commandSender.changeStatus(application, enable, commandArgs);
        }
        catch(final RCException e) {
            throw new CHIPException.ExecutorRCException("Send changeStatus to " + application, e);
        }
    }

    public void sendPublish(final String application) throws ExecutorRCException {
        ExecutorRC.log.debug("Send publish command to app " + application);

        try {
            this.commandSender.publish(application);
        }
        catch(final Exception e) {
            throw new CHIPException.ExecutorRCException("Send publish to " + application, e);
        }
    }

    public void makeTransition(final String application, final FSMCommands fsmComm) throws ExecutorRCException {
        // TODO This method could be executed synchronously, if written like sendResynch()
        // or like sendUserBroadCast()

        ExecutorRC.log.debug("Send make transition to app " + application);

        try {
            this.commandSender.makeTransition(application, fsmComm);
        }
        catch(final RCException e) {
            throw new CHIPException.ExecutorRCException("Send make transition to " + application, e);
        }
        catch(final IllegalArgumentException e) {
            throw new CHIPException.ExecutorRCException("Send make transition to " + application, e);
        }
    }

    public void testApplication(final String controller, final String[] applications) throws ExecutorRCException {
        final StringBuilder apps = new StringBuilder();
        for(int i = 0; i < applications.length; i = i + 1) {
            if(i == 0) {
                apps.append(applications[i]);
            } else {
                apps.append("," + applications[i]);
            }
        }

        ExecutorRC.log.debug("Test: controller " + controller + ", apps: " + apps.toString());

        try {
            this.commandSender.testApp(controller, applications);
        }
        catch(final RCException e) {
            throw new CHIPException.ExecutorRCException("Test application: " + apps.toString(), e);
        }
    }

    public void pingApplication(final String application) throws ExecutorRCException {
        try {
            this.commandSender.ping(application);
        }
        catch(final RCException e) {
            throw new CHIPException.ExecutorRCException("Failed to ping application: " + application, e);
        }
    }
    
    public void notifyResInfoProvider(final String[] disabled, final String[] enabled) throws ExecutorRCException {
        // Notify the ResourcesInfoProvider
        final String command = "RESET";
        final long timeInNano = System.currentTimeMillis() * 1000000;
        final String commandArgs = ResourcesInfo.Utils.encode_user_cmd(timeInNano, disabled, enabled);

        ExecutorRC.log.debug("ResInfoProvider: encoded command is " + command + " " + commandArgs);

        try {
            this.sendUserCommand("ResInfoProvider", command, new String[] {commandArgs});
        }
        catch(final ExecutorRCException e) {
            throw new CHIPException.ExecutorRCException("Notify ResInfoProvider", e);
        }
    }

    private void sendSynchTransCommand(final String application,
                                       final TransitionCmd command,
                                       final Boolean sendSynch,
                                       final int timeoutInSeconds)
        throws RCException,
            WaitTimeoutException,
            InterruptedException
    {
        if(!sendSynch.booleanValue()) {
            this.commandSender.executeCommand(application, command);
        } else {
            final boolean withinTimeout = this.commandSender.executeCommand(application, command, timeoutInSeconds * 1000);
            if(withinTimeout == false) {
                throw new CHIPException.WaitTimeoutException("Timeout (" + timeoutInSeconds + " s) for command execution elapsed");
            }
        }
    }

    private void sendSynchCommand(final String application, final Command command, final Boolean sendSynch, final int timeoutInSeconds)
        throws RCException,
            WaitTimeoutException,
            InterruptedException
    {
        if(!sendSynch.booleanValue()) {
            this.commandSender.executeCommand(application, command);
        } else {
            final boolean withinTimeout = this.commandSender.executeCommand(application, command, timeoutInSeconds * 1000);
            if(withinTimeout == false) {
                throw new CHIPException.WaitTimeoutException("Timeout (" + timeoutInSeconds + " s) for command execution elapsed");
            }
        }
    }
}
