package chip.executor;


public interface Executor {
	
	public static enum Category {RC, PMG, IGUI, IS, ERS, Trigger, Test}
	
	public Category getCategory();
	
}