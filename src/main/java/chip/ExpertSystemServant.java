package chip;

import ipc.Partition;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import chip.dao.EsperDAO;
import chip.event.dynamic.ApplicationInfo;
import chip.event.dynamic.CompTestResultInfo;
import chip.event.dynamic.RCApplicationInfo;
import chip.event.dynamic.TransitionInfo;
import chip.event.en.FSM;
import chip.event.en.MEMBERSHIP;
import chip.event.en.RCCOMMAND;
import chip.event.en.STATUS;
import chip.event.en.TRCOMMAND;
import chip.event.issue.test.TestFollowUp;
import chip.event.k8s.Container;
import chip.event.k8s.Node;
import chip.event.k8s.Pod;
import chip.event.k8s.Workload;
import chip.msg.Common;
import chip.msg.Internal;
import chip.msg.TestFailed;
import chip.utils.ExecutorFactory;
import es.rc.Action;
import es.rc.ApplicationProcessPair;
import es.rc.ApplicationStatePair;
import es.rc.ComponentTestStatus;
import es.ExpertSystem;
import es.ExpertSystemOperations;
import es.rc.ProcessInformation;
import es.rc.StateInformation;
import es.rc.TestResult;
import es.rc.TestStatusInformation;
import es.k8s.node.NodeInfo;
import es.k8s.workload.WorkloadInfo;
import es.k8s.pod.PodInfo;
import es.k8s.container.ContainerInfo;


/**
 * Class implementing the ExpertSystem Corba interface, to receive update notification and problems
 * 
 * @author ganders, lmagnoni
 */
public class ExpertSystemServant extends ipc.NamedObject<ExpertSystem> implements ExpertSystemOperations {

    private final CEPService cep;
    private final CHIP chip;
    private final EsperDAO dao;
    private final ConcurrentHashMap<String, ReentrantLock> appLocks;
    private final ConcurrentHashMap<String, Long> appLastTime;
    private final ConcurrentHashMap<String, Long> rcAppLastTime;
    
    // See ATDAQCCCES-154 for the reason why this is needed
    private final ThreadPoolExecutor reloadExecutor = ExecutorFactory.newThreadPool("EsperReloader", 1, true);
    
    private static final Log log = LogFactory.getLog(ExpertSystemServant.class);
    
    public ExpertSystemServant(final CHIP chip,
                               final CEPService cep,
                               final Partition partitionName,
                               final String servantName,
                               final int appNum)
    {
        super(partitionName, servantName);
        this.cep = cep;
        this.chip = chip;
        this.dao = new EsperDAO(cep);
        this.appLocks = new ConcurrentHashMap<String, ReentrantLock>(appNum, 0.75f, 25);
        this.appLastTime = new ConcurrentHashMap<String, Long>(appNum, 0.75f, 25);
        this.rcAppLastTime = new ConcurrentHashMap<String, Long>(appNum, 0.75f, 25);
    }

    /**
     * Update non-state aware application information
     */
    @Override
    public void applicationProcessUpdate(final String applicationName, final ProcessInformation info) {
        try {
            this.cep.getProcessingExecutor().execute(() -> {
                final ReentrantLock lk = ExpertSystemServant.this.appLocks.computeIfAbsent(applicationName, app -> {
                    return new ReentrantLock();
                });
                
                try {
                    lk.lock();

                    final Long lastUpdate = ExpertSystemServant.this.appLastTime.get(applicationName);
                    if((lastUpdate == null) || (info.infoTime >= lastUpdate.longValue())) {
                        final ApplicationInfo applicationUpdate = new ApplicationInfo(applicationName,
                                                                                      Long.valueOf(info.infoTime), // info creation time,from controller in nanosecond
                                                                                      Long.valueOf(System.currentTimeMillis()), // event creation time - millisec
                                                                                      ExpertSystemServant.this.convertMembership(info.membership),
                                                                                      info.runningHost,
                                                                                      STATUS.valueOf(info.status),
                                                                                      Boolean.valueOf(info.isNotResponding),
                                                                                      Boolean.valueOf(info.isRestarting),
                                                                                      Long.valueOf(info.exitCode),
                                                                                      Long.valueOf(info.exitSignal),
                                                                                      FSM.valueOf(info.parentInfo.fsmState),
                                                                                      Boolean.valueOf(info.parentInfo.isTransitioning),
                                                                                      info.parentInfo.currentTransition.command.isEmpty() ? TRCOMMAND._EMPTY
                                                                                                                                          : TRCOMMAND.valueOf(info.parentInfo.currentTransition.command),
                                                                                      info.parentInfo.lastTransition.command.isEmpty() ? TRCOMMAND._EMPTY
                                                                                                                                       : TRCOMMAND.valueOf(info.parentInfo.lastTransition.command),
                                                                                      Boolean.valueOf(info.parentInfo.isExiting),
                                                                                      Boolean.valueOf(info.parentInfo.isInitializing));
                        ExpertSystemServant.this.cep.injectEventSynch(applicationUpdate);
                        ExpertSystemServant.this.appLastTime.put(applicationName, Long.valueOf(info.infoTime));
                    } else {
                        // log.info("Out of order information for application " + applicationName);
                    }
                }
                catch(final IllegalArgumentException e) {
                    ExpertSystemServant.log.error("Unable to map enum type for " + e);
                }
                catch(final NullPointerException e) {
                    ExpertSystemServant.log.error("Null updated received ");
                    e.printStackTrace();
                }
                catch(final Exception e) {
                    ExpertSystemServant.log.error("Very bad! Unexpected exception thrown " + e);
                    e.printStackTrace();
                }
                finally {
                    lk.unlock();
                }
            });
        }
        catch(final RejectedExecutionException ex) {
            ExpertSystemServant.log.warn("Cannot submit event to the engine, probably database is being reloaded or the application is exiting");
        }
    }

    /**
     * Update state-aware application information
     */
    @Override
    public void applicationStateUpdate(final String applicationName, final StateInformation info) {
        try {
            this.cep.getProcessingExecutor().execute(() -> {
                {
                    final ReentrantLock lk = ExpertSystemServant.this.appLocks.computeIfAbsent(applicationName, app -> {
                        return new ReentrantLock();
                    });
                    
                    try {
                        lk.lock();

                        final Long lastUpdate = ExpertSystemServant.this.rcAppLastTime.get(applicationName);
                        if((lastUpdate == null) || (info.infoTime >= lastUpdate.longValue())) {
                            final RCApplicationInfo rcApplicationUpdate =
                                                                        new RCApplicationInfo(applicationName,
                                                                                              Long.valueOf(info.infoTime), // info creation time, from controller in nanosec
                                                                                              Long.valueOf(System.currentTimeMillis()), // event creation time, millis
                                                                                              FSM.valueOf(info.fsmState),
                                                                                              info.currentTransition.command.isEmpty() ? Boolean.FALSE
                                                                                                                                       : Boolean.TRUE,
                                                                                              info.currentTransition.command.isEmpty() ? TRCOMMAND._EMPTY
                                                                                                                                       : TRCOMMAND.valueOf(info.currentTransition.command),
                                                                                              info.currentTransition.uid,
                                                                                              info.lastTransition.command.isEmpty() ? TRCOMMAND._EMPTY
                                                                                                                                    : TRCOMMAND.valueOf(info.lastTransition.command),
                                                                                              Long.valueOf(info.lastTransitionTime),
                                                                                              Boolean.valueOf(info.isInternalError),
                                                                                              info.lastCommand.name.isEmpty() ? RCCOMMAND._EMPTY
                                                                                                                              : RCCOMMAND.valueOf(info.lastCommand.name),
                                                                                              info.lastCommand.arguments,
                                                                                              info.lastCommand.uid,
                                                                                              Boolean.valueOf(info.isBusy),
                                                                                              FSM.valueOf(info.parentInfo.fsmState),
                                                                                              Boolean.valueOf(info.parentInfo.isTransitioning),
                                                                                              info.parentInfo.currentTransition.command.isEmpty() ? TRCOMMAND._EMPTY
                                                                                                                                                  : TRCOMMAND.valueOf(info.parentInfo.currentTransition.command),
                                                                                              info.parentInfo.lastTransition.command.isEmpty() ? TRCOMMAND._EMPTY
                                                                                                                                               : TRCOMMAND.valueOf(info.parentInfo.lastTransition.command),
                                                                                              Boolean.valueOf(info.parentInfo.isExiting),
                                                                                              Boolean.valueOf(info.parentInfo.isInitializing));

                            ExpertSystemServant.this.cep.injectEventSynch(rcApplicationUpdate);
                            ExpertSystemServant.this.rcAppLastTime.put(applicationName, Long.valueOf(info.infoTime));
                        } else {
                            // log.info("Out of order information for RC application " + applicationName);
                        }
                    }
                    catch(final IllegalArgumentException e) {
                        ExpertSystemServant.log.error("Unable to map enum type for " + e);
                    }
                    catch(final NullPointerException e) {
                        ExpertSystemServant.log.error("Null updated received ");
                        e.printStackTrace();
                    }
                    catch(final Exception e) {
                        ExpertSystemServant.log.error("Very bad! Unexpected exception thrown " + e);
                        e.printStackTrace();
                    }
                    finally {
                        lk.unlock();
                    }
                }
            });
        }
        catch(final RejectedExecutionException ex) {
            ExpertSystemServant.log.warn("Cannot submit event to the engine, probably database is being reloaded or the application is exiting");
        }
    }

    @Override
    public void applicationTestUpdate(final String applicationName, final TestStatusInformation testInfo) {
        try {
            this.cep.getProcessingExecutor().execute(() -> {
                {
                    final TestResult globalTestResult = testInfo.globalResult;
                    final ComponentTestStatus[] compTestArr = testInfo.components;

                    if(globalTestResult.value() != TestResult._PASSED) {
                        final Map<String, String> comps = new HashMap<>();

                        final StringBuilder ersMsg = new StringBuilder();
                        ersMsg.append("Test failure for application: ");
                        ersMsg.append(applicationName);
                        ersMsg.append(" , global result: ");
                        ersMsg.append(globalTestResult.toString());
                        ersMsg.append("\n");

                        for(final ComponentTestStatus compTest : compTestArr) {
                            final String compName = compTest.componentName;
                            final TestResult compTestResult = compTest.result;

                            if((compTestResult.value() != TestResult._PASSED) && (compTestResult.value() != TestResult._UNSUPPORTED)) {
                                comps.put(compName, compTestResult.toString());

                                ersMsg.append("Component: ");
                                ersMsg.append(compName);
                                ersMsg.append(" , result: ");
                                ersMsg.append(compTestResult.toString());
                                ersMsg.append("\n");

                                for(final Action action : compTest.actions) {
                                    ersMsg.append(" Follow-up action: ");
                                    ersMsg.append(action.name);
                                    ersMsg.append("   parameters: ");
                                    ersMsg.append(action.parameters);
                                    ersMsg.append("\n");
                                    ersMsg.append("   diagnosis: ");
                                    ersMsg.append(action.diagnosis);
                                    ersMsg.append("\n");
                                }
                            }
                        }

                        if(globalTestResult.value() == TestResult._FAILED) {
                            if(ExpertSystemServant.this.dao.getAllErrorsOfApplication(applicationName).isEmpty() == true) {
                                ers.Logger.error(new TestFailed(ersMsg.toString(), applicationName, comps));
                            } else {
                                ExpertSystemServant.log.error(ersMsg);
                            }
                        }
                    }

                    final long timestamp = System.currentTimeMillis();

                    for(final ComponentTestStatus compTest : compTestArr) {
                        int actionCounter = 0;

                        final String compName = compTest.componentName;
                        final TestResult compTestResult = compTest.result;
                        final Action[] actionList = compTest.actions;

                        if(actionList.length == 0) {
                            final CompTestResultInfo testResultUpdate = new CompTestResultInfo(applicationName,
                                                                                               Long.valueOf(timestamp),
                                                                                               compTestResult,
                                                                                               CompTestResultInfo.ACTION_TYPE.NONE,
                                                                                               "",
                                                                                               "",
                                                                                               0);

                            ExpertSystemServant.this.cep.injectEventSynch(new TestFollowUp(applicationName,
                                                                                           "empty",
                                                                                           "empty",
                                                                                           globalTestResult,
                                                                                           compName,
                                                                                           testResultUpdate,
                                                                                           TestFollowUp.STATUS._NEW,
                                                                                           TestFollowUp.ACTION.NONE,
                                                                                           actionCounter));
                            actionCounter++;
                        } else {
                            for(final Action act : actionList) {
                                CompTestResultInfo testResultInfo = null;
                                try {
                                    testResultInfo = new CompTestResultInfo(applicationName,
                                                                            Long.valueOf(timestamp),
                                                                            compTestResult,
                                                                            CompTestResultInfo.ACTION_TYPE.convert(act.name),
                                                                            act.parameters,
                                                                            act.diagnosis,
                                                                            act.timeout);
                                }
                                catch(final IllegalArgumentException ex) {
                                    testResultInfo = new CompTestResultInfo(applicationName,
                                                                            Long.valueOf(timestamp),
                                                                            compTestResult,
                                                                            CompTestResultInfo.ACTION_TYPE.NONE,
                                                                            "",
                                                                            "",
                                                                            0);

                                    ers.Logger.warning(new Common("Test action \"" + act.name + "\" is not properly formed: " + ex, ex));
                                }

                                ExpertSystemServant.this.cep.injectEventSynch(new TestFollowUp(applicationName,
                                                                                               "empty",
                                                                                               "empty",
                                                                                               globalTestResult,
                                                                                               compName,
                                                                                               testResultInfo,
                                                                                               TestFollowUp.STATUS._NEW,
                                                                                               TestFollowUp.ACTION.NONE,
                                                                                               actionCounter));
                                actionCounter++;
                            }
                        }
                    }
                }
            });
        }
        catch(final RejectedExecutionException ex) {
            ExpertSystemServant.log.warn("Cannot submit event to the engine, probably database is being reloaded or the application is exiting");
        }
    }

    @Override
    public void dbReloadInit() {        
        final FutureTask<Void> t = new FutureTask<>(new Callable<Void>() {
            @Override
            public Void call() {
                // Stop statement evaluation and clear Esper working memory
                ExpertSystemServant.this.chip.stopInjectors();
                ExpertSystemServant.this.cep.stop();

                return null;
            }
        });
        
        this.reloadExecutor.submit(t);
        
        try {
            t.get();
        }
        catch(final InterruptedException ex) {
            // Restore the interrupted state
            Thread.currentThread().interrupt();
            ers.Logger.warning(new Internal("The reload procedure was interrupted"));
        }
        catch(final ExecutionException ex) {
            ers.Logger.error(new Internal("Error initiating the reload procedure: " + ex, ex));
        }

    }

    @Override
    public void dbReloadDone() {       
        final FutureTask<Void> t = new FutureTask<>(new Callable<Void>() {
            @Override
            public Void call() {
                // Initialize Esper statement,
                // Read new configuration from Config
                ExpertSystemServant.this.chip.reconfigure();
                ExpertSystemServant.this.chip.startInjectors();
                
                return null;
            }
        });
        
        this.reloadExecutor.submit(t);
        
        try {
            t.get();
        }
        catch(final InterruptedException ex) {
            // Restore the interrupted state
            Thread.currentThread().interrupt();
            ers.Logger.warning(new Internal("The reload procedure was interrupted"));
        }
        catch(final ExecutionException ex) {
            ers.Logger.error(new Internal("Error finalizing the reload procedure: " + ex, ex));
        }
    }

    /**
     * Convert Membership from RunController way to ES
     * 
     * @param m
     * @return
     */
    private MEMBERSHIP convertMembership(final boolean m) {
        return (m == true) ? MEMBERSHIP._IN : MEMBERSHIP._OUT;
    }

    @Override
    public void reset() {        
        final FutureTask<Void> t = new FutureTask<>(new Callable<Void>() {
            @Override
            public Void call() {
                ExpertSystemServant.this.chip.stopInjectors();
                ExpertSystemServant.this.cep.stop();
                ExpertSystemServant.this.chip.reconfigure();
                ExpertSystemServant.this.chip.startInjectors();

                return null;
            }
        });
        
        this.reloadExecutor.submit(t);
        
        try {
            t.get();
        }
        catch(final InterruptedException ex) {
            // Restore the interrupted state
            Thread.currentThread().interrupt();
            ers.Logger.warning(new Internal("The engine reset procedure was interrupted"));
        }
        catch(final ExecutionException ex) {
            ers.Logger.error(new Internal("Error completing the engine reset procedure: " + ex, ex));
        }

    }

    @Override
    public void applicationsProcessUpdate(final ApplicationProcessPair[] appPairs) {
        for(final ApplicationProcessPair pair : appPairs) {
            this.applicationProcessUpdate(pair.applicationName, pair.info);
        }
    }

    @Override
    public void applicationsStateUpdate(final ApplicationStatePair[] appPairs) {
        for(final ApplicationStatePair pair : appPairs) {
            this.applicationStateUpdate(pair.applicationName, pair.info);
        }
    }
    
    @Override
    public void transitionDone(final String transition, final String state) {
        // This must be synch so that the method returns when all the actions in the
        // ESPER engine have been properly completed
        // Send anyway the event to ESPER using the proper processing thread pool
        // (used internally in injectEvent) so that the order events are processed 
        // is kept
        try {
            this.cep.injectEvent(new TransitionInfo(transition, state)).get();
        }
        catch(final ExecutionException ex) {
            // Restore the interrupted state
            Thread.currentThread().interrupt();
            ers.Logger.warning(new Internal("The \"transition done\" procedure was interrupted"));
        }
        catch(final InterruptedException ex) {
            ers.Logger.error(new Internal("Error completing the \"transition done\" procedure: " + ex, ex));
        }
    }

    @Override
    public void nodeUpdate(final NodeInfo nodeInfo) {
        final Node infoUpdate = new Node(nodeInfo);
        
        try {
            this.cep.getProcessingExecutor().execute(() -> {
                ExpertSystemServant.this.cep.injectEventSynch(infoUpdate);
            });
        }
        catch(final RejectedExecutionException ex) {
            ExpertSystemServant.log.warn("Cannot submit event to the engine, probably database is being reloaded or the application is exiting");
        }
    }

    @Override
    public void workloadUpdate(final WorkloadInfo workloadInfo) {
        final Workload infoUpdate = new Workload(workloadInfo);
        
        try {
            this.cep.getProcessingExecutor().execute(() -> {
                ExpertSystemServant.this.cep.injectEventSynch(infoUpdate);
            });
        }
        catch(final RejectedExecutionException ex) {
            ExpertSystemServant.log.warn("Cannot submit event to the engine, probably database is being reloaded or the application is exiting");
        }
    }

    @Override
    public void podUpdate(final PodInfo podInfo) {
        final Pod infoUpdate = new Pod(podInfo);
        
        try {
            this.cep.getProcessingExecutor().execute(() -> {
                ExpertSystemServant.this.cep.injectEventSynch(infoUpdate);
            });
        }
        catch(final RejectedExecutionException ex) {
            ExpertSystemServant.log.warn("Cannot submit event to the engine, probably database is being reloaded or the application is exiting");
        }
    }
    
    @Override
    public void containerUpdate(final ContainerInfo containerInfo) {        
        final Container infoUpdate = new Container(containerInfo);
        
        try {
            this.cep.getProcessingExecutor().execute(() -> {
                ExpertSystemServant.this.cep.injectEventSynch(infoUpdate);
            });
        }
        catch(final RejectedExecutionException ex) {
            ExpertSystemServant.log.warn("Cannot submit event to the engine, probably database is being reloaded or the application is exiting");
        }
    }
}
