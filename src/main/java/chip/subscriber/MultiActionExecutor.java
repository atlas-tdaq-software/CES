package chip.subscriber;

import chip.event.issue.Issue;

public interface MultiActionExecutor {

	public abstract void update(Issue[] issueIn, Issue[] issueRemove);

}