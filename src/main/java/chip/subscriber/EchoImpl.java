package chip.subscriber;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class EchoImpl implements Echo {
    
    private static final Log log = LogFactory.getLog(EchoImpl.class);
    
    /* (non-Javadoc)
     * @see ch.atdaq.ces.subscriber.Echo#update(java.lang.String)
     */
    @Override
    public void update(final String sentence) {
        EchoImpl.log.info("Subscriber: "+sentence);
    }

}
