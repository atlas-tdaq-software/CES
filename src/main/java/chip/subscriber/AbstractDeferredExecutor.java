package chip.subscriber;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;

import chip.utils.ExecutorFactory;

public abstract class AbstractDeferredExecutor {
    private final static ExecutorService tp = ExecutorFactory.newThreadPool("ActionExec", Integer.getInteger("chip.executor.threads", 25).intValue(), false);
    
    final protected void execute(final Runnable task) {
        try {
            AbstractDeferredExecutor.tp.execute(task);
        }
        catch(final RejectedExecutionException | NullPointerException ex) {
            ex.printStackTrace();
        }
    }
}
