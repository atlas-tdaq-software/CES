package chip.subscriber.core;

import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.dao.EsperDAO;
import chip.event.issue.Issue;
import chip.executor.ExecutorRC;
import chip.subscriber.MultiActionExecutor;


public class ProblemExecutorSynch implements MultiActionExecutor {

    @Autowired
    CEPService cep;

    private final ProblemExecutorImpl pExec;

    public ProblemExecutorSynch(final ExecutorRC execRC, final EsperDAO dao) {
        super();

        this.pExec = new ProblemExecutorImpl(execRC, dao);
    }

    @Override
    public void update(final Issue[] issueIn, final Issue[] issueRemove) {
        this.pExec.execute(issueIn, issueRemove, this.cep);
    }
}
