package chip.subscriber.core;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.espertech.esper.common.client.EPException;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;

import chip.CEPService;
import chip.CHIPException.ExecutorRCException;
import chip.dao.EsperDAO;
import chip.event.en.STATUS;
import chip.event.issue.Issue;
import chip.event.issue.core.Problem;
import chip.event.meta.ErrorItem;
import chip.event.meta.RCApplication;
import chip.executor.ExecutorRC;
import chip.utils.ExecutorFactory;
import daq.rc.ErrorElement;


public class ProblemExecutorImpl {
    private static final Log log = LogFactory.getLog(ProblemExecutorImpl.class);

    private final EsperDAO dao;
    private final ExecutorRC execRC;
    private final ThreadPoolExecutor tp = ExecutorFactory.newThreadPool("Error-setter", 1, true);

    public ProblemExecutorImpl(final ExecutorRC execRC, final EsperDAO dao) {
        this.execRC = execRC;
        this.dao = dao;
    }

    public void execute(final Issue[] issueIn, final Issue[] issueRemove, final CEPService cep) {
        if((issueIn.length == 0) && (issueRemove.length == 0)) {
            ProblemExecutorImpl.log.debug("Problem subscriber called without any events. Abort!");
            return;
        }

        Problem[] problem;
        if(issueIn.length == 0) {
            problem = new Problem[issueRemove.length];
            for(int i = 0; i < issueRemove.length; i++) {
                problem[i] = (Problem) issueRemove[i];
            }
        } else {
            problem = new Problem[issueIn.length];
            for(int i = 0; i < issueIn.length; i++) {
                problem[i] = (Problem) issueIn[i];
            }
        }

        switch(problem[0].getAction()) {
            case RESTART: 
            {
                // The restart is grouping events in a time window (for the same controller)
                // Look at "problems.epl"

                final Set<String> apps = new TreeSet<>();
                for(final Problem p : problem) {
                    apps.add(p.getApplication());
                }

                try {
                    this.execRC.restartApps(problem[0].getController(), apps.toArray(new String[apps.size()]));
                }
                catch(final ExecutorRCException e) {
                    for(final Problem p : problem) {
                        final Problem event = new Problem(p.getController(),
                                                          p.getApplication(),
                                                          p.getType(),
                                                          Problem.STATUS.PROBLEM,
                                                          Problem.ACTION.NONE,
                                                          p.getId());
                        event.setError("Failed to restart application " + problem[0].getApplication(), e);
                        cep.injectEventSynch(event);
                    }
                }
            }

            break;

            case IGNORE: 
            {
                // The ignore is grouping events in a time window (for the same controller)
                // Look at "problems.epl"

                final Set<String> apps = new TreeSet<>();
                for(final Problem p : problem) {
                    apps.add(p.getApplication());
                }
                
                try {
                    this.execRC.putAppsOutFsm(problem[0].getController(), apps.toArray(new String[apps.size()]));
                }
                catch(final ExecutorRCException e) {
                    for(final Problem p : problem) {
                        final Problem event = new Problem(p.getController(),
                                                          p.getApplication(),
                                                          p.getType(),
                                                          Problem.STATUS.PROBLEM,
                                                          Problem.ACTION.NONE,
                                                          p.getId());
                        event.setError("Failed to ignore application " + problem[0].getApplication(), e);
                        cep.injectEventSynch(event);
                    }
                }                
            }

            break;

            case INCLUDE: 
            {
                // The include is grouping events in a time window (for the same controller)
                // Look at "problems.epl"

                final Set<String> apps = new TreeSet<>();
                for(final Problem p : problem) {
                    apps.add(p.getApplication());
                }
                
                try {
                    this.execRC.putAppsInFsm(problem[0].getController(), apps.toArray(new String[apps.size()]));
                }
                catch(final ExecutorRCException e) {
                    for(final Problem p : problem) {
                        final Problem event = new Problem(p.getController(),
                                                          p.getApplication(),
                                                          p.getType(),
                                                          Problem.STATUS.PROBLEM,
                                                          Problem.ACTION.NONE,
                                                          p.getId());
                        event.setError("Failed to include application " + problem[0].getApplication(), e);
                        cep.injectEventSynch(event);
                    }
                }                
            }

            break;
            
            // NOTE: special handling of SET_ERROR and REMOVE_ERROR
            // Calls to the listener for these two commands are serialized by the ESPER statement (see 
            // SUBSCRIBER_ProblemExecutor2 in problems.epl). Here the execution is sent to a separate
            // thread (actually a thread pool of size 1) to not keep the ESPER engine busy.
            // Indeed the subscriber is synchronous and the remote calls to the controller may 
            // take a long time
            case SET_ERROR: 
            {
                final String controller = problem[0].getController();
                final boolean sendCommand = this.canCtrlReceiveCommands(controller);
                
                if(sendCommand == true) {
                    this.tp.execute(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                final List<ErrorItem> errorItems = ProblemExecutorImpl.this.dao.getAllErrorsOfController(controller);
                                final LinkedList<ErrorElement> errorForController = new LinkedList<ErrorElement>();
                                for(final ErrorItem err : errorItems) {
                                    errorForController.add(new ErrorElement(err.getApplication(),
                                                                            err.getErrorDescription(),
                                                                            err.getErrorType()));
                                }
                            
                                ProblemExecutorImpl.this.execRC.setControllerErrors(controller, errorForController);                            
                            }
                            catch(final EPRuntimeDestroyedException e) {
                                // Engine stopped (usually during database reloading)
                                ProblemExecutorImpl.log.info("Cannot query the engine because it has been destroyed: " + e);
                            }
                            catch(final ExecutorRCException e) {
                                // Error sending command to controller
                                final Problem event = new Problem(controller,
                                                                  problem[0].getApplication(),
                                                                  problem[0].getType(),
                                                                  Problem.STATUS.PROBLEM,
                                                                  Problem.ACTION.NONE,
                                                                  problem[0].getId());
                                event.setError("Failed to set errors of controller " + controller, e);
                                cep.injectEventSynch(event);
                            }
                        }
                    });
                }
            }

            break;

            case REMOVE_ERROR: 
            {
                final String controller = problem[0].getController();
                final boolean sendCommand = this.canCtrlReceiveCommands(controller);
                
                this.tp.execute(new Runnable() {
                    @Override
                    public void run() {
                        if(sendCommand == true) {
                            try {
                                final List<ErrorItem> errorItems2 = ProblemExecutorImpl.this.dao.getAllErrorsOfController(controller);
                                final LinkedList<ErrorElement> errorForController2 = new LinkedList<ErrorElement>();
                                for(final ErrorItem err : errorItems2) {
                                    errorForController2.add(new ErrorElement(err.getApplication(),
                                                                             err.getErrorDescription(),
                                                                             err.getErrorType()));
                                }

                                ProblemExecutorImpl.this.execRC.setControllerErrors(controller, errorForController2);                            
                            }
                            catch(final EPRuntimeDestroyedException e) {
                                // Engine stopped (usually during database reloading)
                                ProblemExecutorImpl.log.info("Cannot query the engine because it has been destroyed: " + e);
                            }
                            catch(final ExecutorRCException e) {
                                final Problem event = new Problem(controller,
                                                                  problem[0].getApplication(),
                                                                  problem[0].getType(),
                                                                  Problem.STATUS.PROBLEM,
                                                                  Problem.ACTION.NONE,
                                                                  problem[0].getId());
                                event.setError("Failed to remove errors of controller " + controller, e);
                                cep.injectEventSynch(event);
                            }
                        }
                        
                        for(final Problem element : problem) {
                            cep.injectEventSynch(new Problem(element.getController(),
                                                             element.getApplication(),
                                                             element.getType(),
                                                             Problem.STATUS.DONE,
                                                             Problem.ACTION.NONE,
                                                             element.getId()));
                        }
                    }
                });
            }

            break;

            default:
                ProblemExecutorImpl.log.error("Action not supported: " + problem[0].getAction());
        }
    }
    
    private boolean canCtrlReceiveCommands(final String ctrl) {
        boolean doit = true;
        
        try {
            final RCApplication app = ProblemExecutorImpl.this.dao.getRCApplication(ctrl);
            final STATUS status = app.getStatus();
            if((status == STATUS.ABSENT) || (status == STATUS.EXITED) || (status == STATUS.FAILED) || (status == STATUS.TERMINATING)) {
                return false;
            }
        }
        catch(final NullPointerException ex) {
            // App not found, try to send the command anyway
        }
        catch(final EPRuntimeDestroyedException ex) {
            // Engine stopped (usually during database reloading)
            ProblemExecutorImpl.log.info("Cannot query the engine because it has been destroyed: " + ex);
            doit = false;
        }
        catch(final EPException ex) {
            ProblemExecutorImpl.log.warn("Unexpected exception", ex);
        }
        
        return doit;
    }
}
