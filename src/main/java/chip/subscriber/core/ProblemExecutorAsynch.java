package chip.subscriber.core;

import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.dao.EsperDAO;
import chip.event.issue.Issue;
import chip.executor.ExecutorRC;
import chip.subscriber.DeferredMultiActionExecutor;


public class ProblemExecutorAsynch extends DeferredMultiActionExecutor {
    @Autowired
    CEPService cep;

    private final ProblemExecutorImpl pExec;

    public ProblemExecutorAsynch(final ExecutorRC execRC, final EsperDAO dao) {
        super();

        this.pExec = new ProblemExecutorImpl(execRC, dao);
    }

    @Override
    public void doUpdate(final Issue[] issueIn, final Issue[] issueRemove) {
        this.pExec.execute(issueIn, issueRemove, this.cep);
    }
}
