package chip.subscriber;

import chip.event.issue.Issue;


public interface ActionExecutor {
    public void update(Issue issue);
}
