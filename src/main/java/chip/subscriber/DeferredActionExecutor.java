package chip.subscriber;

import chip.event.issue.Issue;

public abstract class DeferredActionExecutor extends AbstractDeferredExecutor implements ActionExecutor {
    @Override
    final public void update(final Issue issue) {
        super.execute(new Runnable() {
            @Override
            public void run() {
                doUpdate(issue);
            }
        });
    }

    abstract protected void doUpdate(final Issue issue);
}
