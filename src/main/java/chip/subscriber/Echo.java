package chip.subscriber;

/**
 * Placeholder interface to mark subscriber
 * for the 'Echo' type.
 * 
 * @author lucamag
 *
 */
public interface Echo {

    /**
     * Callback invoked by Esper with the data selected
     * by the epl statement. 
     * <p>
     * This method requires only a String as input data.
     *  
     * @param sentence
     */
    public abstract void update(String sentence);

}