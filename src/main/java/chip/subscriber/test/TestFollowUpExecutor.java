package chip.subscriber.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.CHIPException.DAQConfigurationError;
import chip.CHIPException.ExecutorPMGException;
import chip.CHIPException.ExecutorRCException;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.dynamic.CompTestResultInfo;
import chip.event.en.MEMBERSHIP;
import chip.event.issue.Issue;
import chip.event.issue.test.TestFollowUp;
import chip.event.issue.test.TestFollowUp.ACTION;
import chip.event.issue.test.TestFollowUp.STATUS;
import chip.event.meta.RCApplication;
import chip.executor.ExecutorPMG;
import chip.executor.ExecutorRC;
import chip.msg.Common;
import chip.msg.HostDisabled;
import chip.msg.Internal;
import chip.subscriber.DeferredMultiActionExecutor;
import config.ConfigException;
import dal.RM_Resource;


public class TestFollowUpExecutor extends DeferredMultiActionExecutor {

    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(TestFollowUpExecutor.class);

    private final ExecutorRC execRC;
    private final ExecutorPMG execPMG;
    private final DAQConfigReader daqReader;
    private final EsperDAO dao;
        
    public TestFollowUpExecutor(final ExecutorRC execRC,
                                final ExecutorPMG execPMG,
                                final EsperDAO dao,
                                final DAQConfigReader daqReader)
    {
        super();
        this.execRC = execRC;
        this.execPMG = execPMG;
        this.daqReader = daqReader;
        this.dao = dao;
    }

    @Override
    public void doUpdate(final Issue[] issueIn, final Issue[] issueRemove) {  
        if((issueIn.length == 0) && (issueRemove.length == 0)) {
            // not good
            TestFollowUpExecutor.log.warn("Received no events!");
            return;
        }
        
        final TestFollowUp[] testFollUp;
        if(issueIn.length == 0) {
            testFollUp = new TestFollowUp[issueRemove.length];
            for(int i = 0; i < issueRemove.length; i++) {
                testFollUp[i] = (TestFollowUp) issueRemove[i];
            }
        } else {
            testFollUp = new TestFollowUp[issueIn.length];
            for(int i = 0; i < issueIn.length; i++) {
                testFollUp[i] = (TestFollowUp) issueIn[i];
            }
        }
        
        if((testFollUp.length > 1) && (testFollUp[0].getAction() != TestFollowUp.ACTION.DISABLE_HOST)) {
            TestFollowUpExecutor.log.error("Bad number of DISABLE_HOST actions");
            return;
        }
                
        switch(testFollUp[0].getAction()) {
            case NONE:
                break;
            case EXECUTE_PROGRAM:
                this.executeProgram(testFollUp[0]);
                break;
            case NOTIFY_EXECUTE_FAILED:
                this.notifyExecutionFailed(testFollUp[0]);
                break;
            case TEST:
                this.test(testFollUp[0]);
                break;
            case TEST_ALL_CHILDREN:
                this.testAllChildren(testFollUp[0]);
                break;
            case DISABLE_HOST:
                this.disableNode(testFollUp);
                break;
            case RESTART_APP:
                this.restartApp(testFollUp[0]);
                break;
            case NOTIFY_CANNOT_RESTART:
                this.notifyCannotRestart(testFollUp[0]);
                break;
            case DISABLE_APP:
                this.disableApp(testFollUp[0]);
                break;
            case NOTIFY_DISABLE_FAILED:
                this.notifyDisableFailed(testFollUp[0]);
                break;
            case NOTIFY_DISABLE_OK:
                this.notifyDisableOk(testFollUp[0]);
                break;
            default:
                break;
        }
    }
    
    private void disableApp(final TestFollowUp testFolloUp) {
        try {
            this.execRC.sendChangeStatus(testFolloUp.getController(), false, new String[] { testFolloUp.getApplicationName() });
            
            final TestFollowUp event = new TestFollowUp(testFolloUp.getApplicationName(),
                                                        testFolloUp.getController(),
                                                        testFolloUp.getRunningHost(),
                                                        testFolloUp.getGlobalTestResult(),
                                                        testFolloUp.getComponent(),
                                                        testFolloUp.getCompTestResult(),
                                                        STATUS.DONE,
                                                        ACTION.NONE,
                                                        testFolloUp.getActionCounter());
            this.cep.injectEvent(event);        
        }
        catch(final ExecutorRCException ex) {
            ers.Logger.error(new Internal("Disable application " + testFolloUp.getApplicationName(), ex));
            
            final TestFollowUp event = new TestFollowUp(testFolloUp.getApplicationName(),
                                                        testFolloUp.getController(),
                                                        testFolloUp.getRunningHost(),
                                                        testFolloUp.getGlobalTestResult(),
                                                        testFolloUp.getComponent(),
                                                        testFolloUp.getCompTestResult(),
                                                        STATUS.FAILED,
                                                        ACTION.NONE,
                                                        testFolloUp.getActionCounter());
            this.cep.injectEvent(event);        
        }
    }
    
    private void restartApp(final TestFollowUp testFolloUp) {
        try {
            this.execRC.restartApps(testFolloUp.getController(), new String[] { testFolloUp.getApplicationName() });
        
            final TestFollowUp event = new TestFollowUp(testFolloUp.getApplicationName(),
                                                        testFolloUp.getController(),
                                                        testFolloUp.getRunningHost(),
                                                        testFolloUp.getGlobalTestResult(),
                                                        testFolloUp.getComponent(),
                                                        testFolloUp.getCompTestResult(),
                                                        STATUS.DONE,
                                                        ACTION.NONE,
                                                        testFolloUp.getActionCounter());
            this.cep.injectEvent(event);        
        }
        catch(final ExecutorRCException ex) {
            ers.Logger.error(new Internal("Restart application " + testFolloUp.getApplicationName(), ex));
            
            final TestFollowUp event = new TestFollowUp(testFolloUp.getApplicationName(),
                                                        testFolloUp.getController(),
                                                        testFolloUp.getRunningHost(),
                                                        testFolloUp.getGlobalTestResult(),
                                                        testFolloUp.getComponent(),
                                                        testFolloUp.getCompTestResult(),
                                                        STATUS.FAILED,
                                                        ACTION.NONE,
                                                        testFolloUp.getActionCounter());
            this.cep.injectEvent(event);        
        }        
    }
    
    private void notifyCannotRestart(final TestFollowUp testFollowUp) {
        final StringBuilder msg = new StringBuilder("Test for application \"");
        msg.append(testFollowUp.getApplicationName());
        msg.append("\" failed but the corresponding RESTART action could not be accomplished because the maximum number of restarts has been reached");
        
        ers.Logger.warning(new Common(msg.toString()));
        
        final TestFollowUp event = new TestFollowUp(testFollowUp.getApplicationName(),
                                                    testFollowUp.getController(),
                                                    testFollowUp.getRunningHost(),
                                                    testFollowUp.getGlobalTestResult(),
                                                    testFollowUp.getComponent(),
                                                    testFollowUp.getCompTestResult(),
                                                    STATUS.DONE,
                                                    ACTION.NONE,
                                                    testFollowUp.getActionCounter());
        this.cep.injectEvent(event);        
    }
    
    private void notifyDisableOk(final TestFollowUp testFollUp) {
        final CompTestResultInfo testResult = testFollUp.getCompTestResult();
        
        final String problematicHost = testResult.getRebootHost();
        ers.Logger.warning(new HostDisabled(problematicHost));

        final TestFollowUp event = new TestFollowUp(testFollUp.getApplicationName(),
                                                    testFollUp.getController(),
                                                    testFollUp.getRunningHost(),
                                                    testFollUp.getGlobalTestResult(),
                                                    testFollUp.getComponent(),
                                                    testFollUp.getCompTestResult(),
                                                    STATUS.DONE,
                                                    ACTION.NONE,
                                                    testFollUp.getActionCounter());
        this.cep.injectEvent(event);
    }

    private void notifyDisableFailed(final TestFollowUp testFollUp) {
        final CompTestResultInfo testResult = testFollUp.getCompTestResult();

        final String problematicHost = testResult.getRebootHost();

        final TestFollowUp event = new TestFollowUp(testFollUp.getApplicationName(),
                                                    testFollUp.getController(),
                                                    testFollUp.getRunningHost(),
                                                    testFollUp.getGlobalTestResult(),
                                                    testFollUp.getComponent(),
                                                    testFollUp.getCompTestResult(),
                                                    STATUS.FAILED,
                                                    ACTION.NONE,
                                                    testFollUp.getActionCounter());

        event.setError("Failed to disable computer " + problematicHost + " in the configuration database");
        this.cep.injectEvent(event);

        ers.Logger.error(new Common("Failed to disable computer " + problematicHost + " in the configuration database"));
    }

    private void notifyExecutionFailed(final TestFollowUp testFollUp) {
        final CompTestResultInfo testResult = testFollUp.getCompTestResult();
        
        final String program = testResult.getExecProgramUID();        

        final TestFollowUp event = new TestFollowUp(testFollUp.getApplicationName(),
                                                    testFollUp.getController(),
                                                    testFollUp.getRunningHost(),
                                                    testFollUp.getGlobalTestResult(),
                                                    testFollUp.getComponent(),
                                                    testFollUp.getCompTestResult(),
                                                    STATUS.FAILED,
                                                    ACTION.NONE,
                                                    testFollUp.getActionCounter());
        
        event.setError("Failed to execute program \"" + program + "\" as a test follow-up action for component \"" + testResult.getName()
                       + "\" of application \"" + testFollUp.getApplicationName());

        this.cep.injectEvent(event);
    }

    private void disableNode(final TestFollowUp[] testFollUp) {
        /*
         * change_node_state.py -h Usage: change_node_state.py -r <release> -s <state> node1 node2 ... <release> --> Valid release name. Use
         * 'all' to update HW files for the release currently in use in P1 <state> --> new state for the nodes. Accepted values are 'on' and
         * 'off'
         */

        final StringBuilder problematicHosts = new StringBuilder();
        
        for(final TestFollowUp tfu : testFollUp) {
            final String h = tfu.getCompTestResult().getRebootHost();

            problematicHosts.append(h);
            problematicHosts.append(' ');
        }

        final String tdaqAppName = this.daqReader.getApplicationName();

        // Input
        final String computerProgUID = "change_node_state.py";

        // Output
        final Map<String, String> env = new HashMap<String, String>();
        final List<String> progNameList = new ArrayList<String>();
        final List<RM_Resource> needs = new ArrayList<RM_Resource>();

        final String hostName;
        try {
            hostName = this.daqReader.getAppConfiguration().get(tdaqAppName).get_host().UID();
            TestFollowUpExecutor.log.debug(computerProgUID + " will be executed on host " + hostName);
            
            this.daqReader.getProgramInfo(env, progNameList, needs, computerProgUID, hostName);
            // The application must always work with HEAD
            // The TDAQ_DB_VERSION env is passed from parent to children, but it is not updated on reload
            env.remove("TDAQ_DB_VERSION");
        }
        catch(final ConfigException | DAQConfigurationError e) {
            for(final TestFollowUp tfp : testFollUp) {
                final TestFollowUp event = new TestFollowUp(tfp.getApplicationName(),
                                                            tfp.getController(),
                                                            tfp.getRunningHost(),
                                                            tfp.getGlobalTestResult(),
                                                            tfp.getComponent(),
                                                            tfp.getCompTestResult(),
                                                            STATUS.FAILED,
                                                            ACTION.NONE,
                                                            tfp.getActionCounter());
                event.setError("Failed to get database info for " + computerProgUID, e);
                this.cep.injectEvent(event);
            }
            
            return;
        }

        // now build progNames string
        final StringBuilder progNames = new StringBuilder();
        for(final String name : progNameList) {
            progNames.append(name + ":");
        }

        final String startArgs = "-s off " + problematicHosts.toString().trim();

        TestFollowUpExecutor.log.debug(computerProgUID + " will be started with arguments: " + startArgs);

        String rmSwobject = "";
        if(needs.isEmpty()) {
            rmSwobject = "";
        } else {
            rmSwobject = computerProgUID;
        }

        final int initTimeout = 0; // ?? Needs to be checked
        final int autoKillTimeout = 180; // ?? Needs to be checked

        try {
            final String workingDir = this.daqReader.getDalPartition().get_WorkingDirectory();
            final String logPath = this.daqReader.getDalPartition().get_LogRoot() + "/" + this.daqReader.getPartitionName();
            
            final List<TestFollowUp> executingEvents = new ArrayList<>();            
            for(final TestFollowUp tfu : testFollUp) {
                final TestFollowUp executingEvent = new TestFollowUp(tfu.getApplicationName(),
                                                                     tfu.getController(),
                                                                     tfu.getRunningHost(),
                                                                     tfu.getGlobalTestResult(),
                                                                     tfu.getComponent(),
                                                                     tfu.getCompTestResult(),
                                                                     STATUS.DISABLING,
                                                                     ACTION.NONE,
                                                                     tfu.getActionCounter());
                executingEvents.add(executingEvent);
            }
            
            final List<TestFollowUp> succeededEvents = new ArrayList<>();
            for(final TestFollowUp tfu : testFollUp) {
                final TestFollowUp succeededEvent = new TestFollowUp(tfu.getApplicationName(),
                                                                     tfu.getController(),
                                                                     tfu.getRunningHost(),
                                                                     tfu.getGlobalTestResult(),
                                                                     tfu.getComponent(),
                                                                     tfu.getCompTestResult(),
                                                                     STATUS.DISABLED_OK,
                                                                     ACTION.NONE,
                                                                     tfu.getActionCounter());
                succeededEvents.add(succeededEvent);
            }

            final List<TestFollowUp> failedEvents = new ArrayList<>();
            for(final TestFollowUp tfu : testFollUp) {
                final TestFollowUp failedEvent = new TestFollowUp(tfu.getApplicationName(),
                                                                  tfu.getController(),
                                                                  tfu.getRunningHost(),
                                                                  tfu.getGlobalTestResult(),
                                                                  tfu.getComponent(),
                                                                  tfu.getCompTestResult(),
                                                                  STATUS.DISABLED_FAILED,
                                                                  ACTION.NONE,
                                                                  tfu.getActionCounter());
                failedEvents.add(failedEvent);
            }
            
            this.execPMG.executeTestExecAction(executingEvents,
                                               succeededEvents,
                                               failedEvents,
                                               hostName,
                                               this.daqReader.getPartitionName(),
                                               computerProgUID + "_" + Integer.toString(problematicHosts.toString().hashCode()),
                                               progNames.toString(),
                                               env,
                                               workingDir,
                                               startArgs,
                                               logPath,
                                               rmSwobject,
                                               initTimeout,
                                               autoKillTimeout);
        }
        catch(final ExecutorPMGException | ConfigException e) {            
            TestFollowUpExecutor.log.error("Failed to execute " + computerProgUID + ": " + e.toString());
            
            for(final TestFollowUp tfu : testFollUp) {
                final TestFollowUp event = new TestFollowUp(tfu.getApplicationName(),
                                                            tfu.getController(),
                                                            tfu.getRunningHost(),
                                                            tfu.getGlobalTestResult(),
                                                            tfu.getComponent(),
                                                            tfu.getCompTestResult(),
                                                            STATUS.FAILED,
                                                            ACTION.NONE,
                                                            tfu.getActionCounter());
                event.setError("Execution of " + computerProgUID + " failed", e);
                this.cep.injectEvent(event);
            }
            
            ers.Logger.error(new Common("Failed to disable computer(s) " + problematicHosts.toString() + " in the configuration database: " + e, e));
            
            return;
        }
    }

    private void executeProgram(final TestFollowUp testFollUp) {
        final String applicationName = testFollUp.getApplicationName();
        final String controller = testFollUp.getController();
        final CompTestResultInfo testResult = testFollUp.getCompTestResult();

        try {
            // Extract parameters from action           
            final String computerProgUID = testResult.getExecProgramUID();
            final String hostName = testResult.getExecHost();
            final String startArgs = testResult.getExecParams();            
            final int initTimeout = testResult.getExecInitTimeout();               
            final int autoKillTimeout = testResult.getTimeout();
    
            // Output
            final Map<String, String> env = new HashMap<String, String>();
            final List<String> progNameList = new ArrayList<String>();
            final List<RM_Resource> needs = new ArrayList<RM_Resource>();
    
            this.daqReader.getProgramInfo(env, progNameList, needs, computerProgUID, hostName);
            
            // now build progNames string
            final StringBuilder progNames = new StringBuilder();
            for(final String name : progNameList) {
                progNames.append(name + ":");
            }
    
            final String workingDir = this.daqReader.getDalPartition().get_WorkingDirectory();
            final String logPath = this.daqReader.getDalPartition().get_LogRoot();
    
            String rmSwobject = "";
            if(needs.isEmpty()) {
                rmSwobject = "";
            } else {
                rmSwobject = computerProgUID;
            }

            final TestFollowUp executingEvent = new TestFollowUp(testFollUp.getApplicationName(),
                                                                 testFollUp.getController(),
                                                                 testFollUp.getRunningHost(),
                                                                 testFollUp.getGlobalTestResult(),
                                                                 testFollUp.getComponent(),
                                                                 testFollUp.getCompTestResult(),
                                                                 STATUS.EXECUTING,
                                                                 ACTION.NONE,
                                                                 testFollUp.getActionCounter());

            final TestFollowUp succeededEvent = new TestFollowUp(testFollUp.getApplicationName(),
                                                                 testFollUp.getController(),
                                                                 testFollUp.getRunningHost(),
                                                                 testFollUp.getGlobalTestResult(),
                                                                 testFollUp.getComponent(),
                                                                 testFollUp.getCompTestResult(),
                                                                 STATUS.EXECUTED_OK,
                                                                 ACTION.NONE,
                                                                 testFollUp.getActionCounter());

            final TestFollowUp failedEvent = new TestFollowUp(testFollUp.getApplicationName(),
                                                              testFollUp.getController(),
                                                              testFollUp.getRunningHost(),
                                                              testFollUp.getGlobalTestResult(),
                                                              testFollUp.getComponent(),
                                                              testFollUp.getCompTestResult(),
                                                              STATUS.EXECUTED_FAILED,
                                                              ACTION.NONE,
                                                              testFollUp.getActionCounter());

            this.execPMG.executeTestExecAction(Collections.singletonList(executingEvent),
                                               Collections.singletonList(succeededEvent),
                                               Collections.singletonList(failedEvent),
                                               hostName,
                                               this.daqReader.getPartitionName(),
                                               computerProgUID,
                                               progNames.toString(),
                                               env,
                                               workingDir,
                                               startArgs,
                                               logPath,
                                               rmSwobject,
                                               initTimeout,
                                               autoKillTimeout);
        }
        catch(final ExecutorPMGException | DAQConfigurationError | ConfigException e) {
            ers.Logger.error(new Internal("Failed to execute follow up action of test for app " + testFollUp.getApplicationName() + " :"
                                          + e.toString(), e));
            
            this.cep.injectEvent(new TestFollowUp(applicationName,
                                                  controller,
                                                  testFollUp.getRunningHost(),
                                                  testFollUp.getGlobalTestResult(),
                                                  testFollUp.getComponent(),
                                                  testFollUp.getCompTestResult(),
                                                  STATUS.FAILED,
                                                  ACTION.NONE,
                                                  testFollUp.getActionCounter()));
        }
    }

    private void test(final TestFollowUp testFollUp) {
        final String applicationName = testFollUp.getApplicationName();
        final String controller = testFollUp.getController();

        try {
            this.execRC.testApplication(controller, new String[] {applicationName});
        }
        catch(final ExecutorRCException e) {
            ers.Logger.error(new Internal("Retest app " + applicationName, e));
            this.cep.injectEvent(new TestFollowUp(applicationName,
                                                  controller,
                                                  testFollUp.getRunningHost(),
                                                  testFollUp.getGlobalTestResult(),
                                                  testFollUp.getComponent(),
                                                  testFollUp.getCompTestResult(),
                                                  STATUS.FAILED,
                                                  ACTION.NONE,
                                                  testFollUp.getActionCounter()));
        }
    }

    private void testAllChildren(final TestFollowUp testFollUp) {
        final String controller = testFollUp.getController();
        final List<RCApplication> apps = this.dao.getChildrenOfController(controller);
        
        final String[] appNames = apps.stream()
                                      .filter(app -> { return app.getMembership().equals(MEMBERSHIP._IN); }) // Do not test apps that are OUT
                                      .filter(app -> { return app.getStatus().equals(chip.event.en.STATUS.UP); }) // Test only apps that are UP
                                      .map(RCApplication::getName)
                                      .filter(appName -> { return !appName.equals(controller); }) // Be sure the controller itself is not 
                                                                                                  // part of the list (valid for the RootController)
                                      .toArray(String[]::new);
        
        try {
            this.execRC.testApplication(controller, appNames);
        }
        catch(final ExecutorRCException e) {
            ers.Logger.error(new Internal("Testing all children of " + controller, e));
            this.cep.injectEvent(new TestFollowUp("ALL",
                                                  controller,
                                                  testFollUp.getRunningHost(),
                                                  testFollUp.getGlobalTestResult(),
                                                  testFollUp.getComponent(),
                                                  testFollUp.getCompTestResult(),
                                                  STATUS.FAILED,
                                                  ACTION.NONE,
                                                  testFollUp.getActionCounter()));
        }
    }
}
