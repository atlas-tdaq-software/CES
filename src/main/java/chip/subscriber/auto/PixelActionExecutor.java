package chip.subscriber.auto;

import is.InfoNotFoundException;
import is.RepositoryNotFoundException;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import rc.HoldTriggerInfoNamed.CausedBy;
import rc.HoldTriggerInfoNamed.Reason;
import chip.CEPService;
import chip.CHIPException.ExecutorIGUIException;
import chip.CHIPException.ExecutorRCException;
import chip.CHIPException.ExecutorTriggerException;
import chip.CHIPException.WaitTimeoutException;
import chip.DAQConfigReader;
import chip.event.config.PixelConfig;
import chip.event.config.RCApplicationConfig;
import chip.event.en.FSM;
import chip.event.issue.Issue;
import chip.event.issue.auto.PixelAction;
import chip.event.issue.auto.PixelAction.STATUS;
import chip.executor.ExecutorIGUI;
import chip.executor.ExecutorRC;
import chip.executor.ExecutorTrigger;
import chip.subscriber.DeferredActionExecutor;
import chip.utils.HoldTriggerInfoPublisher;
import chip.utils.HoldTriggerInfoPublisher.Token;
import daq.eformat.DetectorMask;
import daq.eformat.DetectorMask.SUBDETECTOR;


public class PixelActionExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    @Autowired
    PixelConfig pixConfig;

    private static final Log log = LogFactory.getLog(PixelActionExecutor.class);

    private final ExecutorRC execRC;
    private final ExecutorIGUI execIGUI;
    private final ExecutorTrigger execTrigger;
    private final DAQConfigReader daqConf;

    @Value("#{configuration['chip.pix.up_command.timeout']}")
    Integer pixelUpTimeout;
    
    @Value("#{configuration['chip.pix.down_command.timeout']}")
    Integer pixelDownTimeout;
    
    public PixelActionExecutor(final ExecutorRC execRC,
                               final ExecutorIGUI execIGUI,
                               final ExecutorTrigger execTrigger,
                               final DAQConfigReader daqConf)
    {
        this.execRC = execRC;
        this.execIGUI = execIGUI;
        this.execTrigger = execTrigger;
        this.daqConf = daqConf;
    }

    @Override
    public void doUpdate(final Issue issue) {
        final PixelAction pixAct = (PixelAction) issue;

        final Map<String, RCApplicationConfig> rcApps = this.daqConf.getRCApplications();
        if((rcApps.containsKey(this.pixConfig.getCtrlPixTop()) == false) && (rcApps.containsKey(this.pixConfig.getCtrlPixIBL()) == false)
           && (rcApps.containsKey(this.pixConfig.getCtrlPixBLayer()) == false)
           && (rcApps.containsKey(this.pixConfig.getCtrlPixBarrel()) == false)
           && (rcApps.containsKey(this.pixConfig.getCtrlPixDisk()) == false))
        {
            // PIXEL not in the partition
            PixelActionExecutor.log.info("Not sending the " + pixAct.getType().toString()
                                         + " command because the Pixel controllers are not in the partition");

            final PixelAction event = new PixelAction(pixAct.getType(),
                                                      PixelAction.STATUS.DONE,
                                                      pixAct.getController(),
                                                      pixAct.getState(),
                                                      pixAct.getId());
            this.cep.injectEvent(event);
        } else {
            final DetectorMask dmask = new DetectorMask();
            final String ctrlName = pixAct.getController();

            if(ctrlName.equals(this.pixConfig.getCtrlPixIBL())) {
                dmask.set(SUBDETECTOR.PIXEL_IBL);
            } else if(ctrlName.equals(this.pixConfig.getCtrlPixBLayer())) {
                dmask.set(SUBDETECTOR.PIXEL_B_LAYER);
            } else if(ctrlName.equals(this.pixConfig.getCtrlPixBarrel())) {
                dmask.set(SUBDETECTOR.PIXEL_BARREL);
            } else if(ctrlName.equals(this.pixConfig.getCtrlPixDisk())) {
                dmask.set(SUBDETECTOR.PIXEL_DISK);
            } else if(ctrlName.equals(this.pixConfig.getCtrlPixTop())) {
                dmask.set(SUBDETECTOR.PIXEL_IBL);
                dmask.set(SUBDETECTOR.PIXEL_B_LAYER);
                dmask.set(SUBDETECTOR.PIXEL_BARREL);
                dmask.set(SUBDETECTOR.PIXEL_DISK);
            } else {
                this.injectFailure(pixAct, STATUS.FAILED, ctrlName + " is not a valid PIXEL controller name", null);
                return;
            }

            switch(pixAct.getType()) {
                case PIXEL_DOWN:
                    this.doPixelDOWN(pixAct, dmask);
                    break;
                case PIXEL_UP:
                    this.doPixelUP(pixAct, dmask);
                    break;
                default:
                    break;
            }
        }
    }

    private void doPixelUP(final PixelAction action, final DetectorMask dmask) {
        final String pixelController = action.getController();

        final Map<String, RCApplicationConfig> rcApps = this.daqConf.getRCApplications();
        if(rcApps.containsKey(pixelController) == false) {
            PixelActionExecutor.log.info("Not sending the PIXEL_UP command to controller \"" + pixelController
                                         + "\" because not in the partition");

            final PixelAction event = new PixelAction(action.getType(),
                                                      PixelAction.STATUS.DONE,
                                                      action.getController(),
                                                      action.getState(),
                                                      action.getId());
            this.cep.injectEvent(event);
        } else {
            final FSM state = action.getState();
            long ecr = 0;
            Token t = null;

            try {
                if(state.equals(FSM.RUNNING) == true) {
                    try {
                        ecr = this.execTrigger.hold(dmask).getEcrCounter();
                    }
                    catch(final ExecutorTriggerException e) {
                        this.injectFailure(action, STATUS.FAILED, "Not able to hold the trigger", e);
                        return;
                    }
    
                    try {
                        t = HoldTriggerInfoPublisher.publish(this.daqConf.getPartitionName(), CausedBy.PIX, Reason.CustomRecovery);                                                            
                    }
                    catch(final Exception e) {
                        final PixelAction event = new PixelAction(action.getType(),
                                                                  PixelAction.STATUS.PROBLEM,
                                                                  action.getController(),
                                                                  action.getState(),
                                                                  action.getId());
                        event.setError("Failed to publish HoldTriggerInfo to IS", e);
                        this.cep.injectEvent(event);
                    }
                }
    
                // send PIXEL_UP to relevant controller
                boolean failed = false;
                boolean resumeTrigger = true;
                try {
                    this.execRC.sendUserBroadcast(pixelController, "PIXEL_UP", new String[] {String.valueOf(ecr)}, Boolean.TRUE, this.pixelUpTimeout.intValue());
                }
                catch(final ExecutorRCException e) {
                    this.injectFailure(action, STATUS.FAILED, "Not able to send command PIXEL_UP to " + pixelController, e);
    
                    final Throwable cause = e.getCause();
                    if((WaitTimeoutException.class.isInstance(cause) == true) || (InterruptedException.class.isInstance(cause) == true)) {
                        // The command was sent but it timed-out
                        resumeTrigger = false;
                    }
    
                    failed = true;
                }
                finally {
                    if(state.equals(FSM.RUNNING) == true) {
                        if(resumeTrigger == true) {
                            try {
                                this.execTrigger.resume(dmask);
                            }
                            catch(final ExecutorTriggerException e) {
                                if(failed == false) {
                                    this.injectFailure(action, STATUS.FAILED, "Not able to resume trigger", e);
                                    failed = true;
                                } else {
                                    PixelActionExecutor.log.error("Failed to resume the trigger", e);
                                }
    
                                this.sendErrorToIGUI("After a PIXEL_UP procedure the trigger could not be resumed; consider to resume it manually");
                            }
                        } else {
                            this.sendErrorToIGUI("The PIXEL_UP action for controller \"" + pixelController
                                                 + "\" failed and the trigger is not automatically resumed. Before resuming it manually, consult the relevant expert");
                        }
                    }                
                }
    
                if(failed == false) {
                    final PixelAction event = new PixelAction(action.getType(),
                                                              PixelAction.STATUS.DONE,
                                                              action.getController(),
                                                              action.getState(),
                                                              action.getId());
                    this.cep.injectEvent(event);
                }
            }
            finally {
                if(t != null) {
                    try {
                        HoldTriggerInfoPublisher.remove(t);
                    }
                    catch(final RepositoryNotFoundException | InfoNotFoundException e) {
                        PixelActionExecutor.log.error("Failed to remove IS information about trigger on hold: " + e, e);
                    }
                }                
            }
        }
    }

    private void doPixelDOWN(final PixelAction action, final DetectorMask dmask) {
        final String ctrName = action.getController();

        final Map<String, RCApplicationConfig> rcApps = this.daqConf.getRCApplications();
        if(rcApps.containsKey(ctrName) == false) {
            PixelActionExecutor.log.info("Not sending the PIXEL_DOWN command to controller \"" + ctrName
                                         + "\" because not in the partition");

            final PixelAction event =
                                    new PixelAction(action.getType(), PixelAction.STATUS.DONE, ctrName, action.getState(), action.getId());
            this.cep.injectEvent(event);
        } else {
            final FSM state = action.getState(); // here: RootController state
            long ecr = 0;
            Token t = null;
            
            try {
                if(state.equals(FSM.RUNNING) == true) {
                    try {
                        ecr = this.execTrigger.hold(dmask).getEcrCounter();
                    }
                    catch(final ExecutorTriggerException e) {
                        this.injectFailure(action, STATUS.FAILED, "Not able to hold the trigger", e);
                        return;
                    }
    
                    try {
                        t = HoldTriggerInfoPublisher.publish(this.daqConf.getPartitionName(), CausedBy.PIX, Reason.CustomRecovery);                                        
                    }
                    catch(final Exception e) {
                        final PixelAction event = new PixelAction(action.getType(),
                                                                  PixelAction.STATUS.PROBLEM,
                                                                  ctrName,
                                                                  action.getState(),
                                                                  action.getId());
                        event.setError("Failed to publish HoldTriggerInfo to IS", e);
                        this.cep.injectEvent(event);
                    }
                }
    
                try {
                    this.execRC.sendUserBroadcast(ctrName, "PIXEL_DOWN", new String[] {String.valueOf(ecr)}, Boolean.TRUE, this.pixelDownTimeout.intValue());
                }
                catch(final ExecutorRCException e) {
                    this.injectFailure(action, STATUS.PROBLEM, "Not able to send command PIXEL_DOWN to " + ctrName, e);
                }
    
                try {
                    if(state.equals(FSM.RUNNING) == true) {
                        this.execTrigger.resume(dmask);
                    }
                    
                    final PixelAction event =
                        new PixelAction(action.getType(), PixelAction.STATUS.DONE, ctrName, action.getState(), action.getId());
                    this.cep.injectEvent(event);
                }
                catch(final ExecutorTriggerException e) {
                    this.injectFailure(action, STATUS.FAILED, "Not able to resume trigger", e);
                }
            }
            finally {
                if(t != null) {
                    try {
                        HoldTriggerInfoPublisher.remove(t);
                    }
                    catch(final RepositoryNotFoundException | InfoNotFoundException e) {
                        PixelActionExecutor.log.error("Failed to remove IS information about trigger on hold: " + e, e);
                    }
                }                
            }
        }
    }

    private void injectFailure(final PixelAction pixAct, final STATUS status, final String reason, final Exception e) {
        final PixelAction event = new PixelAction(pixAct.getType(), status, pixAct.getController(), pixAct.getState(), pixAct.getId());
        if(e != null) {
            event.setError(reason, e);
        } else {
            event.setError(reason);
        }

        this.cep.injectEvent(event);
    }

    private void sendErrorToIGUI(final String msg) {
        try {
            this.execIGUI.showError(msg);
        }
        catch(final ExecutorIGUIException ex) {
            PixelActionExecutor.log.error("Failed to send message to the IGUI", ex);
        }
    }
}
