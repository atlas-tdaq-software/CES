package chip.subscriber.auto;

import java.util.Map;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import rc.Ready4PhysicsInfoNamed;
import TRIGGER.AutoPrescalerCommander;
import TRIGGER.CommandExecutionFailed;
import chip.CEPService;
import chip.CHIPException;
import chip.CHIPException.DAQConfigurationError;
import chip.CHIPException.ExecutorIGUIException;
import chip.CHIPException.ExecutorISException;
import chip.CHIPException.ExecutorRCException;
import chip.CHIPException.ExecutorTriggerException;
import chip.CHIPException.InfoNotFound;
import chip.CHIPException.PublishException;
import chip.DAQConfigReader;
import chip.event.issue.Issue;
import chip.event.issue.auto.WarmStart;
import chip.event.issue.auto.WarmStart.STATUS;
import chip.executor.ExecutorIGUI;
import chip.executor.ExecutorIS;
import chip.executor.ExecutorRC;
import chip.executor.ExecutorTrigger;
import chip.msg.Common;
import chip.subscriber.DeferredActionExecutor;
import daq.EsperUtils.ISReader;
import daq.EsperUtils.TypedObject;


public class WarmStartExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(WarmStartExecutor.class);

    private final ExecutorRC execRC;
    private final ExecutorIGUI execIGUI;
    private final ExecutorTrigger execTrigger;
    private final DAQConfigReader daqReader;
    private final AtomicBoolean isWarmStartOnGoing = new AtomicBoolean(false);

    // The fairness is set to true in order to guarantee the order of execution
    // in case multiple events are queued
    private final ReentrantLock lock = new ReentrantLock(true);

    @Value("#{configuration['chip.ws.broadcast.timeout']}")
    Integer broadcastTimeout;
 
    @Value("#{configuration['chip.ws.beamspot.controller']}")
    String beamSpotController;
    
    @Value("#{configuration['chip.ws.beamspot.app.warm.stop.1']}")
    String appWarmStop_1;
        
    @Value("#{configuration['chip.ws.beamspot.app.warm.start']}")
    String appWarmStart;
    
    public WarmStartExecutor(final ExecutorRC execRC,
                             final ExecutorIGUI execIGUI,
                             final ExecutorTrigger execTrigger,
                             final DAQConfigReader daqReader)
    {
        this.execRC = execRC;
        this.execIGUI = execIGUI;
        this.execTrigger = execTrigger;
        this.daqReader = daqReader;
    }

    @Override
    public void doUpdate(final Issue issue) {
        final WarmStart warmStart = (WarmStart) issue;

        if((warmStart.getType().equals(WarmStart.TYPE.WARM_START) == true)
           && (this.isWarmStartOnGoing.compareAndSet(false, true) == false))
        {
            this.injectFailure(warmStart, STATUS.FAILED, "a Warm Start procedure is already on-going");
        } else {
            this.lock.lock();

            try {
                switch(warmStart.getType()) {
                    case WARM_START:
                        try {
                            this.warmStart(warmStart);
                        }
                        finally {
                            this.isWarmStartOnGoing.set(false);
                        }
                        break;
                    case WARM_STOP:
                        this.warmStop(warmStart);
                        break;
                    default:
                        break;
                }
            }
            finally {
                this.lock.unlock();
            }
        }
    }

    private void injectFailure(final WarmStart warmStart, final STATUS status, final String reason) {
        this.injectFailure(warmStart, status, reason, null);
    }

    private void injectFailure(final WarmStart warmStart, final STATUS status, final String reason, final Exception e) {
        final WarmStart event = new WarmStart(warmStart.getType(), status, warmStart.getState());
        if(e != null) {
            event.setError(reason, e);
        } else {
            event.setError(reason);
        }

        this.cep.injectEvent(event);
    }

    // This is executed in ExpertSystemServant.transitionDone
    // It is executed when the INITIAL state is reached
    // It must be executed synchronously because the action must be completed
    // before the transition is done 
    public void resetR4P() {
        // publish ATLAS_READY = false
        try {
            this.setAtlasReady(false);
        }
        catch(final InfoNotFound | PublishException e) {
            ers.Logger.error(new Common("Failed to publish RunParams.Ready4Physics IS info: " + e, e));
        }
    }

    private void warmStart(final WarmStart warmStart) {
        final String partitionName = this.daqReader.getPartitionName();

        final TypedObject l1PsTypedObj = ExecutorIS.getPrescales(partitionName, ExecutorIS.PRESCALE_TYPE.PHYSICS_L1);
        if(l1PsTypedObj == null) {
            WarmStartExecutor.log.error("Failed to read L1PsKey from IS.");
        }

        final TypedObject hltPsTypedObj = ExecutorIS.getPrescales(partitionName, ExecutorIS.PRESCALE_TYPE.PHYSICS_HLT);
        if(hltPsTypedObj == null) {
            WarmStartExecutor.log.error("Failed to read HltPsKey from IS.");
        }

        if((l1PsTypedObj == null) || (hltPsTypedObj == null)) {
            try {
                this.execIGUI.showError("Failed to execute warm start: Could not find the L1Prescales and HltPrescales physics keys in IS");
            }
            catch(final ExecutorIGUIException e) {
                this.injectFailure(warmStart, STATUS.PROBLEM, "Failed to show IGUI error popup", e);
            }

            this.injectFailure(warmStart, STATUS.FAILED, "Could not find the L1Prescales and HltPrescales physics keys in IS");

            return;
        }

        final int l1Ps = l1PsTypedObj.getInteger().intValue();
        final int hltPs = hltPsTypedObj.getInteger().intValue();

        // search for AutoPrescaler in the partition
        String autoPrescalerName = null;
        AutoPrescalerCommander autoPrescaler = null;
        try {
            autoPrescalerName = ExecutorIS.getAutoPrescalerName(partitionName);
        }
        catch(final ExecutorISException e) {
            this.injectFailure(warmStart, STATUS.PROBLEM, "Not able to get autoprescaler from IPC", e);
            // return;
        }

        if(autoPrescalerName == null) {
            this.injectFailure(warmStart, STATUS.PROBLEM, "No autoprescaler found in partition");
            // return;
        } else {
            // create autoprescaler commander
            autoPrescaler = new AutoPrescalerCommander(new ipc.Partition(partitionName), autoPrescalerName, "CHIP");

            try {
                autoPrescaler.hold();
            }
            catch(final CommandExecutionFailed e) {
                this.injectFailure(warmStart, STATUS.PROBLEM, "Not able to hold the autoprescaler", e);
                // return;
            }
        }

        // set prescales
        try {
            this.execTrigger.setHLTPrescales(l1Ps, hltPs);
        }
        catch(final ExecutorTriggerException e) {
            try {
                if(autoPrescaler != null) {
                    autoPrescaler.resume();
                }
            }
            catch(final CommandExecutionFailed e1) {
                this.injectFailure(warmStart, STATUS.PROBLEM, "Failed to resume autoPrescaler after failed warm start", e1);
            }

            this.injectFailure(warmStart, STATUS.FAILED, "Failed to set the physics prescales", e);

            return;
        }

        // publish ATLAS Ready
        try {
            this.setAtlasReady(true);
        }
        catch(InfoNotFound | PublishException e) {
            this.injectFailure(warmStart, STATUS.PROBLEM, "Failed to publish RunParams.Ready4Physics IS info", e);
        }

        // increase lumi block
        try {
            this.execTrigger.increaseLumiBlock();
        }
        catch(final ExecutorTriggerException e) {
            this.injectFailure(warmStart, STATUS.PROBLEM, "Failed to increase the luminosity block at end of warm start", e);
        }
        
        // Broadcast command here
        // This blocks until the command is done
        this.broadcastCommand(warmStart);
        
        // start BeamSpotInvalidator_HLTParameters application
        try {
            this.execRC.startApps(this.beamSpotController, new String[] { this.appWarmStart });
        }
        catch(final ExecutorRCException e) {
            this.injectFailure(warmStart,
                               STATUS.PROBLEM,
                               "Failed to request start of BeamSpotInvalidator_HLTParameters from controller BeamSpotController",
                               e);
        }

        // resume auto prescaler
        try {
            if(autoPrescaler != null) {
                autoPrescaler.resume();
            }
        }
        catch(final CommandExecutionFailed e) {
            this.injectFailure(warmStart, STATUS.PROBLEM, "Failed to resume auto prescaler at end of warm start", e);
            // return;
        }

        // Restart WarmStartStopReactor applications
        this.restartApps(warmStart);

        final WarmStart event = new WarmStart(warmStart.getType(), STATUS.DONE, warmStart.getState());
        this.cep.injectEvent(event);
    }

    private void warmStop(final WarmStart warmStart) {
        // publish ATLAS_READY = false
        try {
            this.setAtlasReady(false);
        }
        catch(InfoNotFound | PublishException e) {
            this.injectFailure(warmStart, STATUS.PROBLEM, "Failed to publish RunParams.Ready4Physics IS info", e);
        }

        final String partitionName = this.daqReader.getPartitionName();

        final TypedObject l1PsTypedObj = ExecutorIS.getPrescales(partitionName, ExecutorIS.PRESCALE_TYPE.STANDBY_L1);

        if(l1PsTypedObj == null) {
            WarmStartExecutor.log.error("Failed to read L1PsKey from IS.");
        }

        final TypedObject hltPsTypedObj = ExecutorIS.getPrescales(partitionName, ExecutorIS.PRESCALE_TYPE.STANDBY_HLT);
        if(hltPsTypedObj == null) {
            WarmStartExecutor.log.error("Failed to read HltPsKey from IS.");
        }

        if((l1PsTypedObj == null) || (hltPsTypedObj == null)) {
            try {
                this.execIGUI.showError("Failed to execute warm stop: Could not find the L1Prescales and HltPrescales standby keys in IS");
            }
            catch(final ExecutorIGUIException e) {
                this.injectFailure(warmStart, STATUS.PROBLEM, "Failed to show IGUI error popup", e);
            }

            this.injectFailure(warmStart, STATUS.FAILED, "Could not find the L1Prescales and HltPrescales standby keys in IS");

            return;
        }

        final int l1Ps = l1PsTypedObj.getInteger().intValue();
        final int hltPs = hltPsTypedObj.getInteger().intValue();

        // search for AutoPrescaler in the partition
        String autoPrescalerName = null;
        AutoPrescalerCommander autoPrescaler = null;
        try {
            autoPrescalerName = ExecutorIS.getAutoPrescalerName(partitionName);
        }
        catch(final ExecutorISException e) {
            this.injectFailure(warmStart, STATUS.PROBLEM, "Not able to get autoprescaler from IPC", e);
            // return;
        }

        if(autoPrescalerName == null) {
            this.injectFailure(warmStart, STATUS.PROBLEM, "No autoprescaler found in partition");
            // return;
        } else {
            // create autoprescaler commander
            autoPrescaler = new AutoPrescalerCommander(new ipc.Partition(partitionName), autoPrescalerName, "CHIP");

            try {
                autoPrescaler.hold();
            }
            catch(final CommandExecutionFailed e) {
                this.injectFailure(warmStart, STATUS.PROBLEM, "Not able to hold the autoprescaler", e);
                // return;
            }
        }

        // set prescales
        try {
            this.execTrigger.setHLTPrescales(l1Ps, hltPs);
        }
        catch(final ExecutorTriggerException e) {
            try {
                if(autoPrescaler != null) {
                    autoPrescaler.resume();
                }
            }
            catch(final CommandExecutionFailed e1) {
                this.injectFailure(warmStart, STATUS.PROBLEM, "Failed to resume autoPrescaler after failed warm stop", e1);
            }

            this.injectFailure(warmStart, STATUS.FAILED, "Failed to set the physics prescales", e);

            return;
        }

        // increase lumi block
        try {
            this.execTrigger.increaseLumiBlock();
        }
        catch(final ExecutorTriggerException e) {
            this.injectFailure(warmStart, STATUS.PROBLEM, "Failed to increase the luminosity block at end of warm stop", e);
        }
        
        // Send broadcast here
        // This blocks until the command is done
        this.broadcastCommand(warmStart);

        // start BeamSpotInvalidator_HLTParameters application
        try {
            this.execRC.startApps(this.beamSpotController, new String[] { this.appWarmStop_1 });
        }
        catch(final ExecutorRCException e) {
            this.injectFailure(warmStart, STATUS.PROBLEM, "Failed to request start of BeamSpotInvalidator_HLTParameters "
                                                          + "and BeamSpotArchiver_PerBunchLiveMon from controller BeamSpotController", e);
        }

        // resume auto prescaler
        try {
            if(autoPrescaler != null) {
                autoPrescaler.resume();
            }
        }
        catch(final CommandExecutionFailed e) {
            this.injectFailure(warmStart, STATUS.PROBLEM, "Failed to resume auto prescaler at end of warm stop", e);
            // return;
        }

        // Restart WarmStartStopReactor applications
        this.restartApps(warmStart);

        final WarmStart event = new WarmStart(warmStart.getType(), STATUS.DONE, warmStart.getState());
        this.cep.injectEvent(event);
    }

    private void restartApps(final WarmStart event) {
        final Map<String, ConcurrentSkipListSet<String>> apps = (event.getType() == WarmStart.TYPE.WARM_START
                                                                                                             ? this.daqReader.getWarmStartApps()
                                                                                                             : this.daqReader.getWarmStopApps());
        for(final Map.Entry<String, ConcurrentSkipListSet<String>> me : apps.entrySet()) {
            try {
                this.execRC.restartApps(me.getKey(), me.getValue().toArray(new String[me.getValue().size()]));
            }
            catch(final ExecutorRCException ex) {
                this.injectFailure(event, STATUS.PROBLEM, "Some applications could not be restarted", ex);
            }
        }
    }

    private void setAtlasReady(final boolean ready) throws InfoNotFound, PublishException {
        final ipc.Partition partition = new ipc.Partition(this.daqReader.getPartitionName());

        final String partitionName = this.daqReader.getPartitionName();

        final TypedObject runNumberTypedObj = ISReader.getInfoByName(partitionName, "RunParams.LumiBlock", "RunNumber");
        final TypedObject lbNumberTypedObj = ISReader.getInfoByName(partitionName, "RunParams.LumiBlock", "LumiBlockNumber");

        if(runNumberTypedObj == null) {
            throw new CHIPException.InfoNotFound("Failed to get run number from IS", null);
        }

        if(lbNumberTypedObj == null) {
            throw new InfoNotFound("Failed to get lumi block number from IS", null);
        }

        try {
            final Ready4PhysicsInfoNamed r4physics = new rc.Ready4PhysicsInfoNamed(partition, "RunParams.Ready4Physics");
            r4physics.ready4physics = ready;
            r4physics.run_number = runNumberTypedObj.getInteger().intValue();
            r4physics.lumi_block = lbNumberTypedObj.getInteger().intValue() + 1;
            r4physics.checkin();
        }
        catch(final Exception e) {
            throw new CHIPException.PublishException("Failed to publish RunParams.Ready4Physics IS info", e);
        }
    }

    private void broadcastCommand(final WarmStart event) {
        final String command = (event.getType() == WarmStart.TYPE.WARM_START) ? "WarmStart" : "WarmStop";

        try {
            this.execRC.sendUserBroadcast(this.daqReader.getRootController().UID(),
                                          command,
                                          new String[] {},
                                          Boolean.TRUE,
                                          this.broadcastTimeout.intValue());
        }
        catch(final ExecutorRCException | DAQConfigurationError ex) {
            this.injectFailure(event, STATUS.PROBLEM, "Problem broadcasting the command \"" + command + "\" to the RC tree", ex);
        }
    }
}
