package chip.subscriber.auto;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Value;

import com.espertech.esper.common.client.configuration.ConfigurationException;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;

import RCUtils.tools.JVMMonitor;
import chip.CEPService;
import chip.event.issue.Issue;
import chip.event.issue.auto.EventEnableCHIPMetricsIs;
import chip.subscriber.ActionExecutor;


public class SubscriberEnableCHIPMetricsIs implements ActionExecutor {
    // Using SpEL to get properties list of files
    @Value("#{configuration['chip.isServerChipMetrics']}")
    private String server_name;

    private final CEPService cep;
    private volatile JVMMonitor jvmMonitor = null;

    public SubscriberEnableCHIPMetricsIs(final CEPService cep) {
        this.cep = cep;
        
        // Remove published IS information at exit
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                final JVMMonitor jm = SubscriberEnableCHIPMetricsIs.this.jvmMonitor;
                if(jm != null) {
                    jm.stopAndClean();
                }
            }
        });
    }

    @Override
    public void update(final Issue originalEvent) {
        final EventEnableCHIPMetricsIs event = (EventEnableCHIPMetricsIs) originalEvent;
        final long interval = event.getInterval();

        JVMMonitor jvm = this.jvmMonitor;
        if(jvm == null) {
            synchronized(this) {
                jvm = this.jvmMonitor;
                if(jvm == null) {
                    // Note, server_name is available only after the constructor has been fully executed
                    jvm = (this.jvmMonitor = new JVMMonitor(this.server_name));
                }
            }
        }
                
        // Enable/disable JVM statistics
        if(event.getType() == EventEnableCHIPMetricsIs.TYPE.ENABLE) {
            try {
                jvm.restart(interval, TimeUnit.MILLISECONDS);
            }
            catch(final IllegalArgumentException ex) {
                final EventEnableCHIPMetricsIs problem = new EventEnableCHIPMetricsIs(event.getType(),
                                                                                      EventEnableCHIPMetricsIs.STATUS.PROBLEM,
                                                                                      interval);
                problem.setError("Cannot enable JVM statistics: interval is less than 0");
                this.cep.injectEventSynch(problem);
            }
        } else {
            jvm.stop();
        }

        // Enable/disable Esper metrics
        try {
            // Disable first, otherwise double counting may happen
            this.cep.getEsper().getMetricsService().setMetricsReportingInterval(null, interval);
            this.cep.getEsper().getMetricsService().setMetricsReportingDisabled();
                        
            if(event.getType() == EventEnableCHIPMetricsIs.TYPE.ENABLE) {
                this.cep.getEsper().getMetricsService().setMetricsReportingEnabled();
            }

            this.cep.injectEventSynch(new EventEnableCHIPMetricsIs(event.getType(), EventEnableCHIPMetricsIs.STATUS.DONE, interval));
        }
        catch(final ConfigurationException | EPRuntimeDestroyedException e) {
            final EventEnableCHIPMetricsIs failedEvent = new EventEnableCHIPMetricsIs(event.getType(),
                                                                                      EventEnableCHIPMetricsIs.STATUS.FAILED,
                                                                                      interval);
            failedEvent.setError("ESPER configuration Exception: " + e, e);
            this.cep.injectEventSynch(failedEvent);
        }
    }
}
