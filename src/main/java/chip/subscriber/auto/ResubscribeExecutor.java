package chip.subscriber.auto;

import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.CHIP;
import chip.event.issue.Issue;
import chip.subscriber.DeferredActionExecutor;


public class ResubscribeExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    private final CHIP ces;

    public ResubscribeExecutor(final CHIP ces) {
        this.ces = ces;
    }

    @Override
    public void doUpdate(final Issue issue) {
        // Restart failed injectors here
        this.ces.retryStartInjectors();
    }
}
