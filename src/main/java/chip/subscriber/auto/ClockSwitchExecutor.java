package chip.subscriber.auto;

import is.InfoNotCompatibleException;
import is.InfoNotFoundException;
import is.RepositoryNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import rc.HoldTriggerInfoNamed.CausedBy;
import rc.HoldTriggerInfoNamed.Reason;
import chip.CEPService;
import chip.CHIPException.DAQConfigurationError;
import chip.CHIPException.ExecutorIGUIException;
import chip.CHIPException.ExecutorRCException;
import chip.CHIPException.ExecutorTriggerException;
import chip.CHIPException.WaitTimeoutException;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.config.RCApplicationConfig;
import chip.event.en.FSM;
import chip.event.issue.Issue;
import chip.event.issue.auto.ClockSwitch;
import chip.event.issue.auto.ClockSwitch.STATUS;
import chip.executor.ExecutorIGUI;
import chip.executor.ExecutorRC;
import chip.executor.ExecutorTrigger;
import chip.msg.Common;
import chip.subscriber.DeferredActionExecutor;
import chip.utils.ExecutorFactory;
import chip.utils.HoldTriggerInfoPublisher;
import chip.utils.HoldTriggerInfoPublisher.Token;
import daq.EsperUtils.ISReader;
import daq.EsperUtils.TypedObject;
import daq.rc.Command.FSMCommands;
import daq.rc.RCException.CORBAException;


public class ClockSwitchExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    private final static Log log = LogFactory.getLog(ClockSwitchExecutor.class);

    private final static String L1_CALO_CONTROLLER_DEFAULT = "l1calo-processor";
    private final static String L1_CALO_RESYNC_CMD_DEFAULT = "l1caloResync";
    private final static int L1_CALO_TIMEOUT_DEFAULT = 60;

    private final static String CSR_CMD_DEFAULT = "clockSwitchReconf";
    
    private final static String AFP_CONTROLLER_DEFAULT = "AFP-RCD-RCE-P1-VLDB";
    private final static int AFP_TIMEOUT_DEFAULT = 15;    
    
    private final static String ZDC_CONTROLLER_DEFAULT = "ZDC-RCD-CRATE-BOTTOM";
    private final static int ZDC_TIMEOUT_DEFAULT = 15;    

    private final static String MUCTPI_CONTROLLER_DEFAULT = "MuctpiController";
    private final static int MUCTPI_TIMEOUT_DEFAULT = 15;    

    private final static String NSW_CONTROLLER_DEFAULT = "NSW-ClockSwitchReceiver";
    private final static int NSW_TIMEOUT_DEFAULT = 15;    
 
    private final static String LAR_CONTROLLER_DEFAULT = "LAr-rcApp";
    private final static int LAR_TIMEOUT_DEFAULT = 15;    
    
    private final static int CTP_TIMEOUT_DEFAULT = 15;    
    
    @Value("#{configuration['chip.cs.l1calo.controller']}")
    private String L1_CALO_CONTROLLER;

    @Value("#{configuration['chip.cs.l1calo.cmd']}")
    private String L1_CALO_RESYNC_CMD;

    @Value("#{configuration['chip.cs.l1calo.1.timeout']}")
    private Integer l1Calo1Timeout;

    @Value("#{configuration['chip.cs.l1calo.2.timeout']}")
    private Integer l1Calo2Timeout;

    @Value("#{configuration['chip.cs.l1calo.3.timeout']}")
    private Integer l1Calo3Timeout;

    @Value("#{configuration['chip.cs.l1calo.fail.timeout']}")
    private Integer l1CaloFailTimeout;

    @Value("#{configuration['chip.cs.resynch.cmd']}")
    private String CSR_CMD;
    
    @Value("#{configuration['chip.cs.afp.controller']}")
    private String AFP_CONTROLLER;

    @Value("#{configuration['chip.cs.afp.timeout']}")
    private Integer afpTimeout;

    @Value("#{configuration['chip.cs.zdc.controller']}")
    private String ZDC_CONTROLLER;

    @Value("#{configuration['chip.cs.zdc.timeout']}")
    private Integer zdcTimeout;  

    @Value("#{configuration['chip.cs.muctpi.controller']}")
    private String MUCTPI_CONTROLLER;

    @Value("#{configuration['chip.cs.muctpi.timeout']}")
    private Integer muctpiTimeout;      

    @Value("#{configuration['chip.cs.nsw.controller']}")
    private String NSW_CONTROLLER;

    @Value("#{configuration['chip.cs.lar.controller']}")
    private String LAR_CONTROLLER;    
    
    @Value("#{configuration['chip.cs.nsw.timeout']}")
    private Integer nswTimeout;      
    
    @Value("#{configuration['chip.cs.lar.timeout']}")
    private Integer larTimeout;         
    
    @Value("#{configuration['chip.cs.ctp.timeout']}")
    private Integer ctpTimeout;  
    
    private enum CSR_CMD_ARG {
        BrefToBC1,
        BC1ToBref
    }
        
    private enum EXIT_REASON {
        FAILURE,
        SUCCESS
    }

    private final ExecutorRC execRC_partition;
    private final ExecutorRC execRC_initialL1CT;
    private final ExecutorIGUI execIGUI;
    private final ExecutorTrigger execTrigger;
    private final DAQConfigReader daqReader;

    private final static ThreadPoolExecutor resynchTp = ExecutorFactory.newThreadPool("ClockSwitchResynch", 5, true);
    
    public ClockSwitchExecutor(final ExecutorRC execRC_partition,
                               final ExecutorRC execRC_initialL1CT,
                               final ExecutorIGUI execIGUI,
                               final ExecutorTrigger execTrigger,
                               final EsperDAO dao,
                               final DAQConfigReader daqReader)
    {
        this.execRC_partition = execRC_partition;
        this.execRC_initialL1CT = execRC_initialL1CT;
        this.execIGUI = execIGUI;
        this.execTrigger = execTrigger;
        this.daqReader = daqReader;
    }

    @Override
    public void doUpdate(final Issue issue) {
        final ClockSwitch clockSwitch = (ClockSwitch) issue;

        switch(clockSwitch.getType()) {
            case SELECT_INTERNAL:
                this.selectInternalClock(clockSwitch);
                break;
            case SELECT_LHC:
                this.selectLhcClock(clockSwitch);
                break;
            case SELECT_INTERNAL_AND_DO_UNCONFIG:
                this.selectInternalAndUnconfig(clockSwitch);
                break;
            default:
                break;
        }
    }

    // This is executed in ExpertSystemServant.transitionDone
    // It is executed when the NONE state is reached
    // It must be executed synchronously because the action must be completed
    // before the transition is done 
    public void setAutomaticClock() {
        final rc.AutomaticClockNamed clk = new rc.AutomaticClockNamed(new ipc.Partition(this.daqReader.getPartitionName()),
                                                                      "RunParams.AutomaticClock");
        clk.value = true;

        try {
            clk.checkin();
        }
        catch(final RepositoryNotFoundException | InfoNotCompatibleException ex) {
            ers.Logger.error(new Common("Not able to set clock switch to automatic mode: " + ex, ex));
        }
    }

    private synchronized void selectInternalAndUnconfig(final ClockSwitch clockSwitch) {
        ers.Logger.info(new Common("The clock has to be switched back to INTERNAL since the beam mode changed to "
                                   + clockSwitch.getBeamMode() + ". Afterwards ATLAS will be unconfigured."));

        final EXIT_REASON exit = this.selectInternalClock(clockSwitch);

        if(exit == EXIT_REASON.FAILURE) {
            // Logging of problem done in selectInternalClock method
            return;
        }

        try {
            final String rootName = this.daqReader.getRootController().UID();
            this.execRC_partition.makeTransition(rootName, FSMCommands.UNCONFIG);
        }
        catch(final ExecutorRCException | DAQConfigurationError e) {
            final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                      STATUS.FAILED,
                                                      clockSwitch.getBeamMode(),
                                                      clockSwitch.getState(),
                                                      clockSwitch.getId());
            event.setError("Not able to send unconfig to the Root Controller", e);
            this.cep.injectEvent(event);
            return;
        }

        final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                  STATUS.DONE,
                                                  clockSwitch.getBeamMode(),
                                                  clockSwitch.getState(),
                                                  clockSwitch.getId());
        this.cep.injectEvent(event);
    }

    private synchronized void selectLhcClock(final ClockSwitch clockSwitch) {
        final Boolean rcStateIsRunning = (clockSwitch.getState() == FSM.RUNNING) ? Boolean.TRUE : Boolean.FALSE;

        TypedObject externalClockChoiceTypedObj = null;
        Exception cause = null;
        try {
            externalClockChoiceTypedObj = ISReader.getInfoByIndex("initial", "LHC.ExternalClockChoice", 0);
        }
        catch(final Exception ex) {
            cause = ex;
        }

        if(externalClockChoiceTypedObj == null) {
            final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                      STATUS.FAILED,
                                                      clockSwitch.getBeamMode(),
                                                      clockSwitch.getState(),
                                                      clockSwitch.getId());

            final String errMsg = "Could not retrieve new keys for clock from IS! Check if IS object LHC.ExternalClockCoice exists";
            if(cause == null) {
                event.setError(errMsg);
            } else {
                event.setError(errMsg, cause);
            }
            this.cep.injectEvent(event);

            return;
        }

        final String externalClockChoice = externalClockChoiceTypedObj.getString();

        if(externalClockChoice.isEmpty()) {
            final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                      STATUS.FAILED,
                                                      clockSwitch.getBeamMode(),
                                                      clockSwitch.getState(),
                                                      clockSwitch.getId());
            event.setError("Could not retrieve new keys for clock from IS! Check if IS object LHC.ExternalClockCoice exists");
            this.cep.injectEvent(event);
            return;
        }

        if(rcStateIsRunning.booleanValue() == true) {
            Token publishToken = null;            
            try {
                this.execTrigger.hold();
                publishToken = this.publishHoldInfo(clockSwitch);

                ers.Logger.info(new Common("Sending resynch command to L1Calo before changing clock to external."));

                this.sendL1CaloResync_pre(clockSwitch);

                ers.Logger.info(new Common("1 second before changing clock to external."));

                try {
                    Thread.sleep(1000);
                }
                catch(final InterruptedException e) {
                    final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                              STATUS.PROBLEM,
                                                              clockSwitch.getBeamMode(),
                                                              clockSwitch.getState(),
                                                              clockSwitch.getId());
                    event.setError("Problem while sleeping", e);
                    this.cep.injectEvent(event);
                    
                    // Restore the state
                    Thread.currentThread().interrupt();
                }
            }
            catch(final ExecutorTriggerException e) {
                final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                          STATUS.FAILED,
                                                          clockSwitch.getBeamMode(),
                                                          clockSwitch.getState(),
                                                          clockSwitch.getId());
                event.setError("Failed to hold the trigger", e);
                this.cep.injectEvent(event);
                return;
            }
            finally {
                if(publishToken != null) {
                    try {
                        HoldTriggerInfoPublisher.remove(publishToken);
                    }
                    catch(final RepositoryNotFoundException | InfoNotFoundException e) {
                        ClockSwitchExecutor.log.error("Failed to remove IS information about trigger on hold: " + e, e);
                    }
                }
            }
        }

        boolean failed = false;
        try {
            this.execRC_initialL1CT.sendUserCommand("RF2TTCApp", "SELECT", externalClockChoice.split("\\s+"), Boolean.TRUE, 60);
        }
        catch(final ExecutorRCException e) {
            // Send special resynch to L1Calo because of the failure
            this.sendL1CaloResync_failure(clockSwitch);

            final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                      STATUS.FAILED,
                                                      clockSwitch.getBeamMode(),
                                                      clockSwitch.getState(),
                                                      clockSwitch.getId());
            event.setError("Failed to talk to RF2TTCApp in initialL1CT partition! Could not switch clock to " + externalClockChoice, e);
            this.cep.injectEvent(event);

            failed = true;
        }
        finally {
            if(rcStateIsRunning.booleanValue() == true) {
                if(failed == false) {
                    try {
                        Thread.sleep(3000);
                    }
                    catch(final InterruptedException e) {
                        final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                                  STATUS.PROBLEM,
                                                                  clockSwitch.getBeamMode(),
                                                                  clockSwitch.getState(),
                                                                  clockSwitch.getId());
                        event.setError("Problem while sleeping", e);
                        this.cep.injectEvent(event);
                        
                        // Restore the state
                        Thread.currentThread().interrupt();
                    }

                    this.sendResynchsParallel(clockSwitch, CSR_CMD_ARG.BrefToBC1);                    
                }

                try {
                    this.execTrigger.resume();
                }
                catch(final ExecutorTriggerException e) {
                    if(failed == false) {
                        final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                                  STATUS.FAILED,
                                                                  clockSwitch.getBeamMode(),
                                                                  clockSwitch.getState(),
                                                                  clockSwitch.getId());
                        event.setError("Failed to resume the trigger", e);
                        this.cep.injectEvent(event);

                        failed = true;
                    } else {
                        ClockSwitchExecutor.log.error("Failed to resume the trigger", e);
                    }

                    this.sendErrorToIGUI("Failed to resume the trigger after a clock switch; a manual intervention is needed");
                }
            }
        }

        if(failed == false) {
            final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                      STATUS.DONE,
                                                      clockSwitch.getBeamMode(),
                                                      clockSwitch.getState(),
                                                      clockSwitch.getId());
            this.cep.injectEvent(event);
        }
    }

    private synchronized EXIT_REASON selectInternalClock(final ClockSwitch clockSwitch) {
        final Boolean rcIsRunning = (clockSwitch.getState() == FSM.RUNNING) ? Boolean.TRUE : Boolean.FALSE;
        if(rcIsRunning.booleanValue()) {
            Token publishToken = null;
            try {
                this.execTrigger.hold();
                publishToken = this.publishHoldInfo(clockSwitch);

                ers.Logger.info(new Common("Sending resynch command to L1Calo before changing clock to internal."));

                this.sendL1CaloResync_pre(clockSwitch);

                ers.Logger.info(new Common("1 second before changing clock to internal"));

                try {
                    Thread.sleep(1000);
                }
                catch(final InterruptedException e) {
                    final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                              STATUS.PROBLEM,
                                                              clockSwitch.getBeamMode(),
                                                              clockSwitch.getState(),
                                                              clockSwitch.getId());
                    event.setError("Problem while sleeping", e);
                    this.cep.injectEvent(event);
                    
                    // Restore the state
                    Thread.currentThread().interrupt();
                }
            }
            catch(final ExecutorTriggerException e) {
                final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                          STATUS.FAILED,
                                                          clockSwitch.getBeamMode(),
                                                          clockSwitch.getState(),
                                                          clockSwitch.getId());
                event.setError("Failed to hold the trigger.", e);
                this.cep.injectEvent(event);
                return EXIT_REASON.FAILURE;
            }
            finally {
                if(publishToken != null) {
                    try {
                        HoldTriggerInfoPublisher.remove(publishToken);
                    }
                    catch(final RepositoryNotFoundException | InfoNotFoundException e) {
                        ClockSwitchExecutor.log.error("Failed to remove IS information about trigger on hold: " + e, e);
                    }
                }
            }
        }

        boolean failed = false;
        try {
            this.execRC_initialL1CT.sendUserCommand("RF2TTCApp", "SELECT", new String[] {"BCREF", "INT"}, Boolean.TRUE, 60);
        }
        catch(final ExecutorRCException e) {
            // Send command to L1Calo because of the failure
            this.sendL1CaloResync_failure(clockSwitch);

            final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                      STATUS.FAILED,
                                                      clockSwitch.getBeamMode(),
                                                      clockSwitch.getState(),
                                                      clockSwitch.getId());
            event.setError("Failed to talk to RF2TTCApp in initialL1CT partition! Could not switch clock to BCREF", e);
            this.cep.injectEvent(event);

            failed = true;
        }
        finally {
            if(rcIsRunning.booleanValue() == true) {
                if(failed == false) {
                    try {
                        Thread.sleep(3000);
                    }
                    catch(final InterruptedException e) {
                        final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                                  STATUS.PROBLEM,
                                                                  clockSwitch.getBeamMode(),
                                                                  clockSwitch.getState(),
                                                                  clockSwitch.getId());
                        event.setError("Problem while sleeping", e);
                        this.cep.injectEvent(event);
                        
                        // Restore the state
                        Thread.currentThread().interrupt();
                    }

                    this.sendResynchsParallel(clockSwitch, CSR_CMD_ARG.BC1ToBref);                    
                }

                try {
                    this.execTrigger.resume();
                }
                catch(final ExecutorTriggerException e) {
                    if(failed == false) {
                        final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                                  STATUS.FAILED,
                                                                  clockSwitch.getBeamMode(),
                                                                  clockSwitch.getState(),
                                                                  clockSwitch.getId());
                        event.setError("Failed to resume the trigger", e);
                        this.cep.injectEvent(event);

                        failed = true;
                    } else {
                        ClockSwitchExecutor.log.error("Failed to resume the trigger", e);
                    }

                    this.sendErrorToIGUI("Failed to resume the trigger after a clock switch; a manual intervention is needed");
                }
            }
        }

        if(failed == false) {
            final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                      STATUS.DONE,
                                                      clockSwitch.getBeamMode(),
                                                      clockSwitch.getState(),
                                                      clockSwitch.getId());
            this.cep.injectEvent(event);
        }

        return((failed == false) ? EXIT_REASON.SUCCESS : EXIT_REASON.FAILURE);
    }

    private String l1caloController() {
        final String controllerName = ((this.L1_CALO_CONTROLLER != null) && (this.L1_CALO_CONTROLLER.isEmpty() == false)) ? this.L1_CALO_CONTROLLER 
                                                                                                                          : ClockSwitchExecutor.L1_CALO_CONTROLLER_DEFAULT;
        return controllerName;
    }
    
    private String l1caloCommand() {
        final String cmdName = ((this.L1_CALO_RESYNC_CMD != null) && (this.L1_CALO_RESYNC_CMD.isEmpty() == false)) ? this.L1_CALO_RESYNC_CMD
                                                                                                                   : ClockSwitchExecutor.L1_CALO_RESYNC_CMD_DEFAULT;        
        return cmdName;
    }    

    private String csrCommand() {
        final String cmdName = ((this.CSR_CMD != null) && (this.CSR_CMD.isEmpty() == false)) ? this.CSR_CMD
                                                                                             : ClockSwitchExecutor.CSR_CMD_DEFAULT;        
        return cmdName;
    }       
    
    private String afpController() {
        final String controllerName = ((this.AFP_CONTROLLER != null) && (this.AFP_CONTROLLER.isEmpty() == false)) ? this.AFP_CONTROLLER 
                                                                                                                  : ClockSwitchExecutor.AFP_CONTROLLER_DEFAULT;
        return controllerName;
    }
        
    private String zdcController() {
        final String controllerName = ((this.ZDC_CONTROLLER != null) && (this.ZDC_CONTROLLER.isEmpty() == false)) ? this.ZDC_CONTROLLER 
                                                                                                                  : ClockSwitchExecutor.ZDC_CONTROLLER_DEFAULT;
        return controllerName;
    }
            
    private String nswController() {
        final String controllerName = ((this.NSW_CONTROLLER != null) && (this.NSW_CONTROLLER.isEmpty() == false)) ? this.NSW_CONTROLLER 
                                                                                                                  : ClockSwitchExecutor.NSW_CONTROLLER_DEFAULT;
        return controllerName;
    }
    
    private String larController() {
        final String controllerName = ((this.LAR_CONTROLLER != null) && (this.LAR_CONTROLLER.isEmpty() == false)) ? this.LAR_CONTROLLER 
                                                                                                                  : ClockSwitchExecutor.LAR_CONTROLLER_DEFAULT;
        return controllerName;
    }   
    
    private String muctpiController() {
        final String controllerName = ((this.MUCTPI_CONTROLLER != null) && (this.MUCTPI_CONTROLLER.isEmpty() == false)) ? this.MUCTPI_CONTROLLER 
                                                                                                                        : ClockSwitchExecutor.MUCTPI_CONTROLLER_DEFAULT;
        return controllerName;
    }
              
    private void sendL1CaloResync_pre(final ClockSwitch clockSwitch) {
        final String controllerName = this.l1caloController();        
        final String cmdName = this.l1caloCommand();
        
        final Map<String, RCApplicationConfig> allApps = this.daqReader.getRCApplications();
        if(allApps.containsKey(controllerName) == true) {
            try {
                this.execRC_partition.sendUserBroadcast(controllerName,
                                                        cmdName,
                                                        new String[] {"0"},
                                                        Boolean.TRUE,
                                                        60);
            }
            catch(final ExecutorRCException ex) {
                final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                          STATUS.PROBLEM,
                                                          clockSwitch.getBeamMode(),
                                                          clockSwitch.getState(),
                                                          clockSwitch.getId());
                event.setError("Problem sending the resync \"0 \" command to L1Calo", ex);
                this.cep.injectEvent(event);
            }
        } else {
            ClockSwitchExecutor.log.info("Not sending resynch \"0\" command to " + controllerName + " because it is not part of the partition");
        }
    }

    private void sendL1CaloResync_failure(final ClockSwitch clockSwitch) {
        final String controllerName = this.l1caloController();        
        final String cmdName = this.l1caloCommand();

        final Map<String, RCApplicationConfig> allApps = this.daqReader.getRCApplications();
        if(allApps.containsKey(controllerName) == true) {
            try {
                this.execRC_partition.sendUserBroadcast(controllerName,
                                                        cmdName,
                                                        new String[] {"-1"},
                                                        Boolean.TRUE,
                                                        this.l1CaloFailTimeout != null ? this.l1CaloFailTimeout.intValue() : ClockSwitchExecutor.L1_CALO_TIMEOUT_DEFAULT);
            }
            catch(final ExecutorRCException ex) {
                final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                          STATUS.PROBLEM,
                                                          clockSwitch.getBeamMode(),
                                                          clockSwitch.getState(),
                                                          clockSwitch.getId());
                event.setError("Problem sending the resync \"-1\" command to L1Calo", ex);
                this.cep.injectEvent(event);
            }
        } else {
            ClockSwitchExecutor.log.info("Not sending resynch \"-1\" command to " + controllerName + " because it is not part of the partition");
        }
    }

    private void sendL1CaloResync(final ClockSwitch clockSwitch) {
        final String controllerName = this.l1caloController();        
        final String cmdName = this.l1caloCommand();
        
        final Map<String, RCApplicationConfig> allApps = this.daqReader.getRCApplications();
        if(allApps.containsKey(controllerName) == true) {
            short step = 0;

            try {
                step = 1;
                this.execRC_partition.sendUserBroadcast(controllerName,
                                                        cmdName,
                                                        new String[] {"1"},
                                                        Boolean.TRUE,
                                                        this.l1Calo1Timeout != null ? this.l1Calo1Timeout.intValue() : ClockSwitchExecutor.L1_CALO_TIMEOUT_DEFAULT);

                step = 2;
                this.execRC_partition.sendUserBroadcast(controllerName,
                                                        cmdName,
                                                        new String[] {"2"},
                                                        Boolean.TRUE,
                                                        this.l1Calo2Timeout != null ? this.l1Calo2Timeout.intValue() : ClockSwitchExecutor.L1_CALO_TIMEOUT_DEFAULT);

                step = 3;
                this.execRC_partition.sendUserBroadcast(controllerName,
                                                        cmdName,
                                                        new String[] {"3"},
                                                        Boolean.TRUE,
                                                        this.l1Calo3Timeout != null ? this.l1Calo3Timeout.intValue() : ClockSwitchExecutor.L1_CALO_TIMEOUT_DEFAULT);
            }
            catch(final ExecutorRCException ex) {
                // Send the resynch failure command to L1Calo when any of the previous command failed and:
                // - the failure is not due to a communication issue (most likely the application is not running)
                // - the failure is due to the elapsed timeout unless the last command has been sent
                if((CORBAException.class.isInstance(ex.getCause()) == false)
                   && ((WaitTimeoutException.class.isInstance(ex.getCause()) == false) || (step != 3)))
                {
                    this.sendL1CaloResync_failure(clockSwitch);
                }

                final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                          STATUS.PROBLEM,
                                                          clockSwitch.getBeamMode(),
                                                          clockSwitch.getState(),
                                                          clockSwitch.getId());
                event.setError("Problem sending the resync \"" + step + "\" command to L1Calo", ex);
                this.cep.injectEvent(event);
            }
        } else {
            ClockSwitchExecutor.log.info("Not sending resynch commands to " + controllerName + " because it is not part of the partition");
        }
    }
        
    private void sendCtpResync(final ClockSwitch clockSwitch, final CSR_CMD_ARG cmdArg) {
        final String cmdName = this.csrCommand();
        
        try {
            this.execTrigger.sendRCUserCommand(cmdName,
                                               new String[] {cmdArg.toString()},
                                               Boolean.TRUE,
                                               this.ctpTimeout != null ? this.ctpTimeout.intValue() : ClockSwitchExecutor.CTP_TIMEOUT_DEFAULT);
        }
        catch(final ExecutorRCException ex) {
            final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                      STATUS.PROBLEM,
                                                      clockSwitch.getBeamMode(),
                                                      clockSwitch.getState(),
                                                      clockSwitch.getId());
            event.setError("Problem sending the resync \"" + cmdArg.toString() + "\" command to CTP", ex);
            this.cep.injectEvent(event);
        }
    }    
    
    private void sendResync(final ClockSwitch clockSwitch, final CSR_CMD_ARG cmdArg, final String controllerName, final int timeout) {        
        final Map<String, RCApplicationConfig> allApps = this.daqReader.getRCApplications();
        if(allApps.containsKey(controllerName) == true) {
            try {
                this.execRC_partition.sendUserBroadcast(controllerName,
                                                        this.csrCommand(),
                                                        new String[] { cmdArg.toString() },
                                                        Boolean.TRUE,
                                                        timeout);
            }
            catch(final ExecutorRCException ex) {
                final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                          STATUS.PROBLEM,
                                                          clockSwitch.getBeamMode(),
                                                          clockSwitch.getState(),
                                                          clockSwitch.getId());
                event.setError("Problem sending the resync \"" + cmdArg.toString() + "\" command to \"" + controllerName + "\": ", ex);
                this.cep.injectEvent(event);
            }
        } else {
            ClockSwitchExecutor.log.info("Not sending resynch commands to " + controllerName + " because it is not part of the partition");
        }        
    }
    
    private void sendResynchsParallel(final ClockSwitch clockSwitch, final CSR_CMD_ARG cmdArg) {        
        try {
            // Long commands, execute them in parallel
            final List<Future<?>> tasks = new ArrayList<>();
                        
            tasks.add(ClockSwitchExecutor.resynchTp.submit(() -> {
                this.sendCtpResync(clockSwitch, cmdArg);
            }));

            tasks.add(ClockSwitchExecutor.resynchTp.submit(() -> {
                this.sendResync(clockSwitch,
                                cmdArg,
                                this.afpController(),
                                this.afpTimeout != null ? this.afpTimeout.intValue() : ClockSwitchExecutor.AFP_TIMEOUT_DEFAULT);
            }));
            
            tasks.add(ClockSwitchExecutor.resynchTp.submit(() -> {
                this.sendResync(clockSwitch,
                                cmdArg,
                                this.zdcController(),
                                this.zdcTimeout != null ? this.zdcTimeout.intValue() : ClockSwitchExecutor.ZDC_TIMEOUT_DEFAULT);
            }));
            
            tasks.add(ClockSwitchExecutor.resynchTp.submit(() -> {
                this.sendResync(clockSwitch,
                                cmdArg,
                                this.muctpiController(),
                                this.muctpiTimeout != null ? this.muctpiTimeout.intValue() : ClockSwitchExecutor.MUCTPI_TIMEOUT_DEFAULT);
            }));            
            
            tasks.add(ClockSwitchExecutor.resynchTp.submit(() -> {
                this.sendResync(clockSwitch,
                                cmdArg,
                                this.nswController(),
                                this.nswTimeout != null ? this.nswTimeout.intValue() : ClockSwitchExecutor.NSW_TIMEOUT_DEFAULT);
            }));

            tasks.add(ClockSwitchExecutor.resynchTp.submit(() -> {
                this.sendResync(clockSwitch,
                                cmdArg,
                                this.larController(),
                                this.larTimeout != null ? this.larTimeout.intValue() : ClockSwitchExecutor.LAR_TIMEOUT_DEFAULT);
                
                // L1Calo must receive the command after LAr
                this.sendL1CaloResync(clockSwitch);
            }));
            
            for(final Future<?> t : tasks) {
                try {
                    t.get();
                }
                catch(final ExecutionException e) {
                    final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                              STATUS.PROBLEM,
                                                              clockSwitch.getBeamMode(),
                                                              clockSwitch.getState(),
                                                              clockSwitch.getId());
                    event.setError("Unexpected error sending a custom resynch command: " + e.getCause(), e);
                    this.cep.injectEvent(event);
                }                                            
            }
        }
        catch(final InterruptedException e) {
            final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                      STATUS.PROBLEM,
                                                      clockSwitch.getBeamMode(),
                                                      clockSwitch.getState(),
                                                      clockSwitch.getId());
            event.setError("Cannot wait for custom resynch commands to be executed: thread interrupted", e);
            this.cep.injectEvent(event);

            // Restore the state
            Thread.currentThread().interrupt();
        }
        catch(final RejectedExecutionException e) {
            final ClockSwitch event = new ClockSwitch(clockSwitch.getType(),
                                                      STATUS.PROBLEM,
                                                      clockSwitch.getBeamMode(),
                                                      clockSwitch.getState(),
                                                      clockSwitch.getId());
            event.setError("Cannot send custom resynch commands: execution rejected", e);
            this.cep.injectEvent(event);
        }
    }
    
    private Token publishHoldInfo(final ClockSwitch clock) {
        Token t = null;
        
        try {            
            t = HoldTriggerInfoPublisher.publish(this.daqReader.getPartitionName(), CausedBy.Other, Reason.ClockSwitch);
        }
        catch(final Exception e) {
            final ClockSwitch event =
                                    new ClockSwitch(clock.getType(), STATUS.PROBLEM, clock.getBeamMode(), clock.getState(), clock.getId());
            event.setError("Failed to publish HoldTriggerInfo to IS", e);
            this.cep.injectEvent(event);
        }
        
        return t;
    }

    private void sendErrorToIGUI(final String msg) {
        try {
            this.execIGUI.showError(msg);
        }
        catch(final ExecutorIGUIException ex) {
            ClockSwitchExecutor.log.error("Failed to send error message to the IGUI", ex);
        }
    }
}
