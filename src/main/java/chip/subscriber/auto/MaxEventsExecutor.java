package chip.subscriber.auto;

import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.CHIPException.DAQConfigurationError;
import chip.CHIPException.ExecutorRCException;
import chip.DAQConfigReader;
import chip.event.issue.Issue;
import chip.event.issue.auto.MaxEvent;
import chip.executor.ExecutorRC;
import chip.subscriber.DeferredActionExecutor;


public class MaxEventsExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    private final DAQConfigReader daqConfReader;
    private final ExecutorRC execRC;

    public MaxEventsExecutor(final DAQConfigReader conf, final ExecutorRC rcEx) {
        this.daqConfReader = conf;
        this.execRC = rcEx;
    }

    @Override
    public void doUpdate(final Issue issue) {
        final MaxEvent maxEvent = (MaxEvent) issue;

        if(maxEvent.getType().equals(MaxEvent.TYPE.MAX_EVENTS_REACHED) && maxEvent.getStatus().equals(MaxEvent.STATUS._NEW)) {
            try {
                this.execRC.makeTransition(this.daqConfReader.getRootController().UID(), daq.rc.Command.FSMCommands.STOP);

                final MaxEvent event = new MaxEvent(maxEvent.getType(), MaxEvent.STATUS.DONE);
                this.cep.injectEvent(event);
            }
            catch(final ExecutorRCException | DAQConfigurationError ex) {
                final MaxEvent event = new MaxEvent(maxEvent.getType(), MaxEvent.STATUS.FAILED);
                event.setError("Failed to stop the run", ex);
                this.cep.injectEvent(event);

                // ers.Logger.error(new Error.Message("MaxEvents: Failed to stop the run", ex));
            }
        }
    }
}
