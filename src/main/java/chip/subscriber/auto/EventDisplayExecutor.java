package chip.subscriber.auto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import chip.CEPService;
import chip.CHIPException.ExecutorRCException;
import chip.event.issue.Issue;
import chip.event.issue.auto.EventDisplay;
import chip.executor.ExecutorRC;
import chip.subscriber.DeferredActionExecutor;


public class EventDisplayExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    // Using SpEL to get properties list of files
    @Value("#{configuration['chip.EventDisplayAthenaSegment']}")
    private String ATHENA_SEGMENT;
    
    private final ExecutorRC execRC;
    private final ipc.Partition partition;
    
    public EventDisplayExecutor(final ExecutorRC rcEx) {
        this.execRC = rcEx;
        this.partition = new ipc.Partition(rcEx.getPartitionName());
    }

    @Override
    public void doUpdate(final Issue issue) {        
        final EventDisplay edEvent = (EventDisplay) issue;

        if(edEvent.getType().equals(EventDisplay.TYPE.RESTART_APPS_AT_SOR) && edEvent.getStatus().equals(EventDisplay.STATUS._NEW)) {
            if(this.partition.isValid() == true) {
                try {
                    this.execRC.restartApps("RootController", new String[] {this.ATHENA_SEGMENT});

                    final EventDisplay event = new EventDisplay(edEvent.getType(), EventDisplay.STATUS.DONE);
                    this.cep.injectEvent(event);
                }
                catch(final ExecutorRCException ex) {
                    final EventDisplay event = new EventDisplay(edEvent.getType(), EventDisplay.STATUS.FAILED);
                    event.setError("Failed to restart applications in the EventDisplay partition", ex);
                    this.cep.injectEvent(event);
                }
            } else {
                final EventDisplay event = new EventDisplay(edEvent.getType(), EventDisplay.STATUS.FAILED);
                event.setError("The EventDisplay partition is not up");
                this.cep.injectEvent(event);
            }
        }
    }
}
