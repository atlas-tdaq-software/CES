package chip.subscriber.auto;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.CHIPException.DAQConfigurationError;
import chip.CHIPException.ExecutorIGUIException;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.en.STATUS;
import chip.event.issue.Issue;
import chip.event.issue.auto.AutoPilot;
import chip.event.issue.auto.AutoPilot.TYPE;
import chip.event.meta.RCApplication;
import chip.executor.ExecutorIGUI;
import chip.subscriber.DeferredActionExecutor;
import daq.rc.ApplicationState;
import daq.rc.Command;
import daq.rc.CommandNotifier;
import daq.rc.CommandSender;
import daq.rc.RCApplicationStatus;
import daq.rc.RCException;
import daq.rc.Command.FSMCommands;
import daq.rc.Command.TransitionCmd;
import iguiCommander.UserAnswer;
import is.InfoNotCompatibleException;
import is.RepositoryNotFoundException;


public class AutoPilotExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    private final CommandSender rcSender;
    private final EsperDAO dao;
    private final DAQConfigReader configReader;
    private final ExecutorIGUI execIGUI;

    private static final Log log = LogFactory.getLog(AutoPilotExecutor.class);

    // This is for extra safety
    // This class is a singleton and there should never be more than one notifier at any
    // moment in time. To avoid any possible issue, all the created notifiers are added to
    // this list so that we are sure none of them keeps hanging when the auto-pilot
    // is terminated. Note that an hanging notifier means a thread never released
    private final CopyOnWriteArrayList<TransitionNotifier> notifierList = new CopyOnWriteArrayList<>();

    // This is used to wait for the RC command to be executed and, at the same time
    // to stop waiting in case the auto-pilot is stopped
    private class TransitionNotifier extends CommandNotifier {
        private boolean callbackRec = false; // Command executed
        private boolean stopWaiting = false; // Stop to wait, auto-pilot terminated
        private final ReentrantLock lk = new ReentrantLock();
        private final Condition cond = this.lk.newCondition();

        TransitionNotifier() {
            AutoPilotExecutor.this.notifierList.add(this);
        }

        public void stopWaiting() {
            this.lk.lock();
            try {
                this.stopWaiting = true;
                this.cond.signal();
            }
            finally {
                this.lk.unlock();
            }
        }

        public void waitForExecution() throws InterruptedException {
            this.lk.lock();
            try {
                while((this.callbackRec == false) && (this.stopWaiting == false)) {
                    this.cond.await();
                }
            }
            finally {
                this.lk.unlock();
            }
        }

        public boolean waitInterrupted() {
            this.lk.lock();
            try {
                return this.stopWaiting;
            }
            finally {
                this.lk.unlock();
            }
        }
        
        @Override
        public void close() {
            super.close();
            AutoPilotExecutor.this.notifierList.remove(this);
        }

        @Override
        protected void commandExecuted(final Command cmd) {
            this.lk.lock();
            try {
                this.callbackRec = true;
                this.cond.signal();
            }
            finally {
                this.lk.unlock();
            }
        }
    }

    public AutoPilotExecutor(final CommandSender rcExecutor,
                             final EsperDAO dao,
                             final DAQConfigReader daqConfig,
                             final ExecutorIGUI execIGUI)
    {
        this.rcSender = rcExecutor;
        this.dao = dao;
        this.configReader = daqConfig;
        this.execIGUI = execIGUI;
    }

    @Override
    protected void doUpdate(final Issue issue) {
        final AutoPilot autoPilot = (AutoPilot) issue;

        final TYPE type = autoPilot.getType();

        switch(type) {
            case RESOLVE_PROPAGATION_ERROR:
                this.handleCommandPropagationError(autoPilot);
                break;
            case RESOLVE_TRANSITION_TIMEOUT:
                this.handleTransitionTimeout(autoPilot);
                break;
            case DISABLE_AUTOPILOT:
                this.stopAutoPilot(autoPilot);
                break;
            case GO_TO_NONE:
                this.goToState(autoPilot, ApplicationState.NONE);
                break;
            case GO_TO_RUNNING:
                this.goToState(autoPilot, ApplicationState.RUNNING);
                break;
            default:
                break;
        }
    }

    // Set OUT of membership the application that had issues with the command
    private void handleCommandPropagationError(final AutoPilot issue) {
        final String controller = issue.getController();
        final String application = issue.getApplication();

        try {
            this.rcSender.changeMembership(controller, false, new String[] {application});
            this.cep.injectEvent(new AutoPilot(issue.getType(), AutoPilot.STATUS.DONE, AutoPilot.ACTION.NONE, controller, application));
        }
        catch(final RCException ex) {
            final AutoPilot event = new AutoPilot(issue.getType(), AutoPilot.STATUS.FAILED, AutoPilot.ACTION.NONE, controller, application);
            event.setError("Autopilot failed to send the disable command to \"" + controller + "\": " + ex, ex);
            this.cep.injectEvent(event);
        }
    }

    // When a controller sends an ERS message notifying a timeout in executing a transition,
    // then put OUT of membership all the children still not ready
    private void handleTransitionTimeout(final AutoPilot issue) {
        final String controller = issue.getController();

        final List<RCApplication> apps = this.dao.getChildrenOfController(controller);

        final List<String> appsToDisable = new ArrayList<>();
        for(final RCApplication app : apps) {
            // Applications are still transitioning (and, pedantic, still in the parent's state)
            // Applications are still in REQUESTED state
            // Note that the RootController is the parent of itself...
            if((app.getName().equals(controller) == false) &&
               ((app.getIsTransitioning() == Boolean.TRUE) && (app.getParentState().equals(app.getState()) == true)) ||
                (app.getStatus().equals(STATUS.REQUESTED) == true))
            {
                appsToDisable.add(app.getName());
            }
        }

        if(appsToDisable.isEmpty() == false) {
            try {
                this.rcSender.changeMembership(controller, false, appsToDisable.toArray(new String[appsToDisable.size()]));
                this.cep.injectEvent(new AutoPilot(issue.getType(), AutoPilot.STATUS.DONE, AutoPilot.ACTION.NONE, controller, null));
            }
            catch(final RCException ex) {
                final AutoPilot event = new AutoPilot(issue.getType(), AutoPilot.STATUS.FAILED, AutoPilot.ACTION.NONE, controller, null);
                event.setError("Autopilot failed to send the disable command to \"" + controller + "\": " + ex, ex);
                this.cep.injectEvent(event);
            }
        } else {
            this.cep.injectEvent(new AutoPilot(issue.getType(), AutoPilot.STATUS.DONE, AutoPilot.ACTION.NONE, controller, null));
        }
    }

    // This stops all the notifiers waiting for commands to be executed
    private void stopAutoPilot(final AutoPilot issue) {
        for(final TransitionNotifier tn : this.notifierList) {
            tn.stopWaiting();
        }

        this.cep.injectEvent(new AutoPilot(issue.getType(), AutoPilot.STATUS.DONE, AutoPilot.ACTION.NONE, null, null));
    }

    // This implements the automatic move to RUNNING/NONE
    private void goToState(final AutoPilot issue, final ApplicationState state) {
        UserAnswer answer = UserAnswer.NO;
        try {
            final StringBuilder msg = new StringBuilder("It looks like the autopilot has been enabled;");
            msg.append(" the system will be automatically brought to the " + state.toString() + " state.");

            if(state.equals(ApplicationState.RUNNING) == true) {
                msg.append(" Remember to check the run parameters now!");
            }

            msg.append(" Do you really want to go?");

            answer = this.execIGUI.askYesOrNo(msg.toString(), 1000 * 60 * 5); // Wait for 5 minutes

            AutoPilotExecutor.log.info("Auto-pilot init: user answer is " + answer);
        }
        catch(final ExecutorIGUIException e) {
            AutoPilotExecutor.log.warn("Failed to notify the user about the auto-pilot start: " + e, e);
        }

        if(answer.equals(UserAnswer.YES) == true) {            
            try {
                final String rootController = this.configReader.getRootController().UID();
                
                String currentState = this.dao.getRCApplication(rootController).getState().toString();

                // Loop until the final state is reached
                while(currentState.equals(state.toString()) == false) {
                    try (final TransitionNotifier trNot = new TransitionNotifier()) {
                        // Send the command and wait for its execution
                        this.rcSender.executeCommand(rootController, new TransitionCmd(FSMCommands.NEXT_TRANSITION), trNot);
                        trNot.waitForExecution();

                        // Be sure the error state has been removed (if any)
                        // Otherwise the next command may fail because the controller is in an error state
                        // This is needed because a controller can complete a transition and then any
                        // error state is cleaned asynchronously by CHIP itself
                        while((this.dao.getRCApplication(rootController).getInternalError() == Boolean.TRUE) && 
                              (trNot.waitInterrupted() == false)) 
                        {
                            Thread.sleep(500);
                        }
                        
                        // Get the new state of the root controller
                        final RCApplicationStatus appStatus = this.rcSender.getApplicationStatus(rootController);
                        final String newState = appStatus.getAppState().toString();                        
                        if((newState.equals(currentState) == false) && (trNot.waitInterrupted() == false)) {
                            // Transition really done, OK!
                            currentState = newState;
                        } else {
                            // The transition has not been really done:
                            // - the user decided to SHUTDOWN;
                            // - the auto-pilot was explicitly terminated (that's why "waitForExecution" returned)
                            final AutoPilot event = new AutoPilot(issue.getType(),
                                                                  AutoPilot.STATUS.PROBLEM,
                                                                  AutoPilot.ACTION.NONE,
                                                                  null,
                                                                  null);
                            event.setError("The AutoPilot has been interrupted");
                            this.cep.injectEvent(event);

                            // No need to continue
                            break;
                        }
                    }
                }

                this.cep.injectEvent(new AutoPilot(issue.getType(), AutoPilot.STATUS.DONE, AutoPilot.ACTION.NONE, null, null));
            }
            catch(final RCException | InterruptedException | DAQConfigurationError ex)
            {
                final AutoPilot event = new AutoPilot(issue.getType(), AutoPilot.STATUS.FAILED, AutoPilot.ACTION.NONE, null, null);
                event.setError("Autopilot stopped because of an unexpected exception: " + ex, ex);
                this.cep.injectEvent(event);
            }
        } else {
            final AutoPilot event = new AutoPilot(issue.getType(), AutoPilot.STATUS.FAILED, AutoPilot.ACTION.NONE, null, null);
            event.setError("Autopilot stopped because of operator's decision");
            this.cep.injectEvent(event);
        }

        // The job is done, the auto-pilot can be disabled
        this.disableAutoPilot();
    }

    // This update the IS information so that anybody is notified that the auto-pilot is done
    private void disableAutoPilot() {
        try {
            final rc.AutoPilotNamed isInfo = new rc.AutoPilotNamed(new ipc.Partition(this.configReader.getPartitionName()), 
                                                                   "RunCtrlStatistics.Autopilot");
            isInfo.enabled = false;
            isInfo.checkin();
        }
        catch(final RepositoryNotFoundException | InfoNotCompatibleException ex) {
            AutoPilotExecutor.log.error("Could not update the auto-pilot state in IS: " + ex, ex);
        }
    }
}
