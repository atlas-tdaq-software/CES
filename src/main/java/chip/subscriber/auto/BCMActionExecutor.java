package chip.subscriber.auto;

import is.InfoNotCompatibleException;
import is.InfoNotFoundException;
import is.RepositoryNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import rc.RunParamsNamed;
import chip.CEPService;
import chip.CHIPException.ExecutorRCException;
import chip.DAQConfigReader;
import chip.event.issue.Issue;
import chip.event.issue.auto.BCM;
import chip.executor.ExecutorRC;
import chip.subscriber.DeferredActionExecutor;
import chip.utils.ExecutorFactory;
import daq.eformat.DetectorMask;
import daq.eformat.DetectorMask.SUBDETECTOR;


public class BCMActionExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    private final static Log log = LogFactory.getLog(BCMActionExecutor.class);
    private final static String USER_CMD_NAME = "DCM_PROCESSOR_SET_PARAMETERS";
    private final static ExecutorService tPool = ExecutorFactory.newThreadPool("BCMExecutor", 10, true);

    private final ExecutorRC execRC;
    private final DAQConfigReader daqConf;
    private final ipc.Partition partition;

    public BCMActionExecutor(final ExecutorRC execRC, final DAQConfigReader daqConf) {
        this.execRC = execRC;
        this.daqConf = daqConf;
        this.partition = new ipc.Partition(daqConf.getPartitionName());
    }

    @Override
    public void doUpdate(final Issue issue) {        
        final BCM action = (BCM) issue;

        try {
            if(this.isBCMRunning() == true) {
                try {
                    switch(action.getType()) {
                        case BCID_CHECK_DISABLE:
                            this.changeBCIDCheck(false, action);
                            break;
                        case BCID_CHECK_ENABLE:
                            this.changeBCIDCheck(true, action);
                            break;
                        default:
                            break;
                    }
                }
                catch(final InterruptedException ex) {
                    // Restore the interrupted state; nothing more to do...
                    Thread.currentThread().interrupt();
                }
            } else {
                BCMActionExecutor.log.info("BCM is not part of the run, not asking the DCMs to enable/disable the BCID check");
            }

            this.cep.injectEvent(new BCM(action.getType(), BCM.STATUS.DONE));
        }
        catch(final InfoNotFoundException | InfoNotCompatibleException | RepositoryNotFoundException ex) {
            final BCM ev = new BCM(action.getType(), BCM.STATUS.FAILED);
            ev.setError("Cannot determine if BCM is taking part to the run", ex);
            BCMActionExecutor.this.cep.injectEvent(ev);
        }
    }

    synchronized private void changeBCIDCheck(final boolean enable, final BCM action) throws InterruptedException {
        final StringBuilder cmdArgs = new StringBuilder("bcidCheckIgnoreSubdetectors=");
        if(enable == false) {
            cmdArgs.append(DetectorMask.SUBDETECTOR.FORWARD_BCM.id());
        }

        final List<Callable<Void>> tasks = new ArrayList<>();

        final Set<String> controllers = this.daqConf.getDCMSegments();
        for(final String c : controllers) {
            final Callable<Void> call = new Callable<Void>() {
                @Override
                public Void call() {
                    try {
                        BCMActionExecutor.this.execRC.sendUserBroadcast(c,
                                                                        BCMActionExecutor.USER_CMD_NAME,
                                                                        new String[] {cmdArgs.toString()},
                                                                        Boolean.TRUE,
                                                                        10);
                    }
                    catch(final ExecutorRCException ex) {
                        final BCM ev = new BCM(action.getType(), BCM.STATUS.PROBLEM);
                        ev.setError("Failed to send command to controller " + c, ex);
                        BCMActionExecutor.this.cep.injectEvent(ev);
                    }

                    return null;
                }
            };

            tasks.add(call);
        }

        BCMActionExecutor.tPool.invokeAll(tasks);
    }

    private boolean isBCMRunning() throws is.RepositoryNotFoundException, is.InfoNotFoundException, is.InfoNotCompatibleException {
        final RunParamsNamed rp = new RunParamsNamed(this.partition, "RunParams.SOR_RunParams");
        rp.checkout();
        final String dm = rp.det_mask;
        return DetectorMask.decode(dm).isSet(SUBDETECTOR.FORWARD_BCM);
    }
}
