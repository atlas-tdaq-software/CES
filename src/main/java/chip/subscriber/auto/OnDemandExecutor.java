package chip.subscriber.auto;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;

import com.espertech.esper.common.client.EPException;

import chip.CEPService;
import chip.CHIPException.ExecutorPMGException;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.issue.Issue;
import chip.event.issue.auto.OnDemandAction;
import chip.event.issue.auto.OnDemandAction.ACTION;
import chip.event.issue.auto.OnDemandAction.STATUS;
import chip.event.meta.RCApplication;
import chip.executor.ExecutorPMG;
import chip.subscriber.DeferredActionExecutor;
import daq.rc.CommandSender;
import daq.rc.RCException;


public class OnDemandExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    private final CommandSender rcSender;
    private final ExecutorPMG pmgExecutor;
    private final DAQConfigReader daqConfig;
    private final EsperDAO dao;

    public OnDemandExecutor(final CommandSender rcExecutor,
                            final ExecutorPMG pmgExecutor,
                            final DAQConfigReader daqConfig,
                            final EsperDAO dao)
    {
        this.rcSender = rcExecutor;
        this.pmgExecutor = pmgExecutor;
        this.daqConfig = daqConfig;
        this.dao = dao;
    }

    @Override
    protected void doUpdate(final Issue issue) {
        final OnDemandAction action = (OnDemandAction) issue;

        final ACTION type = action.getAction();
        switch(type) {
            case START_APPS:
                this.startApps(action);
                break;
            case KILL_APPS:
                this.killApps(action);
                break;
            case RESTART_APPS:
                this.restartApps(action);
                break;
            default: {
                final OnDemandAction result = new OnDemandAction(action.getType(),
                                                                 STATUS.FAILED,
                                                                 action.getAction(),
                                                                 action.getSender(),
                                                                 action.getCommand(),
                                                                 action.getArguments());
                result.setError("The received command is unknown");
                this.cep.injectEvent(result);
            }
                break;
        }
    }

    private void startApps(final OnDemandAction action) {
        final Set<String> apps =
                               Arrays.stream(action.getArguments().trim().split(",")).map(String::trim).collect(Collectors.toCollection(TreeSet::new));

        int failures = 0;
        for(final String app : apps) {
            final RCApplication rcApp = this.getApplication(app);
            if(rcApp != null) {
                try {
                    this.rcSender.startApplications(rcApp.getController(), new String[] {app});
                }
                catch(final RCException e)
                {
                    ++failures;

                    final OnDemandAction result = new OnDemandAction(action.getType(),
                                                                     STATUS.PROBLEM,
                                                                     action.getAction(),
                                                                     action.getSender(),
                                                                     action.getCommand(),
                                                                     action.getArguments());
                    result.setError("Application \"" + app + "\" cannot be started", e);
                    this.cep.injectEvent(result);
                }
            } else {
                ++failures;

                final OnDemandAction result = new OnDemandAction(action.getType(),
                                                                 STATUS.PROBLEM,
                                                                 action.getAction(),
                                                                 action.getSender(),
                                                                 action.getCommand(),
                                                                 action.getArguments());
                result.setError("Application \"" + app + "\" cannot be found or does not exist");
                this.cep.injectEvent(result);
            }
        }

        this.handleExecutionCompletion(failures, action);
    }

    private void killApps(final OnDemandAction action) {
        final Set<String> apps =
                               Arrays.stream(action.getArguments().trim().split(",")).map(String::trim).collect(Collectors.toCollection(TreeSet::new));

        int failures = 0;
        for(final String app : apps) {
            final RCApplication rcApp = this.getApplication(app);
            if(rcApp != null) {
                try {
                    this.pmgExecutor.killApplication(app, this.daqConfig.getPartitionName(), rcApp.getRunningHost());
                }
                catch(final ExecutorPMGException e) {
                    ++failures;

                    final OnDemandAction result = new OnDemandAction(action.getType(),
                                                                     STATUS.PROBLEM,
                                                                     action.getAction(),
                                                                     action.getSender(),
                                                                     action.getCommand(),
                                                                     action.getArguments());
                    result.setError("Application \"" + app + "\" cannot be killed", e);
                    this.cep.injectEvent(result);
                }
            } else {
                ++failures;

                final OnDemandAction result = new OnDemandAction(action.getType(),
                                                                 STATUS.PROBLEM,
                                                                 action.getAction(),
                                                                 action.getSender(),
                                                                 action.getCommand(),
                                                                 action.getArguments());
                result.setError("Application \"" + app + "\" cannot be found or does not exist");
                this.cep.injectEvent(result);
            }
        }

        this.handleExecutionCompletion(failures, action);
    }

    private void restartApps(final OnDemandAction action) {
        final Set<String> apps =
                               Arrays.stream(action.getArguments().trim().split(",")).map(String::trim).collect(Collectors.toCollection(TreeSet::new));

        int failures = 0;
        for(final String app : apps) {
            final RCApplication rcApp = this.getApplication(app);
            if(rcApp != null) {
                try {
                    this.rcSender.restartApplications(rcApp.getController(), new String[] {app});
                }
                catch(final RCException e)
                {
                    ++failures;

                    final OnDemandAction result = new OnDemandAction(action.getType(),
                                                                     STATUS.PROBLEM,
                                                                     action.getAction(),
                                                                     action.getSender(),
                                                                     action.getCommand(),
                                                                     action.getArguments());
                    result.setError("Application \"" + app + "\" cannot be restarted", e);
                    this.cep.injectEvent(result);
                }
            } else {
                ++failures;

                final OnDemandAction result = new OnDemandAction(action.getType(),
                                                                 STATUS.PROBLEM,
                                                                 action.getAction(),
                                                                 action.getSender(),
                                                                 action.getCommand(),
                                                                 action.getArguments());
                result.setError("Application \"" + app + "\" cannot be found or does not exist");
                this.cep.injectEvent(result);
            }
        }

        this.handleExecutionCompletion(failures, action);
    }

    private RCApplication getApplication(final String appName) {
        try {
            return this.dao.getRCApplication(appName);
        }
        catch(final EPException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    private void handleExecutionCompletion(final int failures, final OnDemandAction action) {
        final Set<String> apps =
                               Arrays.stream(action.getArguments().trim().split(",")).map(String::trim).collect(Collectors.toCollection(TreeSet::new));

        if((failures > 0) && (failures == apps.size())) {
            final OnDemandAction result = new OnDemandAction(action.getType(),
                                                             STATUS.FAILED,
                                                             action.getAction(),
                                                             action.getSender(),
                                                             action.getCommand(),
                                                             action.getArguments());
            result.setError("Failed to execute the received request");
            this.cep.injectEvent(result);
        } else {
            final OnDemandAction result = new OnDemandAction(action.getType(),
                                                             STATUS.DONE,
                                                             action.getAction(),
                                                             action.getSender(),
                                                             action.getCommand(),
                                                             action.getArguments());
            this.cep.injectEvent(result);
        }
    }
}
