package chip.subscriber.auto;

import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.CHIPException.ExecutorIGUIException;
import chip.event.issue.Issue;
import chip.event.issue.auto.UnexpectedStop;
import chip.event.issue.auto.UnexpectedStop.STATUS;
import chip.executor.ExecutorIGUI;
import chip.subscriber.DeferredActionExecutor;


public class UnexpectedStopExecutor extends DeferredActionExecutor {

    @Autowired
    CEPService cep;

    private final ExecutorIGUI execIGUI;

    public UnexpectedStopExecutor(final ExecutorIGUI execIGUI) {
        this.execIGUI = execIGUI;
    }

    @Override
    public void doUpdate(final Issue issue) {

        final UnexpectedStop unexpectedStop = (UnexpectedStop) issue;

        if(unexpectedStop.getType().equals(UnexpectedStop.TYPE.UNEXPECTED_STOP)) {
            if(unexpectedStop.getStatus().equals(UnexpectedStop.STATUS._NEW)) {
                this.askForStopOfRunReason(unexpectedStop);
            }
        }

    }

    private void askForStopOfRunReason(final UnexpectedStop unexpectedStop) {

        try {
            this.execIGUI.stopAlertCommand();
        }
        catch(final ExecutorIGUIException e) {
            final UnexpectedStop event = new UnexpectedStop(unexpectedStop.getType(), STATUS.FAILED, unexpectedStop.getBeamMode());
            event.setError("Failed to ask for stop of run reason", e);
            this.cep.injectEvent(event);
            return;
        }

        final UnexpectedStop event = new UnexpectedStop(unexpectedStop.getType(), STATUS.DONE, unexpectedStop.getBeamMode());
        this.cep.injectEvent(event);

    }

}
