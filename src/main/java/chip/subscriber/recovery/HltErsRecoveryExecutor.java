package chip.subscriber.recovery;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.dao.EsperDAO;
import chip.event.issue.Issue;
import chip.event.issue.recovery.HltErsRecovery;
import chip.event.issue.recovery.HltErsRecovery.ACTION;
import chip.event.issue.recovery.HltErsRecovery.STATUS;
import chip.msg.Common;
import chip.subscriber.DeferredActionExecutor;
import daq.rc.CommandSender;
import daq.rc.RCException;


public class HltErsRecoveryExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(HltErsRecoveryExecutor.class);

    private final CommandSender commandSender;

    public HltErsRecoveryExecutor(final CommandSender commandSender, final EsperDAO dao) {
        this.commandSender = commandSender;
    }

    /*
     * (non-Javadoc)
     * @see ch.atdaq.ces.subscriber.ActionExecutor#update(ch.atdaq.ces.event.meta.Action)
     */
    @Override
    public void doUpdate(final Issue issue) {
        final HltErsRecovery rec = (HltErsRecovery) issue;

        if(rec.getStatus().equals(STATUS.REJECTED)) {
            ers.Logger.warning(new Common("The HLT recovery of type " + rec.getType() + " for host " + rec.getRunningHost()
                                          + " will not be executed: maximum number of allowed recoveries reached"));
        } else {
            switch(rec.getType()) {
                case HLTPU_CRASH:
                    this.handleHltpuCrash(rec);
                    break;
                case HLTPU_TIMEOUT:
                    this.handleHltpuTimeout(rec);
                    break;
                default:
                    break;
            }
        }
    }

    private void handleHltpuCrash(final HltErsRecovery rec) {
        switch(rec.getAction()) {
            case FORK:
                //HltErsRecoveryExecutor.log.debug("HltErsRecovery of type " + rec.getType() + " triggered by application "
                //                                 + rec.getReportingApp() + " , forking process...");

                try {
                    this.commandSender.userCommand(rec.getHltrc(), "FORK_AFTER_CRASH", new String[] {});

                    // Inject DONE
                    final HltErsRecovery resolved = new HltErsRecovery(rec.getReportingApp(),
                                                                       rec.getHltrc(),
                                                                       rec.getRunningHost(),
                                                                       rec.getHltpu(),
                                                                       rec.getType(),
                                                                       HltErsRecovery.STATUS.DONE,
                                                                       ACTION.NONE);
                    this.cep.injectEvent(resolved);
                }
                catch(final RCException e) {
                    final HltErsRecovery event = new HltErsRecovery(rec.getReportingApp(),
                                                                    rec.getHltrc(),
                                                                    rec.getRunningHost(),
                                                                    rec.getHltpu(),
                                                                    rec.getType(),
                                                                    HltErsRecovery.STATUS.FAILED,
                                                                    ACTION.NONE);
                    event.setError("Failed to fork process", e);
                    this.cep.injectEvent(event);
                }

                break;

            default:
                HltErsRecoveryExecutor.log.error("Unknown recovery status: " + rec.getStatus());
        }
    }

    private void handleHltpuTimeout(final HltErsRecovery rec) {
        switch(rec.getAction()) {
            case STOP:
                //HltErsRecoveryExecutor.log.debug("HltErsRecovery of type " + rec.getType() + " triggered by application "
                //                                 + rec.getReportingApp() + ", stopping " + rec.getHltrc());

                final String hltpu = rec.getHltpu();
                try {
                    this.commandSender.userCommand(rec.getHltrc(), "KILL", new String[] { hltpu });
                    
                    final HltErsRecovery event = new HltErsRecovery(rec.getReportingApp(),
                                                                    rec.getHltrc(),
                                                                    rec.getRunningHost(),
                                                                    rec.getHltpu(),
                                                                    rec.getType(),
                                                                    HltErsRecovery.STATUS.DONE,
                                                                    ACTION.NONE);
                    this.cep.injectEvent(event);
                }
                catch(final RCException e) {
                    final HltErsRecovery event = new HltErsRecovery(rec.getReportingApp(),
                                                                    rec.getHltrc(),
                                                                    rec.getRunningHost(),
                                                                    rec.getHltpu(),
                                                                    rec.getType(),
                                                                    HltErsRecovery.STATUS.FAILED,
                                                                    ACTION.NONE);
                    event.setError("Failed to kill process", e);
                    this.cep.injectEvent(event);
                }

                break;

            default:
                HltErsRecoveryExecutor.log.error("Unknown recovery status: " + rec.getStatus());
        }
    }
}
