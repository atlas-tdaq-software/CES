package chip.subscriber.recovery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import chip.CHIPException.DAQConfigurationError;
import chip.CHIPException.ExecutorPMGException;
import chip.CHIPException.ExecutorRCException;
import chip.CEPService;
import chip.DAQConfigReader;
import chip.event.issue.Issue;
import chip.event.issue.recovery.RootCtrl;
import chip.executor.ExecutorPMG;
import chip.executor.ExecutorRC;
import chip.subscriber.DeferredActionExecutor;
import config.ConfigException;
import dal.BaseApplication;
import dal.Computer;
import dal.ComputerProgram;
import dal.RM_Resource;


public class RootCtrlExecutor extends DeferredActionExecutor {
    private static final Log log = LogFactory.getLog(RootCtrlExecutor.class);

    @Autowired
    CEPService cep;

    private final ExecutorRC execRC;
    private final ExecutorPMG execPMG;
    private final DAQConfigReader configReader;

    public RootCtrlExecutor(final ExecutorRC execRC, final ExecutorPMG execPMG, final DAQConfigReader config) {
        this.execRC = execRC;
        this.execPMG = execPMG;
        this.configReader = config;
    }

    @Override
    public void doUpdate(final Issue issue) {
        final RootCtrl rcIssue = (RootCtrl) issue;

        switch(rcIssue.getAction()) {
            case REJECT:
                this.reject(rcIssue);
                break;
            case RESTART:
                this.restart(rcIssue);
                break;
            default:
                this.cep.injectEvent(new RootCtrl(rcIssue.getApplication(),
                                                  rcIssue.getHost(),
                                                  rcIssue.getType(),
                                                  RootCtrl.STATUS.DONE,
                                                  RootCtrl.ACTION.NONE));
                break;

        }
    }

    private void reject(final RootCtrl rcIssue) {
        final RootCtrl failed = new RootCtrl(rcIssue.getApplication(),
                                             rcIssue.getHost(),
                                             rcIssue.getType(),
                                             RootCtrl.STATUS.FAILED,
                                             RootCtrl.ACTION.NONE);
        failed.setError("The root controller cannot be restarted because the maximum number of restarts has been reached");
        this.cep.injectEvent(failed);
    }

    private synchronized void restart(final RootCtrl rcIssue) {
        // If the issue is detected because of the failing health check, then it may be the root controller
        // is still alive. Let's try to eventually kill it. Note that in this case the host itself may be
        // in a bad shape and this call may fail.
        boolean restartIt = true;
        
        if(rcIssue.getType() != RootCtrl.TYPE.CRASH) {
            // Pedantic check: try to ping the root controller over CORBA to double-check its health
            // If the ping succeeds, then consider the root controller as healthy
            try {
                RootCtrlExecutor.log.info("Pinging the root controller over CORBA");
                this.execRC.pingApplication(rcIssue.getApplication());
                RootCtrlExecutor.log.info("Ping succeeded, the root controller is actually alive; no need to restart it");

                restartIt = false;
            }
            catch(final ExecutorRCException e) {
                // Ping failed, go on
                RootCtrlExecutor.log.info("Ping failed, the root controller is definitely not healthy", e);

                try {
                    this.execPMG.killApplication(rcIssue.getApplication(), this.configReader.getDalPartition().UID(), rcIssue.getHost());
                }
                catch(final ExecutorPMGException ex) {
                    final RootCtrl problem = new RootCtrl(rcIssue.getApplication(),
                                                          rcIssue.getHost(),
                                                          rcIssue.getType(),
                                                          RootCtrl.STATUS.PROBLEM,
                                                          RootCtrl.ACTION.NONE);
                    problem.setError("The root controller could not be killed: " + ex + ". Recovery will continue.", ex);
                    this.cep.injectEventSynch(problem);
                }
            }
        }

        if(restartIt == true) {
            try {
                // Collect all the parameters needed to ask the PMG to restart the root controller
                final BaseApplication root = this.configReader.getRootController();
    
                final Map<String, String> env = new TreeMap<>();
                final List<String> binList = new ArrayList<>();
                final StringBuilder startArgs = new StringBuilder();
                final StringBuilder restartArgs = new StringBuilder();
    
                root.get_info(env, binList, startArgs, restartArgs);
    
                String workDir = root.get_StartIn().trim();
                if(workDir.isEmpty() == true) {
                    workDir = this.configReader.getDalPartition().get_WorkingDirectory();
                }
    
                String rmSwObject = "";
                final ComputerProgram cp = root.get_Program();
                final RM_Resource[] res = cp.get_Needs();
                if((res != null) && (res.length > 0)) {
                    rmSwObject = cp.UID();
                }
    
                // Restart the root controller on the proper host
                // Take into account any defined back-up host
                // The primary host is the first one to be tried
                final List<String> hosts = new ArrayList<>();
                hosts.add(root.get_host().UID());
    
                final Computer[] backupHosts = root.get_backup_hosts();
                if(backupHosts != null) {
                    for(final Computer c : backupHosts) {
                        hosts.add(c.UID());
                    }
                }
    
                for(final String h : hosts) {
                    short failures = 0;
    
                    try {
                        this.execPMG.startApplication(h,
                                                      this.configReader.getPartitionName(),
                                                      root.UID(),
                                                      String.join(":", binList),
                                                      env,
                                                      workDir,
                                                      restartArgs.toString(),
                                                      root.get_Logging() ? this.configReader.getDalPartition().get_LogRoot() : "",
                                                      rmSwObject,
                                                      root.get_InputDevice(),
                                                      root.get_InitTimeout(),
                                                      0);
    
                        // To not forget: this event may actually reach the ESPER engine after the PMG IS call-back
                        // following the PMG request to start the process (for instance, if the process fails to start)
                        this.cep.injectEvent(new RootCtrl(rcIssue.getApplication(),
                                                          rcIssue.getHost(),
                                                          rcIssue.getType(),
                                                          RootCtrl.STATUS.DONE,
                                                          RootCtrl.ACTION.NONE));
    
                        break;
                    }
                    catch(final ExecutorPMGException ex) {
                        ++failures;
    
                        if(failures == hosts.size()) {
                            RootCtrlExecutor.log.error("The root controller could not be restarted on host " + h
                                                       + " because of errors with the PMG: " + ex,
                                                       ex);
    
                            final RootCtrl failed = new RootCtrl(rcIssue.getApplication(),
                                                                 rcIssue.getHost(),
                                                                 rcIssue.getType(),
                                                                 RootCtrl.STATUS.FAILED,
                                                                 RootCtrl.ACTION.NONE);
                            failed.setError("The root controller cannot be restarted on host " + h + ": " + ex.getMessage(), ex);
                            this.cep.injectEvent(failed);
                        } else {
                            RootCtrlExecutor.log.error("The root controller could not be restarted on host " + h
                                                       + ", trying with a different back-up host ",
                                                       ex);
    
                            final RootCtrl failed = new RootCtrl(rcIssue.getApplication(),
                                                                 rcIssue.getHost(),
                                                                 rcIssue.getType(),
                                                                 RootCtrl.STATUS.PROBLEM,
                                                                 RootCtrl.ACTION.NONE);
                            failed.setError("The root controller cannot be restarted on host " + h + ": " + ex.getMessage(), ex);
                            this.cep.injectEvent(failed);
                        }
                    }
                }
            }
            catch(final DAQConfigurationError | ConfigException ex) {
                RootCtrlExecutor.log.error("The root controller could not be restarted because of errors accessing the configuration: " + ex, ex);

                final RootCtrl failed = new RootCtrl(rcIssue.getApplication(),
                                                     rcIssue.getHost(),
                                                     rcIssue.getType(),
                                                     RootCtrl.STATUS.FAILED,
                                                     RootCtrl.ACTION.NONE);
                failed.setError("The root controller cannot be restarted: " + ex.getMessage(), ex);
                this.cep.injectEvent(failed);
            }
        } else {
            this.cep.injectEvent(new RootCtrl(rcIssue.getApplication(),
                                              rcIssue.getHost(),
                                              rcIssue.getType(),
                                              RootCtrl.STATUS.DONE,
                                              RootCtrl.ACTION.NONE));
        }
    }
}
