package chip.subscriber.recovery;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.BooleanSupplier;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import DFConfiguration.InputChannel;
import DFConfiguration.InputChannel_Helper;
import DFConfiguration.ROS_Helper;
import DFConfiguration.ReadoutApplication;
import RODBusydal.BusyChannel;
import RODBusydal.BusyChannel_Helper;
import ROSModulesNP_IS_Info.RobinNPDescriptorReadoutModuleInfo;
import TRIGGER.TriggerCommander.HoldTriggerInfo;
import chip.CEPService;
import chip.CHIPException.DAQConfigurationError;
import chip.CHIPException.ExecutorIGUIException;
import chip.CHIPException.ExecutorRCException;
import chip.CHIPException.ExecutorTriggerException;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.config.ApplicationConfig;
import chip.event.config.RCApplicationConfig;
import chip.event.issue.HoldsTrigger;
import chip.event.issue.Issue;
import chip.event.issue.recovery.Stopless;
import chip.event.issue.recovery.Stopless.STATUS;
import chip.executor.ExecutorERS;
import chip.executor.ExecutorIGUI;
import chip.executor.ExecutorRC;
import chip.executor.ExecutorTrigger;
import chip.msg.Common;
import chip.subscriber.DeferredMultiActionExecutor;
import chip.utils.HoldTriggerInfoPublisher;
import chip.utils.HoldTriggerInfoPublisher.Token;
import config.ConfigException;
import config.Configuration;
import dal.Component;
import dal.Component_Helper;
import dal.ResourceBase;
import daq.eformat.DetectorMask;
import ddc.DdcStringInfoNamed;
import ers.BadStreamConfiguration;
import iguiCommander.UserAnswer;
import is.Criteria;
import is.InfoList;
import is.InfoNotCompatibleException;
import is.InfoNotFoundException;
import is.RepositoryNotFoundException;
import rc.HoldTriggerInfoNamed;
import rc.HoldTriggerInfoNamed.Reason;
import swrod.SwRodApplication;
import swrod.SwRodApplication_Helper;
import swrod.SwRodInputLink_Helper;
import swrod.SwRodRob_Helper;


public class StoplessExecutor extends DeferredMultiActionExecutor {
    @Autowired
    CEPService cep;

    private static class StoplessFailure extends Exception {
        private static final long serialVersionUID = -1672535525614330819L;

        private final boolean triggerCanBeResumed;
        
        StoplessFailure(final String reason) {
            super(reason);
            this.triggerCanBeResumed = false;
        }

        StoplessFailure(final String reason, final boolean triggerCanBeReleased) {
            super(reason);
            this.triggerCanBeResumed = triggerCanBeReleased;
        }
        
        StoplessFailure(final String reason, final Throwable cause) {
            super(reason, cause);
            this.triggerCanBeResumed = false;
        }
        
        StoplessFailure(final String reason, final Throwable cause, final boolean triggerCanBeResumed) {
            super(reason, cause);
            this.triggerCanBeResumed = triggerCanBeResumed;
        }
        
        boolean triggerCanBeResumed() {
            return this.triggerCanBeResumed;
        }
    }

    private static final Log log = LogFactory.getLog(StoplessExecutor.class);

    private final static Set<String> BEAM_MODES = Stream.of("FLAT TOP",
                                                            "SQUEEZE",
                                                            "ADJUST",
                                                            "STABLE BEAMS",
                                                            "UNSTABLE BEAMS").collect(Collectors.toCollection(HashSet::new));

    private final static Set<String> MACHINE_MODES = Stream.of("PROTON PHYSICS",
                                                               "ION PHYSICS").collect(Collectors.toCollection(HashSet::new));

    private final static String TDAQ_CES_FORCE_REMOVAL_AUTOMATIC = "TDAQ_CES_FORCE_REMOVAL_AUTOMATIC";
    
    private final ExecutorRC execRC;
    private final ExecutorIGUI execIGUI;
    private final ExecutorTrigger execTrigger;
    private final DAQConfigReader daqReader;

    // This is used to avoid concurrent executions of "doResetAfterStop" and removals/recoveries
    private final ReentrantReadWriteLock lk = new ReentrantReadWriteLock();
    private final Lock readLock = this.lk.readLock();
    private final Lock writeLock = this.lk.writeLock();

    private final HashMap<String, ReentrantLock> appsLock = new HashMap<>();
    
    public StoplessExecutor(final ExecutorRC execRC,
                            final ExecutorIGUI execIGUI,
                            final ExecutorTrigger execTrigger,
                            final EsperDAO dao,
                            final DAQConfigReader daqReader)
    {
        this.execRC = execRC;
        this.execIGUI = execIGUI;
        this.execTrigger = execTrigger;
        this.daqReader = daqReader;
    }

    @Override
    public void doUpdate(final Issue[] issueIn, final Issue[] issueRemove) {
        if((issueIn.length == 0) && (issueRemove.length == 0)) {
            StoplessExecutor.log.debug("Stopless subscriber called without any events. Abort!");
            return;
        }

        Stopless[] stopless;

        if(issueIn.length == 0) {
            stopless = new Stopless[issueRemove.length];
            for(int i = 0; i < issueRemove.length; i++) {
                stopless[i] = (Stopless) issueRemove[i];
            }
        } else {
            stopless = new Stopless[issueIn.length];
            for(int i = 0; i < issueIn.length; i++) {
                stopless[i] = (Stopless) issueIn[i];
            }
        }

        switch(stopless[0].getType()) {
            case RECOVERY:
                this.doRecovery(stopless);
                break;
            case RECOVERY_SWROD:
                this.doRecovery_SWROD(stopless);
                break;
            case REMOVAL:
                this.doRemoval(stopless);
                break;
            case REMOVAL_SWROD:
                this.doRemoval_SWROD(stopless);
                break;
            default:
                break;
        }
    }

    private ReentrantLock getAppLock(final String appName) {
        return this.appsLock.computeIfAbsent(appName, (final String name) -> { return new ReentrantLock(); });
    }
    
    // This is executed in ExpertSystemServant.transitionDone
    // It is executed when the CONNECTED state is reached
    // It must be executed synchronously because the reset must be completed
    // before the transition is done 
    public void doResetAfterStop() {
        this.writeLock.lock();

        try { 
            // Notify the ResourcesInfoProvider
            try {
                final String[] disabled = new String[] {};
                final String[] enabled = this.daqReader.resetDynamicallyDisabledComponents();

                this.execRC.notifyResInfoProvider(disabled, enabled);
            }
            catch(final ExecutorRCException | DAQConfigurationError ex) {
                ers.Logger.error(new Common("Failed to reset the ResInfoProvider after stop of run: " + ex, ex));
            }
        }
        finally {
            this.writeLock.unlock();
        }
    }
    
    private void injectFailure(final Stopless stopless, final STATUS status, final String reason) {
        this.injectFailure(stopless, status, reason, null, true);
    }

    private void injectFailure(final Stopless stopless, final STATUS status, final String reason, final Exception e) {
        this.injectFailure(stopless, status, reason, e, true);
    }

    private void injectFailure(final Stopless stopless,
                               final STATUS status,
                               final String reason,
                               final Exception e,
                               final boolean reportToIGUI)
    {
        final Stopless event = new Stopless(stopless.getRcdApp(),
                                            stopless.getIsInternalError(),
                                            stopless.getForceAuto(),
                                            stopless.getProbComponents(),
                                            stopless.getType(),
                                            status,
                                            stopless.getId());

        if(e != null) {
            event.setError(reason, e);
        } else {
            event.setError(reason);
        }

        this.cep.injectEvent(event);

        if((reportToIGUI == true) && (status.equals(Stopless.STATUS.FAILED) == true))
        {
            final StringBuilder msg = new StringBuilder(reason);

            switch(stopless.getType()) {
                case RECOVERY:
                case RECOVERY_SWROD:
                    msg.insert(0, "Failed to execute stop-less recovery for reporting application \"" + stopless.getRcdApp() + "\": ");
                    msg.append(". The trigger may not be automatically resumed; a manual intervention may be needed");
                    msg.append(". In such a case, consult the corresponding detector expert before manually enabling the trigger");
                    break;
                case REMOVAL:
                case REMOVAL_SWROD:
                    msg.insert(0, "Failed to execute stop-less removal for reporting application \"" + stopless.getRcdApp() + "\": ");
                    break;
                default:
                    break;
            }

            this.reportToIGUI(msg.toString());
        }
    }

    private void reportToIGUI(final String msg) {
        try {
            this.execIGUI.showError(msg);
        }
        catch(final ExecutorIGUIException ex) {
            ers.Logger.error(ex);
        }
    }

    private Token publishHoldInfo(final Stopless stopless, final DetectorMask dmask) {
        // Publish name of detector which causes dead-time
        final HoldTriggerInfoNamed.CausedBy detIdEnum = HoldsTrigger.getCausedBy(dmask);
        if(detIdEnum.equals(HoldTriggerInfoNamed.CausedBy.Other) == true) {
            this.injectFailure(stopless,
                               STATUS.PROBLEM,
                               "Failed to determine the detector of " + stopless.getRcdApp().trim() + ". Associated detector mask is "
                                               + dmask.toString());
        }
        
        Token t = null;
        
        try {
            t = HoldTriggerInfoPublisher.publish(this.daqReader.getPartitionName(), detIdEnum, Reason.StoplessRecovery);
        }
        catch(final InfoNotCompatibleException | RepositoryNotFoundException e) {
            this.injectFailure(stopless, STATUS.PROBLEM, "Failed to publish the detector name to IS", e);
        }
        
        return t;
    }
        
    private void completeRecovery(final Stopless stopless, final DetectorMask dmask, final Token publishToken, boolean failed, boolean resume) {
        try {
            if(resume == true) {
                this.execTrigger.resume(dmask);
            }

            if(failed == false) {
                final Stopless event = new Stopless(stopless.getRcdApp(),
                                                    stopless.getIsInternalError(),
                                                    stopless.getForceAuto(),
                                                    stopless.getProbComponents(),
                                                    stopless.getType(),
                                                    STATUS.DONE,
                                                    stopless.getId());
                this.cep.injectEvent(event);
            }
        }
        catch(final ExecutorTriggerException e) {
            if(failed == false) {
                // Do not send two FAILED events
                this.injectFailure(stopless, STATUS.FAILED, "Not able to resume trigger at the end of recovery", e);
            } else {
                ers.Logger.error(e);

                try {
                    this.execIGUI.showError("The trigger could not be resumed at the end of a stop-less recovery; manual intervention needed!");
                }
                catch(final ExecutorIGUIException ex) {
                    ers.Logger.error(ex);
                }
            }
        }
        finally {
            try {
                if(publishToken != null) {
                    HoldTriggerInfoPublisher.remove(publishToken);
                }
            }
            catch(final RepositoryNotFoundException | InfoNotFoundException e) {
                StoplessExecutor.log.error("Failed to remove IS information about trigger on hold: " + e, e);
            }
        }
    }
    
    ers.Issue sendResynchAndWait(final Stopless stopless, 
                                 final String app,
                                 final HoldTriggerInfo trgInfo,
                                 final String[] components,
                                 final String ersSubscription, 
                                 final Predicate<? super ers.Issue> condition) throws StoplessFailure 
    {
        ers.Issue receivedMessage = null;
        
        try (ExecutorERS sub = new ExecutorERS(this.daqReader.getPartitionName(),
                                               ersSubscription,
                                               condition,
                                               Integer.valueOf(1000)))
        {            
            // Send RESYNC to reporting app
            this.execRC.sendResynch(app, trgInfo, components, false, 0);

            // wait for HardwareRecoverd ers message
            final int timeoutInSeconds = 60;
            final boolean timeoutElapsed = !sub.waitForMessage(timeoutInSeconds);
            receivedMessage = sub.getIssue();

            // notify shifter if timeout occurred
            if((timeoutElapsed == true) || (receivedMessage == null)) {
                throw new StoplessFailure("Did not receive any HardwareRecovered ers messsage " + "from application " + app
                                          + " within timeout (" + timeoutInSeconds + "s)");
            }
        }
        catch(final BadStreamConfiguration e) {
            throw new StoplessFailure("Not able to subscribe to HardwareRecovered messages", e, true);
        }
        catch(final ExecutorRCException e) {
            throw new StoplessFailure("Failed to send resync command to " + app, e, true);
        }
        catch(final InterruptedException e) {
            // Restore the interrupted state
            Thread.currentThread().interrupt();

            throw new StoplessFailure("Failure while waiting for HardwareRecovered messages", e);
        }

        return receivedMessage;
    }
    
    
    
    private void doRecovery_SWROD(final Stopless[] stoplessArr) {
        final Stopless stopless = stoplessArr[0];
        
        if(this.readLock.tryLock() == true) {
            final ReentrantLock appLock = this.getAppLock(stopless.getRcdApp().trim());
            appLock.lock();
            try {
                if(stopless.getIsInternalError() == Boolean.TRUE) {
                    this.injectFailure(stopless, STATUS.FAILED, "Application " + stopless.getRcdApp() + " is in an error state");
                } else {
                    boolean resumeTrigger = false;
                    boolean failed = false;
                         
                    DetectorMask dmask = new DetectorMask();
                    Token publishToken = null;
                    try {
                        // Check resources and reporting app
                        this.checkReportingApp(stopless);
                        final Set<String> componentsToRecover = this.checkSWRODResources(stopless, false);
                    
                        final String reportingApp = stopless.getRcdApp().trim();
                        
                        // Hold the trigger
                        dmask = this.daqReader.getDetMaskForApplication(reportingApp);
                        final HoldTriggerInfo trgInfo;                        
                        try {
                            trgInfo = this.execTrigger.hold(dmask);
                        }
                        catch(final ExecutorTriggerException e) {
                            throw new StoplessFailure("Not able to hold the trigger", e);
                        }
                        
                        publishToken = this.publishHoldInfo(stopless, dmask);
                        
                        // Send the resynch command and wait for the ERS message
                        // The ERS message must contain a subset of the initial channel list                
                        final Predicate<ers.Issue> componnetsChecker = (final ers.Issue issue) -> {
                            final Set<String> recoveredComponents = StoplessExecutor.csvToSet(issue.parameters().get("appId"));        
                            return recoveredComponents.isEmpty() ? true
                                                                 : componentsToRecover.containsAll(recoveredComponents);                    
                        };
        
                        final ers.Issue receivedMessage = this.sendResynchAndWait(stopless, 
                                                                                  reportingApp, 
                                                                                  trgInfo, 
                                                                                  componentsToRecover.toArray(new String[componentsToRecover.size()]), 
                                                                                  "msg=rc::HardwareRecovered and qual=SWROD and app=" + reportingApp, 
                                                                                  componnetsChecker);
        
                        StoplessExecutor.log.debug("Received HardwareRecovered message(s) for app: " + reportingApp);
                        StoplessExecutor.log.debug("HardwareRecovered message(s) for app " + reportingApp + " has msg text "
                                                   + receivedMessage.getMessage());
                        
                        final String[] recoveredComponents = StoplessExecutor.csvToArray(receivedMessage.parameters().get("appId"), false);
                        if(recoveredComponents.length == 0) {
                            throw new StoplessFailure("No channels were recovered", true);
                        }
                        
                        // Enable the recovered components
                        try {
                            this.daqReader.enableComponents(recoveredComponents);
                        }
                        catch(final DAQConfigurationError ex) {
                            throw new StoplessFailure("Configuration error enabling components: " + ex, ex, true);
                        }
                            
                        // notify the ResourcesInfoProvider
                        final String[] disabled = new String[] {};
                        final String[] enabled = recoveredComponents;
                        try {
                            this.execRC.notifyResInfoProvider(disabled, enabled);
                        }
                        catch(final ExecutorRCException e) {
                            this.injectFailure(stopless, STATUS.PROBLEM, "Not able to contact the ResInfoProvider", e);
                        }
                        
                        // Increase luminosity block
                        try {
                            this.execTrigger.increaseLumiBlock();
                        }
                        catch(final ExecutorTriggerException e) {
                            this.injectFailure(stopless, STATUS.PROBLEM, "Failed to increase luminosity block", e);
                        }
                                                
                        resumeTrigger = true;
                    }
                    catch(final StoplessFailure ex) {
                        this.injectFailure(stopless, STATUS.FAILED, ex.getMessage(), ex);
                        
                        failed = true;
                        resumeTrigger = ex.triggerCanBeResumed();
                    }
                    finally {
                        this.completeRecovery(stopless, dmask, publishToken, failed, resumeTrigger);
                    }
                }
            }
            finally {
                appLock.unlock();
                this.readLock.unlock();
            }
        } else {
            this.injectFailure(stopless, 
                               STATUS.FAILED, 
                               "Execution could not be scheduled");
        }
    }

    private void doRecovery(final Stopless[] stoplessArr) {
        final Stopless stopless = stoplessArr[0];

        if(this.readLock.tryLock() == true) {
            final String reportingRcdApp = stopless.getRcdApp().trim();
            
            final ReentrantLock appLock = this.getAppLock(reportingRcdApp);
            appLock.lock();
            try {        
                if(stopless.getIsInternalError() == Boolean.TRUE) {
                    this.injectFailure(stopless, STATUS.FAILED, "Application " + reportingRcdApp + " is in an error state");
                    return;
                }
        
                final dal.Partition dalPart = this.daqReader.getDalPartition();
        
                // Get all resources of partition
                final Map<String, ResourceBase> resourcesMap = this.daqReader.getResources();
        
                // verify if message objects exist and are disabled in database
                final String probComponents = stopless.getProbComponents().trim();
        
                final Map<String, ApplicationConfig> baseApps = this.daqReader.getBaseApplications();
                final Map<String, RCApplicationConfig> rcApps = this.daqReader.getRCApplications();
        
                if(!reportingRcdApp.isEmpty()) {
                    if((!baseApps.containsKey(reportingRcdApp)) && (!rcApps.containsKey(reportingRcdApp))) {
                        this.injectFailure(stopless, STATUS.FAILED, "Application " + reportingRcdApp + " not found in configuration database");
                        return;
                    }
                } else {
                    this.injectFailure(stopless, STATUS.FAILED, "Reporting application not available");
                    return;
                }
        
                final String[] busyChannelNames = StoplessExecutor.csvToArray(probComponents, true);
                if(busyChannelNames.length == 0) {
                    this.injectFailure(stopless, STATUS.FAILED, "No busy channel names are given for app " + reportingRcdApp);
                    return;
                }
        
                // search for the resources corresponding to the busy channel names
                final Set<String> compNameList = this.getBusySources(busyChannelNames);
                if((compNameList == null) || (compNameList.isEmpty() == true)) {
                    this.injectFailure(stopless, STATUS.FAILED, "Not all busy sources found for application " + reportingRcdApp);
                    return;
                }
        
                final String[] compNames = compNameList.toArray(new String[compNameList.size()]);
                // check resources
                if(compNames.length == 0) {
                    this.injectFailure(stopless, STATUS.FAILED, "No problematic resources given for application " + reportingRcdApp);
                    return;
                }
        
                for(final String name : compNames) {
                    if(!resourcesMap.containsKey(name)) {
                        this.injectFailure(stopless, STATUS.FAILED, "Problematic resource " + name + " not found in database");
                        return;
                    }
        
                    final Component resource = Component_Helper.cast(resourcesMap.get(name));
                    if(resource != null) {
                        try {
                            if(!resource.disabled(dalPart)) {
                                this.injectFailure(stopless, STATUS.FAILED, "Problematic resource " + name + " already enabled in database");
                                return;
                            }
                        }
                        catch(final ConfigException ex) {
                            this.injectFailure(stopless, STATUS.FAILED, "Cannot check the disabled status of resource " + name + ": " + ex, ex);
                            return;
                        }
                    } else {
                        this.injectFailure(stopless, STATUS.FAILED, "Problematic resource " + name + " cannot be cast to component object");
                        return;
                    }
                }
        
                boolean failed = false;
                boolean resumeTrigger = false;
                final DetectorMask dmask = this.daqReader.getDetMaskForApplication(reportingRcdApp);
                Token publishToken = null;
                try {
                    final HoldTriggerInfo trgInfo;                    
                    try {
                        trgInfo = this.execTrigger.hold(dmask);
                    }
                    catch(final ExecutorTriggerException e) {
                        throw new StoplessFailure("Not able to hold the trigger", e);
                    }
                    
                    publishToken = this.publishHoldInfo(stopless, dmask);
                    
                    // The ERS message must contain a subset of the initial channel list
                    final Predicate<ers.Issue> channelChecker = (final ers.Issue issue) -> {
                        final Set<String> newBusyChannels = StoplessExecutor.csvToSet(issue.parameters().get("appId"));         
                        final boolean done = newBusyChannels.isEmpty() ? true
                                                                       : Arrays.stream(busyChannelNames).collect(Collectors.toSet()).containsAll(newBusyChannels);                        
                        return done;
                    };
        
                    final ers.Issue receivedMessage = this.sendResynchAndWait(stopless, 
                                                                              reportingRcdApp, 
                                                                              trgInfo, 
                                                                              compNames, 
                                                                              "msg=rc::HardwareRecovered and app=" + reportingRcdApp, 
                                                                              channelChecker);
                    
        
                    StoplessExecutor.log.debug("Received HardwareRecovered message(s) for app: " + reportingRcdApp);
                    StoplessExecutor.log.debug("HardwareRecovered message(s) for app " + reportingRcdApp + " has msg text "
                                               + receivedMessage.getMessage());
        
                    // search for resources corresponding to busy channel names
                    final String[] recoveredBusyChannels = StoplessExecutor.csvToArray(receivedMessage.parameters().get("appId"), true);
                    if(recoveredBusyChannels.length == 0) {
                        throw new StoplessFailure("No channels were recovered", true);
                    }
        
                    final String appToBeNotified = receivedMessage.parameters().get("sender").trim();
        
                    final Set<String> recoveredCompList = this.getBusySources(recoveredBusyChannels);
                    if((recoveredCompList == null) || (recoveredCompList.isEmpty() == true)) {
                        throw new StoplessFailure("Cannot find all the busy sources for busy channels \""
                                                  + StringUtils.join(recoveredBusyChannels, ",") + "\"",
                                                  true);
                    }
        
                    final String[] recoveredComps = recoveredCompList.toArray(new String[recoveredCompList.size()]);
        
                    // now enable recovered components
                    final List<String> newEnabledComps;
                    try {
                        newEnabledComps = this.daqReader.enableComponentsAndGet(recoveredComps);
                    }
                    catch(final DAQConfigurationError ex) {
                        throw new StoplessFailure("Cannot set the enabled status for components: " + ex, ex, true);
                    }
        
                    // check which new enabled resources are of type InputChannel
                    final List<String> newEnabledChannels = new ArrayList<String>();
                    for(final String enabledComp : newEnabledComps) {
                        final ResourceBase res = resourcesMap.get(enabledComp);
                        final InputChannel channel = InputChannel_Helper.cast(res);
                        if(channel != null) {
                            newEnabledChannels.add(enabledComp);
                            StoplessExecutor.log.debug("New enabled channel: " + enabledComp);
                        }
                    }
        
                    if(newEnabledChannels.isEmpty()) {
                        try {
                            this.daqReader.disableComponents(recoveredComps);
                        }
                        catch(final DAQConfigurationError ex) {
                            this.injectFailure(stopless, STATUS.PROBLEM, "Failed to restore the disabled status of components: " + ex, ex);
                        }
                        
                        throw new StoplessFailure("No previously disabled channel could be identified for enabling", true);
                    }
        
                    // Send ENABLE command to relevant ROS applications
                    final Map<String, InputChannel> channelMapChannel = this.daqReader.getChannelMapChannel();
                    final Map<String, ReadoutApplication> channelMapRoApp = this.daqReader.getChannelMapRoApp();
                    for(final String channelName : newEnabledChannels) {
                        final InputChannel channel = channelMapChannel.get(channelName);
                        final ReadoutApplication roApp = channelMapRoApp.get(channelName);
                        if((roApp != null) && (channel != null)) {
                            try {
                                this.execRC.sendChangeStatus(roApp.UID(), true, new String[] {channel.UID()});
                            }
                            catch(final ExecutorRCException e) {
                                if(ROS_Helper.cast(roApp) != null) {
                                    try {
                                        this.daqReader.disableComponents(recoveredComps);
                                    }    
                                    catch(final DAQConfigurationError ex) {
                                        this.injectFailure(stopless, STATUS.PROBLEM, "Failed to restore the disabled status of components: " + ex, ex);
                                    }
    
                                    throw new StoplessFailure("Failed to send enable command to " + roApp.UID(), e);
                                }
        
                                this.injectFailure(stopless, STATUS.PROBLEM, "Failed to send enable command to " + roApp.UID(), e);
                            }
                        }
                    }
        
                    // send ENABLE command to reporting application
                    try {
                        this.execRC.sendChangeStatus(appToBeNotified, true, recoveredBusyChannels);
                    }
                    catch(final ExecutorRCException e) {
                        try {
                            this.daqReader.disableComponents(recoveredComps);
                        }
                        catch(final DAQConfigurationError ex) {
                            this.injectFailure(stopless, STATUS.PROBLEM, "Failed to restore the disabled status of components: " + ex, ex);
                        }

                        throw new StoplessFailure("Failed to send enable command to " + appToBeNotified, e);
                    }
        
                    // notify the ResourcesInfoProvider
                    final String[] disabled = new String[] {};
                    final String[] enabled = recoveredComps;
                    try {
                        this.execRC.notifyResInfoProvider(disabled, enabled);
                    }
                    catch(final ExecutorRCException e) {
                        this.injectFailure(stopless, STATUS.PROBLEM, "Not able to contact the ResInfoProvider", e);
                    }
        
                    // increase luminosity block
                    try {
                        this.execTrigger.increaseLumiBlock();
                    }
                    catch(final ExecutorTriggerException e) {
                        this.injectFailure(stopless, STATUS.PROBLEM, "Failed to increase luminosity block", e);
                    }
        
                    // TODO The following is temporary! Better is to somehow wait for all channels to be enabled.
                    try {
                        StoplessExecutor.log.info("Stopless recovery: 1 second till trigger is released.");
                        Thread.sleep(1000);
                    }
                    catch(final InterruptedException e) {
                        this.injectFailure(stopless, STATUS.PROBLEM, "Problem while sleeping", e);
                    }
        
                    resumeTrigger = true;
                }
                catch(final StoplessFailure ex) {
                    this.injectFailure(stopless, STATUS.FAILED, ex.getMessage(), ex);
                    failed = true;
                    resumeTrigger = ex.triggerCanBeResumed();
                }
                finally {
                    this.completeRecovery(stopless, dmask, publishToken, failed, resumeTrigger);
                }
            }
            finally {
                appLock.unlock();
                this.readLock.unlock();
            }
        } else {
            this.injectFailure(stopless, 
                               STATUS.FAILED, 
                               "Execution could not be scheduled");
        }
    }

    private void reportRemovalErrorsToIGUI(final List<? extends Stopless> failures) {
        if(failures.size() != 0) {
            final StringBuilder msg = new StringBuilder("Failed to execute stop-less removal for components: ");

            for(final Stopless s : failures) {
                msg.append("\n");
                msg.append(s.getConcernedComponent());
                if(s.getError().booleanValue() == true) {
                    msg.append(": ");
                    msg.append(s.getErrorDesc());
                }
            }

            this.reportToIGUI(msg.toString());
        }
    }

    private void checkReportingApp(final Stopless s) throws StoplessFailure {
        final String reportingApp = s.getRcdApp().trim();
        final Map<String, RCApplicationConfig> rcApps = this.daqReader.getRCApplications();
        if(rcApps.containsKey(reportingApp) == false) {
            throw new StoplessFailure("Application \"" + reportingApp + "\" is not a Run Control application");
        }
    }

    private static String[] csvToArray(final String csv, final boolean distinct) {
        final Stream<String> s = Arrays.stream(csv.trim().split(",")).map(String::trim).filter(it -> !it.isEmpty());
        if(distinct == true) {
            return s.distinct().toArray(String[]::new);
        }
        
        return s.toArray(String[]::new);
    }
    
    private static Set<String> csvToSet(final String csv) {
        return Arrays.stream(csv.trim().split(",")).map(String::trim).filter(it -> !it.isEmpty()).collect(Collectors.toSet());
    }
    
    private Set<String> checkSWRODResources(final Stopless s, final boolean shouldBeEnabled) throws StoplessFailure {
        final Set<String> checkedComponents = new TreeSet<>();

        final dal.Partition dalPart = this.daqReader.getDalPartition();

        // Sanitize
        final String[] componentsToDisable = StoplessExecutor.csvToArray(s.getProbComponents(), false);
        if(componentsToDisable.length > 0) {
            // Check the status of components
            final Map<String, ResourceBase> allResources = this.daqReader.getResources();
            for(final String c : componentsToDisable) {
                final ResourceBase res = allResources.get(c);
                if(res != null) {
                    final boolean isDisabled;
                    try {
                        isDisabled = res.disabled(dalPart);
                    }
                    catch(final ConfigException ex) {
                        this.injectFailure(s, STATUS.PROBLEM, "Could not determine the disabled status of resource \"" + res.UID() + "\": " + ex, ex, false);
                        continue;
                    }
                        
                    if(isDisabled != shouldBeEnabled) {
                        // Check that the resource is either a SwRodRob or a SwRodInputLink
                        if((SwRodRob_Helper.cast(res) != null) || (SwRodInputLink_Helper.cast(res) != null)) {
                            checkedComponents.add(c);
                        } else {
                            this.injectFailure(s,
                                               STATUS.PROBLEM,
                                               "Resource \"" + res.UID() + "\" is neither a ROB or an ELink",
                                               null,
                                               false);
                        }
                    } else {
                        this.injectFailure(s,
                                           STATUS.PROBLEM,
                                           "Resource \"" + res.UID() + "\" is already " + ((shouldBeEnabled == true) ? "disabled" : "enabled"),
                                           null,
                                           false);
                    }
                } else {
                    this.injectFailure(s, STATUS.PROBLEM, "Component \"" + c + "\" could not be found", null, false);
                }
            }

            if(checkedComponents.isEmpty() == true) {
                throw new StoplessFailure("None of the channels to be " + ((shouldBeEnabled == true) ? "disabled" : "enabled") + " is valid");
            }
        } else {
            throw new StoplessFailure("Stop-less " + ((shouldBeEnabled == true) ? "removal" : "recovery")
                                      + " invoked with an empty list of channels to be " + ((shouldBeEnabled == true) ? "disabled" : "enabled"));
        }

        return checkedComponents;
    }

    private boolean isRemovalAutomatic(final Stopless[] stArray) {
        // Perform the stop-less removal always automatically when requested
        // If not, then the stop-less removal is executed automatically only when the machine mode is one
        // of the modes listed in MACHINE_MODES.
        // If not, then the stop-less removal is executed automatically only when the beam mode
        // is one the modes listed in BEAM_MODES.
        // The automatic mode can be forced setting the TDAQ_CES_FORCE_REMOVAL_AUTOMATIC env variable to "true"        
        final BooleanSupplier automaticFromEnv = () -> {
            final String forceAuto = System.getenv(StoplessExecutor.TDAQ_CES_FORCE_REMOVAL_AUTOMATIC);
            return Boolean.valueOf(forceAuto).booleanValue();
        };
        
        final BooleanSupplier automaticFromRequest = () -> {
            for(final Stopless se : stArray) {
                if(se.getForceAuto().equals(Boolean.FALSE) == true) {
                    return false;
                }
            }
            
            return true;
        };
        
        final BooleanSupplier automaticFromMachineMode = () -> {
            final String machineMode = this.getMachineMode();
            if(machineMode == null) {
                for(final Stopless se : stArray) {
                    this.injectFailure(se, STATUS.PROBLEM, "Failed to get LHC machine mode", null, false);
                }
            }

            return StoplessExecutor.MACHINE_MODES.contains(machineMode);
        };
        
        final BooleanSupplier automaticFromBeamMode = () -> {
            final String beamMode = this.getBeamMode();
            if(beamMode == null) {
                for(final Stopless se : stArray) {
                    this.injectFailure(se, STATUS.PROBLEM, "Failed to get LHC beam mode", null, false);
                }
            }

            return StoplessExecutor.BEAM_MODES.contains(beamMode);
        };
        
        return (automaticFromEnv.getAsBoolean() 
                || automaticFromRequest.getAsBoolean() 
                || automaticFromMachineMode.getAsBoolean() 
                || automaticFromBeamMode.getAsBoolean());        
    }

    private void askUserAboutRemoval(final Stopless[] stoplessArray, final String message) throws StoplessFailure {
        UserAnswer answer;

        try {
            answer = this.execIGUI.askYesOrNo(message, 60 * 60 * 1000);
        }
        catch(final ExecutorIGUIException e) {
            for(final Stopless se : stoplessArray) {
                this.injectFailure(se, STATUS.PROBLEM, "Failed to show IGUI popup window. Proceeding.", e, false);
            }

            answer = null;
        }

        if(answer == null) {
            for(final Stopless se : stoplessArray) {
                this.injectFailure(se, STATUS.PROBLEM, "User interaction via IGUI commander failed. Proceeding.", null, false);
            }
        } else {
            StoplessExecutor.log.debug("Stopless removal: user answered proceed-question with " + answer.toString());

            if(answer.equals(UserAnswer.CANCELLED) || answer.equals(UserAnswer.NO)) {
                throw new StoplessFailure("User abort");
            }
        }
    }

    private String getSWRODApp(final String resource) throws StoplessFailure {
        // Look first if "resource" is associated to a ReadOutApplication
        // and check that the ReadOutApplication is actually a SWROD
        // This is the case if "resource" is a actually a ROB
        final ReadoutApplication roApp = this.daqReader.getChannelMapRoApp().get(resource);
        if((roApp != null) && (SwRodApplication_Helper.cast(roApp) != null)) {
            return roApp.UID();
        }

        // If we are here it means that "resource" may not be a ROB but it is
        // actually an ELink (an ELink is not an InputChannel)
        final SwRodApplication swRodApp = this.daqReader.getElinkMapSwRod().get(resource);
        if(swRodApp != null) {
            return swRodApp.UID();
        }

        throw new StoplessFailure("Cannot find a SWROD application for the channels to be disabled");
    }

    public void doRemoval_SWROD(final Stopless[] stoplessArr) {
        if(this.readLock.tryLock() == true) {
            try {
                // Check if the operator has to be asked or not
                final boolean automatic = this.isRemovalAutomatic(stoplessArr);
                boolean abortedByTheUser = false;
                if(automatic == false) {
                    try {
                        final String msg = "Stopless removal procedure is going to disable the following ROBs or ELinks: "
                                           + Arrays.stream(stoplessArr).map(s -> s.getConcernedComponent()).collect(Collectors.joining(", "))
                                           + ". Do you want to proceed?";
        
                        this.askUserAboutRemoval(stoplessArr, msg);
                    }
                    catch(final StoplessFailure ex) {
                        abortedByTheUser = true;
        
                        for(final Stopless st : stoplessArr) {
                            this.injectFailure(st, STATUS.FAILED, ex.getMessage(), null, false);
                        }
                    }
                }
        
                if(abortedByTheUser == false) {
                    final List<Stopless> success = new ArrayList<>();
                    final List<Stopless> failures = new ArrayList<>();
        
                    for(final Stopless st : stoplessArr) {
                        final String reportingApp = st.getRcdApp().trim();
                        
                        final ReentrantLock appLock = this.getAppLock(reportingApp);
                        appLock.lock();
                        try {        
                            this.checkReportingApp(st);
                            final String[] componentsToDisable = this.checkSWRODResources(st, true).toArray(new String[0]);
                            final String swRODName = this.getSWRODApp(componentsToDisable[0]);
                            
                            // Disable components
                            try {
                                this.daqReader.disableComponents(componentsToDisable);
                            }
                            catch(final DAQConfigurationError ex) {
                                throw new StoplessFailure("Failed to set the disabled status of the components: " + ex, ex);
                            }
                                
                            // If the reporting app is the SWROD, then send the command only to the SWROD
                            try {
                                this.execRC.sendChangeStatus(swRODName.toString(), false, componentsToDisable);
                            }
                            catch(final ExecutorRCException ex) {
                                // Enable back the components
                                try {
                                    this.daqReader.enableComponents(componentsToDisable);
                                }
                                catch(final DAQConfigurationError exx) {
                                    this.injectFailure(st, STATUS.PROBLEM, "Cannot restore the status of components: " + exx, exx, false);
                                }
                                    
                                throw new StoplessFailure("Failed to notify the SWROD application \"" + swRODName.toString() + "\"", ex);
                            }
        
                            if(reportingApp.equals(swRODName.toString()) == false) {
                                try {
                                    this.execRC.sendChangeStatus(reportingApp, false, StoplessExecutor.csvToArray(st.getProbComponents(), false));
                                }
                                catch(final ExecutorRCException ex) {
                                    // Enable back the components
                                    try {
                                        this.daqReader.enableComponents(componentsToDisable);
                                    }
                                    catch(final DAQConfigurationError exx) {
                                        this.injectFailure(st, STATUS.PROBLEM, "Cannot restore the status of components: " + exx, exx, false);
                                    }
                                        
                                    throw new StoplessFailure("Failed to notify the reporting application \"" + reportingApp + "\"", ex);
                                }
                            }
        
                            // Notify the ResourcesInfoProvider
                            try {
                                final String[] disabled = componentsToDisable;
                                final String[] enabled = new String[] {};
                                this.execRC.notifyResInfoProvider(disabled, enabled);
                            }
                            catch(final ExecutorRCException ex) {
                                this.injectFailure(st, STATUS.PROBLEM, "Not able to contact the ResInfoProvider", ex, false);
                            }
        
                            success.add(st);
                        }
                        catch(final StoplessFailure ex) {
                            failures.add(st);
                            st.setError(ex.getMessage(), ex);
        
                            this.injectFailure(st, STATUS.FAILED, ex.getMessage(), ex, false);
                        }
                        finally {
                            appLock.unlock();
                        }
                    }
        
                    // Increase luminosity block
                    try {
                        this.execTrigger.increaseLumiBlock();
                    }
                    catch(final ExecutorTriggerException ex) {
                        for(final Stopless s : success) {
                            this.injectFailure(s, STATUS.PROBLEM, "Failed to increase luminosity block", ex);
                        }
                    }
        
                    for(final Stopless se : success) {
                        final Stopless event = new Stopless(se.getRcdApp(),
                                                            se.getIsInternalError(),
                                                            se.getForceAuto(),
                                                            se.getProbComponents(),
                                                            se.getType(),
                                                            STATUS.DONE,
                                                            se.getId());
                        this.cep.injectEvent(event);
                    }
        
                    this.reportRemovalErrorsToIGUI(failures);
                }
            }
            finally {
                this.readLock.unlock();
            }
        } else {
            final List<Stopless> all = Arrays.stream(stoplessArr).collect(Collectors.toList());
            final String errMsg = "Execution could not be scheduled";
            all.forEach(s -> { s.setError(errMsg); this.injectFailure(s, STATUS.FAILED, errMsg, null, false); });
            this.reportRemovalErrorsToIGUI(all);
        }
    }

    public void doRemoval(final Stopless[] stoplessArr) {
        if(this.readLock.tryLock() == true) {
            
            // Keep always the same locking order
            final ArrayList<ReentrantLock> orderedLocks = Stream.of(stoplessArr).map(stop -> stop.getRcdApp().trim()) // Get the app name
                                                                                .collect(Collectors.toCollection(TreeSet::new)) // Collect and order the app names
                                                                                .stream()
                                                                                .map(name -> { return this.getAppLock(name); }) // Get the lock object for each app
                                                                                .collect(Collectors.toCollection(ArrayList::new)); // Collect preserving the order
            
            for(final ReentrantLock l : orderedLocks) {
                l.lock();
            }
            
            try {
                // Example: probComp: busy-roc1rod0-ttcbusy1-in0, rcdApp: l1calo-ttc-rc
                final dal.Partition dalPart = this.daqReader.getDalPartition();
        
                // Get all resources of partition
                final Map<String, ResourceBase> resourcesMap = this.daqReader.getResources();
        
                // Mapping the events to the corresponding list of busy channels
                final Map<Stopless, Set<String>> stoplessToBusyChannels = new HashMap<>();
                for(final Stopless sl : stoplessArr) {
                    final String[] bs = StoplessExecutor.csvToArray(sl.getProbComponents(), false);

                    final Set<String> busyChannels = new TreeSet<>();
                    Collections.addAll(busyChannels, bs);
        
                    stoplessToBusyChannels.put(sl, busyChannels);
                }
        
                // verify that message objects exist and are enabled in database
                final Map<String, ApplicationConfig> baseApps = this.daqReader.getBaseApplications();
                final Map<String, RCApplicationConfig> rcApps = this.daqReader.getRCApplications();
        
                // Mapping the events to the corresponding list of busy sources
                final Map<Stopless, Set<String>> stoplessToBusySources = new HashMap<>();
        
                {
                    final List<Stopless> failures = new ArrayList<>();
        
                    final Iterator<Entry<Stopless, Set<String>>> it = stoplessToBusyChannels.entrySet().iterator();
                    while(it.hasNext() == true) {
                        final Map.Entry<Stopless, Set<String>> me = it.next();
        
                        final Stopless item = me.getKey();
                        final String rcdApp = item.getRcdApp();
        
                        if(!rcdApp.isEmpty()) {
                            if((!baseApps.containsKey(rcdApp)) && (!rcApps.containsKey(rcdApp))) {
                                this.injectFailure(item,
                                                   STATUS.FAILED,
                                                   "Application " + rcdApp + " not found in configuration database",
                                                   null,
                                                   false);
        
                                failures.add(item);
                                it.remove();
        
                                continue;
                            }
                        } else {
                            this.injectFailure(item, STATUS.FAILED, "Reporting application not available", null, false);
        
                            failures.add(item);
                            it.remove();
        
                            continue;
                        }
        
                        // search for resources corresponding to busy channel names
                        final Set<String> channelNames = me.getValue();
                        if(channelNames.size() == 0) {
                            this.injectFailure(item, STATUS.FAILED, "No busy channel names are given for app " + rcdApp, null, false);
        
                            failures.add(item);
                            it.remove();
        
                            continue;
                        }
        
                        final Set<String> compNameListHelper = this.getBusySources(channelNames.toArray(new String[channelNames.size()]));
                        if((compNameListHelper == null) || (compNameListHelper.isEmpty() == true)) {
                            this.injectFailure(item,
                                               STATUS.FAILED,
                                               "Cannot find all the busy sources for busy channels \""
                                                              + StringUtils.join(channelNames.iterator(), ",") + "\"",
                                               null,
                                               false);
        
                            failures.add(item);
                            it.remove();
        
                            continue;
                        }
        
                        stoplessToBusySources.put(item, compNameListHelper);
                    }
        
                    this.reportRemovalErrorsToIGUI(failures);
                }
        
                // If empty, then all the events FAILED
                if(stoplessToBusyChannels.isEmpty() == true) {
                    return;
                }
        
                {
                    // Here we just get the list of disabled components/channels in order to properly
                    // send a message to the operator (if needed)
                    final List<String> newDisabledComps = new ArrayList<>();
                
                    {
                        // check that resources are not already disabled
                        final Set<String> allResources = resourcesMap.keySet();
        
                        final List<Stopless> failures = new ArrayList<>();
        
                        final Iterator<Map.Entry<Stopless, Set<String>>> it_2 = stoplessToBusySources.entrySet().iterator();
                        while(it_2.hasNext() == true) {
                            final Map.Entry<Stopless, Set<String>> me = it_2.next();
                            final Stopless item = me.getKey();
        
                            // 1 - Check that all the busy sources are in the list of available resources
                            final Set<String> busySources = me.getValue();
                            if(allResources.containsAll(busySources) == false) {
                                busySources.removeAll(allResources);
        
                                final String msg = "Problematic resources \"" + StringUtils.join(busySources.iterator(), ",")
                                                   + "\" not found in database";
                                this.injectFailure(item, STATUS.FAILED, msg, null, false);
        
                                failures.add(item);
                                stoplessToBusyChannels.remove(item);
                                it_2.remove();
        
                                continue;
                            }
        
                            // 2 - Check that all the busy sources can be casted to Component and that they are not already disabled
                            final List<String> notCastableResources = new ArrayList<>();
                            final List<String> alreadyDisabledResources = new ArrayList<>();
                            
                            try {
                                for(final String bs : busySources) {
                                    final Component resource = Component_Helper.cast(resourcesMap.get(bs));
                                    if(resource == null) {
                                        notCastableResources.add(bs);
                                    } else if(resource.disabled(dalPart) == true) {
                                        alreadyDisabledResources.add(bs);
                                    }
                                }
                            }
                            catch(final ConfigException ex) {
                                this.injectFailure(item, STATUS.FAILED, "Failed to determine the disabled status of components", ex, false);
                                
                                failures.add(item);
                                stoplessToBusyChannels.remove(item);
                                it_2.remove();
        
                                continue;
                            }
        
                            if((notCastableResources.isEmpty() == false) || (alreadyDisabledResources.isEmpty() == false)) {
                                final StringBuilder msg = new StringBuilder();
        
                                if(notCastableResources.isEmpty() == false) {
                                    msg.append("Problematic resources \"");
                                    msg.append(StringUtils.join(notCastableResources.iterator(), ","));
                                    msg.append("\" cannot be cast to component object");
                                }
        
                                if(alreadyDisabledResources.isEmpty() == false) {
                                    msg.append("Problematic resources \"");
                                    msg.append(StringUtils.join(alreadyDisabledResources.iterator(), ","));
                                    msg.append("\" already disabled in the database");
                                }
        
                                this.injectFailure(item, STATUS.FAILED, msg.toString(), null, false);
        
                                failures.add(item);
                                stoplessToBusyChannels.remove(item);
                                it_2.remove();
        
                                continue;
                            }
        
                            // This trick is just to have the new list of disabled components
                            // The real disabling is done afterwards
                            try {
                                final String[] busySourceNames = busySources.toArray(new String[busySources.size()]);
                                newDisabledComps.addAll(this.daqReader.disableComponentsAndGet(busySourceNames));
                                this.daqReader.enableComponents(busySourceNames);
                            }
                            catch(final DAQConfigurationError ex) {
                                this.injectFailure(item, STATUS.FAILED, "Failed to determine the disabled status of components", ex, false);
                                
                                failures.add(item);
                                stoplessToBusyChannels.remove(item);
                                it_2.remove();
        
                                continue;
                            }
                        }
                                
                        this.reportRemovalErrorsToIGUI(failures);
                    }
        
                    // If this is empty, then all the events FAILED
                    if(stoplessToBusySources.isEmpty() == true) {
                        return;
                    }
        
                    // From here the lock is not needed: just retrieving information and sending messages to the operator
                    // IMPORTANT: do not keep the lock while sending the message to the IGUI
        
                    final Set<String> newDisabledChannels = this.getDisabledChannels(newDisabledComps);
                    if(newDisabledChannels.isEmpty() == true) {
                        final Set<Stopless> sls = stoplessToBusySources.keySet();
        
                        for(final Stopless se : sls) {
                            this.injectFailure(se, STATUS.FAILED, "No enabled channel could be identified for disabling", null, false);
                        }
        
                        this.reportRemovalErrorsToIGUI(new ArrayList<Stopless>(sls));
        
                        return;
                    }
        
                    // Check whether the operator has to be asked
                    final boolean automaticStopless = this.isRemovalAutomatic(stoplessToBusySources.keySet().toArray(new Stopless[0]));
        
                    // Ask user for confirmation
                    if(automaticStopless == false) {
                        {
                            // Build flat list of all affected busy channels
                            final List<String> affectedBusyChannels = new ArrayList<String>();
                            for(final Set<String> channelNames : stoplessToBusyChannels.values()) {
                                for(final String channelName : channelNames) {
                                    affectedBusyChannels.add(channelName);
                                }
                            }
        
                            final String listOfBusyChannels = StringUtils.join(affectedBusyChannels.iterator(), ",");
                            final String listOfDisabledComponents = StringUtils.join(newDisabledChannels.iterator(), ",");
        
                            final String msg = "Stopless removal procedure is going to disable the following busy channels: " + listOfBusyChannels
                                               + " (" + listOfDisabledComponents + "). Do you want to proceed?";
                            try {
                                this.askUserAboutRemoval(stoplessToBusySources.keySet().toArray(new Stopless[0]), msg);
                            }
                            catch(final StoplessFailure ex) {
                                for(final Stopless se : stoplessToBusySources.keySet()) {
                                    this.injectFailure(se, STATUS.FAILED, ex.getMessage(), null, false);
                                }
        
                                return;
                            }
                        }
        
                        {
                            // Now check XOff status for relevant ROS applications
                            final Map<String, InputChannel> channelMapChannel = this.daqReader.getChannelMapChannel();
                            final Map<String, ReadoutApplication> channelMapRoApp = this.daqReader.getChannelMapRoApp();
                            boolean channelHasXoff = false;
                            for(final String channelName : newDisabledChannels) {
                                final InputChannel channel = channelMapChannel.get(channelName);
                                final ReadoutApplication roApp = channelMapRoApp.get(channelName);
                                if((channel != null) && (roApp != null) && (ROS_Helper.cast(roApp) != null)) {
                                    // Check the XOFF only for ROS applications
                                    final boolean isXoff = this.isXOFF(channel, roApp.UID());
                                    StoplessExecutor.log.debug("IS info: channel " + channelName + " of RoApp " + roApp.UID() + " has XOFF="
                                                               + String.valueOf(isXoff));
                                    if(isXoff) {
                                        channelHasXoff = true;
                                    }
                                }
                            }
        
                            if(channelHasXoff) {
                                UserAnswer answer;
        
                                try {
                                    answer = this.execIGUI.askYesOrNo("Stopless removal procedure is going to disable a channel which asserts XOff."
                                                                      + " Do you want to proceed?",
                                                                      60 * 60 * 1000);
                                }
                                catch(final ExecutorIGUIException e) {
                                    for(final Stopless se : stoplessToBusySources.keySet()) {
                                        this.injectFailure(se, STATUS.PROBLEM, "Failed to show IGUI popup window. Proceeding.", e);
                                    }
        
                                    answer = null;
                                }
        
                                if(answer != null) {
                                    StoplessExecutor.log.debug("Stopless removal: user answered proceed-question (XOff==true)  with "
                                                               + answer.toString());
                                    if(answer.equals(UserAnswer.CANCELLED) || answer.equals(UserAnswer.NO)) {
                                        for(final Stopless se : stoplessToBusySources.keySet()) {
                                            this.injectFailure(se, STATUS.FAILED, "User abort", null, false);
                                        }
        
                                        return;
                                    }
                                } else {
                                    for(final Stopless se : stoplessToBusySources.keySet()) {
                                        this.injectFailure(se, STATUS.PROBLEM, "User interaction via IGUI commander failed. Proceeding.");
                                    }
                                }
                            }
                        }
                    } else {
                        StoplessExecutor.log.debug("Stopless removal procedure is fully automatic, no operator or ROS Xoff check");
                    }
                }
        
                {        
                    final List<Stopless> failures = new ArrayList<>();
    
                    // check resources again: something may have changed while the lock was not kept
                    // or the same request could be received more than once
                    final Iterator<Map.Entry<Stopless, Set<String>>> it_3 = stoplessToBusySources.entrySet().iterator();
                    while(it_3.hasNext() == true) {
                        final Map.Entry<Stopless, Set<String>> me = it_3.next();
                        final Set<String> busySources = me.getValue();
                        final Stopless item = me.getKey();
    
                        final List<String> alreadyDisabled = new ArrayList<>();
                        try {
                            for(final String bs : busySources) {
                                final Component resource = Component_Helper.cast(resourcesMap.get(bs));
                                if(resource.disabled(dalPart) == true) {
                                    alreadyDisabled.add(bs);
                                }
                            }
                        }
                        catch(final ConfigException ex) {
                            final String msg = "Failed to determine the disabled status of components: " + ex;
                            this.injectFailure(item, STATUS.FAILED, msg, ex, false);

                            failures.add(item);
                            stoplessToBusyChannels.remove(item);
                            it_3.remove();

                            continue;
                        }
    
                        if(alreadyDisabled.isEmpty() == false) {
                            final String msg = "Problematic resources \"" + StringUtils.join(alreadyDisabled.iterator(), ",")
                                               + "\" already disabled in database";
                            this.injectFailure(item, STATUS.FAILED, msg, null, false);
    
                            failures.add(item);
                            stoplessToBusyChannels.remove(item);
                            it_3.remove();
    
                            continue;
                        }
    
                        // now really disable problematic components
                        final String[] busySourcesNames = busySources.toArray(new String[busySources.size()]);
                        final List<String> newDisabledComps;
                        try {
                            newDisabledComps = this.daqReader.disableComponentsAndGet(busySourcesNames);
                        }
                        catch(final DAQConfigurationError ex) {
                            this.injectFailure(item, STATUS.FAILED, "Failed to update the disabled status of components: " + ex, ex, false);
                            
                            failures.add(item);
                            stoplessToBusyChannels.remove(item);
                            it_3.remove();
    
                            continue;
                        }
                            
                        // check which new disabled resources are of type InputChannel
                        final Set<String> newDisabledChannels = this.getDisabledChannels(newDisabledComps);
    
                        if(newDisabledChannels.isEmpty() == true) {
                            try {
                                this.daqReader.enableComponents(busySourcesNames);
                            }
                            catch(final DAQConfigurationError ex) {
                                this.injectFailure(item, STATUS.PROBLEM, "Failed to restore the status of components", ex, false);
                            }
                            
                            final String msg = "No enabled channel could be identified for disabling";
                            this.injectFailure(item, STATUS.FAILED, msg, null, false);
                                    
                            failures.add(item);
                            stoplessToBusyChannels.remove(item);
                            it_3.remove();
    
                            continue;
                        }
    
                        for(final String ch : newDisabledChannels) {
                            StoplessExecutor.log.debug("New disabled channel: " + ch);
                        }
    
                        // Send DISABLE command to relevant ROS applications
                        ReadoutApplication roApp = null;
                        try {
                            for(final String channelName : newDisabledChannels) {
                                final InputChannel channel = this.daqReader.getChannelMapChannel().get(channelName);
                                roApp = this.daqReader.getChannelMapRoApp().get(channelName);
                                if((roApp != null) && (channel != null)) {
                                    try {
                                        this.execRC.sendChangeStatus(roApp.UID(), false, new String[] {channel.UID()});
                                    }
                                    catch(final ExecutorRCException e) {
                                        if(ROS_Helper.cast(roApp) != null) {
                                            throw e;
                                        }
    
                                        this.injectFailure(me.getKey(), STATUS.PROBLEM, "Failed to send disable command to " + roApp.UID(), e);
                                    }
                                }
                            }
                        }
                        catch(final ExecutorRCException e) {
                            try {
                                this.daqReader.enableComponents(busySourcesNames);
                            }
                            catch(final DAQConfigurationError ex) {
                                this.injectFailure(item, STATUS.PROBLEM, "Failed to restore the status of components", ex, false);
                            }
                            
                            final String roName = roApp.UID();
    
                            this.injectFailure(item, STATUS.FAILED, "Failed to send disable command to " + roName, e, false);
                                    
                            failures.add(item);
                            stoplessToBusyChannels.remove(item);
                            it_3.remove();
    
                            continue;
                        }
    
                        // Send DISABLE command to reporting applications
                        final String rcdApp = me.getKey().getRcdApp();
                        final String[] channelNames = StoplessExecutor.csvToArray(me.getKey().getProbComponents(), true);
                        try {
                            this.execRC.sendChangeStatus(rcdApp, false, channelNames);
                        }
                        catch(final ExecutorRCException e) {    
                            try {
                                this.daqReader.enableComponents(busySourcesNames);
                            }
                            catch(final DAQConfigurationError ex) {
                                this.injectFailure(item, STATUS.PROBLEM, "Failed to restore the status of components", ex, false);
                            }
                                
                            this.injectFailure(item, STATUS.FAILED, "Failed to send disable command to " + rcdApp, e, false);
                            
                            failures.add(item);
                            stoplessToBusyChannels.remove(item);
                            it_3.remove();
    
                            continue;
                        }
    
                        try {
                            // Notify the ResourcesInfoProvider
                            final String[] disabled = busySourcesNames;
                            final String[] enabled = new String[] {};
                            this.execRC.notifyResInfoProvider(disabled, enabled);
                        }
                        catch(final ExecutorRCException e) {
                            this.injectFailure(me.getKey(), STATUS.PROBLEM, "Not able to contact the ResInfoProvider", e);
                        }
                    }
                            
                    // increase luminosity block
                    try {
                        this.execTrigger.increaseLumiBlock();
                    }
                    catch(final ExecutorTriggerException e) {
                        for(final Stopless s : stoplessToBusySources.keySet()) {
                            this.injectFailure(s, STATUS.PROBLEM, "Failed to increase luminosity block", e);
                        }
                    }
    
                    this.reportRemovalErrorsToIGUI(failures);
    
                    for(final Stopless se : stoplessToBusySources.keySet()) {
                        final Stopless event = new Stopless(se.getRcdApp(),
                                                            se.getIsInternalError(),
                                                            se.getForceAuto(),
                                                            se.getProbComponents(),
                                                            se.getType(),
                                                            STATUS.DONE,
                                                            se.getId());
                        this.cep.injectEvent(event);
                    }
                }
            }
            finally {
                final ListIterator<ReentrantLock> it = orderedLocks.listIterator(orderedLocks.size());
                while(it.hasPrevious()) {
                    it.previous().unlock();
                }
                
                this.readLock.unlock();
            }
        } else {
            final List<Stopless> all = Arrays.stream(stoplessArr).collect(Collectors.toList());
            final String errMsg = "Execution could not be scheduled";
            all.forEach(s -> { s.setError(errMsg); this.injectFailure(s, STATUS.FAILED, errMsg, null, false); });
            this.reportRemovalErrorsToIGUI(all);
        }
    }

    private String getBeamMode() {
        String mode = null;

        try {
            final DdcStringInfoNamed beamMode = new DdcStringInfoNamed(new ipc.Partition("initial"), "LHC.BeamMode");
            beamMode.checkout();

            mode = beamMode.value;
        }
        catch(final Exception ex) {
            StoplessExecutor.log.warn("Failed to get LHC beam mode.", ex);
        }

        return mode;
    }

    private String getMachineMode() {
        String mode = null;

        try {
            final DdcStringInfoNamed beamMode = new DdcStringInfoNamed(new ipc.Partition("initial"), "LHC.MachineMode");
            beamMode.checkout();

            mode = beamMode.value;
        }
        catch(final Exception ex) {
            StoplessExecutor.log.warn("Failed to get LHC Machine mode.", ex);
        }

        return mode;
    }

    private boolean isXOFF(final InputChannel channel, final String ROSName) {
        try {
            this.execRC.sendPublish(ROSName);
            final String regExp = "ROS\\." + ROSName + "\\.ReadoutModule.*";
            final Pattern regExpPattern = Pattern.compile(regExp);
            final is.Criteria criteria = new is.Criteria(regExpPattern, RobinNPDescriptorReadoutModuleInfo.type, Criteria.Logic.AND);

            final ipc.Partition part = new ipc.Partition(this.daqReader.getPartitionName());
            final InfoList it = new InfoList(part, "DF", criteria);
            StoplessExecutor.log.debug("Found " + it.size() + " ROBIN modules in IS for ROS " + ROSName);
            for(int i = 0; i < it.getSize(); ++i) {
                final RobinNPDescriptorReadoutModuleInfo channelInfo = new RobinNPDescriptorReadoutModuleInfo();
                it.getInfo(i, channelInfo);

                for(int j = 0; j < channelInfo.rolXoffStat.length; ++j) {
                    if((channelInfo.rolXoffStat[j] != 0) && (channelInfo.robId[j] == channel.get_Id())) {
                        StoplessExecutor.log.debug("Found xoff for channel '" + channel.UID() + "' of ROS '" + ROSName + "'.");
                        return true;
                    }
                }
            }

            return false;
        }
        catch(final Exception ex) {
            // Remember that any remote call to the server may cause a CORBA exception to be thrown
            StoplessExecutor.log.error("Failed to check XOff in IS", ex);
            return false;
        }
    }

    private Set<String> getBusySources(final String busyChannelNames[]) {
        // search for resources corresponding to busy channel names
        final Set<String> compNameList = new TreeSet<String>();

        final Configuration config = this.daqReader.getConfig();
        for(final String busyChannelName : busyChannelNames) {
            try {
                final BusyChannel busyChannel = BusyChannel_Helper.get(config, busyChannelName);
                if(busyChannel != null) {
                    final ResourceBase busySource = busyChannel.get_BusySource();
                    if(busySource != null) {
                        final String busySourceName = busySource.UID();
                        StoplessExecutor.log.debug("Found resource " + busySourceName + " for busy channel " + busyChannelName);
                        compNameList.add(busySourceName);
                    }
                } else {
                    StoplessExecutor.log.error("Cannot find busy source for busy channel " + busyChannelName);
                }
            }
            catch(final Exception e) {
                StoplessExecutor.log.error("Configuration for busy channel " + busyChannelName + " not found.", e);
                return null;
            }
        }

        return compNameList;
    }

    private Set<String> getDisabledChannels(final List<String> disabledComponents) {
        // check which new disabled resources are of type InputChannel
        final Set<String> newDisabledChannels = new TreeSet<String>();

        for(final String disabledComp : disabledComponents) {
            final ResourceBase res = this.daqReader.getResources().get(disabledComp);
            if(res != null) {
                final InputChannel channel = InputChannel_Helper.cast(res);
                // final RobinDataChannel channel = RobinDataChannel_Helper.cast(res);
                if(channel != null) {
                    newDisabledChannels.add(disabledComp);
                }
            }
        }

        return newDisabledChannels;
    }
}
