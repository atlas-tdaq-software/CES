package chip.subscriber.recovery;

import is.InfoNotFoundException;
import is.RepositoryNotFoundException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import rc.HoldTriggerInfoNamed.CausedBy;
import rc.HoldTriggerInfoNamed.Reason;
import chip.CEPService;
import chip.CHIPException.ExecutorIGUIException;
import chip.CHIPException.ExecutorRCException;
import chip.CHIPException.ExecutorTriggerException;
import chip.CHIPException.WaitTimeoutException;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.issue.Issue;
import chip.event.issue.recovery.L1Calo;
import chip.event.issue.recovery.L1Calo.STATUS;
import chip.executor.ExecutorIGUI;
import chip.executor.ExecutorRC;
import chip.executor.ExecutorTrigger;
import chip.subscriber.DeferredActionExecutor;
import chip.utils.HoldTriggerInfoPublisher;
import chip.utils.HoldTriggerInfoPublisher.Token;
import daq.eformat.DetectorMask;


public class L1CaloExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(L1CaloExecutor.class);

    private enum EXIT_REASON {
        FAILURE,
        SUCCESS
    }

    private final ExecutorRC execRC;
    private final ExecutorIGUI execIGUI;
    private final ExecutorTrigger execTrigger;
    private final DAQConfigReader daqReader;

    public L1CaloExecutor(final ExecutorRC execRC,
                          final ExecutorIGUI execIGUI,
                          final ExecutorTrigger execTrigger,
                          final EsperDAO dao,
                          final DAQConfigReader daqReader)
    {
        this.execRC = execRC;
        this.execIGUI = execIGUI;
        this.execTrigger = execTrigger;
        this.daqReader = daqReader;
    }

    @Override
    public void doUpdate(final Issue issue) {
        final L1Calo l1Calo = (L1Calo) issue;

        switch(l1Calo.getType()) {
            case RESTORE_PPM_LUT:
                this.restorePpmLut(l1Calo);
                break;
            case SET_PPM_NOISE_CUT:
                this.setPpmNoiseCut(l1Calo);
                break;
            case ZERO_PPM_LUT:
                this.zeroPpmLut(l1Calo);
                break;
            default:
                break;

        }
    }

    private EXIT_REASON basicEventCheck(final L1Calo l1Calo) {
        final String segController = l1Calo.getSegmentController();
        if(segController == null) {
            final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                            l1Calo.getTowerId(),
                                            l1Calo.getNoiseCut(),
                                            l1Calo.getType(),
                                            STATUS.FAILED,
                                            l1Calo.getId());
            event.setError("Failed to determine segment controller (is null)");
            this.cep.injectEvent(event);
            return EXIT_REASON.FAILURE;
        }

        if(segController.equals("")) {
            final L1Calo event2 = new L1Calo(l1Calo.getSegmentController(),
                                             l1Calo.getTowerId(),
                                             l1Calo.getNoiseCut(),
                                             l1Calo.getType(),
                                             STATUS.FAILED,
                                             l1Calo.getId());
            event2.setError("Failed to determine segment controller (is empty)");
            this.cep.injectEvent(event2);
            return EXIT_REASON.FAILURE;
        }

        final String towerIdStr = l1Calo.getTowerId();
        if(towerIdStr == null) {
            final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                            l1Calo.getTowerId(),
                                            l1Calo.getNoiseCut(),
                                            l1Calo.getType(),
                                            STATUS.FAILED,
                                            l1Calo.getId());
            event.setError("Failed to determine towerId (is null)");
            this.cep.injectEvent(event);
            return EXIT_REASON.FAILURE;
        }

        if(towerIdStr.equals("")) {
            final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                            l1Calo.getTowerId(),
                                            l1Calo.getNoiseCut(),
                                            l1Calo.getType(),
                                            STATUS.FAILED,
                                            l1Calo.getId());
            event.setError("Failed to determine towerId (is empty)");
            this.cep.injectEvent(event);
            return EXIT_REASON.FAILURE;
        }

        return EXIT_REASON.SUCCESS;
    }

    private void restorePpmLut(final L1Calo l1Calo) {
        final EXIT_REASON reason = this.basicEventCheck(l1Calo);

        if(reason == EXIT_REASON.FAILURE) {
            return;
        }

        final String segController = l1Calo.getSegmentController();
        final String towerIdStr = l1Calo.getTowerId();

        final DetectorMask dmask = this.daqReader.getDetMaskForApplication(segController);

        L1CaloExecutor.log.debug("L1Calo: Request RestorePpmLut for tower " + towerIdStr);

        final int towerId = Integer.parseInt(towerIdStr);

        Token publishToken = null;
        
        try {
            try {
                final long ecr = this.execTrigger.hold(dmask).getEcrCounter();
                L1CaloExecutor.log.debug("Hold trigger for L1Calo recovery, ecr=" + ecr);
    
                publishToken = this.publishHoldInfo(l1Calo);
            }
            catch(final ExecutorTriggerException e) {
                final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                                l1Calo.getTowerId(),
                                                l1Calo.getNoiseCut(),
                                                l1Calo.getType(),
                                                STATUS.FAILED,
                                                l1Calo.getId());
                event.setError("Failed to hold trigger", e);
                this.cep.injectEvent(event);
                return;
            }
    
            try {
                this.execRC.sendUserBroadcast(segController, "l1caloRestorePpmLut", new String[] {String.valueOf(towerId)}, Boolean.TRUE, 60);
            }
            catch(final ExecutorRCException e) {
                final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                                l1Calo.getTowerId(),
                                                l1Calo.getNoiseCut(),
                                                l1Calo.getType(),
                                                STATUS.FAILED,
                                                l1Calo.getId());
                event.setError("Failed to send user broadcast to l1caloZeroPpmLut", e);
                this.cep.injectEvent(event);
    
                final Throwable cause = e.getCause();
                if((WaitTimeoutException.class.isInstance(cause) == true) || (InterruptedException.class.isInstance(cause) == true)) {
                    // The command has been sent, do not resume the trigger
                    final String msg = "A L1Calo \"Restore PPM LUT\" recovery failed to be executed; the trigger is not automatically released. "
                                       + "Consider to resume it manually after consulting with the relevant expert.";
                    this.sendErrorToIGUI(msg);
                } else {
                    // The command has not been sent at all, resume the trigger
                    try {
                        this.execTrigger.resume(dmask);
                    }
                    catch(final ExecutorTriggerException ex) {
                        L1CaloExecutor.log.error("Failed to resume the trigger", ex);
    
                        final String msg = "Failed to resume the trigger after a L1Calo recovery action. Consider to resume it manually.";
                        this.sendErrorToIGUI(msg);
                    }
                }
    
                return;
            }
    
            try {
                this.execTrigger.resume(dmask);
            }
            catch(final ExecutorTriggerException e) {
                final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                                l1Calo.getTowerId(),
                                                l1Calo.getNoiseCut(),
                                                l1Calo.getType(),
                                                STATUS.FAILED,
                                                l1Calo.getId());
                event.setError("Failed to resume the trigger at end of recovery", e);
                this.cep.injectEvent(event);
    
                final String msg = "Failed to resume the trigger after a L1Calo recovery action. Consider to resume it manually.";
                this.sendErrorToIGUI(msg);
    
                return;
            }
    
            final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                            l1Calo.getTowerId(),
                                            l1Calo.getNoiseCut(),
                                            l1Calo.getType(),
                                            STATUS.DONE,
                                            l1Calo.getId());
            this.cep.injectEvent(event);
        }
        finally {
            if(publishToken != null) {
                try {
                    HoldTriggerInfoPublisher.remove(publishToken);
                }
                catch(final RepositoryNotFoundException | InfoNotFoundException e) {
                    L1CaloExecutor.log.error("Failed to remove IS information about trigger on hold: " + e, e);
                }
            }
        }
    }

    private void setPpmNoiseCut(final L1Calo l1Calo) {
        final EXIT_REASON reason = this.basicEventCheck(l1Calo);

        if(reason == EXIT_REASON.FAILURE) {
            return;
        }

        final String segController = l1Calo.getSegmentController();
        final String towerIdStr = l1Calo.getTowerId();

        final DetectorMask dmask = this.daqReader.getDetMaskForApplication(segController);

        L1CaloExecutor.log.debug("L1Calo: Request SetPpmNoiseCut for tower " + towerIdStr);

        final int towerId = Integer.parseInt(towerIdStr);

        final String noiseCutStr = l1Calo.getNoiseCut();
        if(noiseCutStr == null) {
            final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                            l1Calo.getTowerId(),
                                            l1Calo.getNoiseCut(),
                                            l1Calo.getType(),
                                            STATUS.FAILED,
                                            l1Calo.getId());
            event.setError("Failed to determine noise cut (is null)");
            this.cep.injectEvent(event);
            return;
        }

        if(towerIdStr.equals("")) {
            final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                            l1Calo.getTowerId(),
                                            l1Calo.getNoiseCut(),
                                            l1Calo.getType(),
                                            STATUS.FAILED,
                                            l1Calo.getId());
            event.setError("Failed to determine noise cut (is empty)");
            this.cep.injectEvent(event);
            return;
        }

        final int noiseCut = Integer.parseInt(noiseCutStr);

        L1CaloExecutor.log.debug("L1Calo: Request SetPpmNoiseCut for tower " + towerIdStr + " with noise cut " + noiseCutStr);

        Token publishToken = null;
        
        try {
            try {
                final long ecr = this.execTrigger.hold(dmask).getEcrCounter();
                L1CaloExecutor.log.debug("Hold trigger for L1Calo recovery, ecr=" + ecr);
    
                publishToken = this.publishHoldInfo(l1Calo);
            }
            catch(final ExecutorTriggerException e) {
                final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                                l1Calo.getTowerId(),
                                                l1Calo.getNoiseCut(),
                                                l1Calo.getType(),
                                                STATUS.FAILED,
                                                l1Calo.getId());
                event.setError("Failed to hold trigger", e);
                this.cep.injectEvent(event);
                return;
            }
    
            try {
                this.execRC.sendUserBroadcast(segController,
                                              "l1caloSetPpmNoiseCut",
                                              new String[] {String.valueOf(towerId), String.valueOf(noiseCut)},
                                              Boolean.TRUE,
                                              60);
            }
            catch(final ExecutorRCException e) {
                final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                                l1Calo.getTowerId(),
                                                l1Calo.getNoiseCut(),
                                                l1Calo.getType(),
                                                STATUS.FAILED,
                                                l1Calo.getId());
                event.setError("Failed to send user broadcast to l1caloZeroPpmLut", e);
                this.cep.injectEvent(event);
    
                final Throwable cause = e.getCause();
                if((WaitTimeoutException.class.isInstance(cause) == true) || (InterruptedException.class.isInstance(cause) == true)) {
                    // The command has been sent, do not resume the trigger
                    final String msg = "A L1Calo \"Set PPM Noise Cut\" recovery failed to be executed; the trigger is not automatically released. "
                                       + "Consider to resume it manually after consulting with the relevant expert.";
                    this.sendErrorToIGUI(msg);
                } else {
                    // The command has not been sent at all, resume the trigger
                    try {
                        this.execTrigger.resume(dmask);
                    }
                    catch(final ExecutorTriggerException ex) {
                        L1CaloExecutor.log.error("Failed to resume the trigger", ex);
    
                        final String msg = "Failed to resume the trigger after a L1Calo recovery action. Consider to resume it manually.";
                        this.sendErrorToIGUI(msg);
                    }
                }
    
                return;
            }
    
            try {
                this.execTrigger.resume(dmask);
            }
            catch(final ExecutorTriggerException e) {
                final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                                l1Calo.getTowerId(),
                                                l1Calo.getNoiseCut(),
                                                l1Calo.getType(),
                                                STATUS.FAILED,
                                                l1Calo.getId());
                event.setError("Failed to resume the trigger at end of recovery", e);
                this.cep.injectEvent(event);
    
                final String msg = "Failed to resume the trigger after a L1Calo recovery action. Consider to resume it manually.";
                this.sendErrorToIGUI(msg);
    
                return;
            }
    
            final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                            l1Calo.getTowerId(),
                                            l1Calo.getNoiseCut(),
                                            l1Calo.getType(),
                                            STATUS.DONE,
                                            l1Calo.getId());
            this.cep.injectEvent(event);
        }
        finally {
            if(publishToken != null) {
                try {
                    HoldTriggerInfoPublisher.remove(publishToken);
                }
                catch(final RepositoryNotFoundException | InfoNotFoundException e) {
                    L1CaloExecutor.log.error("Failed to remove IS information about trigger on hold: " + e, e);
                }
            }
        }
    }

    private void zeroPpmLut(final L1Calo l1Calo) {
        final EXIT_REASON reason = this.basicEventCheck(l1Calo);

        if(reason == EXIT_REASON.FAILURE) {
            return;
        }

        final String segController = l1Calo.getSegmentController();
        final String towerIdStr = l1Calo.getTowerId();

        final DetectorMask dmask = this.daqReader.getDetMaskForApplication(segController);

        L1CaloExecutor.log.debug("L1Calo: Request ZeroPpmLut for tower " + towerIdStr);

        final int towerId = Integer.parseInt(towerIdStr);

        Token publishToken = null;
        
        try {
            try {
                final long ecr = this.execTrigger.hold(dmask).getEcrCounter();
                L1CaloExecutor.log.debug("Hold trigger for L1Calo recovery, ecr=" + ecr);
    
                publishToken = this.publishHoldInfo(l1Calo);
            }
            catch(final ExecutorTriggerException e) {
                final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                                l1Calo.getTowerId(),
                                                l1Calo.getNoiseCut(),
                                                l1Calo.getType(),
                                                STATUS.FAILED,
                                                l1Calo.getId());
                event.setError("Failed to hold trigger", e);
                this.cep.injectEvent(event);
                return;
            }
    
            try {
                this.execRC.sendUserBroadcast(segController, "l1caloZeroPpmLut", new String[] {String.valueOf(towerId)}, Boolean.TRUE, 60);
            }
            catch(final ExecutorRCException e) {
                final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                                l1Calo.getTowerId(),
                                                l1Calo.getNoiseCut(),
                                                l1Calo.getType(),
                                                STATUS.FAILED,
                                                l1Calo.getId());
                event.setError("Failed to send user broadcast to l1caloZeroPpmLut", e);
                this.cep.injectEvent(event);
    
                final Throwable cause = e.getCause();
                if((WaitTimeoutException.class.isInstance(cause) == true) || (InterruptedException.class.isInstance(cause) == true)) {
                    // The command has been sent, do not resume the trigger
                    final String msg = "A L1Calo \"Zero PPM Lut\" recovery failed to be executed; the trigger is not automatically released. "
                                       + "Consider to resume it manually after consulting with the relevant expert.";
                    this.sendErrorToIGUI(msg);
                } else {
                    // The command has not been sent at all, resume the trigger
                    try {
                        this.execTrigger.resume(dmask);
                    }
                    catch(final ExecutorTriggerException ex) {
                        L1CaloExecutor.log.error("Failed to resume the trigger", ex);
    
                        final String msg = "Failed to resume the trigger after a L1Calo recovery action. Consider to resume it manually.";
                        this.sendErrorToIGUI(msg);
                    }
                }
    
                return;
            }
    
            try {
                this.execTrigger.resume(dmask);
            }
            catch(final ExecutorTriggerException e) {
                final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                                l1Calo.getTowerId(),
                                                l1Calo.getNoiseCut(),
                                                l1Calo.getType(),
                                                STATUS.FAILED,
                                                l1Calo.getId());
                event.setError("Failed to resume the trigger at end of recovery", e);
                this.cep.injectEvent(event);
    
                final String msg = "Failed to resume the trigger after a L1Calo recovery action. Consider to resume it manually.";
                this.sendErrorToIGUI(msg);
    
                return;
            }
    
            final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                            l1Calo.getTowerId(),
                                            l1Calo.getNoiseCut(),
                                            l1Calo.getType(),
                                            STATUS.DONE,
                                            l1Calo.getId());
            this.cep.injectEvent(event);
        }
        finally {
            if(publishToken != null) {
                try {
                    HoldTriggerInfoPublisher.remove(publishToken);
                }
                catch(final RepositoryNotFoundException | InfoNotFoundException e) {
                    L1CaloExecutor.log.error("Failed to remove IS information about trigger on hold: " + e, e);
                }
            }
        }
    }

    private Token publishHoldInfo(final L1Calo l1Calo) {
        Token t = null;
        
        try {
            t = HoldTriggerInfoPublisher.publish(this.daqReader.getPartitionName(), CausedBy.L1CALO, Reason.CustomRecovery);
        }
        catch(final Exception e) {
            final L1Calo event = new L1Calo(l1Calo.getSegmentController(),
                                            l1Calo.getTowerId(),
                                            l1Calo.getNoiseCut(),
                                            l1Calo.getType(),
                                            STATUS.PROBLEM,
                                            l1Calo.getId());
            event.setError("Failed to publish HoldTriggerInfo to IS", e);
            this.cep.injectEvent(event);
        }
        
        return t;
    }

    private void sendErrorToIGUI(final String msg) {
        try {
            this.execIGUI.showError(msg);
        }
        catch(final ExecutorIGUIException ex) {
            L1CaloExecutor.log.error("Failed to send an error message to the IGUI: " + ex.toString(), ex);
        }
    }
}
