package chip.subscriber.recovery;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.CHIPException.DAQConfigurationError;
import chip.CHIPException.ExecutorRCException;
import chip.CHIPException.ExecutorTriggerException;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.issue.Issue;
import chip.event.issue.recovery.Module;
import chip.event.issue.recovery.Module.STATUS;
import chip.executor.ExecutorIGUI;
import chip.executor.ExecutorRC;
import chip.executor.ExecutorTrigger;
import chip.subscriber.DeferredActionExecutor;
import config.ConfigException;
import dal.Component;
import dal.Component_Helper;
import dal.ResourceBase;


public class ModuleExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    private final ExecutorRC execRC;
    private final ExecutorTrigger execTrigger;
    private final DAQConfigReader daqReader;

    public ModuleExecutor(final ExecutorRC execRC,
                          final ExecutorIGUI execIGUI,
                          final ExecutorTrigger execTrigger,
                          final EsperDAO dao,
                          final DAQConfigReader daqReader)
    {
        this.execRC = execRC;
        this.execTrigger = execTrigger;
        this.daqReader = daqReader;
    }

    @Override
    public void doUpdate(final Issue issue) {
        final Module module = (Module) issue;

        switch(module.getType()) {
            case DISABLE:
                this.disable(module);
                break;
            case ENABLE:
                this.enable(module);
                break;
            default:
                break;
        }
    }

    synchronized private void disable(final Module module) {
        final String modules = module.getModules();
        final String changeLb = module.getChangeLb();

        final Map<String, ResourceBase> resourcesMap = this.daqReader.getResources();
        final dal.Partition dalPart = this.daqReader.getDalPartition();

        // verify that modules exist in database and that they are enabled
        final String[] modNames = modules.split(",");
        for(String name : modNames) {
            name = name.trim();
        }

        if(modNames.length == 0) {
            final Module event = new Module(module.getRequestingApp(),
                                            module.getModules(),
                                            module.getChangeLb(),
                                            module.getType(),
                                            STATUS.FAILED);
            event.setError("No modules are given");
            this.cep.injectEvent(event);

            return;
        }

        final List<String> goodModNames = new ArrayList<>();

        for(final String name : modNames) {
            if(!resourcesMap.containsKey(name)) {
                final Module event = new Module(module.getRequestingApp(),
                                                module.getModules(),
                                                module.getChangeLb(),
                                                module.getType(),
                                                STATUS.PROBLEM);
                event.setError("Module " + name + " was not found in database");
                this.cep.injectEvent(event);

                continue;
            }

            final Component resource = Component_Helper.cast(resourcesMap.get(name));
            if(resource != null) {
                try {
                    if(resource.disabled(dalPart)) {
                        final Module event = new Module(module.getRequestingApp(),
                                                        module.getModules(),
                                                        module.getChangeLb(),
                                                        module.getType(),
                                                        STATUS.PROBLEM);
                        event.setError("Module " + name + " is already disabled in database");
                        this.cep.injectEvent(event);
    
                        continue;
                    }
                }
                catch(final ConfigException ex) {
                    final Module event = new Module(module.getRequestingApp(),
                                                    module.getModules(),
                                                    module.getChangeLb(),
                                                    module.getType(),
                                                    STATUS.PROBLEM);
                    event.setError("Cannot determine the disabled status of module " + name + ": " + ex, ex);
                    this.cep.injectEvent(event);

                    continue;
                }

                goodModNames.add(name);
            } else {
                final Module event = new Module(module.getRequestingApp(),
                                                module.getModules(),
                                                module.getChangeLb(),
                                                module.getType(),
                                                STATUS.PROBLEM);
                event.setError("Module " + name + " cannot be cast to component object");
                this.cep.injectEvent(event);

                continue;
            }
        }

        if(goodModNames.size() != 0) {
            try {
                // Update local resource set
                final String[] goodMods = goodModNames.toArray(new String[goodModNames.size()]);
                this.daqReader.disableComponents(goodMods);

                // Notify the ResourcesInfoProvider
                final String[] disabled = goodMods;
                final String[] enabled = new String[] {};

                this.execRC.notifyResInfoProvider(disabled, enabled);
            }
            catch(final ExecutorRCException | DAQConfigurationError e) {
                final Module event = new Module(module.getRequestingApp(),
                                                module.getModules(),
                                                module.getChangeLb(),
                                                module.getType(),
                                                STATUS.FAILED);
                event.setError("Failed to contact the ResInfoProvider", e);
                this.cep.injectEvent(event);
                return;
            }

            if(changeLb.equals("true")) {
                // increase luminosity block
                try {
                    this.execTrigger.increaseLumiBlock();
                }
                catch(final ExecutorTriggerException e) {
                    final Module event = new Module(module.getRequestingApp(),
                                                    module.getModules(),
                                                    module.getChangeLb(),
                                                    module.getType(),
                                                    STATUS.PROBLEM);
                    event.setError("Failed to increase luminosity block", e);
                    this.cep.injectEvent(event);
                }
            }

            final Module event = new Module(module.getRequestingApp(),
                                            module.getModules(),
                                            module.getChangeLb(),
                                            module.getType(),
                                            STATUS.DONE);
            this.cep.injectEvent(event);
        } else {
            final Module event = new Module(module.getRequestingApp(),
                                            module.getModules(),
                                            module.getChangeLb(),
                                            module.getType(),
                                            STATUS.FAILED);
            event.setError("None of the provided modules could be disabled");
            this.cep.injectEvent(event);
        }
    }

    synchronized private void enable(final Module module) {
        final String modules = module.getModules();
        final String changeLb = module.getChangeLb();

        final Map<String, ResourceBase> resourcesMap = this.daqReader.getResources();
        final dal.Partition dalPart = this.daqReader.getDalPartition();

        // verify that modules exist in database and that they are disabled
        final String[] modNames = modules.split(",");
        for(String name : modNames) {
            name = name.trim();
        }

        if(modNames.length == 0) {
            final Module event = new Module(module.getRequestingApp(),
                                            module.getModules(),
                                            module.getChangeLb(),
                                            module.getType(),
                                            STATUS.FAILED);
            event.setError("No modules are given");
            this.cep.injectEvent(event);

            return;
        }

        final List<String> goodModNames = new ArrayList<>();

        for(final String name : modNames) {
            if(!resourcesMap.containsKey(name)) {
                final Module event = new Module(module.getRequestingApp(),
                                                module.getModules(),
                                                module.getChangeLb(),
                                                module.getType(),
                                                STATUS.PROBLEM);
                event.setError("Module " + name + " was not found in database");
                this.cep.injectEvent(event);

                continue;
            }

            final Component resource = Component_Helper.cast(resourcesMap.get(name));
            if(resource != null) {
                try {
                    if(!resource.disabled(dalPart)) {
                        final Module event = new Module(module.getRequestingApp(),
                                                        module.getModules(),
                                                        module.getChangeLb(),
                                                        module.getType(),
                                                        STATUS.PROBLEM);
                        event.setError("Module " + name + " is already enabled in database");
                        this.cep.injectEvent(event);
    
                        continue;
                    }
                }
                catch(final ConfigException ex) {
                    final Module event = new Module(module.getRequestingApp(),
                                                    module.getModules(),
                                                    module.getChangeLb(),
                                                    module.getType(),
                                                    STATUS.PROBLEM);
                    event.setError("Cannot determine the disabled status of module " + name + ": " + ex, ex);
                    this.cep.injectEvent(event);

                    continue;
                }

                goodModNames.add(name);
            } else {
                final Module event = new Module(module.getRequestingApp(),
                                                module.getModules(),
                                                module.getChangeLb(),
                                                module.getType(),
                                                STATUS.PROBLEM);
                event.setError("Module " + name + " cannot be cast to component object");
                this.cep.injectEvent(event);

                continue;
            }
        }

        if(goodModNames.size() != 0) {
            try {
                // Update local resource set
                final String goodMods[] = goodModNames.toArray(new String[goodModNames.size()]);
                this.daqReader.enableComponents(goodMods);

                // Notify the ResourcesInfoProvider
                final String[] disabled = new String[] {};
                final String[] enabled = goodMods;
                
                this.execRC.notifyResInfoProvider(disabled, enabled);
            }
            catch(final ExecutorRCException | DAQConfigurationError e) {
                final Module event = new Module(module.getRequestingApp(),
                                                module.getModules(),
                                                module.getChangeLb(),
                                                module.getType(),
                                                STATUS.FAILED);
                event.setError("Failed to contact the ResInfoProvider", e);
                this.cep.injectEvent(event);

                return;
            }

            if(changeLb.equals("true")) {
                // increase luminosity block
                try {
                    this.execTrigger.increaseLumiBlock();
                }
                catch(final ExecutorTriggerException e) {
                    final Module event = new Module(module.getRequestingApp(),
                                                    module.getModules(),
                                                    module.getChangeLb(),
                                                    module.getType(),
                                                    STATUS.PROBLEM);
                    event.setError("Failed to increase luminosity block", e);
                    this.cep.injectEvent(event);
                }
            }

            final Module event = new Module(module.getRequestingApp(),
                                            module.getModules(),
                                            module.getChangeLb(),
                                            module.getType(),
                                            STATUS.DONE);
            this.cep.injectEvent(event);
        } else {
            final Module event = new Module(module.getRequestingApp(),
                                            module.getModules(),
                                            module.getChangeLb(),
                                            module.getType(),
                                            STATUS.FAILED);
            event.setError("None of the provided modules could be enabled");
            this.cep.injectEvent(event);
        }
    }
}
