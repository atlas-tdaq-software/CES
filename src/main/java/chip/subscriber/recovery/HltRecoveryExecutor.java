package chip.subscriber.recovery;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.dao.EsperDAO;
import chip.event.issue.Issue;
import chip.event.issue.recovery.HltRecovery;
import chip.event.issue.recovery.HltRecovery.ACTION;
import chip.event.issue.recovery.HltRecovery.STATUS;
import chip.msg.Common;
import chip.subscriber.DeferredActionExecutor;
import daq.rc.CommandSender;
import daq.rc.RCException;


public class HltRecoveryExecutor extends DeferredActionExecutor {

    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(HltRecoveryExecutor.class);

    private final CommandSender commandSender;

    public HltRecoveryExecutor(final CommandSender commandSender, final EsperDAO dao) {
        this.commandSender = commandSender;
    }

    /*
     * (non-Javadoc)
     * @see ch.atdaq.ces.subscriber.ActionExecutor#update(ch.atdaq.ces.event.meta.Action)
     */
    @Override
    public void doUpdate(final Issue issue) {
        final HltRecovery rec = (HltRecovery) issue;

        final String dcm = rec.getDcm();
        final String hltrc = rec.getHltrc();
        final String hltpu = rec.getHltpu();

        if((dcm == null) || dcm.isEmpty() || (hltrc == null) || hltrc.isEmpty() || (hltpu == null) || hltpu.isEmpty()) {
            final HltRecovery event = new HltRecovery(dcm,
                                                      hltpu,
                                                      hltrc,
                                                      rec.getDeadapplication(),
                                                      rec.getController(),
                                                      rec.getRunningHost(),
                                                      rec.getType(),
                                                      HltRecovery.STATUS.FAILED,
                                                      ACTION.NONE);
            event.setError("One of the application names is empty!");
            this.cep.injectEvent(event);
        } else {
            final STATUS st = rec.getStatus();
            if(st.equals(STATUS.REJECTED) == true) {
                ers.Logger.warning(new Common("The HLT recovery of type " + rec.getType() + " for host " + rec.getRunningHost()
                                              + " will not be executed: maximum number of allowed recoveries reached"));
            } else {
                // Compute application list
                String[] applications_to_change_membership = {};
                String[] applications_to_stop = {};
                String[] applications_to_start = {};

                if(rec.getAction() == HltRecovery.ACTION.DISABLE_ON_FAILURE) {
                    applications_to_change_membership = new String[] {dcm, hltpu, hltrc};
                } else {
                    // If the recovery was ON_HOLD, then kill also the application initiating the recovery
                    // (i.e., the one that usually dies): in this way the recovery is started from scratch

                    // NOTE: the 'applications_to_stop' always contains the application that initiated the recovery
                    // and, in principle, the application that died. That is done because:
                    // - it does not have any impact on performance (the command sent to the controller is always the same);
                    // - it helps for the ON_HOLD status, when the application needs to be stopped;
                    // - it helps for cases in which the application dead is "simulated" (i.e., an easy way to re-initiate a recovery)
                    switch(rec.getType()) {
                        case DCM:
                            applications_to_stop = new String[] {dcm, hltpu, hltrc};
                            applications_to_start = new String[] {dcm, hltpu, hltrc};
                            applications_to_change_membership = new String[] {dcm, hltpu, hltrc};
                            break;
                        case HLTRC:
                            applications_to_stop = new String[] {hltpu, hltrc};
                            applications_to_start = new String[] {hltpu, hltrc};
                            applications_to_change_membership = new String[] {hltpu, hltrc};
                            break;
                        case HLTPU:
                            applications_to_stop = new String[] {hltrc, hltpu};
                            applications_to_start = new String[] {hltrc, hltpu};
                            applications_to_change_membership = new String[] {hltrc, hltpu};
                            break;
                        default:
                            HltRecoveryExecutor.log.error("HLTRecovery TYPE not recognized: " + rec.getType());
                            break;
                    }
                }

                switch(rec.getAction()) {
                    case DISABLE_ON_FAILURE: {
                        try {
                            this.commandSender.changeMembership(rec.getController(), false, applications_to_change_membership);

                            final HltRecovery done = new HltRecovery(dcm,
                                                                     hltpu,
                                                                     hltrc,
                                                                     rec.getDeadapplication(),
                                                                     rec.getController(),
                                                                     rec.getRunningHost(),
                                                                     rec.getType(),
                                                                     STATUS.DONE,
                                                                     ACTION.NONE);

                            this.cep.injectEvent(done);
                        }
                        catch(final RCException ex) {
                            final HltRecovery failed = new HltRecovery(dcm,
                                                                       hltpu,
                                                                       hltrc,
                                                                       rec.getDeadapplication(),
                                                                       rec.getController(),
                                                                       rec.getRunningHost(),
                                                                       rec.getType(),
                                                                       HltRecovery.STATUS.FAILED,
                                                                       ACTION.NONE);

                            failed.setError("Failed to disable applications", ex);
                            this.cep.injectEvent(failed);
                        }
                    }

                        break;

                    case STOP: {
                        // The ON_HOLD status always comes with the STOP action
                        final HltRecovery ev = new HltRecovery(dcm,
                                                               hltpu,
                                                               hltrc,
                                                               rec.getDeadapplication(),
                                                               rec.getController(),
                                                               rec.getRunningHost(),
                                                               rec.getType(),
                                                               rec.getStatus().equals(STATUS.ON_HOLD) ? HltRecovery.STATUS.ON_HOLD
                                                                                                      : HltRecovery.STATUS.STOP_IN_PROGRESS,
                                                               ACTION.NONE);

                        this.cep.injectEventSynch(ev);

                        // Take application(S) OUT and STOP
                        try {
                            this.commandSender.changeMembership(rec.getController(), false, applications_to_change_membership);

                            this.commandSender.stopApplications(rec.getController(), applications_to_stop);
                        }
                        catch(final RCException e) {
                            final HltRecovery event = new HltRecovery(dcm,
                                                                      hltpu,
                                                                      hltrc,
                                                                      rec.getDeadapplication(),
                                                                      rec.getController(),
                                                                      rec.getRunningHost(),
                                                                      rec.getType(),
                                                                      HltRecovery.STATUS.FAILED,
                                                                      ACTION.NONE);
                            event.setError("Failed to disable and stop applications", e);
                            this.cep.injectEvent(event);
                        }
                    }

                        break;

                    case _START: {
                        // Inject START_IN_PROGRESS
                        final HltRecovery start_in_p = new HltRecovery(dcm,
                                                                       hltpu,
                                                                       hltrc,
                                                                       rec.getDeadapplication(),
                                                                       rec.getController(),
                                                                       rec.getRunningHost(),
                                                                       rec.getType(),
                                                                       HltRecovery.STATUS.START_IN_PROGRESS,
                                                                       ACTION.NONE);

                        // Inject new event
                        this.cep.injectEventSynch(start_in_p);

                        // Restart application(S)
                        try {
                            this.commandSender.restartApplications(rec.getController(), applications_to_start);
                        }
                        catch(final RCException e) {
                            final HltRecovery event = new HltRecovery(dcm,
                                                                      hltpu,
                                                                      hltrc,
                                                                      rec.getDeadapplication(),
                                                                      rec.getController(),
                                                                      rec.getRunningHost(),
                                                                      rec.getType(),
                                                                      HltRecovery.STATUS.FAILED,
                                                                      ACTION.NONE);
                            event.setError("Failed to start applications", e);
                            this.cep.injectEvent(event);
                        }
                    }

                        break;

                    case ENABLE: {
                        // Take application(S) back IN
                        try {
                            this.commandSender.changeMembership(rec.getController(), true, applications_to_change_membership);
                        }
                        catch(final RCException e) {
                            final HltRecovery event = new HltRecovery(dcm,
                                                                      hltpu,
                                                                      hltrc,
                                                                      rec.getDeadapplication(),
                                                                      rec.getController(),
                                                                      rec.getRunningHost(),
                                                                      rec.getType(),
                                                                      HltRecovery.STATUS.FAILED,
                                                                      ACTION.NONE);
                            event.setError("Failed to enable applications", e);
                            this.cep.injectEvent(event);
                        }

                        // Create RESOLVED event
                        final HltRecovery resolved = new HltRecovery(dcm,
                                                                     hltpu,
                                                                     hltrc,
                                                                     rec.getDeadapplication(),
                                                                     rec.getController(),
                                                                     rec.getRunningHost(),
                                                                     rec.getType(),
                                                                     HltRecovery.STATUS.DONE,
                                                                     ACTION.NONE);

                        // Inject event
                        this.cep.injectEvent(resolved);
                    }

                        break;

                    default: {
                        HltRecoveryExecutor.log.error("HLTRecovery ACTION not recognized: " + rec.getAction());
                    }

                        break;
                }
            }
        }
    }
}
