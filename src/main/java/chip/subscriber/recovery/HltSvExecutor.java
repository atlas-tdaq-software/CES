package chip.subscriber.recovery;

import java.util.EnumSet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

import rc.HoldTriggerInfoNamed.CausedBy;
import rc.HoldTriggerInfoNamed.Reason;
import chip.CEPService;
import chip.CHIPException.ExecutorIGUIException;
import chip.CHIPException.ExecutorRCException;
import chip.CHIPException.ExecutorTriggerException;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.issue.Issue;
import chip.event.issue.recovery.HltSv;
import chip.event.issue.recovery.HltSv.ACTION;
import chip.event.issue.recovery.HltSv.STATUS;
import chip.executor.ExecutorIGUI;
import chip.executor.ExecutorRC;
import chip.executor.ExecutorTrigger;
import chip.subscriber.DeferredActionExecutor;
import chip.utils.HoldTriggerInfoPublisher;
import chip.utils.HoldTriggerInfoPublisher.Token;
import daq.eformat.DetectorMask;
import daq.eformat.DetectorMask.SUBDETECTOR;
import is.InfoNotFoundException;
import is.RepositoryNotFoundException;


public class HltSvExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(HltSvExecutor.class);

    private final ExecutorRC execRC;
    private final ExecutorIGUI execIGUI;
    private final ExecutorTrigger execTrigger;
    private final DAQConfigReader daqReader;
    private final Token publishToken;
    private final DetectorMask dmask = new DetectorMask(EnumSet.of(SUBDETECTOR.TDAQ_L2SV));

    public HltSvExecutor(final ExecutorRC execRC,
                         final ExecutorIGUI execIGUI,
                         final ExecutorTrigger execTrigger,
                         final EsperDAO dao,
                         final DAQConfigReader daqReader)
    {
        this.execRC = execRC;
        this.execIGUI = execIGUI;
        this.execTrigger = execTrigger;
        this.daqReader = daqReader;
        this.publishToken = HoldTriggerInfoPublisher.generateToken(this.daqReader.getPartitionName(), "HLTSV");
    }

    @Override
    public void doUpdate(final Issue issue) {
        final HltSv hltSv = (HltSv) issue;

        switch(hltSv.getType()) {
            case CRASH:
                switch(hltSv.getAction()) {
                    case RESTART:
                        this.restartHltSV(hltSv);
                        break;
                    case RESUME_TRIGGER:
                        this.resumeTrigger(hltSv);
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    private void resumeTrigger(final HltSv hltSv) {
        try {
            this.execRC.putAppsInFsm(hltSv.getController(), new String[] {hltSv.getApplication()});
        }
        catch(final ExecutorRCException e1) {
            final HltSv event = new HltSv(hltSv.getController(),
                                          hltSv.getApplication(),
                                          hltSv.getType(),
                                          STATUS.PROBLEM,
                                          ACTION.NONE,
                                          hltSv.getId());
            event.setError("Not able to put HLTSV back in FSM", e1);
            this.cep.injectEvent(event);
        }

        try {
            this.execTrigger.resume(this.dmask);
            
            this.cep.injectEvent(new HltSv(hltSv.getController(),
                                           hltSv.getApplication(),
                                           hltSv.getType(),
                                           STATUS.DONE,
                                           ACTION.NONE,
                                           hltSv.getId()));
        }
        catch(final ExecutorTriggerException e) {
            final HltSv event = new HltSv(hltSv.getController(),
                                          hltSv.getApplication(),
                                          hltSv.getType(),
                                          STATUS.FAILED,
                                          ACTION.NONE,
                                          hltSv.getId());
            event.setError("Cannot resume trigger", e);
            this.cep.injectEvent(event);

            try {
                this.execIGUI.showError("Failed to resume the trigger after an HLTSV recovery; manual intervention is needed");
            }
            catch(final ExecutorIGUIException ex) {
                HltSvExecutor.log.error("Failed to send error message to the IGUI", ex);
            }
        }
        finally {
            try {
                HoldTriggerInfoPublisher.remove(this.publishToken);
            }
            catch(final RepositoryNotFoundException | InfoNotFoundException e) {
                HltSvExecutor.log.error("Failed to remove IS information about trigger on hold: " + e, e);
            }
        }
    }

    private void restartHltSV(final HltSv hltSv) {
        final String controller = hltSv.getController();
        final String appName = hltSv.getApplication();

        // Put the trigger on hold
        try {
            this.execTrigger.hold(this.dmask);
        }
        catch(final ExecutorTriggerException e) {
            final HltSv event = new HltSv(hltSv.getController(),
                                          hltSv.getApplication(),
                                          hltSv.getType(),
                                          STATUS.FAILED,
                                          ACTION.NONE,
                                          hltSv.getId());
            event.setError("Cannot hold trigger", e);
            this.cep.injectEvent(event);
            return;
        }

        try {
            HoldTriggerInfoPublisher.publish(this.publishToken, CausedBy.DAQ, Reason.CustomRecovery);
        }
        catch(final Exception e) {
            final HltSv event = new HltSv(hltSv.getController(),
                                          hltSv.getApplication(),
                                          hltSv.getType(),
                                          STATUS.PROBLEM,
                                          ACTION.NONE,
                                          hltSv.getId());
            event.setError("Failed to publish HoldTriggerInfo to IS", e);
            this.cep.injectEvent(event);
        }

        try {
            final HltSv event = new HltSv(hltSv.getController(),
                                          hltSv.getApplication(),
                                          hltSv.getType(),
                                          STATUS.RESTARTING,
                                          ACTION.NONE,
                                          hltSv.getId());
            this.cep.injectEventSynch(event);

            this.execRC.putAppsOutFsm(controller, new String[] {appName});
            this.execRC.restartApps(controller, new String[] {appName});
        }
        catch(final ExecutorRCException e) {
            final HltSv event = new HltSv(hltSv.getController(),
                                          hltSv.getApplication(),
                                          hltSv.getType(),
                                          STATUS.FAILED,
                                          ACTION.NONE,
                                          hltSv.getId());
            event.setError("Not able to put HLTSV out of FSM and restart it", e);
            this.cep.injectEvent(event);

            try {
                HoldTriggerInfoPublisher.remove(this.publishToken);
            }
            catch(final RepositoryNotFoundException | InfoNotFoundException e1) {
                HltSvExecutor.log.error("Failed to remove IS information about trigger on hold: " + e1, e1);
            }
            
            try {
                final String msg =
                                 "The HLTSV recovery failed its execution: the application could not be put out of membership or restarted. "
                                   + "The trigger is not released; consider a manual release after consulting the relative expert";
                this.execIGUI.showError(msg);
            }
            catch(final ExecutorIGUIException ex) {
                HltSvExecutor.log.error("Failed to send error message to the IGUI", ex);
            }
        }
    }
}
