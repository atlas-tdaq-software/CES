package chip.subscriber.recovery;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.locks.ReentrantLock;

import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.CHIPException.ExecutorRCException;
import chip.DAQConfigReader;
import chip.event.issue.Issue;
import chip.event.issue.recovery.DCM;
import chip.event.issue.recovery.DCM.TYPE;
import chip.executor.ExecutorRC;
import chip.subscriber.DeferredMultiActionExecutor;
import chip.utils.ExecutorFactory;


public class DCMExecutor extends DeferredMultiActionExecutor {
    @Autowired
    CEPService cep;

    private final ReentrantLock lk = new ReentrantLock(true);
    private final ExecutorRC execRC;
    private final DAQConfigReader daqConf;
    private final static ExecutorService tPool = ExecutorFactory.newThreadPool("DCMExecutor", 10, true);

    private enum EVENT_TYPE {
        ROS,
        SFO
    }

    public DCMExecutor(final ExecutorRC execRC, final DAQConfigReader daqConf) {
        this.execRC = execRC;
        this.daqConf = daqConf;
    }

    @Override
    public void doUpdate(final Issue[] issueIn, final Issue[] issueRemove) {
        this.lk.lock();

        try {
            final List<DCM> ros = new ArrayList<>();
            final List<DCM> sfo = new ArrayList<>();

            for(final Issue i : issueIn) {
                final DCM event = (DCM) i;
                if(event.getType().equals(TYPE.ROS) == true) {
                    ros.add(event);
                } else if(event.getType().equals(TYPE.SFO) == true) {
                    sfo.add(event);
                }
            }

            try {
                this.execute(ros, EVENT_TYPE.ROS);
                this.execute(sfo, EVENT_TYPE.SFO);
            }
            catch(final InterruptedException ex) {
                // Restore the interrupted state; nothing more to do...
                Thread.currentThread().interrupt();
            }
            finally {
                for(final DCM e : ros) {
                    this.cep.injectEvent(new DCM(e.getType(), e.getName(), e.getIsUP().booleanValue(), DCM.STATUS.DONE));
                }

                for(final DCM e : sfo) {
                    this.cep.injectEvent(new DCM(e.getType(), e.getName(), e.getIsUP().booleanValue(), DCM.STATUS.DONE));
                }
            }
        }
        finally {
            this.lk.unlock();
        }
    }

    private void execute(final List<? extends DCM> events, final EVENT_TYPE type) throws InterruptedException {
        final String commandUP;
        final String commandDOWN;

        if(type.equals(EVENT_TYPE.ROS) == true) {
            commandUP = "ROS_UP";
            commandDOWN = "ROS_DOWN";
        } else if(type.equals(EVENT_TYPE.SFO) == true) {
            commandUP = "SFO_UP";
            commandDOWN = "SFO_DOWN";
        } else {
            throw new IllegalArgumentException();
        }

        final Set<String> controllers = this.daqConf.getDCMSegments();

        boolean sendAndClear = false;
        boolean up = true;
        final List<String> apps = new ArrayList<>();

        final ListIterator<? extends DCM> it = events.listIterator();
        while(it.hasNext()) {
            final DCM e = it.next();
            apps.add(e.getName());

            up = e.getIsUP().booleanValue();

            final int ni = it.nextIndex();
            if(ni != events.size()) {
                final boolean up_new = events.get(ni).getIsUP().booleanValue();

                if(up_new != up) {
                    // The next one is different
                    sendAndClear = true;
                }
            } else {
                // This is the last event
                sendAndClear = true;
            }

            if(sendAndClear == true) {
                final boolean captureUP = up;
                final List<Callable<Void>> tasks = new ArrayList<>();

                for(final String c : controllers) {
                    final Callable<Void> call = new Callable<Void>() {
                        @Override
                        public Void call() throws Exception {
                            try {
                                if(captureUP == true) {
                                    DCMExecutor.this.execRC.sendUserBroadcast(c,
                                                                              commandUP,
                                                                              apps.toArray(new String[apps.size()]),
                                                                              Boolean.TRUE,
                                                                              10);
                                } else {
                                    DCMExecutor.this.execRC.sendUserBroadcast(c,
                                                                              commandDOWN,
                                                                              apps.toArray(new String[apps.size()]),
                                                                              Boolean.TRUE,
                                                                              10);
                                }
                            }
                            catch(final ExecutorRCException ex) {
                                for(final String appName : apps) {
                                    final DCM ev = new DCM(type.equals(EVENT_TYPE.ROS) ? DCM.TYPE.ROS : DCM.TYPE.SFO,
                                                           appName,
                                                           captureUP,
                                                           DCM.STATUS.PROBLEM);
                                    e.setError("Error sending command " + (captureUP == true ? commandUP : commandDOWN) + " to controller "
                                               + c, ex);
                                    DCMExecutor.this.cep.injectEvent(ev);
                                }
                            }

                            return null;
                        }
                    };

                    tasks.add(call);
                }

                // This returns when all the tasks have been completed
                DCMExecutor.tPool.invokeAll(tasks);

                apps.clear();
                sendAndClear = false;
            }
        }
    }
}
