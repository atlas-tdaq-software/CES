package chip.subscriber.recovery;

import is.InfoNotFoundException;
import is.RepositoryNotFoundException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import TRIGGER.TriggerCommander.HoldTriggerInfo;
import rc.HoldTriggerInfoNamed;
import rc.HoldTriggerInfoNamed.Reason;
import chip.CEPService;
import chip.CHIPException.ExecutorIGUIException;
import chip.CHIPException.ExecutorRCException;
import chip.CHIPException.ExecutorTriggerException;
import chip.CHIPException.WaitTimeoutException;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.issue.HoldsTrigger;
import chip.event.issue.Issue;
import chip.event.issue.recovery.Resynch;
import chip.event.issue.recovery.Resynch.STATUS;
import chip.executor.ExecutorIGUI;
import chip.executor.ExecutorRC;
import chip.executor.ExecutorTrigger;
import chip.subscriber.DeferredActionExecutor;
import chip.utils.HoldTriggerInfoPublisher;
import chip.utils.HoldTriggerInfoPublisher.Token;
import daq.eformat.DetectorMask;


public class ResynchExecutor extends DeferredActionExecutor {

    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(ResynchExecutor.class);
    
    private final ExecutorRC execRC;
    private final ExecutorIGUI execIGUI;
    private final ExecutorTrigger execTrigger;
    private final DAQConfigReader daqReader;

    @Value("#{configuration['chip.resynch.timeout']}")
    Integer resynchTimeout;

    public ResynchExecutor(final ExecutorRC execRC,
                           final ExecutorIGUI execIGUI,
                           final ExecutorTrigger execTrigger,
                           final EsperDAO dao,
                           final DAQConfigReader daqReader)
    {
        this.execRC = execRC;
        this.execIGUI = execIGUI;
        this.execTrigger = execTrigger;
        this.daqReader = daqReader;
    }

    @Override
    public void doUpdate(final Issue issue) {
        final Resynch resynch = (Resynch) issue;

        switch(resynch.getType()) {
            case RESYNCH:
                this.doResynch(resynch);
                break;
            default:
                break;
        }
    }

    synchronized private void doResynch(final Resynch resynch) {
        final String controller = resynch.getController();
        final Boolean forceTriggerRelease = resynch.getForceTriggerRelease();

        final DetectorMask dmask = this.daqReader.getDetMaskForApplication(controller);

        // hold trigger
        final HoldTriggerInfo trgInfo;
        try {
            trgInfo = this.execTrigger.hold(dmask);
        }
        catch(final ExecutorTriggerException e) {
            final Resynch event = new Resynch(controller, resynch.getType(), STATUS.FAILED, forceTriggerRelease, resynch.getId());
            final String msg = "Failed to hold the trigger";
            event.setError(msg, e);
            this.cep.injectEvent(event);

            try {
                this.execIGUI.showError("The resynch for controller " + controller + " cannot be started: " + msg);
            }
            catch(final ExecutorIGUIException ex) {
                ers.Logger.error(ex);
            }

            return;
        }

        // publish name of detector which causes dead-time
        final HoldTriggerInfoNamed.CausedBy detIdEnum = HoldsTrigger.getCausedBy(dmask);
        if(detIdEnum.equals(HoldTriggerInfoNamed.CausedBy.Other) == true) {
            final Resynch event = new Resynch(controller, resynch.getType(), STATUS.PROBLEM, forceTriggerRelease, resynch.getId());
            event.setError("Failed to determine the detector of " + controller + ". Associated detector mask is " + dmask.toString());
            this.cep.injectEvent(event);
        }
                
        Token publishToken = null;
        
        try {
            try {
                publishToken = HoldTriggerInfoPublisher.publish(this.daqReader.getPartitionName(), detIdEnum, Reason.Resync);            
            }
            catch(final Exception e) {
                final Resynch event = new Resynch(controller, resynch.getType(), STATUS.PROBLEM, forceTriggerRelease, resynch.getId());
                event.setError("Failed to publish the name of the detector to IS", e);
                this.cep.injectEvent(event);
            }
                            
            // send RESYNCH to controller
            try {
                this.execRC.sendResynch(controller, trgInfo, new String[] {}, true, this.resynchTimeout.intValue());
                
                // increase luminosity block
                try {
                    this.execTrigger.increaseLumiBlock();
                }
                catch(final ExecutorTriggerException e) {
                    final Resynch event = new Resynch(controller, resynch.getType(), STATUS.PROBLEM, forceTriggerRelease, resynch.getId());
                    event.setError("Failed to increase luminosity block", e);
                    this.cep.injectEvent(event);
                }
        
                // resume trigger
                try {
                    this.execTrigger.resume(dmask);
                }
                catch(final ExecutorTriggerException e) {
                    final Resynch event = new Resynch(controller, resynch.getType(), STATUS.PROBLEM, forceTriggerRelease, resynch.getId());
                    event.setError("Not able to resume trigger at end of recovery", e);
                    this.cep.injectEvent(event);
                }
        
                final Resynch event = new Resynch(controller, resynch.getType(), STATUS.DONE, forceTriggerRelease, resynch.getId());
                this.cep.injectEvent(event);
            }
            catch(final ExecutorRCException e) {
                final StringBuilder msg = new StringBuilder();
                msg.append("Failed to perform resynch of ");
                msg.append(controller);
    
                boolean triggerToBeResumed = true;
                boolean triggerResumed = false;
                
                final Throwable cause = e.getCause();
                
                if(WaitTimeoutException.class.isInstance(cause) == true) {
                    msg.append(" (timeout elapsed)");
                    triggerToBeResumed = forceTriggerRelease.booleanValue();
                } else if(InterruptedException.class.isInstance(cause) == true) {
                    msg.append(" (operation interrupted)");
                    triggerToBeResumed = false;
                }
                                
                if(triggerToBeResumed == true) {
                    try {
                        this.execTrigger.resume(dmask);
                        triggerResumed = true;
                    }
                    catch(final ExecutorTriggerException ex) {
                        final Resynch event = new Resynch(controller, resynch.getType(), STATUS.PROBLEM, forceTriggerRelease, resynch.getId());
                        event.setError("Not able to resume the trigger after a failure in executing a resynch command", ex);
                        this.cep.injectEvent(event);
                    }
                }
    
                final Resynch event = new Resynch(controller, resynch.getType(), STATUS.FAILED, forceTriggerRelease, resynch.getId());
                event.setError(msg.toString(), e);
                this.cep.injectEvent(event);
    
                if(triggerResumed == false) {
                    msg.append(". Please, note that the trigger is not resumed and a manual intervention is needed if appropriate");
                }
    
                try {
                    this.execIGUI.showError(msg.toString());
                }
                catch(final ExecutorIGUIException ex) {
                    ers.Logger.error(ex);
                }    
            }
        }
        finally {
            if(publishToken != null) {
                try {
                    HoldTriggerInfoPublisher.remove(publishToken);
                }
                catch(final RepositoryNotFoundException | InfoNotFoundException e) {
                    ResynchExecutor.log.error("Failed to remove IS information about trigger on hold: " + e, e);
                }
            }
        }
    }
}
