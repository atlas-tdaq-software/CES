package chip.subscriber.recovery;

import is.InfoNotFoundException;
import is.RepositoryNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import TRIGGER.TriggerCommander.HoldTriggerInfo;
import rc.HoldTriggerInfoNamed;
import rc.HoldTriggerInfoNamed.Reason;
import chip.CEPService;
import chip.CHIPException.DAQConfigurationError;
import chip.CHIPException.ExecutorIGUIException;
import chip.CHIPException.ExecutorISException;
import chip.CHIPException.ExecutorRCException;
import chip.CHIPException.ExecutorTriggerException;
import chip.CHIPException.WaitTimeoutException;
import chip.DAQConfigReader;
import chip.dao.EsperDAO;
import chip.event.issue.HoldsTrigger;
import chip.event.issue.Issue;
import chip.event.issue.recovery.TTCRestart;
import chip.event.issue.recovery.TTCRestart.STATUS;
import chip.executor.ExecutorIGUI;
import chip.executor.ExecutorIS;
import chip.executor.ExecutorRC;
import chip.executor.ExecutorTrigger;
import chip.subscriber.DeferredActionExecutor;
import chip.utils.HoldTriggerInfoPublisher;
import chip.utils.HoldTriggerInfoPublisher.Token;
import dal.Component;
import daq.eformat.DetectorMask;


public class TTCRestartExecutor extends DeferredActionExecutor {
    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(TTCRestartExecutor.class);

    private final ExecutorRC execRC;
    private final ExecutorTrigger execTrigger;
    private final DAQConfigReader daqReader;
    private final ExecutorIGUI execIgui;

    @Value("#{configuration['chip.ttcrestart.waitforexit.timeout']}")
    Integer waitForExitTimeout;

    @Value("#{configuration['chip.ttcrestart.waitforup.timeout']}")
    Integer waitForUpTimeout;
   
    @Value("#{configuration['chip.ttcrestart.waitforrunning.timeout']}")
    Integer waitForRunningTimeout;
 
    @Value("#{configuration['chip.ttcrestart.resynch.timeout']}")
    Integer resynchTimeout;
    
    private static class TTCRestartFailure extends Exception {
        private static final long serialVersionUID = -6783760855160291908L;
        private final boolean resumeTrigger;

        TTCRestartFailure(final String message, final Exception cause, final boolean resumeTrigger) {
            super(message, cause);
            this.resumeTrigger = resumeTrigger;
        }

        boolean resumeTrigger() {
            return this.resumeTrigger;
        }

        @Override
        public synchronized Exception getCause() {
            return (Exception) super.getCause();
        }
    }

    public TTCRestartExecutor(final ExecutorRC execRC,
                              final ExecutorTrigger execTrigger,
                              final EsperDAO dao,
                              final DAQConfigReader daqReader,
                              final ExecutorIGUI execIgui)
    {
        this.execRC = execRC;
        this.execTrigger = execTrigger;
        this.daqReader = daqReader;
        this.execIgui = execIgui;
    }

    @Override
    public void doUpdate(final Issue issue) {        
        final TTCRestart ttcRestart = (TTCRestart) issue;

        switch(ttcRestart.getType()) {
            case TTC_RESTART:
                this.ttcRestart(ttcRestart);
                break;
            default:
                break;
        }
    }
    
    private void ttcRestart(final TTCRestart ttcRestart) {
        final String name = ttcRestart.getName();
        final String controller = ttcRestart.getController();
        final String segment = ttcRestart.getSegment();
        final Boolean controlsTTCPartitions = ttcRestart.getControlsTTCPartitions();
        final Boolean restartableDuringRun = ttcRestart.getRestartableDuringRun();
        final String id = ttcRestart.getId();

        if(controlsTTCPartitions.booleanValue() == false) {
            final TTCRestart event = new TTCRestart(name,
                                                    controller,
                                                    segment,
                                                    restartableDuringRun,
                                                    controlsTTCPartitions,
                                                    ttcRestart.getType(),
                                                    STATUS.FAILED,
                                                    id);

            final String msg = "The flag controlsTTCPartitions for controller \"" + controller + "\" is set to false, but must be true";
            event.setError(msg);
            this.cep.injectEvent(event);

            try {
                this.execIgui.showError("The TTC restart cannot be executed: " + msg);
            }
            catch(final ExecutorIGUIException ex) {
                ers.Logger.error(ex);
            }
        } else {
            TTCRestartExecutor.log.debug("Begin TTC restart for " + name);
            TTCRestartExecutor.log.debug("Configuration: controlsTTCPartitions=" + String.valueOf(controlsTTCPartitions)
                                         + ", restartableDuringRun=" + String.valueOf(restartableDuringRun));

            final DetectorMask detmask = this.daqReader.getDetMaskForApplication(name);

            boolean resumeTrigger = true;
            boolean failed = false;

            Token publishToken = null;
            
            try {
                // hold trigger
                final HoldTriggerInfo trgInfo;
                try {
                    trgInfo = this.execTrigger.hold(detmask);
                }
                catch(final ExecutorTriggerException e) {
                    final String msg = "Failed to hold the trigger";
                    throw new TTCRestartFailure(msg, e, false);
                }

                // publish name of detector which causes dead-time
                final HoldTriggerInfoNamed.CausedBy detIdEnum = HoldsTrigger.getCausedBy(detmask);
                if(detIdEnum.equals(HoldTriggerInfoNamed.CausedBy.Other) == true) {
                    final TTCRestart event = new TTCRestart(name,
                                                            controller,
                                                            segment,
                                                            restartableDuringRun,
                                                            controlsTTCPartitions,
                                                            ttcRestart.getType(),
                                                            STATUS.PROBLEM,
                                                            id);
                    event.setError("Not able to determine the detector of " + name + ". Associated detector mask is " + detmask.toString());
                    this.cep.injectEvent(event);
                }

                try {
                    publishToken = HoldTriggerInfoPublisher.publish(this.daqReader.getPartitionName(), detIdEnum, Reason.TTCRestart);                    
                }
                catch(final Exception e) {
                    final TTCRestart event = new TTCRestart(name,
                                                            controller,
                                                            segment,
                                                            restartableDuringRun,
                                                            controlsTTCPartitions,
                                                            ttcRestart.getType(),
                                                            STATUS.PROBLEM,
                                                            id);
                    event.setError("Failed to to publish detector name to IS", e);
                    this.cep.injectEvent(event);
                }

                // disable TTC controller
                try {
                    this.execRC.putAppsOutFsm(controller, new String[] {name});
                }
                catch(final ExecutorRCException e) {
                    final String msg = "The controller could not disable application " + name;
                    throw new TTCRestartFailure(msg, e, true);
                }

                try(ExecutorIS<rc.DAQApplicationInfo> isReceiver = new ExecutorIS<>(rc.DAQApplicationInfo.class,
                                                                                    this.daqReader.getPartitionName(),
                                                                                    "RunCtrl.Supervision." + name,
                                                                                    (info) -> {
                                                                                        return info.status.equals(daq.rc.ProcessStatus.EXITED.toString());
                                                                                    }))
                {
                    isReceiver.startWatching();
                    
                    // restart TTC controller
                    this.execRC.stopApps(controller, new String[] { name });

                    // wait until TTC controller is EXITED
                    isReceiver.waitForISMessage(this.waitForExitTimeout.intValue(), TimeUnit.SECONDS);   
                    
                    TTCRestartExecutor.log.info("Application \"" + name + "\" has been stopped");
                }
                catch(final ExecutorRCException e) {
                    final String msg = "Failed to stop application \"" + name + "\": " + e;
                    throw new TTCRestartFailure(msg, e, true);
                }
                catch(final WaitTimeoutException e) {
                    final String msg = "Failed to stop application \"" + name + "\" within the defined timeout";
                    throw new TTCRestartFailure(msg, e, false);
                }
                catch(final ExecutorISException e) {
                    final String msg = "Cannot determine whether the application \"" + name + "\" has been stopped: " + e;
                    throw new TTCRestartFailure(msg, e, false);
                }
                catch(final InterruptedException e) {
                    final String msg = "Thread has been interrupted while waiting for the application \"" + name + "\" to be stopped";
                    Thread.currentThread().interrupt(); // Restore the interrupted state
                    throw new TTCRestartFailure(msg, e, false);
                }

                // find all dynamically disabled resources which need to be reset
                final List<String> segmentResources;
                try {
                    segmentResources = this.daqReader.getResourcesOfSegment(segment);
                }
                catch(final DAQConfigurationError ex) {
                    final String msg = "Some error occurred accessing the configuration: " + ex;
                    throw new TTCRestartFailure(msg, ex, true);
                }
                
                final Map<String, Component> dynDisabledComponents = this.daqReader.getDynDisabledComponents();

                final List<String> componentToBeReset = new ArrayList<String>();

                for(final String res : segmentResources) {
                    if(dynDisabledComponents.containsKey(res)) {
                        componentToBeReset.add(res);
                    }
                }

                // now re-enable the components which are part of the restarted TTC partition
                if(!componentToBeReset.isEmpty()) {
                    final String[] disabled = new String[] {};
                    final String[] enabled = componentToBeReset.toArray(new String[componentToBeReset.size()]);

                    try {
                        // update local resources view
                        this.daqReader.enableComponents(enabled);
                    }
                    catch(final DAQConfigurationError ex) {
                        final String msg = "Some error occurred accessing the configuration: " + ex;
                        throw new TTCRestartFailure(msg, ex, true);
                    }
    
                    
                    // also notify the ResourcesInfoProvider
                    try {
                        this.execRC.notifyResInfoProvider(disabled, enabled);
                    }
                    catch(final ExecutorRCException e) {
                        final TTCRestart event = new TTCRestart(name,
                                                                controller,
                                                                segment,
                                                                restartableDuringRun,
                                                                controlsTTCPartitions,
                                                                ttcRestart.getType(),
                                                                STATUS.PROBLEM,
                                                                id);
                        event.setError("Failed to notify the ResInfoProvider", e);
                        this.cep.injectEvent(event);
                    }
                }

                try(ExecutorIS<rc.DAQApplicationInfo> isReceiver_up = new ExecutorIS<>(rc.DAQApplicationInfo.class,
                                                                                       this.daqReader.getPartitionName(),
                                                                                       "RunCtrl.Supervision." + name,
                                                                                       (info) -> {
                                                                                           return (info.status.equals(daq.rc.ProcessStatus.UP.toString()) == true);
                                                                                       });
                    ExecutorIS<rc.RCStateInfo> isReceiver_running = new ExecutorIS<>(rc.RCStateInfo.class,
                                                                                     this.daqReader.getPartitionName(),
                                                                                     "RunCtrl." + name,
                                                                                     (info) -> {
                                                                                         return((info.state.equals(daq.rc.ApplicationState.RUNNING.toString()) == true)
                                                                                                && (info.transitioning == false));
                                                                                     }))                
                {                    
                    // start TTC controller
                    this.execRC.startApps(controller, new String[] {name});
                    
                    // Why to wait first for UP? Basically to avoid the following scenario:
                    // - The TTC controller dies (and the IS FSM state is not cleaned - i.e., RUNNING);
                    // - The start command is sent to the controller;
                    // - Before the controller is actually UP, a check on the state is done and the previous value (RUNNING) is picked up.
                    // In this case the wait for RUNNING would trigger before the controller is even started.
                    // Remind that the controller's controller, considers the start command done as soon as the command is sent to the PMG
                    // because the controller at this point is out of membership.
                    
                    // wait until TTC controller is UP again  
                    isReceiver_up.startWatching();
                    isReceiver_up.waitForISMessage(this.waitForUpTimeout.intValue(), TimeUnit.SECONDS);
                    
                    TTCRestartExecutor.log.info("Application \"" + name + "\" is up again");
                    
                    // wait until TTC controller reaches RUNNING state again
                    isReceiver_running.startWatching();
                    isReceiver_running.waitForISMessage(this.waitForRunningTimeout.intValue(), TimeUnit.SECONDS);
                    
                    TTCRestartExecutor.log.info("Application \"" + name + "\" is now in RUNNING state");
                }
                catch(final ExecutorRCException e) {
                    final String msg = "Failed to start application \"" + name + "\": " + e;
                    throw new TTCRestartFailure(msg, e, false);
                }
                catch(final WaitTimeoutException e) {
                    final String msg = "Failed to bring application \"" + name + "\" to the RUNNING state within the defined timeout";
                    throw new TTCRestartFailure(msg, e, false);
                }
                catch(final ExecutorISException e) {
                    final String msg = "Cannot determine whether the application \"" + name + "\" has been restarted: " + e;
                    throw new TTCRestartFailure(msg, e, false);
                }
                catch(final InterruptedException e) {
                    final String msg = "Thread has been interrupted while waiting for the application \"" + name + "\" to be restarted";
                    Thread.currentThread().interrupt(); // Restore the interrupted state
                    throw new TTCRestartFailure(msg, e, false);
                }

                // send RESYNCH to TTC controller
                try {
                    this.execRC.sendResynch(name, trgInfo, new String[] {}, true, this.resynchTimeout.intValue());
                }
                catch(final ExecutorRCException e) {
                    String msg = "The resynch procedure could not be completed";

                    final Throwable cause = e.getCause();
                    if((WaitTimeoutException.class.isInstance(cause) == true) || (InterruptedException.class.isInstance(cause) == true)) {
                        msg += " (timeout elapsed)";
                    }

                    throw new TTCRestartFailure(msg, e, false);
                }

                // enable TTC controller
                try {
                    this.execRC.putAppsInFsm(controller, new String[] {name});
                }
                catch(final ExecutorRCException e) {
                    final String msg = "The controller could not enable application " + name;
                    throw new TTCRestartFailure(msg + ". Application should be re-enabled manually", e, true);
                }

                // increase luminosity block
                try {
                    this.execTrigger.increaseLumiBlock();
                }
                catch(final ExecutorTriggerException e) {
                    final TTCRestart event = new TTCRestart(name,
                                                            controller,
                                                            segment,
                                                            restartableDuringRun,
                                                            controlsTTCPartitions,
                                                            ttcRestart.getType(),
                                                            STATUS.PROBLEM,
                                                            id);
                    event.setError("Failed to increase the luminosity block", e);
                    this.cep.injectEvent(event);
                }
            }
            catch(final TTCRestartFailure ex) {
                final TTCRestart event = new TTCRestart(name,
                                                        controller,
                                                        segment,
                                                        restartableDuringRun,
                                                        controlsTTCPartitions,
                                                        ttcRestart.getType(),
                                                        STATUS.FAILED,
                                                        id);
                event.setError(ex.getMessage(), ex.getCause());
                this.cep.injectEvent(event);

                resumeTrigger = ex.resumeTrigger();
                failed = true;

                try {
                    // Inform the user
                    final StringBuilder msg = new StringBuilder();
                    msg.append("The TTC restart procedure for controller ");
                    msg.append(ttcRestart.getController());
                    msg.append(" (segment ");
                    msg.append(ttcRestart.getSegment());
                    msg.append(") could not be completed. The reason for such a failure is :");
                    msg.append(ex.getMessage());
                    if(resumeTrigger == false) {
                        msg.append(". In this case the trigger is not automatically resumed and a manual intervention is needed if appropriate!");
                    }

                    this.execIgui.showError(msg.toString());
                }
                catch(final ExecutorIGUIException ex1) {
                    ers.Logger.error(ex1);
                }
            }
            finally {
                try {
                    if(resumeTrigger == true) {
                        this.execTrigger.resume(detmask);
                    }

                    if(failed == false) {
                        final TTCRestart event = new TTCRestart(name,
                                                                controller,
                                                                segment,
                                                                restartableDuringRun,
                                                                controlsTTCPartitions,
                                                                ttcRestart.getType(),
                                                                STATUS.DONE,
                                                                id);
                        this.cep.injectEvent(event);
                    }
                }
                catch(final ExecutorTriggerException e) {
                    if(failed == false) {
                        final TTCRestart event = new TTCRestart(name,
                                                                controller,
                                                                segment,
                                                                restartableDuringRun,
                                                                controlsTTCPartitions,
                                                                ttcRestart.getType(),
                                                                STATUS.FAILED,
                                                                id);
                        event.setError("Not able to resume trigger at the end of TTC restart", e);
                        this.cep.injectEvent(event);
                    } else {
                        // If failed, then it means that the FAILED event has already been generated
                        // Just log the failure but not send another FAILED event to the engine
                        ers.Logger.error(e);
                    }

                    try {
                        this.execIgui.showError("The trigger could not be resumed as part of the TTC restart procedure: a manual intervention is needed");
                    }
                    catch(final ExecutorIGUIException ex1) {
                        ers.Logger.error(ex1);
                    }
                }
                finally {                
                    if(publishToken != null) {
                        try {
                            HoldTriggerInfoPublisher.remove(publishToken);
                        }
                        catch(final RepositoryNotFoundException | InfoNotFoundException e) {
                            TTCRestartExecutor.log.error("Failed to remove IS information about trigger on hold: " + e, e);
                        }
                    }
                }
            }
        }
    }
}
