package chip.subscriber;

import chip.event.issue.Issue;

public abstract class DeferredMultiActionExecutor extends AbstractDeferredExecutor implements MultiActionExecutor {
    @Override
    final public void update(final Issue[] issueIn, final Issue[] issueRemove) {    
        super.execute(new Runnable() {
            @Override
            public void run() {
                doUpdate(issueIn, issueRemove);
            }
        });
    }

    abstract protected void doUpdate(final Issue[] issueIn, final Issue[] issueRemove);
}
