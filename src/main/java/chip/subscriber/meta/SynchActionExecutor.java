package chip.subscriber.meta;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;

import chip.CEPService;
import chip.event.issue.Issue;
import chip.event.issue.meta.SynchAction;
import chip.msg.Internal;
import chip.subscriber.ActionExecutor;
import chip.subscriber.auto.ClockSwitchExecutor;
import chip.subscriber.auto.WarmStartExecutor;
import chip.subscriber.recovery.StoplessExecutor;


public class SynchActionExecutor implements ActionExecutor {
    @Autowired
    CEPService cep;

    private static final Log log = LogFactory.getLog(SynchActionExecutor.class);

    public SynchActionExecutor() {
    }

    @Override
    public void update(final Issue issue) {
        final SynchAction actionIssue = (SynchAction) issue;

        try {
            switch(actionIssue.getAction()) {
                case RESET_RESOURCES_AFTER_STOP: {
                    final StoplessExecutor se = this.cep.getBeanWithSpring(chip.subscriber.recovery.StoplessExecutor.class);
                    se.doResetAfterStop();

                    SynchActionExecutor.log.info("Dynamic resources have been reset");
                }
                    break;
                case SET_AUTOMATIC_CLOCK: {
                    final ClockSwitchExecutor cse = this.cep.getBeanWithSpring(chip.subscriber.auto.ClockSwitchExecutor.class);
                    cse.setAutomaticClock();

                    SynchActionExecutor.log.info("Clock switch set to automatic mode");
                }
                    break;
                case RESET_R4P: {
                    final WarmStartExecutor wse = this.cep.getBeanWithSpring(chip.subscriber.auto.WarmStartExecutor.class);
                    wse.resetR4P();

                    SynchActionExecutor.log.info("R4P flag has been reset");
                }
                    break;
                default:
                    break;
            }

            this.cep.injectEvent(new SynchAction(actionIssue.getTransition(),
                                                 actionIssue.getState(),
                                                 actionIssue.getType(),
                                                 SynchAction.STATUS.DONE,
                                                 SynchAction.ACTION.NONE));
        }
        catch(final BeansException ex) {
            ers.Logger.error(new Internal("Unexpected error: " + ex, ex));
            ex.printStackTrace();

            this.cep.injectEvent(new SynchAction(actionIssue.getTransition(),
                                                 actionIssue.getState(),
                                                 actionIssue.getType(),
                                                 SynchAction.STATUS.FAILED,
                                                 SynchAction.ACTION.NONE));
        }
    }
}
