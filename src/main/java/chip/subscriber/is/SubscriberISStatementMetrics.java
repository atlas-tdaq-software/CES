package chip.subscriber.is;

import ipc.Partition;
import is.InfoNotCompatibleException;
import is.InvalidCriteriaException;
import is.RepositoryNotFoundException;

import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;

import com.espertech.esper.common.client.metric.StatementMetric;

import chip.DAQConfigReader;
import chip.is.CHIPStatementMetricsNamed;



public class SubscriberISStatementMetrics {
    private static final Log log = LogFactory.getLog(SubscriberISStatementMetrics.class);
    private final Partition partition;

    // Using SpEL to get properties list of files
    @Value("#{configuration['chip.isServerChipMetrics']}")
    private volatile String server_name;

    private final static String info_name_statement = "CHIPStatementMetrics";

    public SubscriberISStatementMetrics(final DAQConfigReader daqConf) {
        this.partition = new ipc.Partition(daqConf.getPartitionName());
        
        // Hook to remove IS information at exit
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                final is.Repository isRepo = new is.Repository(SubscriberISStatementMetrics.this.partition);
                try {
                    isRepo.removeAll(SubscriberISStatementMetrics.this.server_name, 
                                     new is.Criteria(Pattern.compile(SubscriberISStatementMetrics.info_name_statement + ".*")));
                }
                catch(final RepositoryNotFoundException | InvalidCriteriaException ex) {
                    ex.printStackTrace();
                }
            }            
        });
    }

    public void update(final StatementMetric statementMetric) {
        final String name_statement = this.server_name + "." + SubscriberISStatementMetrics.info_name_statement;

        try {
            final CHIPStatementMetricsNamed chip_statement_metrics = new CHIPStatementMetricsNamed(this.partition,
                                                                                                   name_statement + "."
                                                                                                       + statementMetric.getStatementName());

            chip_statement_metrics.engineURI = statementMetric.getRuntimeURI();
            chip_statement_metrics.cpuTime = statementMetric.getCpuTime();
            chip_statement_metrics.numInput = statementMetric.getNumInput();
            chip_statement_metrics.numOutputIStream = statementMetric.getNumOutputIStream();
            chip_statement_metrics.numOutputRStream = statementMetric.getNumOutputRStream();
            chip_statement_metrics.statementName = statementMetric.getStatementName();
            chip_statement_metrics.timestamp = statementMetric.getTimestamp();
            chip_statement_metrics.wallTime = statementMetric.getWallTime();

            if(chip_statement_metrics.numInput > 0) { // avoid division by zero
                chip_statement_metrics.cpuTimePerStatementInput = chip_statement_metrics.cpuTime / chip_statement_metrics.numInput;
                chip_statement_metrics.wallTimePerStatementInput = chip_statement_metrics.wallTime / chip_statement_metrics.numInput;
            } else {
                chip_statement_metrics.cpuTimePerStatementInput = chip_statement_metrics.cpuTime;
                chip_statement_metrics.wallTimePerStatementInput = chip_statement_metrics.wallTime;
            }

            chip_statement_metrics.checkin();

        }
        catch(final is.RepositoryNotFoundException | InfoNotCompatibleException e) {
            SubscriberISStatementMetrics.log.warn("Error writing to IS: " + e, e);
        }
    }
}
