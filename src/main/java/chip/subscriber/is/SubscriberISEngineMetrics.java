package chip.subscriber.is;

import ipc.Partition;
import is.InfoNotCompatibleException;
import is.InvalidCriteriaException;
import is.RepositoryNotFoundException;

import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;

import com.espertech.esper.common.client.metric.RuntimeMetric;

import chip.DAQConfigReader;
import chip.is.CHIPEngineMetricsNamed;



public class SubscriberISEngineMetrics {
    private static final Log log = LogFactory.getLog(SubscriberISEngineMetrics.class);
    private final Partition partition;

    // Using SpEL to get properties list of files
    @Value("#{configuration['chip.isServerChipMetrics']}")
    private volatile String server_name;

    private final static String info_name_engine = "CHIPEngineMetrics";

    public SubscriberISEngineMetrics(final DAQConfigReader daqConf) {
        this.partition = new ipc.Partition(daqConf.getPartitionName());
        
        // Hook to remove IS information at exit
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                final is.Repository isRepo = new is.Repository(SubscriberISEngineMetrics.this.partition);
                try {
                    isRepo.removeAll(SubscriberISEngineMetrics.this.server_name, 
                                     new is.Criteria(Pattern.compile(SubscriberISEngineMetrics.info_name_engine + ".*")));
                }
                catch(final RepositoryNotFoundException | InvalidCriteriaException ex) {
                    ex.printStackTrace();
                }
            }            
        });
    }

    public void update(final RuntimeMetric engineMetric) {
        final String name_engine = this.server_name + "." + SubscriberISEngineMetrics.info_name_engine;

        try {
            final CHIPEngineMetricsNamed chip_engine_metrics = new CHIPEngineMetricsNamed(this.partition, name_engine);
            chip_engine_metrics.engineURI = engineMetric.getRuntimeURI();
            chip_engine_metrics.inputCount = engineMetric.getInputCount();
            chip_engine_metrics.inputCountDelta = engineMetric.getInputCountDelta();
            chip_engine_metrics.scheduleDepth = engineMetric.getScheduleDepth();
            chip_engine_metrics.timestamp = engineMetric.getTimestamp();

            chip_engine_metrics.checkin();
        }
        catch(final is.RepositoryNotFoundException | InfoNotCompatibleException e) {
            SubscriberISEngineMetrics.log.warn("Error writing to IS: " + e, e);
        }
    }
}
