package chip.listener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPStatement;


public class EchoImpl implements Echo {
    private static final Log log = LogFactory.getLog(EchoImpl.class);

    /* (non-Javadoc)
     * @see ch.atdaq.ces.listener.Echo#update(java.lang.String)
     */
    @Override
    public void update(final EventBean[] newEvents, final EventBean[] oldEvents, EPStatement stmt, EPRuntime rt) {
        EchoImpl.log.debug("Listener: " + newEvents[0].get("what"));
    }
}
