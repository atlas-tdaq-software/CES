package chip.listener;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.common.client.render.EPRenderEventService;

import chip.CEPService;


public class PrintInfo extends Print {
    private static final Log log = LogFactory.getLog(PrintInfo.class);
    private final EPRenderEventService eventRenderer;

    public PrintInfo(final CEPService cep) {
        this.eventRenderer = cep.getEsper().getRenderEventService();
    }

    @Override
    public void doUpdate(final EventBean[] newEvents, final EventBean[] oldEvents) {
        for(final EventBean e : newEvents) {
            PrintInfo.log.info(e.getEventType().getName()
                               + StringEscapeUtils.unescapeJava(": " + this.eventRenderer.getJSONRenderer(e.getEventType()).render(e)));
        }
    }
}
