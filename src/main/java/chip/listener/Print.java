package chip.listener;

import java.util.concurrent.ExecutorService;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPStatement;

import chip.utils.ExecutorFactory;


public abstract class Print implements Echo {
    private final static ExecutorService tp = ExecutorFactory.newThreadPool("Print", 1, false);

    @Override
    public final void update(final EventBean[] newEvents, final EventBean[] oldEvents, EPStatement stmt, EPRuntime rt) {
        Print.tp.execute(new Runnable() {
            @Override
            public void run() {
                doUpdate(newEvents, oldEvents);
            }
        });
    }

    protected abstract void doUpdate(final EventBean[] newEvents, final EventBean[] oldEvents);
}
