package chip.listener;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.common.client.render.EPRenderEventService;



public class PrintXml extends Print {
    private static final Log log = LogFactory.getLog(PrintXml.class);
    private final EPRenderEventService eventRenderer;
    private Writer writer = null;

    public PrintXml(final chip.CEPService cep, final String outFile) {
        try {
            this.writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outFile), "utf-8"));
        }
        catch(final UnsupportedEncodingException e) {
            PrintXml.log.warn("Unsopport encoding: " + e);
        }
        catch(final FileNotFoundException e) {
            PrintXml.log.warn("File not found: " + e);
        }
        this.eventRenderer = cep.getEsper().getRenderEventService();
    }

    @Override
    public void doUpdate(final EventBean[] newEvents, final EventBean[] oldEvents) {
        for(final EventBean eb : newEvents) {
            try {
                if(this.writer != null) {
                    this.writer.write(this.eventRenderer.renderXML(eb.getEventType().getName(), eb) + "\n");
                }
            }
            catch(final IOException e) {
                PrintXml.log.warn("Failed to write XML file: " + e);
            }
        }
    }
}
