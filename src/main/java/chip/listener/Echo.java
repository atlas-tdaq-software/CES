package chip.listener;

import com.espertech.esper.runtime.client.UpdateListener;

/**
 * Placeholder interface to mark listener
 * of the 'Echo' type.
 * It extends the standard Esper UpdateListener
 * interface.
 * 
 * @author lucamag
 *
 */
public interface Echo extends UpdateListener {

}