package chip.listener;

import java.util.stream.Collectors;

import com.espertech.esper.common.client.EventBean;

import chip.DAQConfigReader;
import chip.event.issue.Issue;
import chip.event.issue.Issue.ISSUETYPE;
import chip.event.issue.TriggerHeldIssue;
import chip.event.issue.core.Problem;
import chip.event.issue.recovery.HltErsRecovery;
import chip.event.issue.recovery.HltRecovery;
import chip.event.issue.recovery.RecoveryEvent;
import chip.event.issue.test.TestFollowUp;
import chip.msg.AutoProc;
import chip.msg.Common;
import chip.msg.Core;
import chip.msg.HoldingTriggerAction;
import chip.msg.Recovery;
import chip.msg.Test;



// Note: send to this listener only events inheriting from chip.event.issue.Issue
public class PrintErs extends Print {
    private final DAQConfigReader daqConf;

    public PrintErs() {
        this.daqConf = null;
    }

    public PrintErs(final DAQConfigReader daqConf) {
        this.daqConf = daqConf;
    }

    @Override
    public void doUpdate(final EventBean[] newEvents, final EventBean[] oldEvents) {
        for(final EventBean e : newEvents) {
            final Issue issue = (Issue) e.getUnderlying();

            final Boolean isError = issue.getError();
            final Exception ex = issue.errorException();
            final String cat = issue.getIssueCategory().toString();

            ers.Issue ersIssue = null;

            if(cat.equals(ISSUETYPE.TEST.name())) {
                final TestFollowUp tst = (TestFollowUp) issue;

                final StringBuilder msg = new StringBuilder();
                msg.append("Test follw-up for application \"");
                msg.append(tst.getApplicationName());
                msg.append("\" running on host \"");
                msg.append(tst.getRunningHost());
                msg.append("\" ");

                final String status = issue.getIssueStatus().toString();
                if(status.equals("_NEW") == true) {
                    msg.append(" has been started");
                } else if(status.equals("DONE") == true) {
                    msg.append("has been concluded successfully");
                } else if(status.equals("PROBLEM") == true) {
                    msg.append("is being resolved but a not-critical step could not be completed");
                } else if(status.equals("FAILED") == true) {
                    msg.append("could not be concluded because of errors");
                } else {
                    msg.append(" is on-going ");
                }

                msg.append("(status is \"");
                msg.append(status);
                msg.append("\", action is \"");
                msg.append(issue.getIssueAction().toString());
                msg.append("\")");

                if(isError.booleanValue()) {
                    msg.append(". Occurred errors: ");
                    msg.append(issue.getErrorDesc());
                }

                if(ex != null) {
                    ersIssue = new Test(msg.toString(),
                                        issue.getIssueId(),
                                        issue.getIssueStatus().toString(),
                                        tst.getApplicationName(),
                                        tst.getRunningHost(),
                                        issue.getIssueAction().toString(),
                                        ex);
                } else {
                    ersIssue = new Test(msg.toString(),
                                        issue.getIssueId(),
                                        issue.getIssueStatus().toString(),
                                        tst.getApplicationName(),
                                        tst.getRunningHost(),
                                        issue.getIssueAction().toString());
                }
            } else if(cat.equals(ISSUETYPE.CORE.name())) {
                final Problem prb = (Problem) issue;

                final StringBuilder msg = new StringBuilder();
                msg.append("Problem of type \"");
                msg.append(issue.getIssueType().toString());
                msg.append("\" for application \"");
                msg.append(prb.getApplication());
                msg.append("\" whose controller is \"");
                msg.append(prb.getController());
                msg.append("\" ");

                final String status = issue.getIssueStatus().toString();
                if(status.equals("_NEW") == true) {
                    msg.append("has been identified; proper actions will be eventually taken");
                } else if(status.equals("DONE") == true) {
                    msg.append("has been resolved successfully");
                } else if(status.equals("PROBLEM") == true) {
                    msg.append("is being resolved but a not-critical step could not be completed");
                } else if(status.equals("FAILED") == true) {
                    msg.append("could not be resolved because of errors");
                } else {
                    msg.append("is being handled and is now in status ");
                    msg.append(issue.getIssueStatus());
                    msg.append(" (action is ");
                    msg.append(prb.getAction().toString());
                    msg.append(")");
                }

                if(isError.booleanValue()) {
                    msg.append(". Occurred errors: ");
                    msg.append(issue.getErrorDesc());
                }

                if(ex != null) {
                    ersIssue = new Core(msg.toString(),
                                        issue.getIssueType().toString(),
                                        issue.getIssueId(),
                                        issue.getIssueStatus().toString(),
                                        prb.getApplication(),
                                        prb.getController(),
                                        prb.getAction().toString(),
                                        ex);
                } else {
                    ersIssue = new Core(msg.toString(),
                                        issue.getIssueType().toString(),
                                        issue.getIssueId(),
                                        issue.getIssueStatus().toString(),
                                        prb.getApplication(),
                                        prb.getController(),
                                        prb.getAction().toString());
                }
            } else if(cat.equals(ISSUETYPE.RECOVERY.name())) {
                final RecoveryEvent re = (RecoveryEvent) issue;

                final StringBuilder msg = new StringBuilder();
                msg.append("Automatic recovery of category \"");
                msg.append(issue.getIssueId());
                msg.append("\" and of type \"");
                msg.append(issue.getIssueType().toString());
                msg.append("\" for component(s) \"");
                msg.append(re.getConcernedComponent());
                msg.append("\" ");

                final String status = issue.getIssueStatus().toString();
                if(status.equals("_NEW") == true) {
                    msg.append("has been initiated");
                } else if(status.equals("DONE") == true) {
                    msg.append("has been completed successfully");
                } else if(status.equals("PROBLEM") == true) {
                    msg.append("encountered a problem");
                } else if(status.equals("FAILED") == true) {
                    msg.append("failed to be properly executed");
                } else {
                    msg.append("is being executed and is now in status ");
                    msg.append(issue.getIssueStatus());

                    if(HltRecovery.class.isInstance(issue) || HltErsRecovery.class.isInstance(issue)) {
                        msg.append(" (action is ");
                        msg.append(issue.getIssueAction());
                        msg.append(")");
                    }
                }

                final String det = RecoveryEvent.extractConcernedDetector(re, this.daqConf).stream().collect(Collectors.joining(","));

                if(isError.booleanValue()) {
                    msg.append(". Occurred errors: ");
                    msg.append(issue.getErrorDesc());
                }

                if(ex != null) {
                    ersIssue = new Recovery(msg.toString(),
                                            issue.getIssueType().toString(),
                                            issue.getIssueId(),
                                            issue.getIssueStatus().toString(),
                                            re.getConcernedComponent(),
                                            det,
                                            ex);
                } else {
                    ersIssue = new Recovery(msg.toString(),
                                            issue.getIssueType().toString(),
                                            issue.getIssueId(),
                                            issue.getIssueStatus().toString(),
                                            re.getConcernedComponent(),
                                            det);
                }
            } else if(cat.equals(ISSUETYPE.AUTO_PROCEDURE.name())) {
                final StringBuilder msg = new StringBuilder();
                msg.append("Automatic procedure of category \"");
                msg.append(issue.getIssueId());
                msg.append("\" and of type \"");
                msg.append(issue.getIssueType().toString());
                msg.append("\" ");

                final String status = issue.getIssueStatus().toString();
                if(status.equals("_NEW") == true) {
                    msg.append("has been initiated");
                } else if(status.equals("DONE") == true) {
                    msg.append("has been completed successfully");
                } else if(status.equals("PROBLEM") == true) {
                    msg.append("encountered a problem");
                } else if(status.equals("FAILED") == true) {
                    msg.append("failed to be properly executed");
                } else {
                    msg.append("is being executed and is now in status ");
                    msg.append(issue.getIssueStatus());
                }

                if(isError.booleanValue()) {
                    msg.append(". Occurred errors: ");
                    msg.append(issue.getErrorDesc());
                }

                if(ex != null) {
                    ersIssue = new AutoProc(msg.toString(),
                                            issue.getIssueType().toString(),
                                            issue.getIssueId(),
                                            issue.getIssueStatus().toString(),
                                            ex);
                } else {
                    ersIssue = new AutoProc(msg.toString(),
                                            issue.getIssueType().toString(),
                                            issue.getIssueId(),
                                            issue.getIssueStatus().toString());
                }
            } else {
                final StringBuilder msg = new StringBuilder();

                msg.append("Recovery/action of category \"");
                msg.append(cat);
                msg.append("\" is being-executed. ");
                msg.append("Status: ");
                msg.append(issue.getIssueStatus().toString());
                msg.append(", action: ");
                msg.append(issue.getIssueAction().toString());

                if(isError.booleanValue()) {
                    msg.append(". Occurred problems: ");
                    msg.append(issue.getErrorDesc());
                }

                if(ex != null) {
                    ersIssue = new Common(msg.toString(), ex);
                } else {
                    ersIssue = new Common(msg.toString());
                }
            }

            ersIssue.setQualifiers(issue.additionalQualifiers());

            final String status = issue.getIssueStatus().toString();
            if(status.equals("_NEW") || status.equals("DONE")) {
                ers.Logger.info(ersIssue);
            } else if(status.equals("PROBLEM")) {
                ers.Logger.warning(ersIssue);
            } else if(status.equals("FAILED")) {
                ers.Logger.error(ersIssue);
            } else {
                ers.Logger.log(ersIssue);
            }
            
            if((status.equals("_NEW") || status.equals("DONE") || status.equals("FAILED"))
               && (TriggerHeldIssue.class.isInstance(issue) == true))
            {
                final TriggerHeldIssue trgIssue = TriggerHeldIssue.class.cast(issue);
                if(trgIssue.isTriggerHeld() == true) {
                    final boolean isStart = status.equals("_NEW");
                    final String type = trgIssue.getReason().toString();
                    final String det = trgIssue.getCausedBy(this.daqConf).name();
                    
                    final StringBuilder msg = new StringBuilder(isStart == true ? "Starting " : "Completed " );
                    msg.append("an automatic action requiring the trigger to be held of type \"");
                    msg.append(type);
                    msg.append("\" for detector \"");
                    msg.append(det);
                    msg.append("\"");
                                        
                    final HoldingTriggerAction holdTrgIssue = new HoldingTriggerAction(msg.toString(),
                                                                                       det,
                                                                                       type,
                                                                                       trgIssue.getTime(),
                                                                                       isStart,
                                                                                       trgIssue.getId());
                    
                    ers.Logger.info(holdTrgIssue);
                }
            }
        }
    }
}
