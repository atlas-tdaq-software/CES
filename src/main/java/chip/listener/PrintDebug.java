package chip.listener;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import chip.CEPService;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.common.client.render.EPRenderEventService;


public class PrintDebug extends Print {
    private static final Log log = LogFactory.getLog(PrintDebug.class);
    private final EPRenderEventService eventRenderer;

    public PrintDebug(final CEPService cep) {
        this.eventRenderer = cep.getEsper().getRenderEventService();
    }

    @Override
    public void doUpdate(final EventBean[] newEvents, final EventBean[] oldEvents) {
        for(final EventBean e : newEvents) {
            PrintDebug.log.debug(e.getEventType().getName()
                                 + StringEscapeUtils.unescapeJava(": " + this.eventRenderer.getJSONRenderer(e.getEventType()).render(e)));
        }
    }
}
