package chip.event.en;

import daq.rc.ProcessStatus;

public enum STATUS {
    NOTAV(ProcessStatus.NOTAV),
    ABSENT(ProcessStatus.ABSENT),
    REQUESTED(ProcessStatus.REQUESTED),
    UP(ProcessStatus.UP),
    TERMINATING(ProcessStatus.TERMINATING),
    EXITED(ProcessStatus.EXITED),
    FAILED(ProcessStatus.FAILED);
    
    private STATUS(final ProcessStatus status) {
        assert (this.name().equals(status.name()));
    }
}
