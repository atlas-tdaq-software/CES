package chip.event.en;

public enum IFDIES {
    RESTART,
    IGNORE,
    ERROR,
    HANDLE;
}
