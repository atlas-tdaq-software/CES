package chip.event.en;

public enum IFFAILED {
    ERROR,
    IGNORE,
    RESTART,
    HANDLE;
}
