package chip.event.en;

import daq.rc.ApplicationState;


/**
 * As by RunControl ApplicationState
 * https://svnweb.cern.ch/trac/atlastdaq/browser/DAQ/online/RunControl/trunk/jsrc/daq/rc/ApplicationState.java
 * 
 * @author lmagnoni
 */
public enum FSM {
    ABSENT(ApplicationState.ABSENT),
    NONE(ApplicationState.NONE),
    INITIAL(ApplicationState.INITIAL),
    CONFIGURED(ApplicationState.CONFIGURED),
    CONNECTED(ApplicationState.CONNECTED),
    GTHSTOPPED(ApplicationState.GTHSTOPPED),
    SFOSTOPPED(ApplicationState.SFOSTOPPED),
    HLTSTOPPED(ApplicationState.HLTSTOPPED),
    DCSTOPPED(ApplicationState.DCSTOPPED),
    ROIBSTOPPED(ApplicationState.ROIBSTOPPED),
    RUNNING(ApplicationState.RUNNING),
    _EMPTY(null);

    private FSM(final ApplicationState state) {
        assert (state == null || this.name().equals(state.name()));
    }

    public boolean isEqualOrGreater(final FSM other) {
        return this.ordinal() >= other.ordinal();
    }

    public boolean isGreater(final FSM other) {
        return this.ordinal() > other.ordinal();
    }
}
