package chip.event.en;

public enum IFERROR {
    ERROR,
    IGNORE,
    RESTART,
    HANDLE;
}
