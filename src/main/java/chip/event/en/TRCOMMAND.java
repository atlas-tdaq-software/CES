package chip.event.en;

import daq.rc.Command.FSMCommands;


/**
 * Transition command https://svnweb.cern.ch/trac/atlastdaq/browser/DAQ/online/RunControl/trunk/jsrc/daq/rc/Command.java
 * 
 * @author lmagnoni INITIALIZE, CONFIGURE, CONFIG, CONNECT, START, STOP, STOPROIB, STOPDC, STOPHLT, STOPRECORDING, STOPGATHERING,
 *         STOPARCHIVING, DISCONNECT, UNCONFIG, UNCONFIGURE, SHUTDOWN, USERBROADCAST, RELOAD_DB, RESYNCH, NEXT_TRANSITION, SUB_TRANSITION,
 *         INIT_FSM, STOP_FSM;
 */

public enum TRCOMMAND {
    _EMPTY(FSM._EMPTY, null),
    INITIALIZE(FSM.INITIAL, FSMCommands.INITIALIZE),
    INIT_FSM(FSM.INITIAL, FSMCommands.INIT_FSM),
    CONFIGURE(FSM.CONFIGURED, FSMCommands.CONFIGURE),
    CONFIG(FSM.CONFIGURED, FSMCommands.CONFIG),
    CONNECT(FSM.CONNECTED, FSMCommands.CONNECT),
    START(FSM.RUNNING, FSMCommands.START),
    STOPROIB(FSM.ROIBSTOPPED, FSMCommands.STOPROIB),
    STOPDC(FSM.DCSTOPPED, FSMCommands.STOPDC),
    STOPHLT(FSM.HLTSTOPPED, FSMCommands.STOPHLT),
    STOPRECORDING(FSM.SFOSTOPPED, FSMCommands.STOPRECORDING),
    STOPGATHERING(FSM.GTHSTOPPED, FSMCommands.STOPGATHERING),
    STOPARCHIVING(FSM.CONNECTED, FSMCommands.STOPARCHIVING),
    DISCONNECT(FSM.CONFIGURED, FSMCommands.DISCONNECT),
    UNCONFIGURE(FSM.INITIAL, FSMCommands.UNCONFIGURE),
    UNCONFIG(FSM.INITIAL, FSMCommands.UNCONFIG),
    STOP(FSM.CONNECTED, FSMCommands.STOP),
    SHUTDOWN(FSM.NONE, FSMCommands.SHUTDOWN),
    USERBROADCAST(FSM._EMPTY, FSMCommands.USERBROADCAST),
    RELOAD_DB(FSM._EMPTY, FSMCommands.RELOAD_DB),
    RESYNCH(FSM._EMPTY, FSMCommands.RESYNCH),
    NEXT_TRANSITION(FSM._EMPTY, FSMCommands.NEXT_TRANSITION),
    SUB_TRANSITION(FSM._EMPTY, FSMCommands.SUB_TRANSITION),
    STOP_FSM(FSM._EMPTY, FSMCommands.STOP_FSM);

    private final FSM destinationState;

    private TRCOMMAND(final FSM destinationState, final FSMCommands fsmCommand) {
        this.destinationState = destinationState;
        assert (fsmCommand == null || this.name().equals(fsmCommand.name()));
    }

    public FSM getDestinationState() {
        return this.destinationState;
    }
}
