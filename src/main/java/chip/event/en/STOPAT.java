package chip.event.en;

import chip.DAQConfigReader;


public enum STOPAT {
    SOR,
    EOR,
    UNCONFIGURE,
    SHUTDOWN,
    USERDEFINED;

    private FSM toFSM() {
        switch(this) {
            case SOR:
                return FSM.RUNNING;
            case EOR:
                return FSM.CONNECTED;
            case UNCONFIGURE:
                return FSM.INITIAL;
            case SHUTDOWN:
                return FSM.NONE;
            case USERDEFINED:
                return FSM._EMPTY;
            default:
                return FSM._EMPTY;
        }
    }

    public FSM toFSM(final DAQConfigReader.APP_TYPE type) {
        switch(type) {
            case OS_INFRASTRUCTURE:
                return FSM.ABSENT;
            case ONLINE_SEGMENT:
                return FSM.NONE;
            case INFRASTRUCTURE:
                return FSM.NONE;
            case RUNCONTROL:
                return FSM.NONE;
            default:
                return this.toFSM();
        }
    }
}
