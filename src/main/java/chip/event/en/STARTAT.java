package chip.event.en;

import chip.DAQConfigReader;


public enum STARTAT {
    BOOT,
    CONFIGURE,
    SOR,
    EOR,
    USERDEFINED;

    private FSM toFSM() {
        switch(this) {
            case BOOT:
                return FSM.INITIAL;
            case CONFIGURE:
                return FSM.CONFIGURED;
            case SOR:
                return FSM.RUNNING;
            case EOR:
                return FSM.CONNECTED;
            case USERDEFINED:
                return FSM._EMPTY;
            default:
                return FSM._EMPTY;
        }
    }

    public FSM toFSM(final DAQConfigReader.APP_TYPE type) {
        /**
         * startAtState/stopAtState indicates the FMS state of the RootController or Controller responsible for the application at which the
         * app is expected to be running / not running. This field are set with this algorithm: - if Online_segment application
         * startAtState/stopAtState=FSM.NONE/ | StopAT non esiste a meno che user defined - if Infra/RC_application
         * startAtState/stopAtState=FSM.INITIAL | STOP AT NONE ROOTController o ABSENT O USER DEFINED - if startAt=Boot ->
         * startAtState=FSM.INITIAL - if startAt=SOR -> startAtState=FSM.RUNNING - if stopAt=EOR -> stopAtState=FSM.STOPRECORDING
         */

        switch(type) {
            case OS_INFRASTRUCTURE:
                return FSM.NONE;
            case ONLINE_SEGMENT:
                return FSM.INITIAL;
            case INFRASTRUCTURE:
                return FSM.INITIAL;
            case RUNCONTROL:
                return FSM.INITIAL;
            default:
                return this.toFSM();
        }
    }
}
