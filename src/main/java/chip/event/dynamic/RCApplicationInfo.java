package chip.event.dynamic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import rc.RCStateInfoNamed;
import chip.event.en.FSM;
import chip.event.en.RCCOMMAND;
import chip.event.en.TRCOMMAND;
import daq.EsperUtils.CepException.InvalidEventException;


public class RCApplicationInfo extends Info {

    private static final Log log = LogFactory.getLog(RCApplicationInfo.class);

    private final FSM state;
    private final Boolean isTransitioning;
    private final TRCOMMAND currentTransitionCommand;
    private final String currentTransitionCommandUID;
    private final TRCOMMAND previousTransitionCommand;
    private final Long transitionTime;
    private final Boolean internalError;
    private final RCCOMMAND rccommand;
    private final String[] rccommandArgs;
    private final String rccommandUID;
    private final Boolean busy;

    private final FSM parentState;
    private final Boolean parentIsTransitioning;
    private final TRCOMMAND parentCurrentTransitionCommand;
    private final TRCOMMAND parentPreviousTransitionCommand;
    private final Boolean parentIsExiting;
    private final Boolean parentIsInitializing;

    public RCApplicationInfo(final String name,
                             final Long infoTimestamp,
                             final Long eventTimestamp,
                             final FSM state,
                             final Boolean isTransitioning,
                             final TRCOMMAND currentTransitionCommand,
                             final String currentTransitionCommandUID,
                             final TRCOMMAND previousTransitionCommand,
                             final Long transitionTime,
                             final Boolean internalError,
                             final RCCOMMAND rccommand,
                             final String[] rccommandArgs,
                             final String rccommandUID,
                             final Boolean busy,
                             final FSM parentState,
                             final Boolean parentIsTransitioning,
                             final TRCOMMAND parentCurrentTransitionCommand,
                             final TRCOMMAND parentPreviousTransitionCommand,
                             final Boolean parentIsExiting,
                             final Boolean parentIsInitializing)
    {
        super(name, infoTimestamp, eventTimestamp);
        this.state = state;
        this.isTransitioning = isTransitioning;
        this.currentTransitionCommand = currentTransitionCommand;
        this.currentTransitionCommandUID = currentTransitionCommandUID;
        this.previousTransitionCommand = previousTransitionCommand;
        this.transitionTime = transitionTime;
        this.internalError = internalError;
        this.rccommand = rccommand;
        this.rccommandArgs = rccommandArgs;
        this.rccommandUID = rccommandUID;
        this.busy = busy;
        this.parentState = parentState;
        this.parentIsTransitioning = parentIsTransitioning;
        this.parentCurrentTransitionCommand = parentCurrentTransitionCommand;
        this.parentPreviousTransitionCommand = parentPreviousTransitionCommand;
        this.parentIsExiting = parentIsExiting;
        this.parentIsInitializing = parentIsInitializing;
    }

    public static class Builder {
        private final String applicationName;
        private Long timestamp; // long in nanoseconds
        private Long eventTimestamp; // millisecond - event creation time
        private FSM state;
        private Boolean isTransitioning;
        private TRCOMMAND currentTransitionCommand;
        private String currentTransitionCommandUID;
        private TRCOMMAND previousTransitionCommand;
        private Long transitionTime; // long in milliseconds
        private Boolean internalError;
        private RCCOMMAND rccommand;
        private String[] rccommandArgs;
        private String rccommandUID;
        private Boolean busy;
        private FSM parentState;
        private Boolean parentIsTransitioning;
        private TRCOMMAND parentCurrentTransitionCommand;
        private TRCOMMAND parentPreviousTransitionCommand;
        private Boolean parentIsExiting;
        private Boolean parentIsInitializing;

        public Builder(final String applicationName) {
            this.applicationName = applicationName;
        }

        public Builder timestamp(final Long time_stamp) {
            this.timestamp = time_stamp;
            return this;
        }

        public Builder eventTimestamp(final Long event_Timestamp) {
            this.eventTimestamp = event_Timestamp;
            return this;
        }

        public Builder state(final FSM state_) {
            this.state = state_;
            return this;
        }

        public Builder isTransitioning(final Boolean is_Transitioning) {
            this.isTransitioning = is_Transitioning;
            return this;
        }

        public Builder currentTransitionCommand(final TRCOMMAND current_TransitionCommand) {
            this.currentTransitionCommand = current_TransitionCommand;
            return this;
        }

        public Builder currentTransitionCommandUID(final String current_TransitionCommand_Uid) {
            this.currentTransitionCommandUID = current_TransitionCommand_Uid;
            return this;
        }

        public Builder previousTransitionCommand(final TRCOMMAND previous_TransitionCommand) {
            this.previousTransitionCommand = previous_TransitionCommand;
            return this;
        }
        
        public Builder transitionTime(final Long transition_Time) {
            this.transitionTime = transition_Time;
            return this;
        }

        public Builder internalError(final Boolean internal_Error) {
            this.internalError = internal_Error;
            return this;
        }

        public Builder rccommand(final RCCOMMAND rc_command) {
            this.rccommand = rc_command;
            return this;
        }

        public Builder rccommandUID(final String rc_command_uid) {
            this.rccommandUID = rc_command_uid;
            return this;
        }
        
        public Builder rccommandArgs(final String[] rc_commandArgs) {
            this.rccommandArgs = rc_commandArgs;
            return this;
        }

        public Builder busy(final Boolean busy_) {
            this.busy = busy_;
            return this;
        }

        public Builder parentState(final FSM parent_State) {
            this.parentState = parent_State;
            return this;
        }

        public Builder parentIsTransitioning(final Boolean parent_IsTransitioning) {
            this.parentIsTransitioning = parent_IsTransitioning;
            return this;
        }

        public Builder parentCurrentTransitionCommand(final TRCOMMAND parent_CurrentTransitionCommand) {
            this.parentCurrentTransitionCommand = parent_CurrentTransitionCommand;
            return this;
        }

        public Builder parentPreviousTransitionCommand(final TRCOMMAND parent_PreviousTransitionCommand) {
            this.parentPreviousTransitionCommand = parent_PreviousTransitionCommand;
            return this;
        }
        
        public Builder parentIsExiting(final Boolean parent_IsExiting) {
            this.parentIsExiting = parent_IsExiting;
            return this;
        }
        
        public Builder parentIsInitializing(final Boolean parent_IsInitializing) {
            this.parentIsInitializing = parent_IsInitializing;
            return this;
        }

        public RCApplicationInfo build() {
            return new RCApplicationInfo(this);
        }

    }

    public RCApplicationInfo(final Builder b) {
        super(b.applicationName, b.timestamp, b.eventTimestamp);
        this.state = b.state;
        this.isTransitioning = b.isTransitioning;
        this.currentTransitionCommand = b.currentTransitionCommand;
        this.currentTransitionCommandUID = b.currentTransitionCommandUID;
        this.previousTransitionCommand = b.previousTransitionCommand;
        this.transitionTime = b.transitionTime;
        this.internalError = b.internalError;
        this.rccommand = b.rccommand;
        this.rccommandArgs = b.rccommandArgs;
        this.rccommandUID = b.rccommandUID;
        this.busy = b.busy;
        this.parentState = b.parentState;
        this.parentIsTransitioning = b.parentIsTransitioning;
        this.parentCurrentTransitionCommand = b.parentCurrentTransitionCommand;
        this.parentPreviousTransitionCommand = b.parentPreviousTransitionCommand;
        this.parentIsExiting = b.parentIsExiting;
        this.parentIsInitializing = b.parentIsInitializing;
    }

    /**
     * @return the applicationState
     */
    public final FSM getState() {
        return this.state;
    }

    /**
     * @return the isTransitioning
     */
    public final Boolean getIsTransitioning() {
        return this.isTransitioning;
    }

    /**
     * @return the currentTransitionCommand
     */
    public final TRCOMMAND getCurrentTransitionCommand() {
        return this.currentTransitionCommand;
    }

    public final String getCurrentTransitionCommandUID() {
        return this.currentTransitionCommandUID;
    }

    public final TRCOMMAND getPreviousTransitionCommand() {
        return this.previousTransitionCommand;
    }
    
   /**
     * @return the transitionTime
     */
    public final Long getTransitionTime() {
        return this.transitionTime;
    }

    /**
     * @return the internalError
     */
    public final Boolean getInternalError() {
        return this.internalError;
    }

    /**
     * @return the rccommand
     */
    public final RCCOMMAND getRccommand() {
        return this.rccommand;
    }

    public String[] getRccommandArgs() {
        return this.rccommandArgs;
    }
    
    public String getRccommandUID() {
        return this.rccommandUID;
    }
    
    /**
     * @return the busy
     */
    public final Boolean getBusy() {
        return this.busy;
    }

    /**
     * @return the parentState
     */
    public final FSM getParentState() {
        return this.parentState;
    }

    /**
     * @return the parentIsTransitioning
     */
    public final Boolean getParentIsTransitioning() {
        return this.parentIsTransitioning;
    }

    public final Boolean getParentIsInitializing() {
        return this.parentIsInitializing;
    }
    
    /**
     * @return the parentCurrentTransitionCommand
     */
    public final TRCOMMAND getParentCurrentTransitionCommand() {
        return this.parentCurrentTransitionCommand;
    }

    public final TRCOMMAND getParentPreviousTransitionCommand() {
        return this.parentPreviousTransitionCommand;
    }
    
    /**
     * @return the parentIsExiting
     */
    public final Boolean getParentIsExiting() {
        return this.parentIsExiting;
    }

    public static RCApplicationInfo build(final RCStateInfoNamed rcInfo, final String name, final RCStateInfoNamed parent) throws InvalidEventException {
        try {
            final Builder b = new RCApplicationInfo.Builder(name).timestamp(rcInfo.getTime() != null
                                                                                             ? Long.valueOf(rcInfo.getTime().getTimeMicro() * 1000)
                                                                                             : Long.valueOf(System.currentTimeMillis() * 1000000))
                                                                 .eventTimestamp(Long.valueOf(System.currentTimeMillis()))
                                                                 .state(FSM.valueOf(rcInfo.state.toUpperCase()))
                                                                 .isTransitioning(Boolean.valueOf(rcInfo.transitioning))
                                                                 .currentTransitionCommand(rcInfo.currentTransitionName.isEmpty() ? TRCOMMAND._EMPTY 
                                                                                                                               : TRCOMMAND.valueOf(rcInfo.currentTransitionName))
                                                                 .currentTransitionCommandUID("")
                                                                 .previousTransitionCommand(rcInfo.lastTransitionName.isEmpty() ? TRCOMMAND._EMPTY
                                                                                                                                : TRCOMMAND.valueOf(rcInfo.lastTransitionName))
                                                                 .transitionTime(Long.valueOf((long) rcInfo.transitionTime))
                                                                 .internalError(Boolean.valueOf(rcInfo.fault))
                                                                 .rccommand(rcInfo.lastCmdName.isEmpty() ? RCCOMMAND._EMPTY
                                                                                                         : RCCOMMAND.valueOf(rcInfo.lastCmdName))
                                                                 .rccommandUID("")
                                                                 .rccommandArgs(rcInfo.lastCmdArgs)
                                                                 .busy(Boolean.valueOf(rcInfo.busy));
                        
            if(parent != null) {
                b.parentState(FSM.valueOf(parent.state.toUpperCase()))
                 .parentIsTransitioning(Boolean.valueOf(parent.transitioning))
                 .parentCurrentTransitionCommand(parent.currentTransitionName.isEmpty() ? TRCOMMAND._EMPTY 
                                                                                     : TRCOMMAND.valueOf(parent.currentTransitionName.toUpperCase()))
                 .parentPreviousTransitionCommand(parent.lastTransitionName.isEmpty() ? TRCOMMAND._EMPTY
                                                                                      : TRCOMMAND.valueOf(parent.lastTransitionName.toUpperCase()))
                 .parentIsExiting(Boolean.FALSE)
                 .parentIsInitializing(Boolean.valueOf(!parent.fullyInitialized));
            } else {
                b.parentState(FSM._EMPTY)
                 .parentIsTransitioning(Boolean.FALSE)
                 .parentCurrentTransitionCommand(TRCOMMAND._EMPTY)
                 .parentPreviousTransitionCommand(TRCOMMAND._EMPTY)
                 .parentIsExiting(Boolean.FALSE)
                 .parentIsInitializing(Boolean.FALSE);
            }
                                                        
            final RCApplicationInfo appinfo = b.build();
            return appinfo;
        }
        catch(final Exception e) { // TODO temporary catch to cover all possible problems on enum translation from RC to ES
            RCApplicationInfo.log.error("Error while building the RCApplicationInfo event " + e);
            throw new InvalidEventException("Error while building new RCApplicationInfo event ", e);
        }
    }
}
