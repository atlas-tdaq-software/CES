package chip.event.dynamic;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import chip.event.en.FSM;
import chip.event.en.MEMBERSHIP;
import chip.event.en.STATUS;
import chip.event.en.TRCOMMAND;
import daq.EsperUtils.CepException.InvalidEventException;


public class ApplicationInfo extends Info {
    /**
     * Logger
     */
    private static final Log log = LogFactory.getLog(ApplicationInfo.class.getName());

    // Host the application is running on
    private final String runningHost;
    // Application Status
    private final STATUS status;
    private final Boolean isNotResponding;
    private final MEMBERSHIP membership;
    private final Boolean isRestarting;

    private final Long exitCode;
    private final Long exitSignal;

    private final FSM parentState;
    private final Boolean parentIsTransitioning;
    private final TRCOMMAND parentCurrentTransitionCommand;
    private final TRCOMMAND parentPreviousTransitionCommand;
    private final Boolean parentIsExiting;
    private final Boolean parentIsInitializing;

    public ApplicationInfo(final String name,
                           final Long timestamp,
                           final Long eventTimestamp,
                           final MEMBERSHIP membership,
                           final String runningHost,
                           final STATUS status,
                           final Boolean notResponding,
                           final Boolean restarting,
                           final Long exitCode,
                           final Long exitSignal,
                           final FSM parentState,
                           final Boolean parentIsTransitioning,
                           final TRCOMMAND parentCurrentTransitionCommand,
                           final TRCOMMAND parentPreviousTransitionCommand,
                           final Boolean parentIsExiting,
                           final Boolean parentIsInitializing)
    {
        super(name, timestamp, eventTimestamp);
        this.membership = membership;
        this.runningHost = runningHost;
        this.status = status;
        this.isNotResponding = notResponding;
        this.isRestarting = restarting;
        this.exitCode = exitCode;
        this.exitSignal = exitSignal;
        this.parentState = parentState;
        this.parentIsTransitioning = parentIsTransitioning;
        this.parentCurrentTransitionCommand = parentCurrentTransitionCommand;
        this.parentPreviousTransitionCommand = parentPreviousTransitionCommand;
        this.parentIsExiting = parentIsExiting;
        this.parentIsInitializing = parentIsInitializing;
    }

    public static class Builder {
        private final String applictionName;
        private Long timestamp;
        private Long eventTimestamp;
        private String runningHost;
        private STATUS status;
        private Boolean isNotResponding;
        private MEMBERSHIP membership;
        private Boolean isRestarting;
        private Long exitCode;
        private Long exitSignal;
        private FSM parentState;
        private Boolean parentIsTransitioning;
        private TRCOMMAND parentCurrentTransitionCommand;
        private TRCOMMAND parentPreviousTransitionCommand;
        private Boolean parentIsExiting;
        private Boolean parentIsInitializing;

        public Builder(final String applicationName) {
            this.applictionName = applicationName;
        }

        public Builder timestamp(final Long time_stamp) {
            this.timestamp = time_stamp;
            return this;
        }

        public Builder eventTimestamp(final Long event_timestamp) {
            this.eventTimestamp = event_timestamp;
            return this;
        }

        public Builder runningHost(final String running_host) {
            this.runningHost = running_host;
            return this;
        }

        public Builder status(final STATUS status_) {
            this.status = status_;
            return this;
        }

        public Builder isNotResponding(final Boolean is_NotResponding) {
            this.isNotResponding = is_NotResponding;
            return this;
        }

        public Builder isRestarting(final Boolean is_Restarting) {
            this.isRestarting = is_Restarting;
            return this;
        }

        public Builder membership(final MEMBERSHIP m) {
            this.membership = m;
            return this;
        }

        public Builder exitCode(final Long exit_Code) {
            this.exitCode = exit_Code;
            return this;
        }

        public Builder exitSignal(final Long exit_Signal) {
            this.exitSignal = exit_Signal;
            return this;
        }

        public Builder parentState(final FSM parent_State) {
            this.parentState = parent_State;
            return this;
        }

        public Builder parentIsTransitioning(final Boolean parent_IsTransitioning) {
            this.parentIsTransitioning = parent_IsTransitioning;
            return this;
        }

        public Builder parentCurrentTransitionCommand(final TRCOMMAND parent_CurrentTransitionCommand) {
            this.parentCurrentTransitionCommand = parent_CurrentTransitionCommand;
            return this;
        }

        public Builder parentPreviousTransitionCommand(final TRCOMMAND parent_PreviousTransitionCommand) {
            this.parentPreviousTransitionCommand = parent_PreviousTransitionCommand;
            return this;
        }
        
        public Builder parentIsExiting(final Boolean parent_IsExiting) {
            this.parentIsExiting = parent_IsExiting;
            return this;
        }

        public Builder parentIsInitializing(final Boolean parent_IsInitializing) {
            this.parentIsInitializing = parent_IsInitializing;
            return this;
        }
        
        public ApplicationInfo build() {
            return new ApplicationInfo(this);
        }
    }

    public ApplicationInfo(final Builder b) {
        super(b.applictionName, b.timestamp, b.eventTimestamp);
        this.membership = b.membership;
        this.runningHost = b.runningHost;
        this.status = b.status;
        this.isNotResponding = b.isNotResponding;
        this.isRestarting = b.isRestarting;
        this.exitCode = b.exitCode;
        this.exitSignal = b.exitSignal;
        this.parentState = b.parentState;
        this.parentIsTransitioning = b.parentIsTransitioning;
        this.parentCurrentTransitionCommand = b.parentCurrentTransitionCommand;
        this.parentPreviousTransitionCommand = b.parentPreviousTransitionCommand;
        this.parentIsExiting = b.parentIsExiting;
        this.parentIsInitializing = b.parentIsInitializing;
    }

    public static ApplicationInfo build(final rc.DAQApplicationInfoNamed info, final rc.RCStateInfoNamed parent) throws InvalidEventException {
        try {
            final Builder b = new ApplicationInfo.Builder(info.applicationName)
                                                 .timestamp(info.getTime() != null ? Long.valueOf(info.getTime().getTimeMicro() * 1000)
                                                                                   : Long.valueOf(System.currentTimeMillis() * 1000000)) 
                                                 .eventTimestamp(Long.valueOf(System.currentTimeMillis()))
                                                 .membership(info.membership ? MEMBERSHIP._IN : MEMBERSHIP._OUT)
                                                 .runningHost(info.host)
                                                 .status(STATUS.valueOf(info.status))
                                                 .isNotResponding(Boolean.valueOf(info.notResponding))
                                                 .isRestarting(Boolean.valueOf(info.restarting))
                                                 .exitCode(Long.valueOf(info.exitCode))
                                                 .exitSignal(Long.valueOf(info.exitSignal));
            
            if(parent != null) {
                b.parentState(FSM.valueOf(parent.state.toUpperCase()))
                 .parentIsTransitioning(Boolean.valueOf(parent.transitioning))
                 .parentCurrentTransitionCommand(parent.currentTransitionName.isEmpty() ? TRCOMMAND._EMPTY : TRCOMMAND.valueOf(parent.currentTransitionName.toUpperCase()))
                 .parentPreviousTransitionCommand(parent.lastTransitionName.isEmpty() ? TRCOMMAND._EMPTY : TRCOMMAND.valueOf(parent.lastTransitionName.toUpperCase()))
                 .parentIsExiting(Boolean.FALSE)
                 .parentIsInitializing(Boolean.valueOf(!parent.fullyInitialized));
            } else {
                b.parentState(FSM._EMPTY)
                 .parentIsTransitioning(Boolean.FALSE)
                 .parentCurrentTransitionCommand(TRCOMMAND._EMPTY)
                 .parentPreviousTransitionCommand(TRCOMMAND._EMPTY)
                 .parentIsExiting(Boolean.FALSE)
                 .parentIsInitializing(Boolean.FALSE);
            }

            final ApplicationInfo appinfo = b.build(); 
            return appinfo;            
        }
        catch(final IllegalArgumentException e) {
            ApplicationInfo.log.error("Unable to map enum type for " + e);
            throw new InvalidEventException("Unable to map enum type", e);
        }
        catch(final NullPointerException e) {
            ApplicationInfo.log.error("Null updated received ");
            throw new InvalidEventException("Null update received", e);
        }
        catch(final Exception e) {
            ApplicationInfo.log.error("Ops: " + e);
            e.printStackTrace();
            throw new InvalidEventException("Impossible just happened", e);
        }
    }

    /**
     * @return the runningHost
     */
    public final String getRunningHost() {
        return this.runningHost;
    }

    /**
     * @return the status
     */
    public final STATUS getStatus() {
        return this.status;
    }

    /**
     * @return the notResponding
     */
    public final Boolean getNotResponding() {
        return this.isNotResponding;
    }

    /**
     * @return the membership
     */
    public final MEMBERSHIP getMembership() {
        return this.membership;
    }

    /**
     * @return the restarting
     */
    public final Boolean getRestarting() {
        return this.isRestarting;
    }

    /**
     * @return the isNotResponding
     */
    public Boolean getIsNotResponding() {
        return this.isNotResponding;
    }

    /**
     * @return the isRestarting
     */
    public Boolean getIsRestarting() {
        return this.isRestarting;
    }

    /**
     * @return the exitCode
     */
    public Long getExitCode() {
        return this.exitCode;
    }

    /**
     * @return the exitSignal
     */
    public Long getExitSignal() {
        return this.exitSignal;
    }

    /**
     * @return the parentState
     */
    public final FSM getParentState() {
        return this.parentState;
    }

    /**
     * @return the parentIsTransitioning
     */
    public final Boolean getParentIsTransitioning() {
        return this.parentIsTransitioning;
    }

    /**
     * @return the parentCurrentTransitionCommand
     */
    public final TRCOMMAND getParentCurrentTransitionCommand() {
        return this.parentCurrentTransitionCommand;
    }

    public final TRCOMMAND getParentPreviousTransitionCommand() {
        return this.parentPreviousTransitionCommand;
    }
    
    /**
     * @return the parentIsExiting
     */
    public final Boolean getParentIsExiting() {
        return this.parentIsExiting;
    }

    public final Boolean getParentIsInitializing() {
        return this.parentIsInitializing;
    }
}
