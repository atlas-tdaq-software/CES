package chip.event.dynamic;

public class TransitionInfo {
    private final String transition;
    private final String state;

    public TransitionInfo(final String transition, final String state) {
        this.transition = transition;
        this.state = state;
    }

    public String getState() {
        return this.state;
    }

    public String getTransition() {
        return this.transition;
    }
}
