package chip.event.dynamic;

public class LHC {
    public static enum TYPE {
        STABLE_BEAMS,
        ADJUST_HANDSHAKE,
        STABLE_BEAMS_DUMPED
    }
    
    private final TYPE type;
    private final int value;
        
    public LHC(final TYPE type, final int value) {
        this.type = type;
        this.value = value;
    }
    
    public TYPE getType() {
        return this.type;
    }
    
    public int getValue() {
        return this.value;
    }
}
