package chip.event.dynamic;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import chip.msg.Common;
import es.rc.TestResult;


public class CompTestResultInfo {
    public final static String REBOOT_ACTION_HOST = "Host";

    private final static String EXEC_ACTION_PROGRAM_UID = "program_uid";
    private final static String EXEC_ACTION_HOST = "host";
    private final static String EXEC_ACTION_PARAMS = "parameters";
    private final static String EXEC_ACTION_INIT_TIMEOUT = "initTimeout";

    private final static ThreadLocal<JsonParser> jsonParser = ThreadLocal.withInitial(() -> new JsonParser());
    private final AtomicReference<JsonElement> jsonElement = new AtomicReference<>();

    public enum ACTION_TYPE {
        NONE,
        REBOOT,
        EXEC,
        RESTART,
        DISABLE;

        private static final ConcurrentHashMap<String, ACTION_TYPE> covertionMap = new ConcurrentHashMap<>();
        
        static {
            ACTION_TYPE.covertionMap.put(TM.TestFailureAction.Action.Execute.toString(), ACTION_TYPE.EXEC);
            ACTION_TYPE.covertionMap.put(TM.TestFailureAction.Action.Reboot.toString(), ACTION_TYPE.REBOOT);
            ACTION_TYPE.covertionMap.put(TM.TestFailureAction.Action.Restart.toString(), ACTION_TYPE.RESTART);
            ACTION_TYPE.covertionMap.put(TM.TestFailureAction.Action.Ignore.toString(), ACTION_TYPE.DISABLE);            
            ACTION_TYPE.covertionMap.put(TM.TestFailureAction.Action.Test.toString(), ACTION_TYPE.NONE);
            
            // Check to be sure that all the actions are covered
            for(final TM.TestFailureAction.Action a : TM.TestFailureAction.Action.values()) {
                final String actionName = a.toString();
                
                if(ACTION_TYPE.covertionMap.containsKey(actionName) == false) {
                    ers.Logger.warning(new Common("Test action \"" + actionName  + "\" is not properly handled"));
                }
            }
        }
        
        static public ACTION_TYPE convert(final String tmAction) {
            final ACTION_TYPE act = ACTION_TYPE.covertionMap.get(tmAction);
            if(act != null) {
                if(act.equals(ACTION_TYPE.NONE) == true) {
                    ers.Logger.warning(new Common("Test action \"" + tmAction + "\" is not supported"));
                }
                
                return act;
            }
            
            throw new IllegalArgumentException("\"" + tmAction + "\" is not a valid test failure action");            
        }
    }

    private final String name;
    private final Long eventTimestamp;
    private final TestResult testResult;
    private final ACTION_TYPE actionType;
    private final String paramaters;
    private final String diagnosis;
    private final int timeout;

    private final StringBuffer rebootHost = new StringBuffer();

    private final StringBuffer execHost = new StringBuffer();
    private final StringBuffer execProgramUID = new StringBuffer();
    private final StringBuffer execParams = new StringBuffer();
    private final AtomicInteger execInitTimeout = new AtomicInteger(0);

    public CompTestResultInfo(final String name,
                              final Long eventTimestamp,
                              final TestResult testResult,
                              final ACTION_TYPE actionType,
                              final String paramaters,
                              final String diagnosis,
                              final int timeout)
        throws IllegalArgumentException
    {
        this.name = name;
        this.eventTimestamp = eventTimestamp;
        this.testResult = testResult;
        this.actionType = actionType;
        this.paramaters = paramaters;
        this.diagnosis = diagnosis;
        this.timeout = timeout;

        if((actionType.equals(ACTION_TYPE.NONE) == false) && (this.paramaters.isEmpty() == false)) {
            try {
                this.jsonElement.set(CompTestResultInfo.jsonParser.get().parse(this.paramaters));
                this.checkDynamicProperties();
            }
            catch(final JsonSyntaxException | NullPointerException | ClassCastException | IllegalStateException ex) {
                throw new IllegalArgumentException("Action parameters are not properly formed: " + ex, ex);
            }
        }
    }

    public ACTION_TYPE getActionType() {
        return this.actionType;
    }

    public String getName() {
        return this.name;
    }

    public Long getEventTimestamp() {
        return this.eventTimestamp;
    }

    public TestResult getTestResult() {
        return this.testResult;
    }

    public String getParameters() {
        return this.paramaters;
    }

    public String getDiagnosis() {
        return this.diagnosis;
    }

    public int getTimeout() {
        return this.timeout;
    }

    public String getRebootHost() {
        return this.rebootHost.toString();
    }

    public String getExecHost() {
        return this.execHost.toString();
    }
    
    public String getExecProgramUID() {
        return this.execProgramUID.toString();
    }
    
    public String getExecParams() {
        return this.execParams.toString();
    }
    
    public int getExecInitTimeout() {
        return this.execInitTimeout.get();
    }
    
    // Add here a check on all the dynamic properties that are used
    // The check is done in the constructor (that throws in case of problems)
    // In this way we are sure that "getDynamicProperty" will not throw when invoked inside the ESPER engine
    private void checkDynamicProperties() throws NullPointerException, ClassCastException, IllegalStateException {
        switch(this.actionType) {
            case REBOOT: {
                this.rebootHost.append(this.jsonElement.get().getAsJsonObject().get(REBOOT_ACTION_HOST).getAsString());
            }
                break;
            case EXEC: {
                final JsonObject jsonObject = this.jsonElement.get().getAsJsonObject();

                this.execProgramUID.append(jsonObject.get(EXEC_ACTION_PROGRAM_UID).getAsString());
                this.execHost.append(jsonObject.get(EXEC_ACTION_HOST).getAsString());
                this.execInitTimeout.set(jsonObject.get(EXEC_ACTION_INIT_TIMEOUT).getAsInt());
                this.execParams.append(jsonObject.get(EXEC_ACTION_PARAMS).getAsString());
            }
                break;
            default:
        }
    }
}
