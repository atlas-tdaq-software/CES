package chip.event.dynamic;

public class Info {
    private final String name;
    private final Long infoTimestamp;
    private final Long eventTimestamp;
    private final Long timestampInMilliSecond;

    /**
     * @param name
     * @param timestamp in Nanoseconds
     */
    public Info(final String name, final Long infoTimestamp, final Long eventTimestamp) {
        super();
        this.name = name;
        // Nanosecond
        this.infoTimestamp = infoTimestamp;
        // Millisecond
        this.timestampInMilliSecond = Long.valueOf(infoTimestamp.longValue() / 1000000);
        this.eventTimestamp = eventTimestamp;
    }

    public String getName() {
        return this.name;
    }

    public Long getTimestamp() {
        return this.infoTimestamp;
    }

    public Long getTimestampInMilliSecond() {
        return this.timestampInMilliSecond;
    }

    public Long getEventTimestamp() {
        return this.eventTimestamp;
    }
}
