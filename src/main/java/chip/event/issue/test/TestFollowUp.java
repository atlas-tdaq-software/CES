package chip.event.issue.test;

import chip.event.dynamic.CompTestResultInfo;
import chip.event.issue.Issue;
import es.rc.TestResult;


public class TestFollowUp extends Issue {

    public static enum TYPE {
        TEST
    }

    public static enum STATUS {
        _NEW,
        EXECUTING,
        EXECUTED_OK,
        EXECUTED_FAILED,
        CHECK_IF_DISABLE,
        CHECK_IF_RESTART,
        DISABLING,
        DISABLED_OK,
        DISABLED_FAILED,
        TESTING,
        TESTED_OK,
        TESTED_FAILED,
        PROBLEM,
        FAILED,
        DONE
    }

    public static enum ACTION {
        NONE,
        EXECUTE_PROGRAM,
        NOTIFY_EXECUTE_FAILED,
        TEST,
        TEST_ALL_CHILDREN,
        RESTART_APP,
        NOTIFY_CANNOT_RESTART,
        DISABLE_APP,
        DISABLE_HOST,
        NOTIFY_DISABLE_OK,
        NOTIFY_DISABLE_FAILED
    }

    private final String applicationName;
    private final String controller;
    private final String runningHost;
    private final String component;
    private final STATUS status;
    private final ACTION action;

    private final TestResult globalTestResult;
    private final CompTestResultInfo compTestResult;

    private final int actionCounter;

    public TestFollowUp(final String applicationName,
                        final String controller,
                        final String runningHost,
                        final TestResult globalTestResult,
                        final String component,
                        final CompTestResultInfo compTestResult,
                        final STATUS status,
                        final ACTION action,
                        final int actionCounter)
    {
        super(ISSUETYPE.TEST, "Test", TYPE.TEST, status, action);
        this.applicationName = applicationName;
        this.controller = controller;
        this.runningHost = runningHost;
        this.globalTestResult = globalTestResult;
        this.component = component;
        this.compTestResult = compTestResult;
        this.status = status;
        this.action = action;
        this.actionCounter = actionCounter;
    }

    public TYPE getType() {
        return TYPE.TEST;
    }
    
    public String getApplicationName() {
        return this.applicationName;
    }

    public String getController() {
        return this.controller;
    }

    public String getRunningHost() {
        return this.runningHost;
    }

    public TestResult getGlobalTestResult() {
        return this.globalTestResult;
    }

    public String getComponent() {
        return this.component;
    }

    public CompTestResultInfo getCompTestResult() {
        return this.compTestResult;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public ACTION getAction() {
        return this.action;
    }

    public int getActionCounter() {
        return this.actionCounter;
    }

}
