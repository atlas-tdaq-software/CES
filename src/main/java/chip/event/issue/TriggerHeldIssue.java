package chip.event.issue;

public abstract class TriggerHeldIssue extends IdentifiableIssue implements HoldsTrigger {
    public TriggerHeldIssue(final ISSUETYPE issueType, final String name, final Enum<?> enumType, final Enum<?> enumStatus, final Enum<?> enumAction, final String id) {
        super(issueType, name, enumType, enumStatus, enumAction, id);
    }
}
