package chip.event.issue;

public interface Identifiable {
    public String getId();
}
