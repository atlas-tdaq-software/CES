package chip.event.issue;

import java.util.Iterator;

import chip.DAQConfigReader;
import daq.eformat.DetectorMask;
import daq.eformat.DetectorMask.SUBDETECTOR;
import rc.HoldTriggerInfoNamed.CausedBy;
import rc.HoldTriggerInfoNamed.Reason;


public interface HoldsTrigger {
    default public boolean isTriggerHeld() {
        return true;
    }

    public CausedBy getCausedBy(final DAQConfigReader config);

    public Reason getReason();

    static CausedBy getCausedBy(final DAQConfigReader conf, final String appName) {
        final DetectorMask dmask = conf.getDetMaskForApplication(appName);
        return HoldsTrigger.getCausedBy(dmask);
    }

    static CausedBy getCausedBy(final DetectorMask dmask) {
        final CausedBy detIdEnum;

        final Iterator<SUBDETECTOR> sdi = dmask.getSubdetectors().iterator();
        if(sdi.hasNext() == true) {
            detIdEnum = chip.utils.DetectorID.toDetectorCausedByEnum(sdi.next().id());
        } else {
            detIdEnum = CausedBy.Other;
        }

        return detIdEnum;
    }

}
