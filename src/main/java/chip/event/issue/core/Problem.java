package chip.event.issue.core;

import chip.event.issue.IdentifiableIssue;


public class Problem extends IdentifiableIssue {
    // TODO: if you update this list, then look at CHIP.configure (where the status of apps is reconstructed from IS)
    public static enum TYPE {
        APPLICATION_DEAD,
        CHILD_INCONSISTENT_STATE,
        APPLICATION_ERROR,
        APPLICATION_FAILED,
        ERS_FATAL,
        BAD_HOST,
        HLTSV_DEAD,
        RESTART_HISTORY_ELASPED
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        WAIT_FOR_RESOLVED,
        RESOLVED,
        DONE,
        NONE
    }

    public static enum ACTION {
        NONE,
        RESTART,
        SET_ERROR,
        REMOVE_ERROR,
        IGNORE,
        INCLUDE
    }

    private final String controller;
    private final String application;
    private final TYPE type;
    private final STATUS status;
    private final ACTION action;

    public Problem(final String controller,
                   final String application,
                   final TYPE type,
                   final STATUS status,
                   final ACTION action,
                   final String id)
    {
        super(ISSUETYPE.CORE, "Core", type, status, action, id);
        this.controller = controller;
        this.application = application;
        this.type = type;
        this.status = status;
        this.action = action;
    }

    public String getController() {
        return this.controller;
    }

    public String getApplication() {
        return this.application;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public ACTION getAction() {
        return this.action;
    }

    public static Problem makeProblem(final String controller,
                                      final String application,
                                      final TYPE type,
                                      final STATUS status,
                                      final ACTION action,
                                      final String id)
    {
        return new Problem(controller, application, type, status, action, id);
    }

    @Override
    public String toString() {
        return "{ controller: " + this.controller + " application: " + this.application + " type: " + this.type.name() + " status: "
               + this.status.name() + " action: " + this.action.name() + " id: " + this.getId() + " }";
    }

    public static String getProblemDescription(final TYPE type) {
        String desc = "";
        switch(type) {
            case APPLICATION_DEAD:
                desc = "app dead or absent";
                break;
            case CHILD_INCONSISTENT_STATE:
                desc = "app in wrong state";
                break;
            case APPLICATION_ERROR:
                desc = "app in error";
                break;
            case APPLICATION_FAILED:
                desc = "app failed to start";
                break;
            case ERS_FATAL:
                desc = "app threw ERS fatal";
                break;
            case BAD_HOST:
                desc = "app on bad host";
                break;
            case HLTSV_DEAD:
                desc = "HLTSV dead or absent";
                break;
            case RESTART_HISTORY_ELASPED:
                desc = "restart app after elapsed timeout";
                break;
            default:
                desc = "description not available";
        }
        return desc;
    }
}
