package chip.event.issue;

import java.util.UUID;


public class IdentifiableIssue extends Issue implements Identifiable {
    private final String id;

    public IdentifiableIssue(final ISSUETYPE issueType, final String name, final Enum<?> enumType, final Enum<?> enumStatus, final Enum<?> enumAction, final String id) {
        super(issueType, name, enumType, enumStatus, enumAction);
        this.id = id;
    }

    @Override
    public String getId() {
        return this.id;
    }

    // ESPER caches the result of user functions taking no arguments
    // That means the randomUUID function cannot be used directly
    public static String generateUUID(final String dummy) {
        return UUID.randomUUID().toString();
    }
}
