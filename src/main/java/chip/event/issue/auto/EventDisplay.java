package chip.event.issue.auto;

import chip.event.issue.Issue;


public class EventDisplay extends Issue {
    public final ISSUETYPE issueType = ISSUETYPE.AUTO_PROCEDURE;

    public static enum TYPE {
        RESTART_APPS_AT_SOR
    }

    public static enum STATUS {
        _NEW,
        FAILED,
        DONE
    }

    private final TYPE type;
    private final STATUS status;

    public EventDisplay(final TYPE type, final STATUS status) {
        super(ISSUETYPE.AUTO_PROCEDURE, "EventDisplay", type, status, type);
        this.type = type;
        this.status = status;
    }

    /**
     * @return the type
     */
    public TYPE getType() {
        return this.type;
    }

    /**
     * @return the status
     */
    public STATUS getStatus() {
        return this.status;
    }
}
