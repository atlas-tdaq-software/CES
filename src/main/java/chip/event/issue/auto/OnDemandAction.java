package chip.event.issue.auto;

import java.util.ArrayList;
import java.util.List;

import chip.event.issue.Issue;


public class OnDemandAction extends Issue {
    public final ISSUETYPE issueType = ISSUETYPE.AUTO_PROCEDURE;

    public static enum TYPE {
        ERS_COMMAND,
        IS_COMMAND
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }

    public static enum ACTION {
        START_APPS,
        RESTART_APPS,
        KILL_APPS,
        UNKNOWN
    }

    private final TYPE type;
    private final STATUS status;
    private final ACTION action;
    private final String sender;
    private final String command;
    private final String arguments;

    public OnDemandAction(final TYPE type,
                          final STATUS status,
                          final ACTION action,
                          final String sender,
                          final String command,
                          final String arguments)
    {
        super(ISSUETYPE.AUTO_PROCEDURE, "OnDemandAction", type, status, action);

        this.type = type;
        this.status = status;
        this.action = action;
        this.sender = sender;
        this.command = command;
        this.arguments = arguments;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public ACTION getAction() {
        return this.action;
    }

    public String getSender() {
        return this.sender;
    }

    public String getCommand() {
        return this.command;
    }

    public String getArguments() {
        return this.arguments;
    }

    @Override
    public List<String> additionalQualifiers() {
        final List<String> l = new ArrayList<>();

        l.add(this.sender);
        l.add(this.command);
        l.add(this.arguments);
        l.add(this.type.toString());
        l.add(this.action.toString());

        return l;
    }
}
