package chip.event.issue.auto;

import java.util.ArrayList;
import java.util.List;

import chip.DAQConfigReader;
import chip.event.en.FSM;
import chip.event.issue.TriggerHeldIssue;
import rc.HoldTriggerInfoNamed.CausedBy;
import rc.HoldTriggerInfoNamed.Reason;


public class ClockSwitch extends TriggerHeldIssue {
    public static enum TYPE {
        SELECT_INTERNAL,
        SELECT_LHC,
        SELECT_INTERNAL_AND_DO_UNCONFIG
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }

    private final TYPE type;
    private final STATUS status;
    private final String beamMode;
    private final FSM state;

    public ClockSwitch(final TYPE type, final STATUS status, final String beamMode, final FSM state, final String id) {
        super(ISSUETYPE.AUTO_PROCEDURE, "ClockSwitch", type, status, type, id);
        this.type = type;
        this.status = status;
        this.beamMode = beamMode;
        this.state = state;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public String getBeamMode() {
        return this.beamMode;
    }

    public FSM getState() {
        return this.state;
    }

    @Override
    public List<String> additionalQualifiers() {
        final List<String> l = new ArrayList<>();

        l.add(this.beamMode.toString());
        l.add(this.state.toString());

        return l;
    }

    @Override
    public boolean isTriggerHeld() {
        return((this.type.equals(TYPE.SELECT_INTERNAL) || this.type.equals(TYPE.SELECT_LHC)) && this.state.equals(FSM.RUNNING));
    }

    @Override
    public CausedBy getCausedBy(final DAQConfigReader conf) {
        return CausedBy.Other;
    }

    @Override
    public Reason getReason() {
        return Reason.ClockSwitch;
    }
}
