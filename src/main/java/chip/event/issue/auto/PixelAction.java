package chip.event.issue.auto;

import java.util.ArrayList;
import java.util.List;

import chip.DAQConfigReader;
import chip.event.en.FSM;
import chip.event.issue.TriggerHeldIssue;
import rc.HoldTriggerInfoNamed.CausedBy;
import rc.HoldTriggerInfoNamed.Reason;


public class PixelAction extends TriggerHeldIssue {
    public static enum TYPE {
        PIXEL_UP,
        PIXEL_DOWN
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }

    private final TYPE type;
    private final STATUS status;
    private final String controller;
    private final FSM state;

    public PixelAction(final TYPE type, final STATUS status, final String controller, final FSM state, final String id) {
        super(ISSUETYPE.AUTO_PROCEDURE, "PixelAction", type, status, type, id);

        this.type = type;
        this.status = status;
        this.controller = controller;
        this.state = state;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public String getController() {
        return this.controller;
    }

    public FSM getState() {
        return this.state;
    }
    
    @Override
    public List<String> additionalQualifiers() {
        final List<String> l = new ArrayList<>();
        
        l.add(this.controller);
        
        return l;
    }

    @Override
    public CausedBy getCausedBy(final DAQConfigReader config) {
        return CausedBy.PIX;
    }

    @Override
    public Reason getReason() {
        return Reason.CustomRecovery;
    }

    @Override
    public boolean isTriggerHeld() {
        return this.state.equals(FSM.RUNNING);
    }
}
