package chip.event.issue.auto;

import chip.event.issue.Issue;


public class EventEnableCHIPMetricsIs extends Issue {
    public static enum TYPE {
        ENABLE,
        DISABLE
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }

    private final TYPE type;
    private final STATUS status;
    private final long interval;

    public EventEnableCHIPMetricsIs(final TYPE type, final STATUS status, final long interval) {
        super(ISSUETYPE.AUTO_PROCEDURE, "IS_METRICS", type, status, type);

        this.type = type;
        this.status = status;
        this.interval = interval;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public long getInterval() {
        return this.interval;
    }
}
