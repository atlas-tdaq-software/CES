package chip.event.issue.auto;

import chip.event.issue.Issue;

public class BCM extends Issue {
    private final TYPE type;
    private final STATUS status;

    public static enum TYPE {
        BCID_CHECK_ENABLE,
        BCID_CHECK_DISABLE
    }
    
    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }
    
    public BCM(final TYPE type, final STATUS status) {
        super(ISSUETYPE.AUTO_PROCEDURE, "BCM", type, status, type);
        
        this.type = type;
        this.status = status;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }
}
