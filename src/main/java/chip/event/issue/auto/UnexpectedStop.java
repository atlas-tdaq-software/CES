package chip.event.issue.auto;

import chip.event.issue.Issue;


public class UnexpectedStop extends Issue {

    public static enum TYPE {
        UNEXPECTED_STOP
    }

    public static enum STATUS {
        _NEW,
        FAILED,
        DONE
    }

    private final TYPE type;
    private final STATUS status;
    private final String beamMode;

    public UnexpectedStop(final TYPE type, final STATUS status, final String beamMode) {
        super(ISSUETYPE.AUTO_PROCEDURE, "Unexpected Stop", type, status, type);
        this.type = type;
        this.status = status;
        this.beamMode = beamMode;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public String getBeamMode() {
        return this.beamMode;
    }

}
