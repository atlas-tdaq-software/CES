package chip.event.issue.auto;

import chip.event.issue.Issue;

public class AutoPilot extends Issue {
    public static enum TYPE {
        GO_TO_RUNNING,
        GO_TO_NONE,
        RESOLVE_TRANSITION_TIMEOUT,
        RESOLVE_PROPAGATION_ERROR,
        DISABLE_AUTOPILOT
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }

    public static enum ACTION {
        NONE,
        _START
    }
    
    private final TYPE type;
    private final STATUS status;
    private final ACTION action;
    private final String controller;
    private final String application;
    
    public AutoPilot(final TYPE type, final STATUS status, final ACTION action, final String controllerName, final String applicationName) {
        super(ISSUETYPE.AUTO_PROCEDURE, "AutoPilot", type, status, action); 
    
        this.type = type;
        this.status = status;
        this.action = action;
        this.controller = controllerName;
        this.application = applicationName;
    } 
    
    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }
    
    public ACTION getAction() {
        return this.action;
    }
    
    public String getController() {
        return this.controller;
    }
    
    public String getApplication() {
        return this.application;
    }
}
