package chip.event.issue.auto;

import chip.event.en.FSM;
import chip.event.issue.Issue;


public class WarmStart extends Issue {
    public static enum TYPE {
        WARM_START,
        WARM_STOP
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }

    private final TYPE type;
    private final STATUS status;

    private final FSM state;

    public WarmStart(final TYPE type,
                     final STATUS status,
                     final FSM state)
    {
        super(ISSUETYPE.AUTO_PROCEDURE, "Warm Start/Stop", type, status, type);
        
        this.type = type;
        this.status = status;
        this.state = state;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public FSM getState() {
        return this.state;
    }
}
