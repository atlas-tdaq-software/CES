package chip.event.issue.meta;

import chip.event.issue.Issue;


public class SynchAction extends Issue {
    public static enum TYPE {
        AT_TRANSITION_DONE
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        DONE,
        FAILED
    }

    public static enum ACTION {
        RESET_RESOURCES_AFTER_STOP,
        SET_AUTOMATIC_CLOCK,
        RESET_R4P,
        NONE
    }

    private final String transition;
    private final String state;
    private final TYPE type;
    private final STATUS status;
    private final ACTION action;

    public SynchAction(final String transition, final String state, TYPE type, STATUS status, ACTION action) {
        super(ISSUETYPE.META, "SynchExecution", type, status, action);

        this.transition = transition;
        this.state = state;
        this.type = type;
        this.status = status;
        this.action = action;
    }

    public String getTransition() {
        return this.transition;
    }
    
    public String getState() {
        return this.state;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public ACTION getAction() {
        return this.action;
    }
}
