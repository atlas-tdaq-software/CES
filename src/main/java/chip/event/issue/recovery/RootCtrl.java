package chip.event.issue.recovery;

import java.util.AbstractMap.SimpleEntry;

import chip.event.issue.Issue;


public class RootCtrl extends Issue implements RecoveryEvent {
    public static enum TYPE {
        NOT_HEALTHY,
        HOST_PROBLEM,
        CRASH
    }

    public static enum STATUS {
        CHECK_NEW,
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }

    public static enum ACTION {
        NONE,
        RESTART,
        REJECT
    }

    private final String application;
    private final String host;
    private final TYPE type;
    private final STATUS status;
    private final ACTION action;

    public RootCtrl(final String application, final String host, final TYPE type, final STATUS status, final ACTION action) {
        super(ISSUETYPE.RECOVERY, "RootCtrl", type, status, action);
        this.application = application;
        this.host = host;
        this.type = type;
        this.status = status;
        this.action = action;
    }

    public String getApplication() {
        return this.application;
    }

    public String getHost() {
        return this.host;
    }
    
    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public ACTION getAction() {
        return this.action;
    }

    @Override
    public String getConcernedComponent() {
        return this.application;
    }

    @Override
    public SimpleEntry<String, Boolean> getDetector() {
        return new SimpleEntry<String, Boolean>(daq.eformat.DetectorMask.SUBDETECTOR.OTHER.toString(), Boolean.TRUE);
    }
}
