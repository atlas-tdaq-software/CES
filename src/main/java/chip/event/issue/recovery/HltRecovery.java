package chip.event.issue.recovery;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

import chip.event.issue.Issue;


public class HltRecovery extends Issue implements RecoveryEvent {
    public static enum TYPE {
        DCM,
        HLTPU,
        HLTRC
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        STOP_IN_PROGRESS,
        PREPARE_START,
        START_IN_PROGRESS,
        DONE,
        ON_HOLD,
        REJECTED
    }

    public static enum ACTION {
        NONE,
        STOP,
        _START,
        ENABLE,
        DISABLE_ON_FAILURE;
    }

    private final String dcm;
    private final String hltpu;
    private final String hltrc;
    private final String controller;
    private final String runningHost;
    private final String deadapplication;
    private final TYPE type;
    private final STATUS status;
    private final ACTION action;

    /*
     * Ctor
     */
    public HltRecovery(final String dcm,
                       final String hltpu,
                       final String hltrc,
                       final String deadapplication,
                       final String controller,
                       final String runningHost,
                       final TYPE type,
                       final STATUS status,
                       final ACTION action)
    {
        super(ISSUETYPE.RECOVERY, "Hlt", type, status, action);
        this.dcm = dcm;
        this.hltpu = hltpu;
        this.hltrc = hltrc;
        this.controller = controller;
        this.runningHost = runningHost;
        this.deadapplication = deadapplication;
        this.type = type;
        this.status = status;
        this.action = action;
    }

    /**
     * @return the dcm
     */
    public String getDcm() {
        return this.dcm;
    }

    /**
     * @return the hLTPU
     */
    public String getHltpu() {
        return this.hltpu;
    }

    /**
     * @return the hLTRC
     */
    public String getHltrc() {
        return this.hltrc;
    }

    /**
     * @return the controller name
     */
    public String getController() {
        return this.controller;
    }

    /**
     * @return the running host
     */
    public String getRunningHost() {
        return this.runningHost;
    }

    /**
     * @return the deadapplication
     */
    public String getDeadapplication() {
        return this.deadapplication;
    }

    /**
     * @return the type
     */
    public TYPE getType() {
        return this.type;
    }

    /**
     * @return the status
     */
    public STATUS getStatus() {
        return this.status;
    }

    public ACTION getAction() {
        return this.action;
    }

    @Override
    public String getConcernedComponent() {
        return this.deadapplication;
    }
    
    @Override
    public List<String> additionalQualifiers() {
        final List<String> l = new ArrayList<>();
        l.add(this.controller);
        l.add(this.hltrc);
        l.add(this.hltpu);
        l.add(this.dcm);
        l.add(this.runningHost);
        l.add(this.action.toString());
        return l;
    }

    @Override
    public SimpleEntry<String, Boolean> getDetector() {
        return new SimpleEntry<String, Boolean>(daq.eformat.DetectorMask.SUBDETECTOR.TDAQ_HLT.toString(), Boolean.TRUE);
    }
}
