package chip.event.issue.recovery;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

import chip.DAQConfigReader;
import chip.event.issue.HoldsTrigger;
import chip.event.issue.TriggerHeldIssue;
import rc.HoldTriggerInfoNamed.CausedBy;
import rc.HoldTriggerInfoNamed.Reason;


public class TTCRestart extends TriggerHeldIssue implements RecoveryEvent {
    public static enum TYPE {
        TTC_RESTART
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }

    private final String name;
    private final String controller;
    private final String segment;

    private final Boolean restartableDuringRun;
    private final Boolean controlsTTCPartitions;

    private final TYPE type;
    private final STATUS status;

    public TTCRestart(final String name,
                      final String controller,
                      final String segment,
                      final Boolean restartableDuringRun,
                      final Boolean controlsTTCPartitions,
                      final TYPE type,
                      final STATUS status,
                      final String id)
    {
        super(ISSUETYPE.RECOVERY, "TTCRestart", type, status, type, id);
        this.name = name;
        this.controller = controller;
        this.segment = segment;
        this.restartableDuringRun = restartableDuringRun;
        this.controlsTTCPartitions = controlsTTCPartitions;
        this.type = type;
        this.status = status;
    }

    public String getName() {
        return this.name;
    }

    public String getController() {
        return this.controller;
    }

    public String getSegment() {
        return this.segment;
    }

    public Boolean getRestartableDuringRun() {
        return this.restartableDuringRun;
    }

    public Boolean getControlsTTCPartitions() {
        return this.controlsTTCPartitions;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    @Override
    public String getConcernedComponent() {
        return this.name;
    }
    
    @Override
    public List<String> additionalQualifiers() {
        final List<String> l = new ArrayList<>();
        l.add(this.segment);
        l.add(this.controller);
        l.add(this.name);
        return l;
    }

    @Override
    public SimpleEntry<String, Boolean> getDetector() {
        return new SimpleEntry<String, Boolean>(this.name, Boolean.FALSE);
    }

    @Override
    public CausedBy getCausedBy(final DAQConfigReader config) {
        return HoldsTrigger.getCausedBy(config, this.name);
    }

    @Override
    public Reason getReason() {
        return Reason.TTCRestart;
    }
}
