package chip.event.issue.recovery;

import java.util.AbstractMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.AbstractMap.SimpleEntry;

import chip.DAQConfigReader;
import daq.eformat.DetectorMask;


public interface RecoveryEvent {
    public String getConcernedComponent();

    /**
     * It possibly returns the detector affected by this recovery event
     * <p>
     * NOTE: use {@link #extractConcernedDetector(RecoveryEvent, DAQConfigReader)} to properly compute the list of concerned detectors.
     *
     * @return If the value of the entry is <em>true</em> then the key identifies the detector, otherwise the key identifies an application
     *         concerned in the recovery event that may belong to one or more (sub)detectors.
     */
    public AbstractMap.SimpleEntry<String, Boolean> getDetector();

    /**
     * It uses all the available information to extract the detector concerned by the recovery event.
     * <p>
     * If no detector can be identified, then the returned list will only contain the "NONE" string.
     * 
     * @param re The recovery event
     * @param daqConf A reference to the {@link DAQConfigReader} object
     * @return A set containing the list of concerned (sub)detectors
     */
    static Set<String> extractConcernedDetector(final RecoveryEvent re, final DAQConfigReader daqConf) {
        final Set<String> det = new TreeSet<>();

        final SimpleEntry<String, Boolean> entry = re.getDetector();
        if(entry.getValue().booleanValue() == true) {
            det.add(entry.getKey());
        } else {
            if((daqConf != null) && (entry.getKey() != null)) {
                final DetectorMask dm = daqConf.getDetMaskForApplication(entry.getKey());
                final Set<DetectorMask.SUBDETECTOR> ddd = dm.getSubdetectors();
                if(ddd.isEmpty() == true) {
                    det.add("NONE");
                } else {
                    ddd.stream().forEach(d -> det.add(d.toString()));
                }
            } else {
                det.add("NONE");
            }
        }

        return det;
    }
}
