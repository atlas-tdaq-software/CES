package chip.event.issue.recovery;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

import chip.DAQConfigReader;
import chip.event.issue.HoldsTrigger;
import chip.event.issue.TriggerHeldIssue;
import rc.HoldTriggerInfoNamed.CausedBy;
import rc.HoldTriggerInfoNamed.Reason;


public class Stopless extends TriggerHeldIssue implements RecoveryEvent {
    public static enum TYPE {
        REMOVAL,
        REMOVAL_SWROD,
        RECOVERY,
        RECOVERY_SWROD
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }

    private final String rcdApp;
    private final Boolean isInternalError;
    private final Boolean forceAuto;
    private final String probComponents;
    private final TYPE type;
    private final STATUS status;

    public Stopless(final String rcdApp,
                    final Boolean isError,
                    final Boolean forceAuto,
                    final String probComponents,
                    final TYPE type,
                    final STATUS status,
                    final String id)
    {
        super(ISSUETYPE.RECOVERY, "Stopless", type, status, type, id);
        this.rcdApp = rcdApp;
        this.isInternalError = isError;
        this.forceAuto = forceAuto;
        this.probComponents = probComponents;
        this.type = type;
        this.status = status;
    }
    
    public String getRcdApp() {
        return this.rcdApp;
    }

    public Boolean getIsInternalError() {
        return this.isInternalError;
    }

    public Boolean getForceAuto() {
        return this.forceAuto;
    }
    
    public String getProbComponents() {
        return this.probComponents;
    }
    
    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    @Override
    public String getConcernedComponent() {
        return this.rcdApp + "/" + this.probComponents;
    }

    @Override
    public List<String> additionalQualifiers() {
        final List<String> l = new ArrayList<>();

        l.add(this.rcdApp);
        l.add(this.probComponents);

        return l;
    }

    @Override
    public SimpleEntry<String, Boolean> getDetector() {
        return new SimpleEntry<String, Boolean>(this.rcdApp, Boolean.FALSE);
    }

    @Override
    public CausedBy getCausedBy(final DAQConfigReader config) {        
        return HoldsTrigger.getCausedBy(config, this.rcdApp);        
    }

    @Override
    public Reason getReason() {
        return Reason.StoplessRecovery;
    }
    
    @Override
    public boolean isTriggerHeld() {
        return (this.type.equals(TYPE.RECOVERY) || this.type.equals(TYPE.RECOVERY_SWROD));
    }
}
