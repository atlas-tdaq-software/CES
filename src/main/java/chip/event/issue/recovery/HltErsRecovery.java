package chip.event.issue.recovery;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

import chip.event.issue.Issue;


public class HltErsRecovery extends Issue implements RecoveryEvent {
    public static enum TYPE {
        HLTPU_CRASH,
        HLTPU_TIMEOUT
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE,
        REJECTED
    }

    public static enum ACTION {
        NONE,
        STOP,
        FORK
    }

    private final String reportingApp;
    private final String hltrc;
    private final String runningHost;
    private final String hltpu;
    private final TYPE type;
    private final STATUS status;
    private final ACTION action;

    public HltErsRecovery(final String reportingApp,
                          final String hltrc,
                          final String runningHost,
                          final String hltpu,
                          final TYPE type,
                          final STATUS status,
                          final ACTION action)
    {
        super(ISSUETYPE.RECOVERY, "HltErs", type, status, action);
        this.reportingApp = reportingApp;
        this.hltrc = hltrc;
        this.runningHost = runningHost;
        this.hltpu = hltpu;
        this.type = type;
        this.status = status;
        this.action = action;
    }

    /**
     * @return the reportingApp
     */
    public String getReportingApp() {
        return this.reportingApp;
    }

    /**
     * @return the hltrc
     */
    public String getHltrc() {
        return this.hltrc;
    }

    /**
     * @return the runningHost
     */
    public String getRunningHost() {
        return this.runningHost;
    }

    /**
     * @return the hltpu
     */
    public String getHltpu() {
        return this.hltpu;
    }

    /**
     * @return the type
     */
    public TYPE getType() {
        return this.type;
    }

    /**
     * @return the status
     */
    public STATUS getStatus() {
        return this.status;
    }

    public ACTION getAction() {
        return this.action;
    }

    @Override
    public String getConcernedComponent() {
        return this.hltrc;
    }
    
    @Override
    public List<String> additionalQualifiers() {
        final List<String> l = new ArrayList<>();
        l.add(this.reportingApp);
        l.add(this.runningHost);
        l.add(this.action.toString());
        return l;
    }

    @Override
    public SimpleEntry<String, Boolean> getDetector() {
        return new SimpleEntry<String, Boolean>(daq.eformat.DetectorMask.SUBDETECTOR.TDAQ_HLT.toString(), Boolean.TRUE);
    }
}
