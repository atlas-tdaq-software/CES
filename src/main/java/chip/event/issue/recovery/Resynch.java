package chip.event.issue.recovery;

import java.util.AbstractMap.SimpleEntry;

import chip.DAQConfigReader;
import chip.event.issue.TriggerHeldIssue;
import rc.HoldTriggerInfoNamed.CausedBy;
import rc.HoldTriggerInfoNamed.Reason;


public class Resynch extends TriggerHeldIssue implements RecoveryEvent {
    public static enum TYPE {
        RESYNCH
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }

    private final String controller;
    private final TYPE type;
    private final STATUS status;
    private final Boolean forceTriggerRelease;

    public Resynch(final String controller, final TYPE type, final STATUS status, final Boolean forceTriggerRelease, final String id) {
        super(ISSUETYPE.RECOVERY, "Resynch", type, status, type, id);
        this.controller = controller;
        this.type = type;
        this.status = status;
        this.forceTriggerRelease = forceTriggerRelease;
    }

    public String getController() {
        return this.controller;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public Boolean getForceTriggerRelease() {
        return this.forceTriggerRelease;
    }
    
    @Override
    public String getConcernedComponent() {
        return this.controller;
    }

    @Override
    public SimpleEntry<String, Boolean> getDetector() {
        return new SimpleEntry<String, Boolean>(this.controller, Boolean.FALSE);
    }

    @Override
    public CausedBy getCausedBy(final DAQConfigReader conf) {
        return chip.event.issue.HoldsTrigger.getCausedBy(conf, this.controller);
    }

    @Override
    public Reason getReason() {
        return Reason.Resync;
    }
}
