package chip.event.issue.recovery;

import java.util.ArrayList;
import java.util.List;
import java.util.AbstractMap.SimpleEntry;

import chip.DAQConfigReader;
import chip.event.issue.TriggerHeldIssue;
import rc.HoldTriggerInfoNamed.CausedBy;
import rc.HoldTriggerInfoNamed.Reason;


public class HltSv extends TriggerHeldIssue implements RecoveryEvent {
    public static enum TYPE {
        CRASH
    }

    public static enum STATUS {
        CHECK_NEW,
        _NEW,
        RESTARTING,
        RESTARTED,
        PROBLEM,
        FAILED,
        DONE
    }

    public static enum ACTION {
        NONE,
        RESTART,
        RESUME_TRIGGER
    }

    private final String controller;
    private final String application;
    private final TYPE type;
    private final STATUS status;
    private final ACTION action;

    public HltSv(final String controller, final String application, final TYPE type, final STATUS status, final ACTION action, final String id) {
        super(ISSUETYPE.RECOVERY, "HltSv", type, status, action, id);
        this.controller = controller;
        this.application = application;
        this.type = type;
        this.status = status;
        this.action = action;
    }

    public String getController() {
        return this.controller;
    }

    public String getApplication() {
        return this.application;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public ACTION getAction() {
        return this.action;
    }

    @Override
    public String getConcernedComponent() {
        return this.application;
    }

    @Override
    public List<String> additionalQualifiers() {
        final List<String> l = new ArrayList<>();
        l.add(this.controller);
        l.add(this.action.toString());
        return l;
    }

    @Override
    public SimpleEntry<String, Boolean> getDetector() {
        return new SimpleEntry<String, Boolean>(daq.eformat.DetectorMask.SUBDETECTOR.TDAQ_L2SV.toString(), Boolean.TRUE);
    }

    @Override
    public CausedBy getCausedBy(final DAQConfigReader conf) {
         return CausedBy.DAQ;
    }

    @Override
    public Reason getReason() {
        return Reason.CustomRecovery;
    }
}
