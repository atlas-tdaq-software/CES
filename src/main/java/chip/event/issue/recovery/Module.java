package chip.event.issue.recovery;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

import chip.event.issue.Issue;


public class Module extends Issue implements RecoveryEvent {
    public static enum TYPE {
        ENABLE,
        DISABLE
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }

    private final String requestingApp;
    private final String modules;
    private final String changeLb;
    private final TYPE type;
    private final STATUS status;

    public Module(final String requestingApp, final String modules, final String changeLb, final TYPE type, final STATUS status) {
        super(ISSUETYPE.RECOVERY, "Module", type, status, type);
        this.requestingApp = requestingApp;
        this.modules = modules;
        this.changeLb = changeLb;
        this.type = type;
        this.status = status;
    }

    public String getRequestingApp() {
        return this.requestingApp;
    }

    public String getModules() {
        return this.modules;
    }

    public String getChangeLb() {
        return this.changeLb;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    @Override
    public String getConcernedComponent() {
        return this.modules;
    }

    @Override
    public List<String> additionalQualifiers() {
        final List<String> l = new ArrayList<>();
        l.add(this.changeLb);
        return l;
    }

    @Override
    public SimpleEntry<String, Boolean> getDetector() {
        return new SimpleEntry<String, Boolean>(this.requestingApp, Boolean.FALSE);
    }
}
