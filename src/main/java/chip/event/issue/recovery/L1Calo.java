package chip.event.issue.recovery;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

import chip.DAQConfigReader;
import chip.event.issue.TriggerHeldIssue;
import rc.HoldTriggerInfoNamed.CausedBy;
import rc.HoldTriggerInfoNamed.Reason;


public class L1Calo extends TriggerHeldIssue implements RecoveryEvent {
    public static enum TYPE {
        ZERO_PPM_LUT,
        RESTORE_PPM_LUT,
        SET_PPM_NOISE_CUT
    }

    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }

    private final String segmentController;
    private final String towerId;
    private final String noiseCut;
    private final TYPE type;
    private final STATUS status;

    public L1Calo(final String segmentController, final String towerId, final String noiseCut, final TYPE type, final STATUS status, final String id) {
        super(ISSUETYPE.RECOVERY, "L1Calo", type, status, type, id);
        this.segmentController = segmentController;
        this.towerId = towerId;
        this.noiseCut = noiseCut;
        this.type = type;
        this.status = status;
    }

    public String getSegmentController() {
        return this.segmentController;
    }

    public String getTowerId() {
        return this.towerId;
    }

    public String getNoiseCut() {
        return this.noiseCut;
    }

    public TYPE getType() {
        return this.type;
    }

    public STATUS getStatus() {
        return this.status;
    }

    @Override
    public String getConcernedComponent() {
        return this.segmentController;
    }
    
    @Override
    public List<String> additionalQualifiers() {
        final List<String> l = new ArrayList<>();
        l.add(this.towerId);
        l.add(this.noiseCut);
        return l;
    }

    @Override
    public SimpleEntry<String, Boolean> getDetector() {
        return new SimpleEntry<String, Boolean>(this.segmentController, Boolean.FALSE);
    }

    @Override
    public CausedBy getCausedBy(final DAQConfigReader conf) {
        return CausedBy.L1CALO;
    }

    @Override
    public Reason getReason() {
        return Reason.CustomRecovery;
    }
}
