package chip.event.issue.recovery;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;

import chip.event.issue.Issue;

public class DCM extends Issue implements RecoveryEvent {
    private final TYPE type;
    private final String name;
    private final STATUS status;
    private final Boolean isUP;
    
    public static enum TYPE {
        ROS,
        SFO
    }
    
    public static enum STATUS {
        _NEW,
        PROBLEM,
        FAILED,
        DONE
    }
    
    public DCM(final TYPE type, final String name, final boolean isUP, final STATUS status) {
        super(ISSUETYPE.RECOVERY, "DCM", type, status, type);
        
        this.type = type;
        this.name = name;
        this.status = status;
        this.isUP = Boolean.valueOf(isUP);
    }

    public TYPE getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }
    
    public STATUS getStatus() {
        return this.status;
    }

    public Boolean getIsUP() {
        return this.isUP;
    }

    @Override
    public String getConcernedComponent() {
        return this.name;
    }
    
    @Override
    public List<String> additionalQualifiers() {
        final List<String> l = new ArrayList<>();
        l.add(String.valueOf(this.isUP.booleanValue()));
        return l;
    }

    @Override
    public SimpleEntry<String, Boolean> getDetector() {
        return new SimpleEntry<String, Boolean>(daq.eformat.DetectorMask.SUBDETECTOR.TDAQ_HLT.toString(), Boolean.TRUE);
    }
}
