package chip.event.issue;

import java.util.Collections;
import java.util.List;


abstract public class Issue {
    public enum ISSUETYPE {
        CORE,
        META,
        RECOVERY,
        AUTO_PROCEDURE,
        TEST
    }

    private final ISSUETYPE issueType;
    private final String issueId;

    private final Enum<?> enumType;
    private final Enum<?> enumStatus;
    private final Enum<?> enumAction;

    private Boolean isError = Boolean.FALSE;
    private String errorDesc = "";
    private Exception errorException = null;

    private final long time;
    
    protected Issue(final ISSUETYPE issueType, final String name, final Enum<?> enumType, final Enum<?> enumStatus, final Enum<?> enumAction)
    {
        this.issueType = issueType;
        this.issueId = name;
        this.enumType = enumType;
        this.enumStatus = enumStatus;
        this.enumAction = enumAction;
        this.time = System.currentTimeMillis();
    }

    public void setError(final String description) {
        this.isError = Boolean.TRUE;
        this.errorDesc = description;

    }

    public void setError(final String description, final Exception e) {
        this.isError = Boolean.TRUE;
        this.errorDesc = description;
        this.errorException = e;
    }
        
    final public Boolean getError() {
        return this.isError;
    }

    final public Exception errorException() {
        return this.errorException;
    }

    final public String getErrorDesc() {
        return this.errorDesc;
    }
    
    public ISSUETYPE getIssueCategory() {
        return this.issueType;
    }

    public String getIssueId() {
        return this.issueId;
    }

    final public Object getIssueType() {
        return this.enumType;
    }
    
    public Object getIssueStatus() {
        return this.enumStatus;
    }
   
    public Object getIssueAction() {
        return this.enumAction;
    }
    
    public long getTime() {
        return this.time;
    }
    
    public List<String> additionalQualifiers() {
        return Collections.emptyList();
    }
}
