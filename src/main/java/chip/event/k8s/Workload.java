package chip.event.k8s;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import chip.utils.JsonRenderer;


public class Workload {
    private final Kind kind;
    private final String name;
    private final String namespace;
    private final Map<String, String> labels = new ConcurrentHashMap<>();
    private final Long desiredReplicas;
    private final Long readyReplicas;
    private final Long availableReplicas;
    private final Boolean isConditionAvailable;
    private final Map<String, Condition> conditions = new ConcurrentHashMap<>();
    private final Long infoTime;

    public enum Kind {
        DEPLOYMENT(es.k8s.workload.Kind.DEPLOYMENT),
        DAEMONSET(es.k8s.workload.Kind.DAEMONSET);

        private final static Map<es.k8s.workload.Kind, Kind> MAP = new ConcurrentHashMap<>();
        private final es.k8s.workload.Kind value;

        static {
            for(final Kind item : Kind.values()) {
                Kind.MAP.put(item.value, item);
            }
        }

        private Kind(final es.k8s.workload.Kind v) {
            this.value = v;
        }

        public es.k8s.workload.Kind original() {
            return this.value;
        }
        
        public static Kind from(final es.k8s.workload.Kind v) {
            final Kind k = Kind.MAP.get(v);
            if(k != null) {
                return k;
            }

            throw new IllegalArgumentException("Workload kind \"" + v.toString() + "\" is not valid");
        }
    }

    public static class Condition {
        private final String type;
        private final ConditionStatus status;
        private final String reason;
        private final String message;
        private final Long time;

        Condition(final es.k8s.workload.Condition condition) {
            this.type = condition.type;
            this.status = ConditionStatus.from(condition.status);
            this.reason = condition.reason;
            this.message = condition.message;
            this.time = Long.valueOf(condition.time);
        }

        public String getType() {
            return this.type;
        }

        public ConditionStatus getStatus() {
            return this.status;
        }

        public String getReason() {
            return this.reason;
        }

        public String getMessage() {
            return this.message;
        }

        public Long getTime() {
            return this.time;
        }
        
        @Override
        public String toString() {
            return JsonRenderer.instance().render(this);
        }
    }

    public Workload(final es.k8s.workload.WorkloadInfo workload) {
        this.kind = Kind.from(workload.kind_);
        this.name = workload.name;
        this.namespace = workload.namespace_;

        for(final es.k8s.Label l : workload.labels) {
            this.labels.put(l.key, l.value);
        }

        this.desiredReplicas = Long.valueOf(workload.desiredReplicas);
        this.readyReplicas = Long.valueOf(workload.readyReplicas);
        this.availableReplicas = Long.valueOf(workload.availableReplicas);
        this.isConditionAvailable = Boolean.valueOf(workload.isConditionAvailable);

        for(final es.k8s.workload.ConditionEntry ce : workload.conditions) {
            this.conditions.put(ce.key, new Condition(ce.value));
        }

        this.infoTime = Long.valueOf(workload.infoTime);
    }

    public Kind getKind() {
        return this.kind;
    }

    public String getName() {
        return this.name;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public Map<String, String> getLabels() {
        return this.labels;
    }

    public Long getDesiredReplicas() {
        return this.desiredReplicas;
    }

    public Long getReadyReplicas() {
        return this.readyReplicas;
    }

    public Long getAvailableReplicas() {
        return this.availableReplicas;
    }

    public Boolean getIsConditionAvailable() {
        return this.isConditionAvailable;
    }

    public Map<String, Condition> getConditions() {
        return this.conditions;
    }

    public Long getInfoTime() {
        return this.infoTime;
    }
    
    @Override
    public String toString() {
        return JsonRenderer.instance().render(this);
    }
}
