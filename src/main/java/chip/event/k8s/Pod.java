package chip.event.k8s;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import chip.utils.JsonRenderer;


public class Pod {
    private final String name;
    private final String host;
    private final String namespace;
    private final Workload belongsTo;
    private final Map<String, String> labels = new ConcurrentHashMap<>();
    private final Phase phase;
    private final Map<String, Condition> conditions = new ConcurrentHashMap<>();
    private final Long infoTime;

    public enum Phase {
        PENDING(es.k8s.pod.Phase.PENDING),
        RUNNING(es.k8s.pod.Phase.RUNNING),
        SUCCEEDED(es.k8s.pod.Phase.SUCCEEDED),
        FAILED(es.k8s.pod.Phase.FAILED),
        UNKNOWN(es.k8s.pod.Phase.UNKNOWN),
        OTHER(es.k8s.pod.Phase.OTHER_);

        private final static Map<es.k8s.pod.Phase, Phase> MAP = new ConcurrentHashMap<>();
        private final es.k8s.pod.Phase value;

        static {
            for(final Phase p : Phase.values()) {
                Phase.MAP.put(p.value, p);
            }
        }

        private Phase(final es.k8s.pod.Phase v) {
            this.value = v;
        }

        public es.k8s.pod.Phase original() {
            return this.value;
        }

        public static Phase from(final es.k8s.pod.Phase v) {
            final Phase p = Phase.MAP.get(v);
            if(p != null) {
                return p;
            }

            throw new IllegalArgumentException("Phase \"" + v.toString() + "\" is not valid");
        }
    }

    public enum ConditionType {
        POD_SCHEDULED(es.k8s.pod.ConditionType.POD_SCHEDULED),
        POD_READY_TO_START_CONTAINERS(es.k8s.pod.ConditionType.POD_READY_TO_START_CONTAINERS),
        CONTAINERS_READY(es.k8s.pod.ConditionType.CONTAINERS_READY),
        INITIALIZED(es.k8s.pod.ConditionType.INITIALIZED),
        READY(es.k8s.pod.ConditionType.READY),
        OTHER(es.k8s.pod.ConditionType.OTHER);

        private final static Map<es.k8s.pod.ConditionType, ConditionType> MAP = new ConcurrentHashMap<>();
        private final es.k8s.pod.ConditionType value;

        static {
            for(final ConditionType ct : ConditionType.values()) {
                ConditionType.MAP.put(ct.value, ct);
            }
        }

        private ConditionType(final es.k8s.pod.ConditionType v) {
            this.value = v;
        }

        public es.k8s.pod.ConditionType original() {
            return this.value;
        }

        public static ConditionType from(final es.k8s.pod.ConditionType v) {
            final ConditionType ct = ConditionType.MAP.get(v);
            if(ct != null) {
                return ct;
            }

            throw new IllegalArgumentException("ConditionType \"" + v.toString() + "\" is not valid");
        }
    }

    public static class Condition {
        private final String reason;
        private final ConditionStatus status;
        private final ConditionType type;
        private final String message;
        private final Long time;

        Condition(final es.k8s.pod.Condition condition) {
            this.reason = condition.reason;
            this.status = ConditionStatus.from(condition.status);
            this.type = ConditionType.from(condition.type);
            this.message = condition.message;
            this.time = Long.valueOf(condition.time);
        }

        public String getReason() {
            return this.reason;
        }

        public ConditionStatus getStatus() {
            return this.status;
        }

        public ConditionType getType() {
            return this.type;
        }

        public String getMessage() {
            return this.message;
        }

        public Long getTime() {
            return this.time;
        }

        @Override
        public String toString() {
            return JsonRenderer.instance().render(this);
        }
    }

    public Pod(final es.k8s.pod.PodInfo pod) {
        this.name = pod.name;
        this.host = pod.host;
        this.namespace = pod.namespace_;
        this.belongsTo = new Workload(pod.belongsTo);

        for(final es.k8s.Label l : pod.labels) {
            this.labels.put(l.key, l.value);
        }

        this.phase = Phase.from(pod.phase_);

        for(final es.k8s.pod.ConditionEntry ce : pod.conditions) {
            this.conditions.put(ConditionType.from(ce.key).name(), new Condition(ce.value));
        }

        this.infoTime = Long.valueOf(pod.infoTime);
    }

    public String getName() {
        return this.name;
    }

    public String getHost() {
        return this.host;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public Workload getBelongsTo() {
        return this.belongsTo;
    }

    public Map<String, String> getLabels() {
        return this.labels;
    }

    public Phase getPhase() {
        return this.phase;
    }

    public Map<String, Condition> getConditions() {
        return this.conditions;
    }

    public Long getInfoTime() {
        return this.infoTime;
    }

    @Override
    public String toString() {
        return JsonRenderer.instance().render(this);
    }
}
