package chip.event.k8s;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import chip.utils.JsonRenderer;


public class Container {
    private final Type type;
    private final Pod belongsTo;
    private final String name;
    private final String host;
    private final String namespace;
    private final State state;
    private final Boolean isReady;
    private final Boolean isStarted;
    private final Long restartCount;
    private final Long exitCode;
    private final Long signal;
    private final Long infoTime;

    public enum Type {
        INIT(es.k8s.container.Type.INIT),
        MAIN(es.k8s.container.Type.MAIN),
        EPHEMERAL(es.k8s.container.Type.EPHEMERAL);

        private final static Map<es.k8s.container.Type, Type> MAP = new ConcurrentHashMap<>();
        private final es.k8s.container.Type value;

        static {
            for(final Type t : Type.values()) {
                Type.MAP.put(t.value, t);
            }
        }

        private Type(final es.k8s.container.Type v) {
            this.value = v;
        }

        
        public es.k8s.container.Type original() {
            return this.value;
        }

        public static Type from(final es.k8s.container.Type v) {
            final Type t = Type.MAP.get(v);
            if(t != null) {
                return t;
            }

            throw new IllegalArgumentException("Type \"" + v.toString() + "\" is not valid");
        }
    }

    public enum State {
        WAITING(es.k8s.container.State.WAITING),
        RUNNING(es.k8s.container.State.RUNNING),
        TERMINATED(es.k8s.container.State.TERMINATED),
        OTHER(es.k8s.container.State.OTHER);

        private final static Map<es.k8s.container.State, State> MAP = new ConcurrentHashMap<>();
        private final es.k8s.container.State value;

        static {
            for(final State s : State.values()) {
                State.MAP.put(s.value, s);
            }
        }

        private State(final es.k8s.container.State v) {
            this.value = v;
        }

        public es.k8s.container.State original() {
            return this.value;
        }
        
        public static State from(final es.k8s.container.State v) {
            final State s = State.MAP.get(v);
            if(s != null) {
                return s;
            }

            throw new IllegalArgumentException("State \"" + v.toString() + "\" is not valid");
        }
    }

    public Container(final es.k8s.container.ContainerInfo container) {
        this.type = Type.from(container.type_);
        this.belongsTo = new Pod(container.belongsTo);
        this.name = container.name;
        this.host = container.host;
        this.namespace = container.namespace_;
        this.state = State.from(container.state_);
        this.isReady = Boolean.valueOf(container.isReady);
        this.isStarted = Boolean.valueOf(container.isStarted);
        this.restartCount = Long.valueOf(container.restartCount);
        this.exitCode = Long.valueOf(container.exitCode);
        this.signal = Long.valueOf(container.signal);
        this.infoTime = Long.valueOf(container.infoTime);
    }

    public Type getType() {
        return this.type;
    }

    public Pod getBelongsTo() {
        return this.belongsTo;
    }

    public String getName() {
        return this.name;
    }

    public String getHost() {
        return this.host;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public State getState() {
        return this.state;
    }

    public Boolean getIsReady() {
        return this.isReady;
    }

    public Boolean getIsStarted() {
        return this.isStarted;
    }

    public Long getRestartCount() {
        return this.restartCount;
    }

    public Long getExitCode() {
        return this.exitCode;
    }

    public Long getSignal() {
        return this.signal;
    }

    public Long getInfoTime() {
        return this.infoTime;
    }
    
    @Override
    public String toString() {
        return JsonRenderer.instance().render(this);
    }
}
