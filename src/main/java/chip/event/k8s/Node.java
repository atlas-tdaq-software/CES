package chip.event.k8s;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import chip.utils.JsonRenderer;


public class Node {
    private final String name;
    private final Boolean isEnabled;
    private final Map<String, Condition> conditions = new ConcurrentHashMap<>();
    private final Long infoTime;

    public enum ConditionType {
        READY(es.k8s.node.ConditionType.READY),
        DISK_PRESSURE(es.k8s.node.ConditionType.DISK_PRESSURE),
        MEMORY_PRESSURE(es.k8s.node.ConditionType.MEMORY_PRESSURE),
        PID_PRESSURE(es.k8s.node.ConditionType.PID_PRESSURE),
        NETWORK_UNAVAILABLE(es.k8s.node.ConditionType.NETWORK_UNAVAILABLE),
        OTHER(es.k8s.node.ConditionType.OTHER);

        private final static Map<es.k8s.node.ConditionType, ConditionType> MAP = new ConcurrentHashMap<>();
        private final es.k8s.node.ConditionType value;

        static {
            for(final ConditionType item : ConditionType.values()) {
                ConditionType.MAP.put(item.value, item);
            }
        }

        private ConditionType(final es.k8s.node.ConditionType v) {
            this.value = v;
        }

        public es.k8s.node.ConditionType original() {
            return this.value;
        }

        static public ConditionType from(final es.k8s.node.ConditionType v) {
            final ConditionType ct = ConditionType.MAP.get(v);
            if(ct != null) {
                return ct;
            }

            throw new IllegalArgumentException("Condition type \"" + v.toString() + "\" is not valid");
        }
    }

    public static class Condition {
        private final ConditionType type;
        private final ConditionStatus status;
        private final String reason;
        private final String message;
        private final Long time;

        Condition(final es.k8s.node.Condition condition) {
            this.type = ConditionType.from(condition.type);
            this.status = ConditionStatus.from(condition.status);
            this.reason = condition.reason;
            this.message = condition.message;
            this.time = Long.valueOf(condition.time);
        }

        public ConditionType getType() {
            return this.type;
        }

        public ConditionStatus getStatus() {
            return this.status;
        }

        public String getReason() {
            return this.reason;
        }

        public String getMessage() {
            return this.message;
        }

        public Long getTime() {
            return this.time;
        }

        @Override
        public String toString() {
            return JsonRenderer.instance().render(this);
        }
    }

    public Node(final es.k8s.node.NodeInfo node) {
        this.name = node.name;
        this.isEnabled = Boolean.valueOf(node.enabled);
        this.infoTime = Long.valueOf(node.infoTime);

        for(final es.k8s.node.ConditionEntry e : node.conditions) {
            this.conditions.put(ConditionType.from(e.key).name(), new Condition(e.value));
        }
    }

    public String getName() {
        return this.name;
    }

    public Boolean getIsEnabled() {
        return this.isEnabled;
    }

    public Map<String, Condition> getConditions() {
        return this.conditions;
    }

    public Long getInfoTime() {
        return this.infoTime;
    }

    @Override
    public String toString() {
        return JsonRenderer.instance().render(this);
    }
}
