package chip.event.k8s;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public enum ConditionStatus {
    TRUE(es.k8s.ConditionStatus.TRUE_),
    FALSE(es.k8s.ConditionStatus.FALSE_),
    UNKNOWN(es.k8s.ConditionStatus.UNKNOWN),
    OTHER(es.k8s.ConditionStatus.OTHER);

    private final static Map<es.k8s.ConditionStatus, ConditionStatus> MAP = new ConcurrentHashMap<>();
    private final es.k8s.ConditionStatus value;

    static {
        for(final ConditionStatus item : ConditionStatus.values()) {
            ConditionStatus.MAP.put(item.value, item);
        }
    }

    private ConditionStatus(final es.k8s.ConditionStatus v) {
        this.value = v;
    }

    public es.k8s.ConditionStatus original() {
        return this.value;
    }
    
    public static ConditionStatus from(es.k8s.ConditionStatus v) {
        final ConditionStatus cs = ConditionStatus.MAP.get(v);
        if(cs != null) {
            return cs;
        }

        throw new IllegalArgumentException("Condition status \"" + v.toString() + "\" is not valid");
    }
}
