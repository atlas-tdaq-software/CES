package chip.event.config;

import java.util.HashSet;
import java.util.Set;

import chip.CHIPException;
import chip.CHIPException.DAQConfigurationError;
import chip.DAQConfigReader;
import chip.event.en.FSM;
import chip.event.en.IFDIES;
import chip.event.en.IFFAILED;
import chip.event.en.STARTAT;
import chip.event.en.STOPAT;
import config.ConfigException;
import config.Configuration;
import dal.Computer;


/**
 * Class representing an application (non state-aware. For state aware application refer to {@link ConfigRcApplication}) event build with
 * the information retrieved from configuration database plus computed fields, such as the controller name.
 * <p>
 * This class is meant to be injected in the CEP engine as configuration event to build the {@link Application} stream.
 * 
 * @author lucamag
 */
public class ApplicationConfig extends ObjectConfig {

    // Attributes as taken from configuration database
    private final Boolean restartableDuringRun;
    private final IFFAILED ifFailed;
    private final IFDIES ifDies;
    private final STARTAT startAt;
    private final STOPAT stopAt;
    private final String runsOn;

    // The next 2 attributes indicated controller's FSM-state at which the application is expected to be running/not running
    // Attributes' value depends of the application type (Online_segment's application, Infrastructure or RunControl, or normal application)
    // and on the startAt/stopAt fileds as expressed in the configuration database.
    // The algorithm selecting the value is in {@link STARTAT.toFSM()} and {@link STOPAT.TOFSM()}
    private final FSM startAtState;
    private final FSM stopAtState;

    // To simplify class hierarchy the backup host attribute is moved to this base application class,
    private final Set<String> backupHosts;

    // Name of the application controller, elaborated visiting recursively the segments tree by {@link DAQConfiguration}
    // Please note controller of an infrastructure application or of a segment's controller is
    // the father segment's controller
    private final String controller;

    private final String segment;

    private final Boolean allowSpontaneousExit;

    /**
     * Constructor for config application event. Unfortunately, no builder pattern cannot be used to allow Esper generating events from
     * statements output.
     * <p>
     * 
     * @param name Application name
     * @param oksClassName OKS Class name
     * @param restartableDuringRun Flag indicating if the application is restartable during run
     * @param ifFailed Action to be taken if the application fails
     * @param ifDies Action to be taken if the application dies
     * @param startAt When the application is started
     * @param stopAt When the application is stopped
     * @param startAtState Controller FSM state at which the application is expected to be running
     * @param stopAtState Controller FSM state at which the application is expected to be not running
     * @param runsOn Host where the application run, as from configuration
     * @param backupHosts Backup of, if any
     * @param controller Name of the controller application
     */

    public ApplicationConfig(final String name,
                             final String oksClassName,
                             final Boolean restartableDuringRun,
                             final IFFAILED ifFailed,
                             final IFDIES ifDies,
                             final STARTAT startAt,
                             final STOPAT stopAt,
                             final FSM startAtState,
                             final FSM stopAtState,
                             final String runsOn,
                             final Set<String> backupHosts,
                             final String controller,
                             final String segment,
                             final Boolean allowSpontaneousExit)
    {
        super(name, oksClassName);
        this.restartableDuringRun = restartableDuringRun;
        this.ifFailed = ifFailed;
        this.ifDies = ifDies;
        this.startAt = startAt;
        this.stopAt = stopAt;
        this.startAtState = startAtState;
        this.stopAtState = stopAtState;
        this.runsOn = runsOn;
        this.backupHosts = backupHosts;
        this.controller = controller;
        this.segment = segment;
        this.allowSpontaneousExit = allowSpontaneousExit;

        // log.debug("Name: "+name+", isControlledBy: "+controller);
    }

    /**
     * Helper method to create a {@link ApplicationConfig} from a {@link dal.BaseApplication} application.
     * 
     * @param app The {@link dal.BaseApplication} describing the application in the database
     * @param controller The name of the application controller
     * @param type The {@link DAQConfigReader.APP_TYPE} type of the application
     * @return The {@link ApplicationConfig} build from database representation
     * @throws DAQConfigurationError 
     */
    public static ApplicationConfig createConfigApplication(final Configuration config,
                                                            final dal.BaseApplication app,
                                                            final String controller,
                                                            final DAQConfigReader.APP_TYPE type,
                                                            final String segment) throws DAQConfigurationError
    {
        try {
        final String name = app.UID();
        final String oksClassName = app.class_name();        
        final Boolean restartableDuringRun = Boolean.valueOf(app.get_RestartableDuringRun());
        final IFFAILED ifFailed = IFFAILED.valueOf(app.get_IfFailsToStart().toUpperCase());
        final IFDIES ifDies = IFDIES.valueOf(app.get_IfExitsUnexpectedly().toUpperCase());

        Boolean spontExit = Boolean.FALSE;
        STARTAT startAt = STARTAT.BOOT;
        STOPAT stopAt = STOPAT.SHUTDOWN;
        final dal.CustomLifetimeApplicationBase cltApp = dal.CustomLifetimeApplicationBase_Helper.cast(app);
        if(cltApp != null) {
            final String[] lt = cltApp.get_Lifetime().split("_");
            if(lt.length == 2) {
                startAt = STARTAT.valueOf(lt[0].toUpperCase());
                stopAt = STOPAT.valueOf(lt[1].toUpperCase());
            }

            spontExit = Boolean.valueOf(cltApp.get_AllowSpontaneousExit());
        }

        final FSM startAtState = startAt.toFSM(type);
        final FSM stopAtState = stopAt.toFSM(type);
        final String runsOn = app.get_host().UID();
        final Set<String> backupHosts = new HashSet<>();

        final Computer[] bh = app.get_backup_hosts();
        if(bh != null) {
            for(final Computer c: bh) {
                if(c.get_State() == true) {
                    backupHosts.add(c.UID());
                }
            }
        }        
        
        return new ApplicationConfig(name,
                                     oksClassName,
                                     restartableDuringRun,
                                     ifFailed,
                                     ifDies,
                                     startAt,
                                     stopAt,
                                     startAtState,
                                     stopAtState,
                                     runsOn,
                                     backupHosts,
                                     controller,
                                     segment,
                                     spontExit);
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Failed to retrieve information about application " + app.UID() + ": " + ex, ex);
        }
    }
    
    /**
     * @return the restartableDuringRun
     */
    public final Boolean getRestartableDuringRun() {
        return this.restartableDuringRun;
    }

    /**
     * @return the ifFailed
     */
    public final IFFAILED getIfFailed() {
        return this.ifFailed;
    }

    /**
     * @return the ifDies
     */
    public final IFDIES getIfDies() {
        return this.ifDies;
    }

    /**
     * @return the startAt
     */
    public final STARTAT getStartAt() {
        return this.startAt;
    }

    /**
     * @return the stopAt
     */
    public final STOPAT getStopAt() {
        return this.stopAt;
    }

    /**
     * @return the startAtState
     */
    public final FSM getStartAtState() {
        return this.startAtState;
    }

    /**
     * @return the stopAtState
     */
    public final FSM getStopAtState() {
        return this.stopAtState;
    }

    /**
     * @return the runsOn
     */
    public final String getRunsOn() {
        return this.runsOn;
    }

    /**
     * @return the backupHosts
     */
    public final Set<String> getBackupHosts() {
        return this.backupHosts;
    }

    /**
     * @return
     */
    public String getController() {
        return this.controller;
    }

    public String getSegment() {
        return this.segment;
    }

    public Boolean getAllowSpontaneousExit() {
        return this.allowSpontaneousExit;
    }

}
