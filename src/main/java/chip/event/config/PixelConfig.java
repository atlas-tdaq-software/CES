package chip.event.config;

public class PixelConfig {
    private final String ctrlPixTop;
    private final String ctrlPixBarrel;
    private final String ctrlPixBLayer;
    private final String ctrlPixDisk;
    private final String ctrlPixIBL;

    public PixelConfig(final String ctrlPixIBL,
                       final String ctrlPixBLayer,
                       final String ctrlPixBarrel,
                       final String ctrlPixDisk,
                       final String ctrlPixTop)
    {
        this.ctrlPixBarrel = ctrlPixBarrel;
        this.ctrlPixBLayer = ctrlPixBLayer;
        this.ctrlPixDisk = ctrlPixDisk;
        this.ctrlPixIBL = ctrlPixIBL;
        this.ctrlPixTop = ctrlPixTop;
    }

    public String getCtrlPixTop() {
        return this.ctrlPixTop;
    }

    public String getCtrlPixBarrel() {
        return this.ctrlPixBarrel;
    }

    public String getCtrlPixBLayer() {
        return this.ctrlPixBLayer;
    }

    public String getCtrlPixDisk() {
        return this.ctrlPixDisk;
    }

    public String getCtrlPixIBL() {
        return this.ctrlPixIBL;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.ctrlPixBLayer == null) ? 0 : this.ctrlPixBLayer.hashCode());
        result = (prime * result) + ((this.ctrlPixBarrel == null) ? 0 : this.ctrlPixBarrel.hashCode());
        result = (prime * result) + ((this.ctrlPixDisk == null) ? 0 : this.ctrlPixDisk.hashCode());
        result = (prime * result) + ((this.ctrlPixIBL == null) ? 0 : this.ctrlPixIBL.hashCode());
        result = (prime * result) + ((this.ctrlPixTop == null) ? 0 : this.ctrlPixTop.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null) {
            return false;
        }
        if(this.getClass() != obj.getClass()) {
            return false;
        }
        final PixelConfig other = (PixelConfig) obj;
        if(this.ctrlPixBLayer == null) {
            if(other.ctrlPixBLayer != null) {
                return false;
            }
        } else if(!this.ctrlPixBLayer.equals(other.ctrlPixBLayer)) {
            return false;
        }
        if(this.ctrlPixBarrel == null) {
            if(other.ctrlPixBarrel != null) {
                return false;
            }
        } else if(!this.ctrlPixBarrel.equals(other.ctrlPixBarrel)) {
            return false;
        }
        if(this.ctrlPixDisk == null) {
            if(other.ctrlPixDisk != null) {
                return false;
            }
        } else if(!this.ctrlPixDisk.equals(other.ctrlPixDisk)) {
            return false;
        }
        if(this.ctrlPixIBL == null) {
            if(other.ctrlPixIBL != null) {
                return false;
            }
        } else if(!this.ctrlPixIBL.equals(other.ctrlPixIBL)) {
            return false;
        }
        if(this.ctrlPixTop == null) {
            if(other.ctrlPixTop != null) {
                return false;
            }
        } else if(!this.ctrlPixTop.equals(other.ctrlPixTop)) {
            return false;
        }
        return true;
    }
}
