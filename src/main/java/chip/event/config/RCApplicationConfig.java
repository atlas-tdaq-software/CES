package chip.event.config;

import java.util.HashSet;
import java.util.Set;

import chip.CHIPException;
import chip.CHIPException.DAQConfigurationError;
import chip.DAQConfigReader;
import chip.event.en.FSM;
import chip.event.en.IFDIES;
import chip.event.en.IFERROR;
import chip.event.en.IFFAILED;
import chip.event.en.STARTAT;
import chip.event.en.STOPAT;
import config.ConfigException;
import dal.Computer;


/**
 * Class representing a state-aware application (for non state-aware application refer to {@link ApplicationConfig}) event build with the
 * information retrieved from configuration database plus computed fields, such as the controller name.
 * <p>
 * This class is meant to be injected in the CEP engine as configuration event to build the {@link Application} stream.
 * 
 * @author lucamag
 */
public class RCApplicationConfig extends ObjectConfig {
    private final Boolean restartableDuringRun;
    private final IFFAILED ifFailed;
    private final IFDIES ifDies;
    private final STARTAT startAt;
    private final STOPAT stopAt;
    private final String runsOn;
    private final FSM startAtState;
    private final FSM stopAtState;
    private final Set<String> backupHosts;
    private final String controller;
    private final String segment;
    private final Boolean allowSpontaneousExit;

    private final Boolean controlsTTCPartitions;
    private final IFERROR ifError;
    private final Boolean isController;

    /**
     * Constructor for the config RunControl application event. Unfortunately, no builder pattern cannot be used to allow Esper generating
     * events from statements output.
     * <p>
     * 
     * @param name Application name
     * @param oksClassName OKS Class name
     * @param restartableDuringRun Flag indicating if the application is restartable during run
     * @param ifFailed Action to be taken if the application fails
     * @param ifDies Action to be taken if the application dies
     * @param startAt When the application is started
     * @param stopAt When the application is stopped
     * @param startAtState Controller FSM state at which the application is expected to be running
     * @param stopAtState Controller FSM state at which the application is expected to be not running
     * @param runsOn Host where the application run, as from configuration
     * @param backupHosts Backup of, if any
     * @param controller Name of the controller application
     * @param controlsTTCPartitions
     * @param ifError Action to be taking if the application is in Error
     */
    public RCApplicationConfig(final String name,
                               final String oksClassName,
                               final Boolean restartableDuringRun,
                               final IFFAILED ifFailed,
                               final IFDIES ifDies,
                               final STARTAT startAt,
                               final STOPAT stopAt,
                               final FSM startAtState,
                               final FSM stopAtState,
                               final String runsOn,
                               final Set<String> backupHosts,
                               final String controller,
                               final String segment,
                               final Boolean allowSpontaneousExit,
                               final Boolean controlsTTCPartitions,
                               final IFERROR ifError,
                               final Boolean isController)
    {
        super(name, oksClassName);
        this.restartableDuringRun = restartableDuringRun;
        this.ifFailed = ifFailed;
        this.ifDies = ifDies;
        this.startAt = startAt;
        this.stopAt = stopAt;
        this.startAtState = startAtState;
        this.stopAtState = stopAtState;
        this.runsOn = runsOn;
        this.backupHosts = backupHosts;
        this.controller = controller;
        this.segment = segment;
        this.allowSpontaneousExit = allowSpontaneousExit;
        this.controlsTTCPartitions = controlsTTCPartitions;
        this.ifError = ifError;
        this.isController = isController;
        // log.debug("Name: "+name+", isControlledBy: "+controller+", isController: "+isController);
    }

    /**
     * Helper method to create a {@link ApplicationConfig} from a {@link dal.BaseApplication} application.
     * 
     * @param baseApp The {@link dal.BaseApplication} describing the application in the database
     * @param rcapp The {@link dal.RunControlApplicationBase} describing the application in the database
     * @param controller The name of the application controller
     * @param type The {@link DAQConfigReader.APP_TYPE} type of the application
     * @return The {@link ApplicationConfig} build from database representation
     * @throws DAQConfigurationError 
     */
    public static RCApplicationConfig createConfigRCApplication(final dal.BaseApplication baseApp,
                                                                final dal.RunControlApplicationBase rcapp,
                                                                final String controller,
                                                                final DAQConfigReader.APP_TYPE type,
                                                                final String segment,
                                                                final Boolean isController) throws DAQConfigurationError
    {        
        try {
            final String name = baseApp.UID();
            final String oksClassName = baseApp.class_name();
            final Boolean restartableDuringRun = Boolean.valueOf(baseApp.get_RestartableDuringRun());
            final IFFAILED ifFailed = IFFAILED.valueOf(baseApp.get_IfFailsToStart().toUpperCase());
            final IFDIES ifDies = IFDIES.valueOf(baseApp.get_IfExitsUnexpectedly().toUpperCase());
            final STARTAT startAt = STARTAT.BOOT;
            final STOPAT stopAt = STOPAT.SHUTDOWN;
            final FSM startAtState = startAt.toFSM(type);
            final FSM stopAtState = stopAt.toFSM(type);
            final String runsOn = baseApp.get_host().UID();
            final Set<String> backupHosts = new HashSet<>();
    
            final Computer[] bh = baseApp.get_backup_hosts();
            if(bh != null) {
                for(final Computer c : bh) {
                    if(c.get_State() == true) {
                        backupHosts.add(c.UID());
                    }
                }
            }
            
            // RunControl application
            final Boolean controlsTTCPartitions = Boolean.valueOf(rcapp.get_ControlsTTCPartitions());
            final IFERROR ifError = IFERROR.valueOf(rcapp.get_IfError().toUpperCase());
    
            return new RCApplicationConfig(name,
                                           oksClassName,
                                           restartableDuringRun,
                                           ifFailed,
                                           ifDies,
                                           startAt,
                                           stopAt,
                                           startAtState,
                                           stopAtState,
                                           runsOn,
                                           backupHosts,
                                           controller,
                                           segment,
                                           Boolean.FALSE,
                                           controlsTTCPartitions,
                                           ifError,
                                           isController);
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Failed to retrieve information about application " + baseApp.UID() + ": " + ex, ex);
        }
    }

    /**
     * @return the restartableDuringRun
     */
    public final Boolean getRestartableDuringRun() {
        return this.restartableDuringRun;
    }

    /**
     * @return the ifFailed
     */
    public final IFFAILED getIfFailed() {
        return this.ifFailed;
    }

    /**
     * @return the ifDies
     */
    public final IFDIES getIfDies() {
        return this.ifDies;
    }

    /**
     * @return the startAt
     */
    public final STARTAT getStartAt() {
        return this.startAt;
    }

    /**
     * @return the stopAt
     */
    public final STOPAT getStopAt() {
        return this.stopAt;
    }

    /**
     * @return the startAtState
     */
    public final FSM getStartAtState() {
        return this.startAtState;
    }

    /**
     * @return the stopAtState
     */
    public final FSM getStopAtState() {
        return this.stopAtState;
    }

    /**
     * @return the runsOn
     */
    public final String getRunsOn() {
        return this.runsOn;
    }

    /**
     * @return the backupHosts
     */
    public final Set<String> getBackupHosts() {
        return this.backupHosts;
    }

    /**
     * @return
     */
    public String getController() {
        return this.controller;
    }

    public String getSegment() {
        return this.segment;
    }

    public Boolean getAllowSpontaneousExit() {
        return this.allowSpontaneousExit;
    }

    /**
     * @return the controlsTTCPartitions
     */
    public final Boolean getControlsTTCPartitions() {
        return this.controlsTTCPartitions;
    }

    /**
     * @return the ifError
     */
    public final IFERROR getIfError() {
        return this.ifError;
    }

    public Boolean getIsController() {
        return this.isController;
    }

}
