package chip.event.config;

/**
 * Basic configuration entity.
 * 
 * @author lucamag
 */
public class ObjectConfig {
    private final String name;
    private final String oksClassName;

    public final String getName() {
        return this.name;
    }

    public final String getOksClassName() {
        return this.oksClassName;
    }

    public ObjectConfig(final String name, final String oksClassName) {
        super();
        this.name = name;
        this.oksClassName = oksClassName;
    }
}
