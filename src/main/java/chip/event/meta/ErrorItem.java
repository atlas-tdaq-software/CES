package chip.event.meta;

public class ErrorItem {

    private final String application;
    private final String controller;
    private final String errorType;
    private final String errorDescription;

    public ErrorItem(final String controller, final String application, final String errorType, final String errorDescription) {
        super();
        this.controller = controller;
        this.application = application;
        this.errorType = errorType;
        this.errorDescription = errorDescription;
    }

    public String getApplication() {
        return this.application;
    }

    public String getController() {
        return this.controller;
    }

    public String getErrorType() {
        return this.errorType;
    }

    public String getErrorDescription() {
        return this.errorDescription;
    }

    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder();
        str.append("Error Item - {controller: ");
        str.append(this.controller);
        str.append(", application: ");
        str.append(this.application);
        str.append(", errorType: ");
        str.append(this.errorType);
        str.append(", errorDescription: ");
        str.append(this.errorDescription);
        str.append("}");
        return str.toString();
    }
}
