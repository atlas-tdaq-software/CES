package chip.event.meta;

import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

import chip.event.en.FSM;
import chip.event.en.IFDIES;
import chip.event.en.IFERROR;
import chip.event.en.IFFAILED;
import chip.event.en.MEMBERSHIP;
import chip.event.en.RCCOMMAND;
import chip.event.en.STARTAT;
import chip.event.en.STATUS;
import chip.event.en.STOPAT;
import chip.event.en.TRCOMMAND;


public class RCApplication {

    protected final String name;
    protected final String oksClassName;
    protected final Boolean restartableDuringRun;
    protected final IFFAILED ifFailed;
    protected final IFDIES ifDies;
    protected final STARTAT startAt;
    protected final STOPAT stopAt;
    // Look ConfigApplication for doc
    protected final FSM startAtState;
    protected final FSM stopAtState;
    protected final Boolean allowSpontaneousExit;
    protected final Set<String> backupHosts;
    private final Boolean controlsTTCPartitions;
    private final IFERROR ifError;
    private final Boolean isController;

    protected final String runningHost;
    protected final STATUS status;
    protected final Boolean notResponding;
    // private final Boolean errorStatus;
    // private final String errorMessage;
    protected final MEMBERSHIP membership;
    protected final Boolean restarting;
    protected final Long exitCode;
    protected final Long exitSignal;
    protected final String controller;
    protected final String segment;

    private final FSM state;
    private final Boolean isTransitioning;
    private final TRCOMMAND currentTransitionCommand;
    private final String currentTransitionCommandUID;
    private final TRCOMMAND previousTransitionCommand;
    private final Long transitionTime;
    private final Boolean internalError;
    private final RCCOMMAND rccommand;
    private final String[] rccommandArgs;
    private final String rccommandUID;
    private final Boolean busy;
    private final Long timestamp;
    private final Long eventTimestamp; // in millisecs

    private final FSM parentState;
    private final Boolean parentIsTransitioning;
    private final TRCOMMAND parentCurrentTransitionCommand;
    private final TRCOMMAND parentPreviousTransitionCommand;
    private final Boolean parentIsExiting;
    private final Boolean parentIsInitializing;

    /**
     * @param name
     * @param oksClassName
     * @param restartableDuringRun
     * @param ifFailed
     * @param ifDies
     * @param startAt
     * @param stopAt
     * @param startAtState
     * @param stopAtState
     * @param allowSpontaneousExit
     * @param backupHosts
     * @param controlsTTCPartitions
     * @param ifError
     * @param isController
     * @param runningHost
     * @param status
     * @param notResponding
     * @param membership
     * @param restarting
     * @param exitCode
     * @param exitSignal
     * @param controller
     * @param segment
     * @param state
     * @param isTransitioning
     * @param currentTransitionCommand
     * @param currentTransitionCommandUID
     * @param previousTransitionCommand
     * @param transitionTime
     * @param internalError
     * @param rccommand
     * @param rccommandArgs
     * @param rccommandUID
     * @param busy
     * @param timestamp
     * @param eventTimestamp
     * @param parentState
     * @param parentIsTransitioning
     * @param parentCurrentTransitionCommand
     * @param parentPreviousTransitionCommand
     * @param parentIsExiting
     * @param parentIsInitializing
     */
    public RCApplication(final String name,
                         final String oksClassName,
                         final Boolean restartableDuringRun,
                         final IFFAILED ifFailed,
                         final IFDIES ifDies,
                         final STARTAT startAt,
                         final STOPAT stopAt,
                         final FSM startAtState,
                         final FSM stopAtState,
                         final Boolean allowSpontaneousExit,
                         final Set<String> backupHosts,
                         final Boolean controlsTTCPartitions,
                         final IFERROR ifError,
                         final Boolean isController,
                         final String runningHost,
                         final STATUS status,
                         final Boolean notResponding,
                         final MEMBERSHIP membership,
                         final Boolean restarting,
                         final Long exitCode,
                         final Long exitSignal,
                         final String controller,
                         final String segment,
                         final FSM state,
                         final Boolean isTransitioning,
                         final TRCOMMAND currentTransitionCommand,
                         final String currentTransitionCommandUID,
                         final TRCOMMAND previousTransitionCommand,
                         final Long transitionTime,
                         final Boolean internalError,
                         final RCCOMMAND rccommand,
                         final String[] rccommandArgs,
                         final String rccommandUID,
                         final Boolean busy,
                         final Long timestamp,
                         final Long eventTimestamp,
                         final FSM parentState,
                         final Boolean parentIsTransitioning,
                         final TRCOMMAND parentCurrentTransitionCommand,
                         final TRCOMMAND parentPreviousTransitionCommand,
                         final Boolean parentIsExiting,
                         final Boolean parentIsInitializing)
    {
        super();
        this.name = name;
        this.oksClassName = oksClassName;
        this.restartableDuringRun = restartableDuringRun;
        this.ifFailed = ifFailed;
        this.ifDies = ifDies;
        this.startAt = startAt;
        this.stopAt = stopAt;
        this.startAtState = startAtState;
        this.stopAtState = stopAtState;
        this.allowSpontaneousExit = allowSpontaneousExit;
        this.backupHosts = backupHosts;
        this.controlsTTCPartitions = controlsTTCPartitions;
        this.ifError = ifError;
        this.isController = isController;
        this.runningHost = runningHost;
        this.status = status;
        this.notResponding = notResponding;
        this.membership = membership;
        this.restarting = restarting;
        this.exitCode = exitCode;
        this.exitSignal = exitSignal;
        this.controller = controller;
        this.segment = segment;
        this.state = state;
        this.isTransitioning = isTransitioning;
        this.currentTransitionCommand = currentTransitionCommand;
        this.currentTransitionCommandUID = currentTransitionCommandUID;
        this.previousTransitionCommand = previousTransitionCommand;
        this.transitionTime = transitionTime;
        this.internalError = internalError;
        this.rccommand = rccommand;
        this.rccommandArgs = rccommandArgs;
        this.rccommandUID = rccommandUID;
        this.busy = busy;
        this.timestamp = timestamp;
        this.eventTimestamp = eventTimestamp;
        this.parentState = parentState;
        this.parentIsTransitioning = parentIsTransitioning;
        this.parentCurrentTransitionCommand = parentCurrentTransitionCommand;
        this.parentPreviousTransitionCommand = parentPreviousTransitionCommand;
        this.parentIsExiting = parentIsExiting;
        this.parentIsInitializing = parentIsInitializing;
    }

    /*
     * Builder(s)
     */

    /**
     * Builder class
     */
    public static class Builder {

        protected String name;
        protected String oksClassName;
        protected Boolean isRestartable;
        protected IFFAILED ifFailed;
        protected IFDIES ifDies;
        protected STARTAT startAt;
        protected STOPAT stopAt;
        // Look ConfigApplication for doc
        protected FSM startAtState;
        protected FSM stopAtState;
        protected Boolean allowSpontaneousExit;
        protected Set<String> backupHosts;
        protected String runningHost;
        protected STATUS status;
        protected Boolean isNotResponding;
        // private final Boolean errorStatus;
        // private final String errorMessage;
        protected MEMBERSHIP membership;
        protected Boolean isRestarting;
        protected Long exitCode;
        protected Long exitSignal;
        protected String controller;
        protected String segment;

        protected Boolean controlsTTC;
        protected IFERROR ifError;
        protected Boolean isController;

        protected FSM state;
        protected Boolean isTransitioning;
        protected TRCOMMAND currentTransitionCommand;
        protected String currentTransitionCommandUID;
        protected TRCOMMAND previousTransitionCommand;
        protected Long transitionTime;
        protected Boolean internalError;
        protected RCCOMMAND rccommand;
        protected String[] rccommandArgs;
        protected String rccommandUID;
        protected Boolean busy;

        protected Long timestamp;
        protected Long eventTimestamp;

        protected FSM parentState;
        protected Boolean parentIsTransitioning;
        protected TRCOMMAND parentCurrentTransitionCommand;
        protected TRCOMMAND parentPreviousTransitionCommand;
        protected Boolean parentIsExiting;
        protected Boolean parentIsInitializing;

        public Builder(final String applicationName) {
            this.name = applicationName;
        }

        public Builder oksClassName(final String oks_ClassName) {
            this.oksClassName = oks_ClassName;
            return this;
        }

        public Builder isRestartable(final Boolean is_Restartable) {
            this.isRestartable = is_Restartable;
            return this;
        }

        public Builder ifFailed(final IFFAILED if_Failed) {
            this.ifFailed = if_Failed;
            return this;
        }

        public Builder ifDies(final IFDIES if_Dies) {
            this.ifDies = if_Dies;
            return this;
        }

        public Builder startAt(final STARTAT start_At) {
            this.startAt = start_At;
            return this;
        }

        public Builder stopAt(final STOPAT stop_At) {
            this.stopAt = stop_At;
            return this;
        }

        public Builder startAtState(final FSM start_AtState1) {
            this.startAtState = start_AtState1;
            return this;
        }

        public Builder stopAtState(final FSM stop_AtState) {
            this.stopAtState = stop_AtState;
            return this;
        }

        public Builder allowSpontaneousExit(final Boolean allow_SpontaneousExit) {
            this.allowSpontaneousExit = allow_SpontaneousExit;
            return this;
        }

        public Builder backupHosts(final Set<String> backup_Hosts) {
            this.backupHosts = backup_Hosts;
            return this;
        }

        public Builder runningHost(final String running_Host) {
            this.runningHost = running_Host;
            return this;
        }

        public Builder status(final STATUS status_) {
            this.status = status_;
            return this;
        }

        public Builder isNotResponding(final Boolean is_NotResponding) {
            this.isNotResponding = is_NotResponding;
            return this;
        }

        public Builder membership(final MEMBERSHIP membership_) {
            this.membership = membership_;
            return this;
        }

        public Builder isRestarting(final Boolean is_Restarting) {
            this.isRestarting = is_Restarting;
            return this;
        }

        public Builder exitCode(final Long exit_Code) {
            this.exitCode = exit_Code;
            return this;
        }

        public Builder exitSignal(final Long exit_Signal) {
            this.exitSignal = exit_Signal;
            return this;
        }

        public Builder controller(final String controller_) {
            this.controller = controller_;
            return this;
        }

        public Builder segment(final String segment_) {
            this.segment = segment_;
            return this;
        }

        public Builder controlsTTC(final Boolean controls_TTC) {
            this.controlsTTC = controls_TTC;
            return this;
        }

        public Builder ifError(final IFERROR ifError_) {
            this.ifError = ifError_;
            return this;
        }

        public Builder isController(final Boolean is_Controller) {
            this.isController = is_Controller;
            return this;
        }

        public Builder state(final FSM state_) {
            this.state = state_;
            return this;
        }

        public Builder isTransitioning(final Boolean is_Transitioning) {
            this.isTransitioning = is_Transitioning;
            return this;
        }

        public Builder currentTransitionCommand(final TRCOMMAND current_TransitionCommand) {
            this.currentTransitionCommand = current_TransitionCommand;
            return this;
        }

        public Builder currentTransitionCommandUID(final String current_TransitionCommandUID) {
            this.currentTransitionCommandUID = current_TransitionCommandUID;
            return this;
        }

        public Builder previousTransitionCommand(final TRCOMMAND previous_TransitionCommand) {
            this.previousTransitionCommand = previous_TransitionCommand;
            return this;
        }

        public Builder transitionTime(final Long transition_Time) {
            this.transitionTime = transition_Time;
            return this;
        }

        public Builder internalError(final Boolean internal_Error) {
            this.internalError = internal_Error;
            return this;
        }

        public Builder rccommand(final RCCOMMAND rc_command) {
            this.rccommand = rc_command;
            return this;
        }

        public Builder rccommandArgs(final String[] rccommand_Args) {
            this.rccommandArgs = rccommand_Args;
            return this;
        }

        public Builder rccommandUID(final String rccommand_UID) {
            this.rccommandUID = rccommand_UID;
            return this;
        }

        public Builder busy(final Boolean busy_) {
            this.busy = busy_;
            return this;
        }

        public Builder timestamp(final Long time_stamp) {
            this.timestamp = time_stamp;
            return this;
        }

        public Builder eventTimestamp(final Long event_Timestamp) {
            this.eventTimestamp = event_Timestamp;
            return this;
        }

        public Builder parentState(final FSM parent_State) {
            this.parentState = parent_State;
            return this;
        }

        public Builder parentIsTransitioning(final Boolean parent_IsTransitioning) {
            this.parentIsTransitioning = parent_IsTransitioning;
            return this;
        }

        public Builder parentCurrentTransitionCommand(final TRCOMMAND parent_CurrentTransitionCommand) {
            this.parentCurrentTransitionCommand = parent_CurrentTransitionCommand;
            return this;
        }

        public Builder parentPreviousTransitionCommand(final TRCOMMAND parent_PreviousTransitionCommand) {
            this.parentPreviousTransitionCommand = parent_PreviousTransitionCommand;
            return this;
        }

        public Builder parentIsExiting(final Boolean parent_IsExiting) {
            this.parentIsExiting = parent_IsExiting;
            return this;
        }

        public Builder parentIsInitializing(final Boolean parent_IsInitializing) {
            this.parentIsInitializing = parent_IsInitializing;
            return this;
        }

        public RCApplication build() {
            return new RCApplication(this);
        }

    }

    /**
     * Builder with default value (factory)
     * 
     * @author lmagnoni
     */
    public static class DefaultBuilder extends Builder {

        public DefaultBuilder(final String applicationName) {
            super(applicationName);

            // Setting default value
            this.oksClassName = "default_oksClassName";
            this.isRestartable = Boolean.FALSE;
            this.ifFailed = IFFAILED.ERROR;
            this.ifDies = IFDIES.ERROR;
            this.startAt = STARTAT.BOOT;
            this.stopAt = STOPAT.EOR;
            // Look ConfigApplication for doc
            this.startAtState = FSM.INITIAL;
            this.stopAtState = FSM.SFOSTOPPED;
            this.allowSpontaneousExit = Boolean.FALSE;
            this.backupHosts = Collections.emptySet();
            this.runningHost = "default_host";
            this.status = STATUS.UP;
            this.isNotResponding = Boolean.FALSE;
            // private final Boolean errorStatus;
            // private final String errorMessage;
            this.membership = MEMBERSHIP._IN;
            this.isRestarting = Boolean.FALSE;
            this.exitCode = Long.valueOf(0);
            this.exitSignal = Long.valueOf(0);
            this.controller = "default_controller";
            this.segment = "deafult_segment";

            this.controlsTTC = Boolean.FALSE;
            this.ifError = IFERROR.ERROR;
            this.isController = Boolean.FALSE;
            this.state = FSM.RUNNING;
            this.isTransitioning = Boolean.FALSE;
            this.currentTransitionCommand = TRCOMMAND.START;
            this.currentTransitionCommandUID = "";
            this.previousTransitionCommand = TRCOMMAND._EMPTY;
            this.transitionTime = Long.valueOf(1L);
            this.internalError = Boolean.FALSE;
            this.rccommand = RCCOMMAND.DISABLE;
            this.rccommandArgs = new String[] {""};
            this.rccommandUID = "";
            this.busy = Boolean.FALSE;
            this.timestamp = Long.valueOf(System.currentTimeMillis());
            this.eventTimestamp = this.timestamp;

            this.parentState = FSM.RUNNING;
            this.parentIsTransitioning = Boolean.FALSE;
            this.parentCurrentTransitionCommand = TRCOMMAND._EMPTY;
            this.parentPreviousTransitionCommand = TRCOMMAND.START;
            this.parentIsExiting = Boolean.FALSE;
            this.parentIsInitializing = Boolean.FALSE;
        }

    }

    /**
     * Builder to copy from existing event
     * 
     * @author lmagnoni
     */
    public static class CopyBuilder extends Builder {
        public CopyBuilder(final RCApplication app) {
            super(app.name);
            this.oksClassName = app.oksClassName;
            this.isRestartable = app.restartableDuringRun;
            this.ifFailed = app.ifFailed;
            this.ifDies = app.ifDies;
            this.startAt = app.startAt;
            this.stopAt = app.stopAt;
            this.startAtState = app.startAtState;
            this.stopAtState = app.stopAtState;
            this.allowSpontaneousExit = app.allowSpontaneousExit;
            this.backupHosts = app.backupHosts;
            this.runningHost = app.runningHost;
            this.status = app.status;
            this.isNotResponding = app.notResponding;
            this.membership = app.membership;
            this.isRestarting = app.restarting;
            this.exitCode = app.exitCode;
            this.exitSignal = app.exitSignal;
            this.controller = app.controller;
            this.segment = app.segment;
            this.controlsTTC = app.controlsTTCPartitions;
            this.ifError = app.ifError;
            this.isController = app.isController;
            this.state = app.state;
            this.isTransitioning = app.isTransitioning;
            this.currentTransitionCommand = app.currentTransitionCommand;
            this.currentTransitionCommandUID = app.currentTransitionCommandUID;
            this.transitionTime = app.transitionTime;
            this.internalError = app.internalError;
            this.rccommand = app.rccommand;
            this.rccommandArgs = app.rccommandArgs;
            this.rccommandUID = app.rccommandUID;
            this.previousTransitionCommand = app.previousTransitionCommand;
            this.busy = app.busy;
            this.timestamp = Long.valueOf(app.timestamp.longValue() + 100);
            this.eventTimestamp = app.eventTimestamp;
            this.parentState = app.parentState;
            this.parentIsTransitioning = app.parentIsTransitioning;
            this.parentCurrentTransitionCommand = app.parentCurrentTransitionCommand;
            this.parentPreviousTransitionCommand = app.parentPreviousTransitionCommand;
            this.parentIsExiting = app.parentIsExiting;
            this.parentIsInitializing = app.parentIsInitializing;
        }
    }

    /**
     * Ctor from builder
     * 
     * @param b builder
     */
    public RCApplication(final Builder b) {
        super();
        this.name = b.name;
        this.oksClassName = b.oksClassName;
        this.restartableDuringRun = b.isRestartable;
        this.ifFailed = b.ifFailed;
        this.ifDies = b.ifDies;
        this.startAt = b.startAt;
        this.stopAt = b.stopAt;
        this.startAtState = b.startAtState;
        this.stopAtState = b.stopAtState;
        this.allowSpontaneousExit = b.allowSpontaneousExit;
        this.backupHosts = b.backupHosts;
        this.runningHost = b.runningHost;
        this.status = b.status;
        this.notResponding = b.isNotResponding;
        this.membership = b.membership;
        this.restarting = b.isRestarting;
        this.exitCode = b.exitCode;
        this.exitSignal = b.exitSignal;
        this.controller = b.controller;
        this.segment = b.segment;
        this.controlsTTCPartitions = b.controlsTTC;
        this.ifError = b.ifError;
        this.isController = b.isController;
        this.state = b.state;
        this.isTransitioning = b.isTransitioning;
        this.currentTransitionCommand = b.currentTransitionCommand;
        this.currentTransitionCommandUID = b.currentTransitionCommandUID;
        this.previousTransitionCommand = b.previousTransitionCommand;
        this.transitionTime = b.transitionTime;
        this.internalError = b.internalError;
        this.rccommand = b.rccommand;
        this.rccommandArgs = b.rccommandArgs;
        this.rccommandUID = b.rccommandUID;
        this.busy = b.busy;
        this.timestamp = b.timestamp;
        this.eventTimestamp = b.eventTimestamp;
        this.parentState = b.parentState;
        this.parentIsTransitioning = b.parentIsTransitioning;
        this.parentCurrentTransitionCommand = b.parentCurrentTransitionCommand;
        this.parentPreviousTransitionCommand = b.parentPreviousTransitionCommand;
        this.parentIsExiting = b.parentIsExiting;
        this.parentIsInitializing = b.parentIsInitializing;
    }

    /**
     * Getters
     */

    public String getName() {
        return this.name;
    }

    public String getOksClassName() {
        return this.oksClassName;
    }

    public Boolean getRestartableDuringRun() {
        return this.restartableDuringRun;
    }

    public IFFAILED getIfFailed() {
        return this.ifFailed;
    }

    public IFDIES getIfDies() {
        return this.ifDies;
    }

    public STARTAT getStartAt() {
        return this.startAt;
    }

    public STOPAT getStopAt() {
        return this.stopAt;
    }

    public FSM getStartAtState() {
        return this.startAtState;
    }

    public FSM getStopAtState() {
        return this.stopAtState;
    }

    public Boolean getAllowSpontaneousExit() {
        return this.allowSpontaneousExit;
    }

    public Set<String> getBackupHosts() {
        return this.backupHosts;
    }

    public String getRunningHost() {
        return this.runningHost;
    }

    public STATUS getStatus() {
        return this.status;
    }

    public Boolean getNotResponding() {
        return this.notResponding;
    }

    public MEMBERSHIP getMembership() {
        return this.membership;
    }

    public Boolean getRestarting() {
        return this.restarting;
    }

    public Long getExitCode() {
        return this.exitCode;
    }

    public Long getExitSignal() {
        return this.exitSignal;
    }

    public String getController() {
        return this.controller;
    }

    public String getSegment() {
        return this.segment;
    }

    /**
     * @return the applicationState
     */
    public final FSM getState() {
        return this.state;
    }

    /**
     * @return the controlsTTCPartitions
     */
    public final Boolean getControlsTTCPartitions() {
        return this.controlsTTCPartitions;
    }

    /**
     * @return the ifError
     */
    public final IFERROR getIfError() {
        return this.ifError;
    }

    public Boolean getIsController() {
        return this.isController;
    }

    /**
     * @return the transitioning
     */
    public final Boolean getIsTransitioning() {
        return this.isTransitioning;
    }

    /**
     * @return the currentTransitionCommand
     */
    public final TRCOMMAND getCurrentTransitionCommand() {
        return this.currentTransitionCommand;
    }

    public final String getCurrentTransitionCommandUID() {
        return this.currentTransitionCommandUID;
    }

    public final TRCOMMAND getPreviousTransitionCommand() {
        return this.previousTransitionCommand;
    }

    /**
     * @return the transitionTime
     */
    public final Long getTransitionTime() {
        return this.transitionTime;
    }

    /**
     * @return the internalError
     */
    public final Boolean getInternalError() {
        return this.internalError;
    }

    /**
     * @return the rccommand
     */
    public final RCCOMMAND getRccommand() {
        return this.rccommand;
    }

    /**
     * @return the rccommandArgs
     */
    public String[] getRccommandArgs() {
        return this.rccommandArgs;
    }

    public String getRccommandUID() {
        return this.rccommandUID;
    }

    /**
     * @return the busy
     */
    public final Boolean getBusy() {
        return this.busy;
    }

    /**
     * Timestamp in nanosecond
     * 
     * @return
     */
    public final Long getTimestamp() {
        return this.timestamp;
    }

    public final Long getEventTimestamp() {
        return this.eventTimestamp;
    }

    /**
     * @return the parentState
     */
    public final FSM getParentState() {
        return this.parentState;
    }

    /**
     * @return the parentIsTransitioning
     */
    public final Boolean getParentIsTransitioning() {
        return this.parentIsTransitioning;
    }

    /**
     * @return the parentCurrentTransitionCommand
     */
    public final TRCOMMAND getParentCurrentTransitionCommand() {
        return this.parentCurrentTransitionCommand;
    }

    public final TRCOMMAND getParentPreviousTransitionCommand() {
        return this.parentPreviousTransitionCommand;
    }

    /**
     * @return the parentIsExiting
     */
    public final Boolean getParentIsExiting() {
        return this.parentIsExiting;
    }

    public final Boolean getParentIsInitializing() {
        return this.parentIsInitializing;
    }

    @Override
    public String toString() {
        return "RCApplication [name=" + this.name + ", oksClassName=" + this.oksClassName + ", restartableDuringRun="
               + this.restartableDuringRun + ", ifFailed=" + this.ifFailed + ", ifDies=" + this.ifDies + ", startAt=" + this.startAt
               + ", stopAt=" + this.stopAt + ", startAtState=" + this.startAtState + ", stopAtState=" + this.stopAtState
               + ", allowSpontaneousExit=" + this.allowSpontaneousExit + ", backupHosts=" + this.backupHosts + ", controlsTTCPartitions="
               + this.controlsTTCPartitions + ", ifError=" + this.ifError + ", isController=" + this.isController + ", runningHost="
               + this.runningHost + ", status=" + this.status + ", notResponding=" + this.notResponding + ", membership=" + this.membership
               + ", restarting=" + this.restarting + ", exitCode=" + this.exitCode + ", exitSignal=" + this.exitSignal + ", controller="
               + this.controller + ", segment=" + this.segment + ", state=" + this.state
               + ", isTransitioning=" + this.isTransitioning + ", currentTransitionCommand=" + this.currentTransitionCommand
               + ", currentTransitionCommandUID=" + this.currentTransitionCommandUID + ", previousTransitionCommand="
               + this.previousTransitionCommand + ", transitionTime=" + this.transitionTime + ", internalError=" + this.internalError
               + ", rccommand=" + this.rccommand + ", rccommandArgs=" + Arrays.toString(this.rccommandArgs) + ", rccommandUID="
               + this.rccommandUID + ", busy=" + this.busy + ", timestamp=" + this.timestamp + ", eventTimestamp=" + this.eventTimestamp
               + ", parentState=" + this.parentState + ", parentIsTransitioning=" + this.parentIsTransitioning
               + ", parentCurrentTransitionCommand=" + this.parentCurrentTransitionCommand + ", parentPreviousTransitionCommand="
               + this.parentPreviousTransitionCommand + ", parentIsExiting=" + this.parentIsExiting + ", parentIsInitializing="
               + this.parentIsInitializing + "]";
    }
}
