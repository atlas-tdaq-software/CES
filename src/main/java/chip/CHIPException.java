package chip;

public abstract class CHIPException extends Exception {
    private static final long serialVersionUID = -5062560842597184836L;

    public static class DAQConfigurationError extends CHIPException {
        private static final long serialVersionUID = -1636579820316754456L;

        public DAQConfigurationError(final String errorDesc, final Throwable exCause) {
            super("Error: " + errorDesc + "", exCause);
        }

    }

    public static class CEPConfigurationError extends CHIPException {
        private static final long serialVersionUID = -1485407394066823822L;

        CEPConfigurationError(final String errorDesc) {
            super("Error: " + errorDesc);
        }
        
        CEPConfigurationError(final String errorDesc, final Throwable exCause) {
            super("Error: " + errorDesc + "", exCause);
        }

    }

    public static class InfoNotFound extends CHIPException {
        private static final long serialVersionUID = -1773103550395836416L;

        public InfoNotFound(final String errorDesc, final Throwable exCause) {
            super("Error: " + errorDesc + "", exCause);
        }

    }

    public static class IPCNotFound extends CHIPException {
        private static final long serialVersionUID = -7666683521973722095L;

        public IPCNotFound(final String errorDesc, final Throwable exCause) {
            super("Error: " + errorDesc + "", exCause);
        }

    }

    public static class PublishException extends CHIPException {
        private static final long serialVersionUID = -1485407394066823822L;

        public PublishException(final String errorDesc, final Throwable exCause) {
            super("Error: " + errorDesc + "", exCause);
        }

        public PublishException(final String errorDesc) {
            super("Error: " + errorDesc + "");
        }
    }

    public static class WaitTimeoutException extends CHIPException {
        private static final long serialVersionUID = -1485407394066823822L;

        public WaitTimeoutException(final String errorDesc, final Throwable exCause) {
            super("Error: " + errorDesc + "", exCause);
        }

        public WaitTimeoutException(final String errorDesc) {
            super("Error: " + errorDesc + "");
        }

    }

    public static class ExecutorTriggerException extends CHIPException {
        private static final long serialVersionUID = -1485407394066823822L;

        public ExecutorTriggerException(final String errorDesc, final Throwable exCause) {
            super("Error: " + errorDesc + "", exCause);
        }

    }

    public static class ExecutorRCException extends CHIPException {
        private static final long serialVersionUID = -1485407394066823822L;

        public ExecutorRCException(final String errorDesc, final Throwable exCause) {
            super("Error: " + errorDesc + "", exCause);
        }
    }

    public static class ExecutorISException extends CHIPException {
        private static final long serialVersionUID = -1485407394066823822L;

        public ExecutorISException(final String errorDesc, final Throwable exCause) {
            super("Error: " + errorDesc + "", exCause);
        }

        public ExecutorISException(final String errorDesc) {
            super("Error: " + errorDesc + "");
        }
    }

    public static class ExecutorPMGException extends CHIPException {
        private static final long serialVersionUID = -1485407394066823822L;

        public ExecutorPMGException(final String errorDesc, final Throwable exCause) {
            super("Error: " + errorDesc + "", exCause);
        }

        public ExecutorPMGException(final String errorDesc) {
            super("Error: " + errorDesc + "");
        }
    }

    public static class ExecutorTestException extends CHIPException {
        private static final long serialVersionUID = -1485407394066823822L;

        public ExecutorTestException(final String errorDesc, final Throwable exCause) {
            super("Error: " + errorDesc + "", exCause);
        }

        public ExecutorTestException(final String errorDesc) {
            super("Error: " + errorDesc + "");
        }
    }

    public static class ExecutorIGUIException extends CHIPException {
        private static final long serialVersionUID = -1485407394066823822L;

        public ExecutorIGUIException(final String errorDesc, final Throwable exCause) {
            super("Error: " + errorDesc + "", exCause);
        }

        public ExecutorIGUIException(final String errorDesc) {
            super("Error: " + errorDesc + "");
        }
    }

    /**
     * Constructor.
     * 
     * @param message Error message
     */
    public CHIPException(final String message) {
        super(message);
    }

    /**
     * Constructor.
     * 
     * @param message Error message
     * @param exCause The cause of this exception
     */
    public CHIPException(final String message, final Throwable exCause) {
        super(message, exCause);
    }

    /**
     * Constructor.
     * 
     * @param exCause The cause of this exception
     */
    public CHIPException(final Throwable exCause) {
        super(exCause);
    }

}
