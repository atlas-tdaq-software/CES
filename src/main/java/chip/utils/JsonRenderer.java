package chip.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class JsonRenderer {
    private final static JsonRenderer INSTANCE = new JsonRenderer();
    private final Gson json;

    private JsonRenderer() {
        this.json =
                  new GsonBuilder()
                      .serializeSpecialFloatingPointValues()
                      .disableHtmlEscaping()
                      .serializeNulls()
                      .enableComplexMapKeySerialization()
                      .create();
    }

    public static JsonRenderer instance() {
        return JsonRenderer.INSTANCE;
    }

    public String render(final Object obj) {
        return this.json.toJson(obj);
    }
}
