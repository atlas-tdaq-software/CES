package chip.utils;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.espertech.esper.common.client.EPException;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;


public class ExecutorFactory {
    private static final Log log = LogFactory.getLog(ExecutorFactory.class);

    private static class ExecutorThreadFactory implements ThreadFactory {
        private final AtomicLong n = new AtomicLong(0);
        private final String id;

        public ExecutorThreadFactory(final String threadID) {
            this.id = threadID;
        }

        @Override
        public Thread newThread(final Runnable task) {
            final Thread t = new Thread(task, "pool-" + this.id + "-" + Long.toString(this.n.incrementAndGet()));

            t.setUncaughtExceptionHandler(new UncaughtExceptionHandler() {
                @Override
                public void uncaughtException(final Thread thr, final Throwable e) {
                    if(EPRuntimeDestroyedException.class.isInstance(e) == true) {
                        ExecutorFactory.log.warn("Thread \"" + thr + "\" tried to access the ESPER engine while being destroyed: " + e, e);
                    } else if(EPException.class.isInstance(e) == true) {
                        ExecutorFactory.log.error("Unexpected exception from the ESPER engine in thread \"" + thr + "\": " + e, e);
                    } else {
                        ExecutorFactory.log.error("Uncaught exception in thread \"" + thr + "\": " + e, e);
                    }
                }
            });

            return t;
        }
    }

    static public ThreadPoolExecutor newThreadPool(final String id, final int size, final boolean allowThreadTimeout) {
        final ThreadPoolExecutor tp = new ThreadPoolExecutor(size,
                                                             size,
                                                             60L,
                                                             TimeUnit.SECONDS,
                                                             new LinkedBlockingQueue<Runnable>(),
                                                             new ExecutorThreadFactory(id));
        tp.allowCoreThreadTimeOut(allowThreadTimeout);
        return tp;
    }
}
