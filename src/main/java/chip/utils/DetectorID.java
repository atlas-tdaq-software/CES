package chip.utils;

import rc.HoldTriggerInfoNamed;


public abstract class DetectorID {
    public static HoldTriggerInfoNamed.CausedBy toDetectorCausedByEnum(final int detID) {
        switch(detID) {
            case 0x11:
            case 0x12:
            case 0x13:
                return HoldTriggerInfoNamed.CausedBy.PIX;
            case 0x14:
                return HoldTriggerInfoNamed.CausedBy.IBL;
            case 0x15:
                return HoldTriggerInfoNamed.CausedBy.DBM;
            case 0x21:
            case 0x22:
            case 0x23:
            case 0x24:
                return HoldTriggerInfoNamed.CausedBy.SCT;
            case 0x31:
            case 0x32:
            case 0x33:
            case 0x34:
                return HoldTriggerInfoNamed.CausedBy.TRT;
            case 0x41:
            case 0x42:
            case 0x43:
            case 0x44:
            case 0x45:
            case 0x46:
            case 0x47:
            case 0x48:
            case 0x49:
            case 0x4a:
            case 0x4b:
            case 0x4c:
                return HoldTriggerInfoNamed.CausedBy.LAR;
            case 0x50:
            case 0x51:
            case 0x52:
            case 0x53:
            case 0x54:
                return HoldTriggerInfoNamed.CausedBy.TIL;
            case 0x61:
            case 0x62:
            case 0x63:
            case 0x64:
                return HoldTriggerInfoNamed.CausedBy.MDT;
            case 0x65:
            case 0x66:
                return HoldTriggerInfoNamed.CausedBy.RPC;
            case 0x67:
            case 0x68:
                return HoldTriggerInfoNamed.CausedBy.TGC;
            case 0x6d:
            case 0x6e:
                return HoldTriggerInfoNamed.CausedBy.STGC;
            case 0x69:
            case 0x6A:
                return HoldTriggerInfoNamed.CausedBy.CSC;
            case 0x6b:
            case 0x6c:
                return HoldTriggerInfoNamed.CausedBy.MMEGA;
            case 0x71:
            case 0x72:
            case 0x73:
            case 0x74:
            case 0x75:
                return HoldTriggerInfoNamed.CausedBy.L1CALO;
            case 0x76:
            case 0x77:
                return HoldTriggerInfoNamed.CausedBy.L1CT;
            case 0x7f:
                return HoldTriggerInfoNamed.CausedBy.FTK;
            case 0x81:
                return HoldTriggerInfoNamed.CausedBy.BCM;
            case 0x82:
                return HoldTriggerInfoNamed.CausedBy.LUCID;
            case 0x83:
                return HoldTriggerInfoNamed.CausedBy.ZDC;
            case 0x84:
                return HoldTriggerInfoNamed.CausedBy.ALFA;
            case 0x85:
                return HoldTriggerInfoNamed.CausedBy.AFP;
            case 0x91:
                return HoldTriggerInfoNamed.CausedBy.L1TOPO;
            case 0x92:
            case 0x93:
            case 0x94:
                return HoldTriggerInfoNamed.CausedBy.L1CALO;
            default:
                return HoldTriggerInfoNamed.CausedBy.Other;
        }

    }

    public static HoldTriggerInfoNamed.CausedBy RobIdToDetectorCausedByEnum(final int robId) {
        return DetectorID.toDetectorCausedByEnum((robId >> 16) & 0xFF);
    }
}
