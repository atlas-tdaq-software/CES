package chip;

import is.InfoEnumeration;
import swrod.SwRodApplication;
import swrod.SwRodApplication_Helper;
import swrod.SwRodDataChannel;
import swrod.SwRodDataChannel_Helper;
import swrod.SwRodInputLink_Helper;
import swrod.SwRodRob;
import swrod.SwRodRob_Helper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import DFConfiguration.InputChannel;
import DFConfiguration.InputChannel_Helper;
import DFConfiguration.ReadoutApplication;
import DFConfiguration.ReadoutApplication_Helper;
import DFConfiguration.ReadoutModule;
import DFConfiguration.ReadoutModule_Helper;
import chip.CHIPException.DAQConfigurationError;
import chip.event.config.ApplicationConfig;
import chip.event.config.RCApplicationConfig;
import config.ConfigException;
import dal.BaseApplication;
import dal.Component;
import dal.Component_Helper;
import dal.Computer;
import dal.ComputerProgram;
import dal.Detector;
import dal.Detector_Helper;
import dal.HW_System;
import dal.MasterTrigger;
import dal.RM_Resource;
import dal.ResourceBase;
import dal.ResourceSet;
import dal.ResourceSetAND;
import dal.ResourceSetAND_Helper;
import dal.ResourceSet_Helper;
import dal.RunControlApplicationBase;
import dal.Segment;
import dal.Tag;
import dal.WarmStartStopReactor;
import dal.WarmStartStopReactor_Helper;
import daq.eformat.DetectorMask;


/**
 * Facade class to interact with DAQ configuration.
 * <p>
 * It implements the algorithm to build the application tree (with isControllerBy relation). Non state-aware applications and state aware
 * applications are returned as flat list with two dedicated methods {@link getSimpleApplication} and {@link getRCApplications}
 * <p>
 * It provides list of applications, and not tree, because the actual tree structure can be derived via proper EPL statements linking
 * application to controllers, etc.
 * 
 * @author ganders, lmagnoni
 */

public class DAQConfigReader {
    /**
     * Logger
     */
    private static final Log log = LogFactory.getLog(DAQConfigReader.class);

    /**
     * Collection of application configurations
     */
    private final ConcurrentMap<String, BaseApplication> appsMap = new ConcurrentHashMap<>();

    /**
     * Collection of non state-aware applications
     */
    private final ConcurrentMap<String, ApplicationConfig> baseApplications = new ConcurrentHashMap<>();

    /**
     * Collection of state aware applications
     */
    private final ConcurrentMap<String, RCApplicationConfig> rcApplications = new ConcurrentHashMap<>();

    private final ConcurrentMap<String, String> applicationToSegmentMap = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, ConcurrentSkipListSet<Integer>> segmentsToDetIdMap = new ConcurrentHashMap<>();

    private final ConcurrentMap<String, Segment> segNameToSeg = new ConcurrentHashMap<>();

    private final ConcurrentMap<String, dal.ResourceBase> resources = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, Component> dynDisabledComponents = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, ReadoutApplication> roApps = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, InputChannel> channelMapChannel = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, ReadoutApplication> channelMapRoApp = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, SwRodApplication> elinkMapSwRod = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, ConcurrentSkipListSet<String>> warmStartApps = new ConcurrentHashMap<>();
    private final ConcurrentMap<String, ConcurrentSkipListSet<String>> warmStopApps = new ConcurrentHashMap<>();

    private final Set<String> dcmParents = new ConcurrentSkipListSet<>();

    /**
     * Config object
     */
    private volatile config.Configuration config;

    private final String configPlugin;
    private final String partName;
    private final String tdaqAppName;

    /**
     * Dal partition object
     */
    private volatile dal.Partition dalPartition;

    // Inner Enum used to mark the type of application to elaborate the startAt/StopAtState field
    public static enum APP_TYPE {
        ONLINE_SEGMENT,
        OS_INFRASTRUCTURE,
        INFRASTRUCTURE,
        RUNCONTROL,
        OTHER
    }

    // 0 - server name
    // 1 - info name
    // 2 - attribute name
    private volatile String[] maxEventISInfo = null;

    /**
     * Ctor.
     * 
     * @param configPlugin the type of configuration plugin (i.e. "oksconfig", "rdbconfig", etc)
     * @param configValue the input string for the specified plugin type
     * @param partition the partition name
     */
    public DAQConfigReader(final String partition, final String configPlugin, final String tdaqAppName) {
        this.configPlugin = configPlugin;
        this.partName = partition;
        this.tdaqAppName = tdaqAppName;
    }

    /**
     * Read DAQ configuration in a tree-like way, starting from the Online_segment, then going deeper on inner segments. While visiting the
     * tree the following fields are computed: <li>
     * <ul>
     * isControlledBy relation to link each application to the responsible controller
     * </ul>
     * <ul>
     * startAtState, stopAtState: overriding user defined startAt/StopAt field if needed
     * </ul>
     * </li>
     * <p>
     * As result of this, the collection of state aware and non state aware applications are populated.
     * <p>
     * This method is annotated with @PostConstruct annotation to assure it is executed by the context manager after the been is created.
     * 
     * @throws CHIPException
     */
    // @PostConstruct
    public void read() throws CHIPException.DAQConfigurationError {
        DAQConfigReader.log.debug("Reading DAQ configuration for partition: " + this.partName + ", config " + this.configPlugin);

        try {
            this.config = new config.Configuration(this.configPlugin);
    
            this.dalPartition = dal.Partition_Helper.get(this.config, this.partName);
    
            this.config.register_converter(new dal.SubstituteVariables(this.dalPartition));
    
            DAQConfigReader.log.trace("Reading DAQ configuration...");
            DAQConfigReader.log.debug("Partition has " + this.dalPartition.get_Segments().length + " first-layer segments");
                
            this.appsMap.clear();
            this.baseApplications.clear();
            this.warmStartApps.clear();
            this.warmStopApps.clear();
            this.rcApplications.clear();
            this.dcmParents.clear();
    
            this.applicationToSegmentMap.clear();
            this.segmentsToDetIdMap.clear();
    
            this.segNameToSeg.clear();
            this.resources.clear();
            this.dynDisabledComponents.clear();
            this.roApps.clear();
            this.channelMapChannel.clear();
            this.channelMapRoApp.clear();
            this.elinkMapSwRod.clear();
    
            this.maxEventISInfo = this.retrieveMaxEventIS();
    
            /**
             * Build a map of all BaseApplication objects (needed for execution of application tests)
             */
    
            BaseApplication[] apps = this.dalPartition.get_all_applications(null, null, null);
            for(final BaseApplication app : apps) {
                this.appsMap.put(app.UID(), app);
            }
    
            /**
             * Online_infra
             */
            final dal.OnlineSegment onlines = this.dalPartition.get_OnlineInfrastructure();
        
            final dal.Segment seg = this.dalPartition.get_segment(onlines.UID());
                
            this.parseOnlineSegment(seg, seg.get_controller().UID());
        
            for(final dal.Segment s : seg.get_nested_segments()) {
                if(s.is_disabled() == false) {
                    this.parseSegment(s, seg.get_controller().UID(), seg.UID());
                }
            }
    
            /**
             * Read in all partition resources (enabled and disabled)
             */
            final List<Segment> segsFlattened = new ArrayList<Segment>();
            this.getAllSubSegments(seg, segsFlattened);
    
            for(final dal.Segment s : segsFlattened) {
                final ResourceBase[] resBase = s.get_Resources();
                final List<ResourceBase> resBaseFlattened = new ArrayList<ResourceBase>();
                this.createFlatListOfResources(resBase, resBaseFlattened);
                for(final ResourceBase rb : resBaseFlattened) {
                    this.resources.put(rb.UID(), rb);
                }
            }
    
            /**
             * Build map of readout applications, additionally build map of channels and corresponding ROS applications
             */
    
            final String useSegments[] = null;
            final dal.Computer[] useHosts = null;
            final BaseApplication[] readoutApps = this.dalPartition.get_all_applications(new String[] {"ReadoutApplication"}, useSegments, useHosts);
    
            for(final BaseApplication app : readoutApps) {
                final ReadoutApplication dfApp = ReadoutApplication_Helper.cast(app);
                if(dfApp != null) {
                    this.roApps.put(dfApp.UID(), dfApp);
                    this.buildChannelMap(dfApp);
                }
            }
    
            /**
             * Dynamically disabled resources
             */
    
            try {
                final ipc.Partition part = new ipc.Partition(this.getPartitionName());
                final Pattern regExpPattern = Pattern.compile("StoplessRecoveryDisabled:.*");
                final is.Criteria criteria = new is.Criteria(regExpPattern);
                final InfoEnumeration it = new InfoEnumeration(part, "Resources", criteria);
                for(final is.Info info : it) {
                    final String infoName = info.getName();
                    final String[] infoNameSplitted = infoName.split(":", 2);
                    if(infoNameSplitted.length == 2) {
                        final String resName = infoNameSplitted[1];
                        final Component res = Component_Helper.get(this.config, resName);
                        if(res != null) {
                            this.dynDisabledComponents.put(resName, res);
                            DAQConfigReader.log.debug("Found disabled resource " + resName + " in IS.");
                        }
                    }
                }
                
                final Component[] compArr = new Component[this.dynDisabledComponents.size()];
                int i = 0;
                for(final Component comp : this.dynDisabledComponents.values()) {
                    compArr[i] = comp;
                    i++;
                }
                
                this.dalPartition.set_disabled(compArr);
            }
            catch(final Exception ex) {
                DAQConfigReader.log.warn("Failed to restore disabled resources from IS.", ex);
            }
    
            DAQConfigReader.log.debug("Reading done.");
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Some error occurred reading the configuration: " + ex, ex);
        }
    }

    private void parseOnlineSegment(final dal.Segment s, final String fatherController) throws CHIPException.DAQConfigurationError {
        this.createApplication(s, fatherController, DAQConfigReader.APP_TYPE.ONLINE_SEGMENT);
        
        // keep track of segment names:
        this.segNameToSeg.put(s.UID(), s);
    }

    private void parseSegment(final dal.Segment s, final String fatherController, final String parentSegment)
        throws CHIPException.DAQConfigurationError
    {        
        this.createApplication(s, fatherController, null);
        
        // keep track of segment names:
        this.segNameToSeg.put(s.UID(), s);

        try {
            final HW_System[] hws = s.get_UsesSystems();
            for(final HW_System hw : hws) {
                final Detector d = Detector_Helper.cast(hw);
                if(d != null) {
                    final int detId = Byte.toUnsignedInt(d.get_LogicalId());
                    if(detId != 0) {
                        final ConcurrentSkipListSet<Integer> ids = new ConcurrentSkipListSet<>();
                        ids.add(Integer.valueOf(detId));
                        
                        final Set<Integer> old = this.segmentsToDetIdMap.putIfAbsent(s.UID(), ids);
                        if(old != null) {
                            old.addAll(ids);
                        }
                    }
                }
            }
            
            for(final dal.Segment inner_segment : s.get_nested_segments()) {
                if(inner_segment.is_disabled() == false) {
                    this.parseSegment(inner_segment, s.get_controller().UID(), s.UID());
                }
            }
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Failed to parse configuration for segment " + s.UID() + ": " + ex, ex);
        }
    }

    /**
     * It retrieves the list of detector IDs associated to a ROS
     * 
     * @param ros The ROS application
     * @return The IDs of detectors corresponding to the ROBs hosted in the ROS
     * @throws DAQConfigurationError 
     */
    private Set<Integer> getDetectors(final DFConfiguration.ROS ros) throws DAQConfigurationError {
        try {
            final Set<Integer> dets = new HashSet<>();
    
            int detId = 0;
            final Detector det = ros.get_Detector();
            // Cast needed because of a wrong mapping between CORBA types and Java
            // (byte is signed in Java but the original type is u8 in C++)
            if((det != null) && ((detId = Byte.toUnsignedInt(det.get_LogicalId())) != 0)) {
                dets.add(Integer.valueOf(detId));
            } else {
                final ResourceBase[] robins = ros.get_Contains();
                for(final ResourceBase r : robins) {
                    final ResourceSet robin = ResourceSet_Helper.cast(r);
                    if(robin != null) {
                        final ResourceBase[] robs = robin.get_Contains();
                        for(final ResourceBase rr : robs) {
                            final DFConfiguration.InputChannel rob = DFConfiguration.InputChannel_Helper.cast(rr);
                            if(rob != null) {
                                final int robId = rob.get_Id();
                                dets.add(Integer.valueOf((robId >> 16) & 0xFF));
                            }
                        }
                    }
                }
            }
    
            return dets;
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Could not get from configuration the detector IDs associated to ROS " + ros.UID() + ": " + ex, ex);
        }
    }

    /**
     * It returns the ID of the detector associated to the <em>ro</em> ReadoutApplication application
     * 
     * @param ro The ReadoutApplication application
     * @return The ID of the detector (zero if no detector is associated to the application)
     * @throws DAQConfigurationError 
     */
    private int getDetector(final DFConfiguration.ReadoutApplication ro) throws DAQConfigurationError {
        try {
            int detId = 0;
    
            final Detector det = ro.get_Detector();
            if(det != null) {
                // Cast needed because of a wrong mapping between CORBA types and Java
                // (byte is signed in Java but the original type is u8 in C++)
                detId = Byte.toUnsignedInt(det.get_LogicalId());
            }
    
            return detId;
        }
        catch(final ConfigException ex) {
            throw new DAQConfigurationError("Cannot retrieve from configuration the detector ID associated to " + ro.UID() + ": " + ex, ex);
        }
    }

    private void checkWarmStartStopApp(final String parent, final BaseApplication app) throws DAQConfigurationError {
        try {
            final WarmStartStopReactor wsa = WarmStartStopReactor_Helper.cast(app);
            if(wsa != null) {
                final ConcurrentSkipListSet<String> apps = new ConcurrentSkipListSet<>();
                final String appID = app.UID();
                
                if(wsa.get_RestartAtWarmStart() == true) {
                    apps.add(appID);
                    
                    final ConcurrentSkipListSet<String> old = this.warmStartApps.putIfAbsent(parent, apps);
                    if(old != null) {
                        old.add(appID);
                    }
                }
                
                if(wsa.get_RestartAtWarmStop() == true) {                
                    apps.add(appID);
                    
                    final ConcurrentSkipListSet<String> old = this.warmStopApps.putIfAbsent(parent, apps);
                    if(old != null) {
                        old.add(appID);
                    }                        
                }
            }
        }
        catch(final ConfigException ex) {
            throw new DAQConfigurationError("Cannot retrieve from configuration the list of applications to restarted at warm start/stop: " + ex, ex);
        }

    }
    
    private void createApplication(final dal.Segment s, final String fatherController, final DAQConfigReader.APP_TYPE type)
        throws CHIPException.DAQConfigurationError
    {
        DAQConfigReader.log.debug("Parsing segment " + s.UID() + " for applications");

        try {
            // Segment controller
            final dal.BaseApplication ctrl = s.get_controller();
    
            final String controller = ctrl.UID();
            DAQConfigReader.log.debug("Segment controller: " + controller);
    
            // keep track of which application belongs to which segment
            this.applicationToSegmentMap.put(controller, s.UID());
    
            this.rcApplications.put(controller,
                                    RCApplicationConfig.createConfigRCApplication(ctrl,
                                                                                  dal.RunControlApplicationBase_Helper.cast(ctrl),
                                                                                  fatherController,
                                                                                  type == DAQConfigReader.APP_TYPE.ONLINE_SEGMENT
                                                                                                                                 ? type
                                                                                                                                 : DAQConfigReader.APP_TYPE.RUNCONTROL,
                                                                                  s.UID(),
                                                                                  Boolean.TRUE));
    
            // Infrastructure applications
            final dal.BaseApplication[] infrApps = s.get_infrastructure();
            if(infrApps != null) {
                for(final dal.BaseApplication app : infrApps) {
    
                    // keep track of which application belongs to which segment
                    this.applicationToSegmentMap.put(app.UID(), s.UID());
    
                    this.baseApplications.put(app.UID(),
                                              ApplicationConfig.createConfigApplication(this.config,
                                                                                        app,
                                                                                        fatherController,
                                                                                        type == DAQConfigReader.APP_TYPE.ONLINE_SEGMENT
                                                                                                                                       ? DAQConfigReader.APP_TYPE.OS_INFRASTRUCTURE
                                                                                                                                       : DAQConfigReader.APP_TYPE.INFRASTRUCTURE,
                                                                                        s.UID()));
                    this.checkWarmStartStopApp(fatherController, app);
                }
            }
    
            // Other applications
            final dal.BaseApplication[] otherApps = s.get_applications();
            if(otherApps != null) {
                for(final dal.BaseApplication app : otherApps) {
                    // keep track of which application belongs to which segment
                    this.applicationToSegmentMap.put(app.UID(), s.UID());
    
                    // Check if it's a RunControlApplication
                    final dal.RunControlApplicationBase rcApp = dal.RunControlApplicationBase_Helper.cast(app);
                    if(rcApp != null) {
                        this.rcApplications.put(app.UID(),
                                                RCApplicationConfig.createConfigRCApplication(app,
                                                                                              rcApp,
                                                                                              controller,
                                                                                              type == DAQConfigReader.APP_TYPE.ONLINE_SEGMENT
                                                                                                                                             ? type
                                                                                                                                             : DAQConfigReader.APP_TYPE.RUNCONTROL,
                                                                                              s.UID(),
                                                                                              Boolean.FALSE));
    
                        if(rcApp.class_name().equals("DcmApplication") == true) {
                            this.dcmParents.add(controller);
                        }
    
                    } else {
                        this.baseApplications.put(app.UID(),
                                                  ApplicationConfig.createConfigApplication(this.config,
                                                                                            app,
                                                                                            controller,
                                                                                            type == DAQConfigReader.APP_TYPE.ONLINE_SEGMENT
                                                                                                                                           ? type
                                                                                                                                           : DAQConfigReader.APP_TYPE.OTHER,
                                                                                            s.UID()));
                    }
    
                    this.checkWarmStartStopApp(controller, app);
                    
                    // Associate the segment the application belongs to, with the corresponding detector IDs
                    // This is valid only for ROS and ReadoutApplication applications
                    final ConcurrentSkipListSet<Integer> segDets = new ConcurrentSkipListSet<>();
                    DFConfiguration.ReadoutApplication roApp = null;
                    final DFConfiguration.ROS rosApp = DFConfiguration.ROS_Helper.cast(app);
                    if(rosApp != null) {
                        segDets.addAll(this.getDetectors(rosApp));
                    } else if((roApp = DFConfiguration.ReadoutApplication_Helper.cast(app)) != null) {
                        final int det = this.getDetector(roApp);
                        if(det != 0) {
                            segDets.add(Integer.valueOf(det));
                        }
                    }
    
                    if(segDets.isEmpty() == false) {
                        final Set<Integer> d = this.segmentsToDetIdMap.putIfAbsent(s.UID(), segDets);
                        if(d != null) {
                            d.addAll(segDets);
                        }
                    }
                }
            }
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Failed to parse segment " + s.UID() + ": " + ex, ex);
        }
    }

    String[] getMaxEventIS() {
        return this.maxEventISInfo;
    }

    public BaseApplication getRootController() throws DAQConfigurationError {
        try {
            final dal.OnlineSegment onlines = this.dalPartition.get_OnlineInfrastructure();
            final dal.Segment seg = this.dalPartition.get_segment(onlines.UID());
            return seg.get_controller();
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Cannot get the Root Controller from the database: " + ex, ex);
        }
    }
    
    public Map<String, Segment> getSegConfiguration() {
        return Collections.unmodifiableMap(this.segNameToSeg);
    }
    
    /**
     * Return the set of application configurations
     * 
     * @return the set of application configurations
     */
    public Map<String, BaseApplication> getAppConfiguration() {
        return Collections.unmodifiableMap(this.appsMap);
    }

    /**
     * Return the set of non state-aware application
     * 
     * @return the set of non state-aware application
     */
    public Map<String, ApplicationConfig> getBaseApplications() {
        return Collections.unmodifiableMap(this.baseApplications);
    }

    /**
     * Return applications to be restarted at warm start
     * 
     * @return Applications to be restarted at warm start (the map's key is the parent of applications)
     */
    public Map<String, ConcurrentSkipListSet<String>> getWarmStartApps() {
        return Collections.unmodifiableMap(this.warmStartApps);
    }
    
    /**
     * Return applications to be restarted at warm stop
     * 
     * @return Applications to be restarted at warm stop (the map's key is the parent of applications)
     */    
    public Map<String, ConcurrentSkipListSet<String>> getWarmStopApps() {
        return Collections.unmodifiableMap(this.warmStopApps);
    }
    
    /**
     * Return the set of state-aware application
     * 
     * @return the set of state-aware application
     */
    public Map<String, RCApplicationConfig> getRCApplications() {
        return Collections.unmodifiableMap(this.rcApplications);
    }

    /**
     * Return the set of readout application dal objects indexed by the application name
     * 
     * @return the set of readout application dal objects indexed by the application name
     */
    public Map<String, ReadoutApplication> getReadoutApplications() {
        return Collections.unmodifiableMap(this.roApps);
    }

    /**
     * Return the set of dal channel objects indexed by channel name
     * 
     * @return the set of dal channel objects indexed by channel name
     */
    public Map<String, InputChannel> getChannelMapChannel() {
        return Collections.unmodifiableMap(this.channelMapChannel);
    }

    /**
     * Return the set of SW ROD applications indexed by ELink name
     * 
     * @return the set of SW ROD applications indexed by ELink name
     */
    public Map<String, SwRodApplication> getElinkMapSwRod() {
        return Collections.unmodifiableMap(this.elinkMapSwRod);
    }
    
     /**
     * Return the set of dal readout applications indexed by channel name
     * 
     * @return the set of dal readout applications indexed by channel name
     */
    public Map<String, ReadoutApplication> getChannelMapRoApp() {
        return Collections.unmodifiableMap(this.channelMapRoApp);
    }

    /**
     * Return the set of dynamically disabled resources
     * 
     * @return the set of dynamically disabled resources
     */
    public Map<String, Component> getDynDisabledComponents() {
        return Collections.unmodifiableMap(this.dynDisabledComponents);
    }

    /**
     * Return the list of segments having at least one DCM application
     * 
     * @return The list of segments having at least one DCM application
     */
    public Set<String> getDCMSegments() {
        return Collections.unmodifiableSet(this.dcmParents);
    }

    /**
     * Return the dal partition object
     * 
     * @return the dal partition object
     */
    public dal.Partition getDalPartition() {
        return this.dalPartition;
    }

    public config.Configuration getConfig() {
        return this.config;
    }

    synchronized public String[] resetDynamicallyDisabledComponents() throws DAQConfigurationError {
        final Set<String> disabledSet = this.dynDisabledComponents.keySet();
        final String[] disabled = disabledSet.toArray(new String[disabledSet.size()]);
        this.enableComponents(disabled);

        this.dynDisabledComponents.clear();
        return disabled;
    }

    public Map<String, dal.ResourceBase> getResources() {
        return Collections.unmodifiableMap(this.resources);
    }

    synchronized private void getAllSubSegments(final Segment sc, final List<Segment> segmentList) throws DAQConfigurationError {
        segmentList.add(sc);
        
        try {
            for(final dal.Segment scInner : sc.get_nested_segments()) {
                this.getAllSubSegments(scInner, segmentList);
            }
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Failed to retrieve all the sub-segments of segment " + sc.UID() + ": " + ex, ex);
        }
    }

    synchronized private void createFlatListOfResources(final ResourceBase[] resBase, final List<ResourceBase> resBaseFlattened) throws DAQConfigurationError {
        try {
            for(final ResourceBase rb : resBase) {
                resBaseFlattened.add(rb);
                final ResourceSet resSet = ResourceSet_Helper.cast(rb);
                if(resSet != null) {
                    this.createFlatListOfResources(resSet.get_Contains(), resBaseFlattened);
                }
            }
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Failed to parse segment get the list of Resources: " + ex, ex);
        }

    }

    synchronized public String getMasterTriggerController() {
        String controllerName = "unknown-MT";

        try {
            final MasterTrigger mt = this.dalPartition.get_MasterTrigger();
            if(mt != null) {
                final RunControlApplicationBase rc = mt.get_Controller();
                if(rc != null) {
                    controllerName = rc.UID();
                    DAQConfigReader.log.info("The partition Master Trigger is " + controllerName);
                } else {
                    DAQConfigReader.log.warn("The Master Trigger does not define a controller; it will not be possible to hold or resume the trigger");
                }
            } else {
                DAQConfigReader.log.warn("The Master Trigger is not defined in the partition; it will not be possible to hold or resume the trigger");
            }
        }
        catch(final Exception ex) {
            DAQConfigReader.log.error("Error looking for the partition Master Trigger: " + ex, ex);
        }

        return controllerName;

    }

    public String getPartitionName() {
        return this.partName;
    }

    public String getApplicationName() {
        return this.tdaqAppName;
    }

    synchronized public DetectorMask getDetMaskForApplication(final String application) {
        final DetectorMask dm = new DetectorMask();

        try {
            final BaseApplication app = this.appsMap.get(application);
            if(app != null) {
                DFConfiguration.ReadoutApplication roApp = null;
    
                final DFConfiguration.ROS ros = DFConfiguration.ROS_Helper.cast(app);
                if(ros != null) {
                    // The application is a ROS
                    final Set<Integer> dets = this.getDetectors(ros);
                    for(final Integer id : dets) {
                        try {
                            dm.set(DetectorMask.fromDetectorID(id.intValue()));
                        }
                        catch(final IllegalArgumentException ex) {
                            DAQConfigReader.log.error("Error determining the detector mask associated to application " + application, ex);
                        }
                    }
                } else if((roApp = DFConfiguration.ReadoutApplication_Helper.cast(app)) != null) {
                    // The application is a ReadoutApplication
                    final int detId = this.getDetector(roApp);
                    if(detId != 0) {
                        try {
                            dm.set(DetectorMask.fromDetectorID(detId));
                        }
                        catch(final IllegalArgumentException ex) {
                            DAQConfigReader.log.error("Error determining the detector mask associated to application " + application, ex);
                        }
                    }
                } else {
                    // Look at the segment the application belongs to
                    final String segmentId = this.applicationToSegmentMap.get(application);
                    if(segmentId != null) {
                        final Set<Integer> ids = new HashSet<>();
    
                        {
                            final Set<Integer> thisSegDets = this.segmentsToDetIdMap.get(segmentId);
                            if(thisSegDets != null) {
                                ids.addAll(thisSegDets);
                            }
                        }
    
                        // Look into the nested segments
                        {
                            final dal.Segment seg = this.segNameToSeg.get(segmentId);
                            final List<Segment> segsFlattened = new ArrayList<Segment>();
                            this.getAllSubSegments(seg, segsFlattened);
                            for(final Segment sc : segsFlattened) {
                                if(sc.is_disabled() == false) {
                                    final Set<Integer> segDets = this.segmentsToDetIdMap.get(sc.UID());
                                    if(segDets != null) {
                                        ids.addAll(segDets);
                                    }
                                }
                            }
                        }
    
                        for(final Integer id : ids) {
                            try {
                                dm.set(DetectorMask.fromDetectorID(id.intValue()));
                            }
                            catch(final IllegalArgumentException ex) {
                                DAQConfigReader.log.error("Error determining the detector mask associated to application " + application, ex);
                            }
                        }
                    } else {
                        DAQConfigReader.log.error("Cannot find the segment application \"" + application + "\" belongs to");
                    }
                }
            } else {
                DAQConfigReader.log.warn("Application " + application + " does not exist or is not valid");
            }    
        }
        catch(final DAQConfigurationError | ConfigException ex) {
            DAQConfigReader.log.warn("Some configuration error occurred while determining the detector mask associated to application " + application);
        }
        
        if(dm.get().isEmpty() == true) {
            DAQConfigReader.log.warn("Failed to find a detector ID for application " + application);
        }

        return dm;
    }

    synchronized private void buildChannelMap(final ReadoutApplication ra) throws DAQConfigurationError {
        DAQConfigReader.log.trace("Building map for ReadoutApplication '" + ra.UID() + "'");

        try {
            final SwRodApplication swRodApp = SwRodApplication_Helper.cast(ra);

            DAQConfigReader.log.trace("Building map for ReadoutApplication '" + ra.UID() + "'");
            final List<InputChannel> channels = this.getChannels(ra);
            DAQConfigReader.log.trace("'" + ra.UID() + "' has " + channels.size() + " channels");
            for(final InputChannel channel : channels) {
                this.channelMapChannel.put(channel.UID(), channel);
                this.channelMapRoApp.put(channel.UID(), ra);

                // Add ELinks for SWROD
                if(swRodApp != null) {
                    final SwRodRob swRodRob = SwRodRob_Helper.cast(channel);
                    if(swRodRob != null) {
                        for(final ResourceBase swRodDataChannelResource : swRodRob.get_Contains()) {
                            final SwRodDataChannel swRodDC = SwRodDataChannel_Helper.cast(swRodDataChannelResource);
                            if(swRodDC != null) {
                                for(final ResourceBase elink : swRodDC.get_Contains()) {
                                    if(SwRodInputLink_Helper.cast(elink) != null) {
                                        this.elinkMapSwRod.put(elink.UID(), swRodApp);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Cannot build the channel map for application " + ra.UID() + ": " + ex, ex);
        }
    }

    synchronized private List<InputChannel> getChannels(final ReadoutApplication ra) throws DAQConfigurationError {
        try {
            final List<InputChannel> channels = new ArrayList<InputChannel>();
            
            final ResourceBase[] rbArr = ra.get_Contains();
            for(final ResourceBase rb : rbArr) {
                final ReadoutModule rm = ReadoutModule_Helper.cast(rb);
                if(rm != null) {
                    final ResourceSetAND rand = ResourceSetAND_Helper.cast(rm);
                    if(rand != null) {
                        final ResourceBase[] rsArr = rand.get_Contains();
                        for(final ResourceBase rs : rsArr) {
                            final InputChannel channel = InputChannel_Helper.cast(rs);
                            if(channel != null) {
                                DAQConfigReader.log.trace("Adding channel " + channel.UID() + " for module: " + rm.UID()
                                                          + " and ReadoutApplication: " + ra.UID());
                                channels.add(channel);                            
                            }
                        }
                    }
                }
            }
            
            return channels;
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Cannot retrieve the list of input channels for application " + ra.UID() + ": " + ex, ex);
        }        
    }

    synchronized public void disableComponents(final String[] compNames) throws DAQConfigurationError {
        try {    
            // disable given resources
            for(final String name : compNames) {
                final Component component = Component_Helper.cast(this.resources.get(name));
                if(component != null) {
                    this.dynDisabledComponents.put(name, component);
                }
            }
            
            final Component[] compArr = this.dynDisabledComponents.values().toArray(new Component[this.dynDisabledComponents.size()]);
            this.dalPartition.set_disabled(compArr);    
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Cannot disable components: " + ex, ex);
        }        
    }    
    
    // It returns a list of components disabled as a consequence of disabling "compNames"
    synchronized public List<String> disableComponentsAndGet(final String[] compNames) throws DAQConfigurationError {
        try {
            // currently disabled resources
            final Set<String> disabledComps = new HashSet<String>();
            for(final Map.Entry<String, ResourceBase> entry : this.resources.entrySet()) {
                final String name = entry.getKey();
                final ResourceBase res = entry.getValue();
                if(res.disabled(this.dalPartition)) {
                    disabledComps.add(name);
                }
            }
    
            // now disable given resources
            for(final String name : compNames) {
                final Component component = Component_Helper.cast(this.resources.get(name));
                if(component != null) {
                    this.dynDisabledComponents.put(name, component);
                }
            }
            
            final Component[] compArr = this.dynDisabledComponents.values().toArray(new Component[this.dynDisabledComponents.size()]);
            this.dalPartition.set_disabled(compArr);
    
            // search for all newly disabled resources
            final List<String> newDisabledComps = new ArrayList<String>();
            for(final Map.Entry<String, ResourceBase> entry : this.resources.entrySet()) {
                final String name = entry.getKey();
                final ResourceBase res = entry.getValue();
                if(res.disabled(this.dalPartition)) {
                    if(disabledComps.contains(name) == false) {
                        newDisabledComps.add(name);
                        DAQConfigReader.log.trace("New disabled component: " + name);
                    }
    
                }
            }
    
            return newDisabledComps;
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Cannot disable components: " + ex, ex);
        }        
    }

    synchronized public void enableComponents(final String[] compNames) throws DAQConfigurationError {
        try {    
            // enable given resources
            for(final String name : compNames) {
                final Component component = Component_Helper.cast(this.resources.get(name));
                if(component != null) {
                    this.dynDisabledComponents.remove(name);
                }
            }
            
            final Component[] compArr = this.dynDisabledComponents.values().toArray(new Component[this.dynDisabledComponents.size()]);
            this.dalPartition.set_disabled(compArr);    
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Cannot enable components: " + ex, ex);
        }        
    }    
    
    // It returns a list of components enabled as a consequence of enabling "compNames"
    synchronized public List<String> enableComponentsAndGet(final String[] compNames) throws DAQConfigurationError {
        try {
            // currently enabled resources
            final Set<String> enabledComps = new HashSet<>();
            for(final Map.Entry<String, ResourceBase> entry : this.resources.entrySet()) {
                final String name = entry.getKey();
                final ResourceBase res = entry.getValue();
                if(!res.disabled(this.dalPartition)) {
                    enabledComps.add(name);
                }
            }
    
            // now enable given resources
            for(final String name : compNames) {
                final Component component = Component_Helper.cast(this.resources.get(name));
                if(component != null) {
                    this.dynDisabledComponents.remove(name);
                }
            }
            
            final Component[] compArr = this.dynDisabledComponents.values().toArray(new Component[this.dynDisabledComponents.size()]);
            this.dalPartition.set_disabled(compArr);
    
            // search for all newly enabled resources
            final List<String> newEnabledComps = new ArrayList<String>();
            for(final Map.Entry<String, ResourceBase> entry : this.resources.entrySet()) {
                final String name = entry.getKey();
                final ResourceBase res = entry.getValue();
                if(!res.disabled(this.dalPartition)) {
                    if(enabledComps.contains(name) == false) {
                        newEnabledComps.add(name);
                        DAQConfigReader.log.trace("New enabled component: " + name);
                    }
                }
            }
    
            return newEnabledComps;
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Cannot enable components: " + ex, ex);
        }        
    }

    synchronized public List<String> getResourcesOfSegment(final String segmentId) throws DAQConfigurationError {
        try {
            final List<String> segmentResources = new ArrayList<String>();
    
            final dal.Segment segConf = this.segNameToSeg.get(segmentId);
    
            final List<Segment> segsFlattened = new ArrayList<Segment>();
            this.getAllSubSegments(segConf, segsFlattened);
    
            for(final dal.Segment s : segsFlattened) {
                final ResourceBase[] resBase = s.get_Resources();
                final List<ResourceBase> resBaseFlattened = new ArrayList<ResourceBase>();
                this.createFlatListOfResources(resBase, resBaseFlattened);
                for(final ResourceBase rb : resBaseFlattened) {
                    segmentResources.add(rb.UID());
                }
            }
    
            return segmentResources;
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Cannot get the list of resources for segment " + segmentId + ": " + ex, ex);
        }        

    }

    public void getProgramInfo(final Map<String, String> env,
                               final List<String> progNames,
                               final List<RM_Resource> needs,
                               final String computerProgUID,
                               final String hostName) throws DAQConfigurationError
    {

        try {
            final ComputerProgram computerProg = dal.ComputerProgram_Helper.get(this.config, computerProgUID);
            final Computer computer = dal.Computer_Helper.get(this.config, hostName);
    
            final Tag[] defaultTags = this.dalPartition.get_DefaultTags();
    
            Tag supportedTag = null;
    
            for(final Tag tag : defaultTags) {
                if(dal.Algorithms.is_compatible(tag, computer, this.dalPartition)) {
                    supportedTag = tag;
                }
                break;
            }
    
            if(supportedTag == null) {
                // TODO throw error
            }
    
            for(final RM_Resource res : computerProg.get_Needs()) {
                needs.add(res);
            }
    
            computerProg.get_info(env, progNames, this.dalPartition, supportedTag, computer);
        }
        catch(final ConfigException ex) {
            throw new CHIPException.DAQConfigurationError("Cannot retrieve information for program " + computerProgUID + ": " + ex, ex);
        }        
    }

    private String[] retrieveMaxEventIS() throws ConfigException {
        // default values
        String serverName = "DF";
        String infoName = "HLTSV";
        String attributeName = "ProcessedEvents";

        final dal.IS_InformationSources isInfoSources = this.dalPartition.get_IS_InformationSource();
        if(isInfoSources != null) {
            final dal.IS_EventsAndRates dalIS = isInfoSources.get_HLT();
            if(dalIS != null) {
                final String counter = dalIS.get_EventCounter();
                if((counter != null) && (counter.length() > 0)) {
                    // Fields are separated by dots
                    final String[] tokens = counter.split("\\.");
                    if(tokens.length >= 3) {
                        serverName = tokens[0];
                        attributeName = tokens[tokens.length - 1];

                        // Calculate the attribute name: it can have '.'
                        final java.lang.StringBuilder pathName = new java.lang.StringBuilder();
                        for(int i = 1; i < (tokens.length - 1); ++i) {
                            pathName.append(tokens[i] + ".");
                        }

                        infoName = pathName.substring(0, pathName.length() - 1); // Do not take into account last '.'
                    } else {
                        System.err.println("Invalid format for max event counter (" + counter + ")"
                                           + " it must have the \"server_name.info_name.attribute_name\" format; using defaults");
                    }
                } else {
                    System.err.println("No IS source defined for max event counter, using defaults");
                }
            }
        }

        return new String[] {serverName, infoName, attributeName};
    }

    // public static void main(String[] args) throws DAQConfigurationError {
    // final DAQConfigReader dc = new DAQConfigReader("ATLAS", "oksconfig:./ATLAS_merged.data.xml", "Boh");
    // dc.read();
    // for(final Map.Entry<String, Set<Integer>> me : dc.segmentsToDetIdMap.entrySet()) {
    // final Set<Integer> dets = me.getValue();
    // final DetectorMask dmask = new DetectorMask();
    // for(final Integer i : dets) {
    // dmask.set(DetectorMask.fromDetectorID(i.intValue()));
    // }
    //
    // System.out.println(me.getKey() + " -> " + dmask.getSubdetectors());
    // }
    //
    // for(final Map.Entry<String, AppConfig> me : dc.appConfigs.entrySet()) {
    // System.out.println(me.getKey() + " --> " + dc.getDetMaskForApplication(me.getKey()).getSubdetectors());
    // }
    // }
    // }
}
