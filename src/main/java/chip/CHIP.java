package chip;

import ipc.InvalidPartitionException;
import is.Criteria;
import is.InfoList;
import is.InfoNotCompatibleException;
import is.InfoNotFoundException;
import is.RepositoryNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import chip.CEPService.EPLReplacementString;
import chip.CHIPException.CEPConfigurationError;
import chip.CHIPException.DAQConfigurationError;
import chip.CHIPException.ExecutorTriggerException;
import chip.event.config.ApplicationConfig;
import chip.event.config.PixelConfig;
import chip.event.config.RCApplicationConfig;
import chip.event.dynamic.ApplicationInfo;
import chip.event.dynamic.RCApplicationInfo;
import chip.event.issue.IdentifiableIssue;
import chip.event.issue.core.Problem;
import chip.executor.ExecutorTrigger;
import chip.injector.GenericISCallback;
import chip.injector.ISInjector;
import chip.injector.Injector;
import chip.injector.Injectors;
import chip.msg.Internal;
import daq.EsperUtils.CepException.InvalidEventException;
import daq.owl.PMGSync;


/**
 * Central Expert System service. It orchestrates interaction between CEP, event injectors, TDAQ services and Subscribers/Listeners.
 * 
 * @author lucamag, ganders
 */
@Component("chip")
public final class CHIP {

    /**
     * Logger
     */
    private static final Log log = LogFactory.getLog(CHIP.class.getName());

    private final String partitionName;

    /**
     * CEP engine instance retrieved by context. Passed as constructor parameter to allow it to be final.
     */
    private final CEPService cep;

    /**
     * Proxy class to read the configuration from the DAQ config
     */
    private final DAQConfigReader daqconfig;

    /**
     * ExecutorTrigger needs to be initialized with config data ..
     */
    private final ExecutorTrigger execTrigger;

    /**
     * Expert system servant
     */
    private ExpertSystemServant servant;

    // Condition variable predicate
    private Boolean isStopped = Boolean.FALSE;

    private Injectors injectorHelper;

    @Autowired
    private PixelConfig pixConfig;
    
    private final ipc.Partition partition;

    // Using SpEL to get properties list of files
    @Value("#{configuration['chip.runInTestMode']}")
    private boolean isTestMode;

    /**
     * Set of injectors
     */
    private final ConcurrentMap<String, Injector> injectors;

    /**
     * @param cep
     */
    public CHIP(final String partitionName,
                final CEPService cep,
                final DAQConfigReader daqConfiguration,
                final ExecutorTrigger execTrigger)
    {
        this.partitionName = partitionName;
        this.cep = cep;
        this.daqconfig = daqConfiguration;
        this.execTrigger = execTrigger;

        // this.partitionName = (String)prop.getProperty("partition");

        // Using HashMap. O(1) for retrieving data
        this.injectors = new ConcurrentHashMap<String, Injector>();

        CHIP.log.debug("Partition name: " + partitionName);
        this.partition = new ipc.Partition(partitionName);

        /**
         * Registering a new JVM shutdown hook for this thread. The @ShutdownHook thread contains the clean up code for connections and
         * subscriptions.
         */
        final ShutdownHook shutdownHook = new ShutdownHook(this);
        Runtime.getRuntime().addShutdownHook(shutdownHook);
    }

    /**
     * Configuration at startup. Application information are retrieved also from IS.
     */
    public void configure() {
        this.configure(Boolean.FALSE);
    }

    /**
     * Reconfiguration driven by startup. No IS reading.
     */
    public void reconfigure() {
        this.configure(Boolean.TRUE);
    }

    /**
     * CHIP Configuration. - configure CEP service,
     * 
     * @throws ConfigurationException
     */
    private void configure(final Boolean isReload) {
        try {

            /**
             * Initialize DAQ configuration reader
             */
            this.daqconfig.read(); // Read list of application from oks
            
            /**
             * Configure the CEP engine
             */
            this.cep.setEPLReplacementValue(EPLReplacementString.ROOT_CONTROLLER_NAME, this.daqconfig.getRootController().UID());
            
            boolean doRecovery = false;
            boolean doAutoProcedure = false;
            if(this.doRecoveryAndAutoProcedures() == true) {
                doRecovery = true;
                doAutoProcedure = true;
            }
            
            CHIP.log.info("Enable recoveries: " + String.valueOf(doRecovery));
            CHIP.log.info("Enable auto procedures: " + String.valueOf(doAutoProcedure));
            
            this.cep.setEPLReplacementValue(EPLReplacementString.DO_RECOVERY, Boolean.toString(doRecovery));
            this.cep.setEPLReplacementValue(EPLReplacementString.DO_AUTO_PROCEDURE, Boolean.toString(doAutoProcedure));

            final String[] isCounter = this.daqconfig.getMaxEventIS();

            CHIP.log.info("Max events IS server name: " + isCounter[0]);
            CHIP.log.info("Max events IS info name: " + isCounter[1]);
            CHIP.log.info("Max events IS attribute name: " + isCounter[2]);

            this.cep.setEPLReplacementValue(EPLReplacementString.MAX_EVENT_IS_SERVER_NAME, isCounter[0]);
            this.cep.setEPLReplacementValue(EPLReplacementString.MAX_EVENT_IS_INFO_NAME, isCounter[1]);
            this.cep.setEPLReplacementValue(EPLReplacementString.MAX_EVENT_IS_ATTRIBUTE_NAME, isCounter[2]);            
            
            if(isReload.booleanValue()) {
                this.cep.initAndConfigure();
            } else {
                this.cep.configure();
            }
            
            /**
             * Inject some configuration events
             */
     
            this.cep.injectEventSynch(this.pixConfig);

            /**
             * Get the list of non state-aware and state-aware applications. Injects the retrieved config-application events into the cep
             * engine.
             */

            for(final Map.Entry<String, ApplicationConfig> app : this.daqconfig.getBaseApplications().entrySet()) {
                this.cep.injectEventSynch(app.getValue());
                CHIP.log.trace("Simple application " + app.getKey() + " injected.");
            }
            for(final Map.Entry<String, RCApplicationConfig> app : this.daqconfig.getRCApplications().entrySet()) {
                this.cep.injectEventSynch(app.getValue());
                CHIP.log.trace("State-aware application " + app.getKey() + " injected");
            }

            /**
             * Create IS/ERS injectors
             */
            this.injectors.clear();

            CHIP.log.info("Parsing IS injectors...");
            this.injectors.putAll(this.injectorHelper.parseISInjectorFromConfig(this.partitionName, this.cep));

            CHIP.log.info("Parsing ERS injectors...");
            this.injectors.putAll(this.injectorHelper.parseERSInjectorFromConfig(this.partitionName, this.cep));

            final ISInjector<GenericISCallback> badInjector = new ISInjector<GenericISCallback>(isCounter[0] + "." + isCounter[1],
                                                                                                this.partitionName,
                                                                                                isCounter[0],
                                                                                                isCounter[1],
                                                                                                ".*",
                                                                                                new GenericISCallback(this.partitionName,
                                                                                                                      isCounter[0],
                                                                                                                      this.cep));
            this.injectors.put(badInjector.getName(), badInjector);

            /**
             * Initialize the trigger executor
             */
            final String masterTriggerController = this.daqconfig.getMasterTriggerController();
            CHIP.log.info("Master trigger is: " + masterTriggerController);
            try {
                this.execTrigger.init(masterTriggerController);
            }
            catch(final ExecutorTriggerException e) {
                ers.Logger.error(new Internal("Failed to initialize the trigger commander", e));
            }

            // Do not read from IS if it is a reload
            if(!isReload.booleanValue()) {
                final Map<String, rc.DAQApplicationInfoNamed> daqAppInfo = new HashMap<>();
                final Map<String, rc.RCStateInfoNamed> rcStateInfo = new HashMap<>();

                final Map<String, RCApplicationConfig> rcApps = this.daqconfig.getRCApplications();
                final Map<String, ApplicationConfig> baseApps = this.daqconfig.getBaseApplications();

                try {
                    CHIP.log.debug("Reading process information from IS...");
                    final InfoList processInfoList = new InfoList(this.partition, "RunCtrl", new Criteria(rc.DAQApplicationInfoNamed.type));
                    final int listSize = processInfoList.size();

                    for(int i = 0; i < listSize; i++) {
                        try {
                            final rc.DAQApplicationInfoNamed processInfoNamed = new rc.DAQApplicationInfoNamed(this.partition,
                                                                                                               "RunCtrl.dummy");
                            processInfoList.getInfo(i, processInfoNamed); // It may throw ArrayIndexOutOfBoundsException

                            // Check if the application is known from config, if yes inject
                            if(baseApps.containsKey(processInfoNamed.applicationName)
                               || rcApps.containsKey(processInfoNamed.applicationName))
                            {
                                daqAppInfo.put(processInfoNamed.applicationName, processInfoNamed);
                            }
                        }
                        catch(final java.lang.ArrayIndexOutOfBoundsException | is.InfoNotCompatibleException ex) {
                            CHIP.log.error("Error while reading application list from IS: " + ex, ex);
                        }
                    }
                }
                catch(final is.InvalidCriteriaException | is.RepositoryNotFoundException ex) {
                    CHIP.log.error("Cannot get list of applications from IS: " + ex, ex);
                }

                CHIP.log.debug("Reading state information from IS...");

                try {
                    final InfoList stateInfoList = new InfoList(this.partition, "RunCtrl", new Criteria(rc.RCStateInfoNamed.type));
                    final int size = stateInfoList.size();

                    for(int i = 0; i < size; i++) {
                        try {
                            final rc.RCStateInfoNamed stateInfoNamed = new rc.RCStateInfoNamed(this.partition, "RunCtrl.dummy");
                            stateInfoList.getInfo(i, stateInfoNamed); // It may throw ArrayIndexOutOfBoundsException
                            final String applicationName = stateInfoNamed.getName().replace("RunCtrl.", "").replace("Supervision.", "");

                            // Check if the application is known from config, if yes inject
                            if(rcApps.containsKey(applicationName)) {
                                rcStateInfo.put(applicationName, stateInfoNamed);
                            }

                        }
                        catch(final java.lang.ArrayIndexOutOfBoundsException | is.InfoNotCompatibleException ex) {
                            CHIP.log.error("Error while reading application list from IS: " + ex, ex);
                        }
                    }
                }
                catch(final is.InvalidCriteriaException | is.RepositoryNotFoundException ex) {
                    CHIP.log.error("Cannot get list of applications from IS: " + ex, ex);
                }

                for(final Map.Entry<String, rc.DAQApplicationInfoNamed> me : daqAppInfo.entrySet()) {
                    try {
                        final String appName = me.getKey();

                        String parentName = null;
                        rc.RCStateInfoNamed parentInfo = null;

                        final RCApplicationConfig rcAppConf = rcApps.get(appName);
                        if(rcAppConf != null) {
                            parentName = rcAppConf.getController();
                        } else {
                            final ApplicationConfig appConf = baseApps.get(appName);
                            if(appConf != null) {
                                parentName = appConf.getController();
                            }
                        }

                        if(parentName != null) {
                            parentInfo = rcStateInfo.get(parentName);
                        }

                        this.cep.injectEventSynch(ApplicationInfo.build(me.getValue(), parentInfo));
                    }
                    catch(final InvalidEventException ex) {
                        CHIP.log.error("Error creating ApplicationInfo from IS info for " + me.getKey());
                    }
                }

                final List<Problem> problems = new ArrayList<Problem>();

                for(final Map.Entry<String, rc.RCStateInfoNamed> me : rcStateInfo.entrySet()) {
                    try {
                        final String appName = me.getKey();

                        rc.RCStateInfoNamed parentInfo = null;
                        final RCApplicationConfig appConf = rcApps.get(appName);
                        if(appConf != null) {
                            final String parentName = appConf.getController();
                            parentInfo = rcStateInfo.get(parentName);

                            if(appConf.getIsController().equals(Boolean.TRUE) == true) {
                                // Re-build the Problem object for a controller that is in error
                                // This needs to be done only for problems that do not depend on
                                // the run control state or status of children.
                                // For instance the BAD_HOST problems depends on the execution
                                // of tests, which is not reflected in the application's RC state.

                                for(final String err : me.getValue().errorReasons) {
                                    // Note: the error is a string with format "BAD_APP_NAME: description (ERROR_TYPE)"
                                    try {
                                        final String[] tk = err.split(" ");
                                        final String childAppErrName = tk[0].substring(0, tk[0].length() - 1);
                                        final String childAppErrType = tk[tk.length - 1].substring(1, tk[tk.length - 1].length() - 1);

                                        if((childAppErrType.equals(Problem.TYPE.BAD_HOST.name()) == true)
                                           || (childAppErrType.equals(Problem.TYPE.HLTSV_DEAD.name()) == true)
                                           || (childAppErrType.equals(Problem.TYPE.ERS_FATAL.name()) == true))
                                        {
                                            problems.add(new Problem(appName,
                                                                     childAppErrName,
                                                                     Problem.TYPE.valueOf(childAppErrType),
                                                                     Problem.STATUS._NEW,
                                                                     Problem.ACTION.NONE,
                                                                     IdentifiableIssue.generateUUID("")));
                                        } else {
                                            CHIP.log.info("Skipping error string \"" + err + "\" for controller \"" + appName + "\"");
                                        }
                                    }
                                    catch(final ArrayIndexOutOfBoundsException ex) {
                                        CHIP.log.error("Failed to decode error string \"" + err + "\" for controller \"" + appName, ex);
                                    }
                                }
                            }
                        }

                        this.cep.injectEventSynch(RCApplicationInfo.build(me.getValue(), appName, parentInfo));
                    }
                    catch(final InvalidEventException ex) {
                        CHIP.log.error("Error creating RCApplicationInfo from IS info for " + me.getKey());
                    }
                }
                
                for(final Problem p : problems) {
                    this.cep.injectEventSynch(p);
                }
            } else {
                // This is important: always send the update about the RootController
                final String rootName = this.daqconfig.getRootController().UID();
                try {
                    final rc.DAQApplicationInfoNamed rootCtrlSup = new rc.DAQApplicationInfoNamed(this.partition,
                                                                                                  "RunCtrl.Supervision." + rootName);
                    rootCtrlSup.checkout();

                    final rc.RCStateInfoNamed rootCtrlState = new rc.RCStateInfoNamed(this.partition, "RunCtrl." + rootName);
                    rootCtrlState.checkout();

                    try {
                        this.cep.injectEventSynch(ApplicationInfo.build(rootCtrlSup, rootCtrlState));
                    }
                    catch(final InvalidEventException ex) {
                        CHIP.log.error("Error creating ApplicationInfo from IS info for " + rootName);
                    }
                }
                catch(final InfoNotFoundException | InfoNotCompatibleException | RepositoryNotFoundException ex) {
                    CHIP.log.error("Error getting IS information for " + rootName);
                }
            }
            
            CHIP.log.info("CES is ready to rock.");
        }
        catch(final CEPConfigurationError e) {
            ers.Logger.fatal(e);
            CHIP.log.error("Error configuring CEP engine " + e);
            System.exit(-1);
        }
        catch(final DAQConfigurationError e) {
            ers.Logger.fatal(e);
            CHIP.log.error("Error reading DAQ Configuration " + e);
            System.exit(-1);
        }

    }

    boolean doRecoveryAndAutoProcedures() {
        return (this.partitionName.equals("ATLAS") || (this.isTestMode == true));
    }
    
    public void start() {

        /**
         * Configure servant
         */
        this.servant = new ExpertSystemServant(this,
                                               this.cep,
                                               this.partition,
                                               es.centralES.value,
                                               this.daqconfig.getBaseApplications().size() + this.daqconfig.getRCApplications().size());

        /**
         * Publish Expert System servant
         */
        try {

            this.servant.publish();

        }
        catch(final InvalidPartitionException e) {
            CHIP.log.error("Partition not found!");
            System.exit(1);
        }

        synchronized(this) {

            CHIP.log.info("Starting CHIP engine");

            /**
             * Starting injectors
             */
            this.startInjectors();

            // Sync with the PMG
            PMGSync.sync();

            // No more job to be done
            while(!this.isStopped.booleanValue()) {
                try {
                    this.wait();
                }
                catch(final InterruptedException e) {
                    CHIP.log.debug("Main interrupted: " + e);
                }
            }

            CHIP.log.info("Bye bye.");

        }

    }

    public void startInjectors() {
        CHIP.log.info("Starting injectors");
        for(final Map.Entry<String, Injector> entry : this.injectors.entrySet()) {
            entry.getValue().start();
        }
    }

    public void stopInjectors() {
        CHIP.log.info("Stopping injectors");
        for(final Map.Entry<String, Injector> entry : this.injectors.entrySet()) {
            entry.getValue().stop();
        }
    }

    /**
     * Start all the injectors, which so far could not be started
     */
    public void retryStartInjectors() {
        CHIP.log.info("Retry to start injectors");
        for(final Map.Entry<String, Injector> entry : this.injectors.entrySet()) {
            entry.getValue().retryStart();
        }
    }

    // @PreDestroy
    public void shutdown() {

        synchronized(this) {
            CHIP.log.info("Shutting down the engine...");

            this.stopInjectors();

            this.cep.stop();

            this.isStopped = Boolean.TRUE;
            this.notify();

            /**
             * Removing ES servant reference from IPC
             */
            try {
                this.servant.withdraw();
            }
            catch(final InvalidPartitionException e) {
                CHIP.log.error("Partition not found");
                e.printStackTrace();
            }
            catch(final Exception ex) {
                CHIP.log.error("Exception while shutting down ");
                ex.printStackTrace();
            }

        }

    }

    /**
     * A shutdown hook is simply an initialized but non-started thread that will be started when the JVM begins its shutdown sequence.
     */
    private class ShutdownHook extends Thread {
        private final CHIP chip;

        public ShutdownHook(final CHIP chip) {
            this.chip = chip;
        }

        @Override
        public void run() {
            this.chip.shutdown();

        }
    }

    /**
     * @return the partitionName
     */
    public String getPartitionName() {
        return this.partitionName;
    }

    /**
     * Get partition
     * 
     * @return
     */
    public ipc.Partition getPartition() {
        return this.partition;
    }

    /**
     * Setter for the injectorHelper class
     * 
     * @param injectorHelper
     */
    @Autowired
    public void setInjectorHelper(final Injectors injectorHelper) {
        this.injectorHelper = injectorHelper;
    }
}
