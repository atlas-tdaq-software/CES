package chip.injector;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import chip.CEPService;


/**
 * Utility class to load Injectors from configuration
 * 
 * @author lmagnoni
 */

@Component("injectors")
public class Injectors {

    /**
     * Logger
     */
    private static final Log log = LogFactory.getLog(Injectors.class);

    @Resource(name = "configuration")
    private Properties prop;

    public Map<String, Injector> parseISInjectorFromConfig(final String defaultPartition, final CEPService cep) {
        Injectors.log.debug("Loading injectors from configuration...");

        // Configuration config = Configuration.getInstance();
        final Map<String, Injector> injectors = new HashMap<String, Injector>();
        // Properties prop = config.getProperties();

        // Set up IS injector from configuration
        // Syntax:
        // [is.INJNAME.partition] - if not defined, defaults to $TDAQ_PARTITION
        // is.INJNAME.server
        // [is.INJNAME.inforname]
        // [is.INJNAME.regex]

        // Extracting the list of INJECTOR NAMES
        final List<String> injectorsNames = new LinkedList<String>();
        for(final Enumeration<Object> e = this.prop.keys(); e.hasMoreElements();) {
            final String name = (String) e.nextElement();
            if(name.contains(".is.")) {
                final String[] tokens = name.split("\\.");
                if(tokens.length >= 2) {
                    if(injectorsNames.indexOf(tokens[0]) == -1) {
                        injectorsNames.add(tokens[0]);
                    }
                }
            }
        }

        // For every AbstractInjector names, look for mandatory and additional informations
        for(final String name : injectorsNames) {
            String partition = null;
            String server = null;
            String infoname = null;
            String regex = null;
            String type = null;

            if(this.prop.containsKey(name + ".is.partition")) {
                partition = this.prop.getProperty(name + ".is.partition");
            } else {
                // Default partition name
                partition = defaultPartition;
                Injectors.log.warn("No partition for injector " + name + " specified. Using default partition " + defaultPartition + ".");
            }

            if(this.prop.containsKey(name + ".is.server")) {
                server = this.prop.getProperty(name + ".is.server");
            } else {
                Injectors.log.error("Invalid injector " + name + " specified. Missing server.");
                continue;
            }

            if(this.prop.containsKey(name + ".is.infoname")) {
                infoname = this.prop.getProperty(name + ".is.infoname");
            }

            if((infoname == null) || infoname.isEmpty()) {
                if(this.prop.containsKey(name + ".is.regex")) {
                    regex = this.prop.getProperty(name + ".is.regex");
                    infoname = "";
                }
            }

            // If not initialized, set regex to empty string
            if(regex == null) {
                regex = "";
            }

            if(this.prop.containsKey(name + ".is.type")) {
                type = this.prop.getProperty(name + ".is.type");
            } else {
                type = "generic";
            }

            switch(type) {
                default:

                    // Creating injector
                    if(server.contains(",")) {
                        final String sa[] = server.split(",");
                        for(final String element : sa) {
                            final ISInjector<GenericISCallback> injector = new ISInjector<GenericISCallback>(partition + "." + element
                                                                                                                 + "." + infoname + regex,
                                                                                                             partition,
                                                                                                             element.replaceAll(" ", ""),
                                                                                                             infoname,
                                                                                                             regex,
                                                                                                             new GenericISCallback(partition,
                                                                                                                                   server,
                                                                                                                                   cep));
                            injectors.put(injector.getName(), injector);
                            Injectors.log.debug("Injector " + name + " - " + partition + "." + element + "." + infoname + regex
                                                + " created.");
                        }
                    } else {
                        final ISInjector<GenericISCallback> injector = new ISInjector<GenericISCallback>(partition + "." + server + "."
                                                                                                             + infoname + regex,
                                                                                                         partition,
                                                                                                         server,
                                                                                                         infoname,
                                                                                                         regex,
                                                                                                         new GenericISCallback(partition,
                                                                                                                               server,
                                                                                                                               cep));
                        injectors.put(injector.getName(), injector);
                        Injectors.log.debug("Injector " + name + " subscribed to " + partition + "." + server + "." + infoname + regex
                                            + " created.");
                    }

            }

        }

        return injectors;
    }

    public Map<String, Injector> parseERSInjectorFromConfig(final String defaultPartition, final CEPService cep) {
        Injectors.log.debug("Loading injectors from configuration...");
        // Configuration config = Configuration.getInstance();
        final Map<String, Injector> injectors = new HashMap<String, Injector>();
        // Properties prop = config.getProperties();

        // Set up ERS injector from configuration
        // Syntax:
        // [ers.INJNAME.partition] - if not defined, defaults to $TDAQ_PARTITION
        // [ers.INJNAME.regex]

        // Extracting the list of INJECTOR NAMES
        final List<String> injectorsNames = new ArrayList<String>();

        for(final Enumeration<Object> e = this.prop.keys(); e.hasMoreElements();) {
            final String name = (String) e.nextElement();
            if(name.contains(".ers.")) {
                final String[] tokens = name.split("\\.");
                if(tokens.length >= 2) {
                    if(injectorsNames.indexOf(tokens[0]) == -1) {
                        injectorsNames.add(tokens[0]);
                    }
                }
            }
        }

        // For every AbstractInjector names, look for mandatory and additional informations
        for(final String name : injectorsNames) {
            String partition = null;
            String regex = null;

            // Default partition name
            partition = defaultPartition;

            if(this.prop.containsKey(name + ".ers.partition")) {
                partition = this.prop.getProperty(name + ".ers.partition");
            } else {
                // Default partition name
                partition = defaultPartition;
                Injectors.log.warn("No partition for injector " + name + " specified. Using default partition " + defaultPartition + ".");
            }

            if(this.prop.containsKey(name + ".ers.regex")) {
                regex = this.prop.getProperty(name + ".ers.regex");
            } else {
                Injectors.log.error("Invalid injector " + name + " specified. Missing partition.");
                continue;
            }

            // Creating injector
            final ERSInjector<MessageCallback> injector = new ERSInjector<MessageCallback>(name,
                                                                                           partition,
                                                                                           regex,
                                                                                           new MessageCallback(partition, cep));
            injectors.put(name, injector);
            Injectors.log.debug("AbstractInjector " + name + " created.");

        }

        return injectors;
    }

}
