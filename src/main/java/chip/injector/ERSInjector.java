package chip.injector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ers.BadStreamConfiguration;
import ers.ReceiverNotFound;
import ers.StreamManager;


/**
 * @author ganders
 * @param <T>
 */
public class ERSInjector<T extends ERSCallback> implements Injector {
    private static final Log log = LogFactory.getLog(ERSInjector.class.getName());

    private final String name;
    private final String partition_name;
    private final String regex;
    private final T callback;

    private boolean ISRUNNING; // set to true when injectors starts successfully, to false when stopped

    public ERSInjector(final String name, final String partition_name, final String regex, final T callback) {
        this.name = name;
        this.partition_name = partition_name;
        this.regex = regex;
        this.callback = callback;

        this.ISRUNNING = false;
    }

    synchronized public boolean isRunning() {
        return this.ISRUNNING;
    }

    @Override
    synchronized public void start() {
        if(this.ISRUNNING == false) {
            ERSInjector.log.info("Starting ers Injector: " + this.name);

            try {
                StreamManager.instance().add_receiver(this.callback, "mts", this.partition_name, this.regex);
                this.ISRUNNING = true;
            }
            catch(final BadStreamConfiguration ex) {
                ERSInjector.log.debug("Failed to add receiver: " + ex.getMessage());
                ERSInjector.log.debug("Cause: " + ex.getCause().getMessage());
                this.ISRUNNING = false;
            }
        } else {
            ERSInjector.log.info("Injector " + this.name + " not started because already running");
        }
    }

    @Override
    synchronized public void stop() {
        if(this.ISRUNNING == true) {
            ERSInjector.log.info("Stopping ers injector: " + this.name);
            try {
                StreamManager.instance().remove_receiver(this.callback);
                ERSInjector.log.info("Ers injector " + this.name + " stopped.");
                this.ISRUNNING = false;
            }
            catch(final ReceiverNotFound ex) {
                ERSInjector.log.error("Failed to remove receiver: " + ex);
                this.ISRUNNING = false;
            }
        } else {
            ERSInjector.log.info("ers injector " + this.name + " not stopped because not running");
        }
    }

    @Override
    public void retryStart() {
        this.start();
    }
}
