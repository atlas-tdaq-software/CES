package chip.injector;

import is.InfoEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import chip.CEPService;
import daq.EsperUtils.CepException.InvalidEventException;
import daq.EsperUtils.Events;
import daq.EsperUtils.ISEvent;


/**
 * IS callback which creates and injects new {@code GenericISInfo} events to the CEP engine for each IS operations received.
 * 
 * @author ganders
 */
public class GenericISCallback implements ISCallback {

    private static final Log log = LogFactory.getLog(GenericISCallback.class);

    private final String server;
    private final CEPService cep;

    private final ipc.Partition partition;

    public GenericISCallback(final String partitionName, final String server, final CEPService cep) {
        this.server = server;
        this.cep = cep;
        this.partition = new ipc.Partition(partitionName);

    }

    @Override
    public void infoCreated(final InfoEvent arg0) {
        this.infoEventGen(ISEvent.OP.CREATE, arg0);
    }

    @Override
    public void infoDeleted(final InfoEvent arg0) {
        this.infoEventGen(ISEvent.OP.DELETE, arg0);
    }

    @Override
    public void infoUpdated(final InfoEvent arg0) {
        this.infoEventGen(ISEvent.OP.UPDATE, arg0);
    }

    @Override
    public void infoSubscribed(final InfoEvent arg0) {
        this.infoEventGen(ISEvent.OP.SUBSCRIBE, arg0);
    }

    private void infoEventGen(final ISEvent.OP operation, final InfoEvent infoEvent) {

        // Inject the event
        // log.debug("Inject ISEvent "+infoEvent.getName() +" of type "+infoEvent.getType().getName());
        try {
            this.cep.injectEvent(Events.createISEvent(operation, infoEvent, this.partition, this.server));
        }
        catch(final InvalidEventException e) {
            GenericISCallback.log.error("Failed to inject event " + infoEvent.getName() + " into CEP engine.", e);
        }
    }

}
