package chip.injector;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import chip.CEPService;
import daq.EsperUtils.ERSEvent;
import daq.EsperUtils.Events;
import ers.Issue;


/**
 * Message callback
 * 
 * @author ganders
 */

public class MessageCallback implements ERSCallback {

    private static final Log log = LogFactory.getLog(MessageCallback.class);

    private final String partition;
    private final CEPService cep;

    public MessageCallback(final String partition, final CEPService cep) {
        this.partition = partition;
        this.cep = cep;
    }

    @Override
    public void receive(final Issue issue) {

        // log.debug("ERS callback received");

        try {
            for(final ERSEvent mess : Events.createERSEvent(issue, this.partition)) {
                this.cep.injectEvent(mess);
            }
        }
        catch(final Exception e) {
            MessageCallback.log.error("Failed to inject ERSEvent into CEP engine.");
        }
    }

}
