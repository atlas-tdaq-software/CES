package chip.injector;

import ipc.Partition;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import chip.CHIPException.IPCNotFound;


/**
 * Injector abstract class. It summarized the functionalities of subscriber entities ( both for MRS and IS) in the TDAQ domains. It relies
 * on DAQ IPC. In particular, it offers a common implementation for the isRunning and isServerAlive methods based on IPC lookup checks. More
 * advanced "isServerAlive" may be implemented in subclasses
 * 
 * @author lmagnoni
 */
public abstract class AbstractIPCInjector implements Injector {

    // ACTIVE: UP and subscribed
    // DOWN: not published in IPC or no IPC partition
    // EXPIRED: UP but restarted and subscription is expired: injector needs to be restarted
    public enum ServerState {
        ACTIVE,
        DOWN,
        EXPIRED
    }

    private final String name; // internal name or rather an ID, provided upon construction
    private final String ipc_name; // name as registered in IPC, needed for lookups
    private final Partition ipc_partition; // IPC partition name, needed for lookups
    private int start_time; // seconds from epoch
    protected boolean ISRUNNING; // set to true when injectors starts successfully, to false when stopped

    private static final Log log = LogFactory.getLog(AbstractIPCInjector.class.getName());

    public AbstractIPCInjector(final String name, final String partition_name, final String ipc_domain) {
        this(name, partition_name, ipc_domain, name);
    }

    public AbstractIPCInjector(final String name, final String partition_name, final String ipc_domain, final String ipc_name) {
        AbstractIPCInjector.log.debug("Creating Injector " + partition_name + "@" + ipc_domain + "@" + name);
        this.ISRUNNING = false;
        this.name = name;
        this.ipc_name = ipc_name;
        this.ipc_partition = new Partition(partition_name);
        this.start_time = 0;
    }

    protected Partition getPartition() {
        return this.ipc_partition;
    }

    public String getName() {
        return this.name;
    }

    private String getIpcName() {
        return this.getPartition().getName() + "@" + this.ipc_name;
    }

    public boolean isRunning() {
        return this.ISRUNNING;
    }

    public ServerState isServerAlive() {
        AbstractIPCInjector.log.trace("Checking isServerAlive for " + this.getIpcName());

        if(!this.getPartition().isValid()) {
            AbstractIPCInjector.log.trace("Partition for server " + this.getIpcName() + " is not up.");
            return ServerState.DOWN;
        }

        try {
            ipc.servant servant;
            servant = this.getIpcObject();

            if(servant == null) {
                AbstractIPCInjector.log.trace("Servant " + this.getIpcName() + " is not running.");
                return ServerState.DOWN;
            }

            ipc.servantPackage.ApplicationContext ipc_info;

            // ipc_info = servant.information() ; // remote call, may cause exceptions
            ipc_info = servant.app_context();

            // log.debug("Servant " + getIpcName() + ": start_time:" + ipc_info.time + " PID:" + ipc_info.pid) ;

            if(this.start_time == 0) {
                AbstractIPCInjector.log.info("Servant " + this.getIpcName() + " checked first time, timestamp stored.");
                this.start_time = ipc_info.time;
            }

            if(ipc_info.time != this.start_time) {
                AbstractIPCInjector.log.info("Servant " + this.getIpcName() + " was restarted since last check, resubscribe needed.");
                this.start_time = ipc_info.time;
                return ServerState.EXPIRED;
            }

        }
        catch(final java.lang.RuntimeException ex) {
            AbstractIPCInjector.log.trace("Failed in getting info for Servant " + this.getIpcName() + ": " + ex);
            return ServerState.DOWN;
        }
        catch(final IPCNotFound e) {
            AbstractIPCInjector.log.error(e);
            return ServerState.DOWN;
        }

        // log.info("Servant " + getIpcName() + " is alive and active." );
        return ServerState.ACTIVE;
    }

    public void checkAndRestart() {
        final ServerState serverAlive = this.isServerAlive();
        if(!this.isRunning() && ((serverAlive == ServerState.ACTIVE) || (serverAlive == ServerState.EXPIRED))) {
            this.start();
        } else if(this.isRunning() && (serverAlive == ServerState.EXPIRED)) {
            this.stop(true);
            this.start();
        } else if(this.isRunning() && (serverAlive == ServerState.DOWN)) {
            // ? Two way to stop the injector, depending on the status of the server
            // this is executed only when serverAlive == false
            this.stop(false);
        }
    }

    public abstract void stop(boolean serverState);

    public abstract ipc.servant getIpcObject() throws IPCNotFound;

}
