package chip.injector;

import ipc.InvalidObjectException;
import ipc.InvalidPartitionException;
import is.Criteria;
import is.Repository;

import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import chip.CHIPException;
import chip.CHIPException.IPCNotFound;


/**
 * Generic IS injector. It is generic in the callback object, that can be specified as constructor paramter and contains the logic to be
 * executed at each IS operations.
 * <p>
 * This class implements the start and stop procedure to subscribe to the specified partition/server/infoname/regex as by paramters.
 * <p>
 * Subscription criteria as defined here in the IS user manual section:
 * http://atlas-tdaq-monitoring.web.cern.ch/atlas-tdaq-monitoring/IS/doc/userguide/html/is-usersguide-16.html#pgfId-808821
 * 
 * @author lmagnoni
 */

public class ISInjector<T extends ISCallback> extends AbstractIPCInjector {

    private static final Log log = LogFactory.getLog(ISInjector.class.getName());
    private static final String ipc_domain_is = "is/repository";
    private static final Class<is.repository> ipc_class = is.repository.class;

    private final String server;
    private final String infoname;
    private final String regex;
    private final Repository is_repository;
    private final T callback;

    private boolean pending_unsubscribe = false;

    /**
     * @param server
     * @param infoname
     * @param criteria
     */

    public ISInjector(final String name,
                      final String partition,
                      final String server,
                      final String infoname,
                      final String regex,
                      final T callback)
    {
        super(name, partition, ISInjector.ipc_domain_is, server);

        this.server = server;
        this.infoname = infoname;
        this.regex = regex.replaceAll(" ", "");
        this.is_repository = new Repository(this.getPartition());

        this.callback = callback;

        // Criteria as defined here in the IS user manual section:
        // http://atlas-tdaq-monitoring.web.cern.ch/atlas-tdaq-monitoring/IS/doc/userguide/html/is-usersguide-16.html#pgfId-808821
    }

    @Override
    synchronized public void start() {
        if(this.ISRUNNING == false) {
            ISInjector.log.info("Starting Injector: " + super.getName());

            try {
                if((this.infoname != null) && !this.infoname.equals("")) {

                    // Subscribing to IS server with specific information name

                    // To clean up subscription in case of server with backup an
                    // an additional unsubscribe is done if was not properly done during the stop
                    if(this.pending_unsubscribe) {
                        try {
                            this.is_repository.unsubscribe(this.server + "." + this.infoname);
                            ISInjector.log.debug("Unsubscribed to: " + this.getPartition().getName() + "." + this.server + "." + this.infoname);

                            this.pending_unsubscribe = false; // Reset flag
                        }
                        catch(final is.RepositoryNotFoundException ex) {
                            ISInjector.log.trace("Pending unsubscribed failed. Repository not found: " + ex);
                            this.pending_unsubscribe = true; // Flag cannot be removed since server is not running and subscription can be
                                                             // still
                                                             // be
                            // present.
                        }
                        catch(final is.SubscriptionNotFoundException ex) {
                            // Quite commmon case, no backup on the server
                            ISInjector.log.trace("Pending Usubscribed failed for " + this.server + "." + this.infoname
                                                 + ". Probably no backup on the server: " + ex);
                            this.pending_unsubscribe = false; // Reset flag
                        }
                    }

                    this.is_repository.subscribe(this.server + "." + this.infoname, this.callback);
                    ISInjector.log.debug("Subscribed to: " + this.getPartition().getName() + "." + this.server + "." + this.infoname);

                } else if((this.regex != null) && !this.regex.equals("")) {
                    final Criteria c = new Criteria(Pattern.compile(this.regex));

                    // Unsubscribe just in case of server with backup
                    if(this.pending_unsubscribe) {

                        try {
                            this.is_repository.unsubscribe(this.server, c);
                            ISInjector.log.debug("Unsubscribed to: " + this.getPartition().getName() + "." + this.server + " with creteria: "
                                                 + c.toString());

                            this.pending_unsubscribe = false; // Reset flag
                        }
                        catch(final is.RepositoryNotFoundException ex) {
                            ISInjector.log.trace("Pending unsubscribed failed. Repository not found: " + ex);
                            this.pending_unsubscribe = true; // Flag cannot be removed since server is not running and subscription can be
                                                             // still
                            // there.
                        }
                        catch(final is.SubscriptionNotFoundException ex) {
                            // Quite commmon case, no backup on the server
                            ISInjector.log.trace("Pending Usubscribed failed for " + this.server + "." + this.infoname
                                                 + ". Probably no backup on the server: " + ex);
                            this.pending_unsubscribe = false; // Reset flag
                        }
                    }
                    ISInjector.log.debug("Server alive: " + this.isServerAlive().toString());
                    ISInjector.log.debug("Trying to subscribe to: " + this.getPartition().getName() + "." + this.server + " with criteria: '"
                                         + this.regex + "'");
                    this.is_repository.subscribe(this.server, c, this.callback);
                    ISInjector.log.debug("Subscribed to: " + this.getPartition().getName() + "." + this.server + " with criteria: '" + this.regex
                                         + "'");
                }

            } // try subscribe to IS
            catch(final is.AlreadySubscribedException ex) {
                ISInjector.log.debug("Failed to subscribe to: " + this.getPartition().getName() + "." + this.server + "; subscription already done");
            }
            catch(final is.InvalidCriteriaException | is.RepositoryNotFoundException ex) {
                ISInjector.log.debug("Failed to subscribe to: " + this.getPartition().getName() + "." + this.server + "; " + ex);
                this.ISRUNNING = false;
                return;
            }

            this.ISRUNNING = true;
            ISInjector.log.debug("Started Injector: " + super.getName());
        } else {
            ISInjector.log.info("Injector " + super.getName() + " not started because already running");
        }
    }

    @Override
    public void stop() {
        this.stop(true);
    }

    @Override
    synchronized public void stop(final boolean serverisrunning) {
        if(this.ISRUNNING == true) {
            ISInjector.log.info("Stopping Injector: " + super.getName() + " server status is " + serverisrunning);
            if(serverisrunning) {
                try {
                    if((this.infoname != null) && !this.infoname.equals("")) {
                        // Subscribing to IS server with specifi information name
                        this.is_repository.unsubscribe(this.server + "." + this.infoname);
                        ISInjector.log.debug("Unsubscribed to: " + this.getPartition().getName() + "." + this.server + "." + this.infoname);
                    } else if((this.regex != null) && !this.regex.equals("")) {
                        final Criteria c = new Criteria(Pattern.compile(this.regex));
                        this.is_repository.unsubscribe(this.server, c);
                        ISInjector.log.debug("Unsubscribed to: " + this.getPartition().getName() + "." + this.server + " with criteria: "
                                             + c.toString());

                    }
                }
                catch(final is.RepositoryNotFoundException ex) // Server went down
                {
                    ISInjector.log.trace("Server is down: " + ex);
                    this.ISRUNNING = false;
                    this.pending_unsubscribe = true;
                }
                catch(final is.SubscriptionNotFoundException ex) {
                    ISInjector.log.error("Failed to unsubscribe from IS: " + ex + "\n");
                    this.ISRUNNING = false;
                }
            } else {
                // Set the flag to indicates subscription not done. This can be a problem in case of
                // server with backup
                this.pending_unsubscribe = true;
            }
            this.ISRUNNING = false;
            ISInjector.log.debug("Stopped Injector: " + super.getName());
        } else {
            ISInjector.log.info("Injector: " + super.getName() + " not stopped because not running");
        }
    }

    @Override
    synchronized public boolean isRunning() {
        return super.isRunning();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + ((this.infoname == null) ? 0 : this.infoname.hashCode());
        result = (prime * result) + ((this.is_repository == null) ? 0 : this.is_repository.hashCode());
        result = (prime * result) + ((this.regex == null) ? 0 : this.regex.hashCode());
        result = (prime * result) + ((this.server == null) ? 0 : this.server.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null) {
            return false;
        }
        if(this.getClass() != obj.getClass()) {
            return false;
        }
        final ISInjector<?> other = (ISInjector<?>) obj;
        if(this.infoname == null) {
            if(other.infoname != null) {
                return false;
            }
        } else if(!this.infoname.equals(other.infoname)) {
            return false;
        }
        if(this.is_repository == null) {
            if(other.is_repository != null) {
                return false;
            }
        } else if(!this.is_repository.equals(other.is_repository)) {
            return false;
        }
        if(this.regex == null) {
            if(other.regex != null) {
                return false;
            }
        } else if(!this.regex.equals(other.regex)) {
            return false;
        }
        if(this.server == null) {
            if(other.server != null) {
                return false;
            }
        } else if(!this.server.equals(other.server)) {
            return false;
        }
        return true;
    }

    @Override
    public ipc.servant getIpcObject() throws IPCNotFound {
        try {
            return this.getPartition().lookup(ISInjector.ipc_class, this.server);
        }
        catch(final InvalidPartitionException e) {
            throw new CHIPException.IPCNotFound("IPC class = " + ISInjector.ipc_class.getName() + ", server = " + this.server, e);
        }
        catch(final InvalidObjectException e) {
            throw new CHIPException.IPCNotFound("IPC class = " + ISInjector.ipc_class.getName() + ", server = " + this.server, e);
        }
    }

    @Override
    public void retryStart() {
        this.start();
    }
}
