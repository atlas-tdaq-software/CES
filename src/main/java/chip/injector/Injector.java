package chip.injector;



/**
 * The injector interface.
 * 
 * @author lmagnoni
 */
public interface Injector {

    /**
     * Start the injector, subscribing to the desired entity
     */
    public abstract void start();

    /**
     * Stop the injector
     */
    public abstract void stop();

    /**
     * Retry to start injectors (subscribing failed before)
     */
    public abstract void retryStart();

}
