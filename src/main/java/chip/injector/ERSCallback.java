package chip.injector;

import ers.IssueReceiver;

/**
 * Marker interface for the callback object of ERS subscriber
 * 
 * @author ganders
 *
 */

public interface ERSCallback extends IssueReceiver {

}
