package chip.injector;

import is.InfoWatcher.ExtendedInfoListener;


/**
 * Marker interface for the callback object of IS subscriber
 * 
 * @author lmagnoni
 */
public interface ISCallback extends ExtendedInfoListener {
}
