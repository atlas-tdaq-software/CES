package chip.msg;

import ers.Issue;


@SuppressWarnings("serial")
public class HoldingTriggerAction extends Issue {
    private final String causedBy;
    private final String reason;
    private final long time;
    private final boolean isStart;
    private final String uid;

    public HoldingTriggerAction(final String msg,
                                final String causedBy,
                                final String reason,
                                final long time,
                                final boolean isStart,
                                final String uid)
    {
        super(msg, causedBy, reason, Long.valueOf(time), Boolean.valueOf(isStart), uid);

        this.causedBy = causedBy;
        this.reason = reason;
        this.time = time;
        this.isStart = isStart;
        this.uid = uid;
    }

    public String getCausedBy() {
        return this.causedBy;
    }

    public String getReason() {
        return this.reason;
    }

    public long getTime() {
        return this.time;
    }

    public boolean isStart() {
        return this.isStart;
    }

    public String getUid() {
        return this.uid;
    }
}
