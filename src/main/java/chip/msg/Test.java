package chip.msg;

import chip.event.issue.Issue;


@SuppressWarnings("serial")
public class Test extends ers.Issue {
    private final String category;
    private final String name;
    private final String status;
    private final String application;
    private final String runningHost;
    private final String action;

    public Test(final String msg,
                final String name,
                final String status,
                final String application,
                final String runningHost,
                final String action,
                final Exception exCause)
    {
        super(msg, Issue.ISSUETYPE.TEST.toString(), name, status, application, runningHost, action, exCause);

        this.category = Issue.ISSUETYPE.TEST.toString();
        this.name = name;
        this.status = status;
        this.application = application;
        this.runningHost = runningHost;
        this.action = action;
    }

    public Test(final String msg,
                final String name,
                final String status,
                final String application,
                final String runningHost,
                final String action)
    {
        super(msg, Issue.ISSUETYPE.TEST.toString(), name, status, application, runningHost, action);

        this.category = Issue.ISSUETYPE.TEST.toString();
        this.name = name;
        this.status = status;
        this.application = application;
        this.runningHost = runningHost;
        this.action = action;
    }

    public String getCategory() {
        return this.category;
    }

    public String getName() {
        return this.name;
    }

    public String getStatus() {
        return this.status;
    }

    public String getApplication() {
        return this.application;
    }

    public String getAction() {
        return this.action;
    }
    
    public String getRunningHost() {
        return this.runningHost;
    }
}
