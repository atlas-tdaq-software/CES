package chip.msg;

import java.util.Map;

import ers.Issue;


@SuppressWarnings("serial")
public class TestFailed extends Issue {
    private final String application;
    private final Map<String, String> components;

    public TestFailed(final String msg, final String application, final Map<String, String> components) {
        super(msg, application, components);

        this.application = application;
        this.components = components;
    }

    public String getApplication() {
        return this.application;
    }

    public Map<String, String> getComponents() {
        return this.components;
    }
}
