package chip.msg;

import ers.Issue;

@SuppressWarnings("serial")
public class HostDisabled extends Issue {
    private final String hostName;
    
    public HostDisabled(final String hostName) {
        super("Host " + hostName + " has been disabled in the database; please reload the configuration at the next occasion", hostName);
        
        this.hostName = hostName;
    }
    
    final String getHostName() {
        return this.hostName;
    }
}
