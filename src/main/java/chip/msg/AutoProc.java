package chip.msg;

import chip.event.issue.Issue;


@SuppressWarnings("serial")
public class AutoProc extends ers.Issue {
    private final String category;
    private final String type;
    private final String name;
    private final String status;

    public AutoProc(final String msg, final String type, final String name, final String status, final Exception exCause) {
        super(msg, Issue.ISSUETYPE.AUTO_PROCEDURE.toString(), type, name, status, exCause);

        this.category = Issue.ISSUETYPE.AUTO_PROCEDURE.toString();
        this.type = type;
        this.name = name;
        this.status = status;
    }

    public AutoProc(final String msg, final String type, final String name, final String status) {
        super(msg, Issue.ISSUETYPE.AUTO_PROCEDURE.toString(), type, name, status);

        this.category = Issue.ISSUETYPE.AUTO_PROCEDURE.toString();
        this.type = type;
        this.name = name;
        this.status = status;
    }

    public String getCategory() {
        return this.category;
    }

    public String getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

    public String getStatus() {
        return this.status;
    }
}
