package chip.msg;

import chip.event.issue.Issue;


@SuppressWarnings("serial")
public class Core extends ers.Issue {
    private final String category;
    private final String type;
    private final String name;
    private final String status;
    private final String application;
    private final String controller;
    private final String action;

    public Core(final String msg,
                final String type,
                final String name,
                final String status,
                final String application,
                final String controller,
                final String action,
                final Exception exCause)
    {
        super(msg, Issue.ISSUETYPE.CORE.toString(), type, name, status, application, controller, action, exCause);

        this.category = Issue.ISSUETYPE.CORE.toString();
        this.type = type;
        this.name = name;
        this.status = status;
        this.application = application;
        this.controller = controller;
        this.action = action;
    }

    public Core(final String msg,
                final String type,
                final String name,
                final String status,
                final String application,
                final String controller,
                final String action)
    {
        super(msg, Issue.ISSUETYPE.CORE.toString(), type, name, status, application, controller, action);

        this.category = Issue.ISSUETYPE.CORE.toString();
        this.type = type;
        this.name = name;
        this.status = status;
        this.application = application;
        this.controller = controller;
        this.action = action;
    }

    public String getCategory() {
        return this.category;
    }

    public String getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

    public String getStatus() {
        return this.status;
    }

    public String getApplication() {
        return this.application;
    }

    public String getController() {
        return this.controller;
    }

    public String getAction() {
        return this.action;
    }
}
