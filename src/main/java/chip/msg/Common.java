package chip.msg;

@SuppressWarnings("serial")
public class Common extends ers.Issue {
    public Common(final String errorDesc, final Exception exCause) {
        super(errorDesc, exCause);
    }

    public Common(final String errorDesc) {
        super(errorDesc);
    }
}
