package chip.msg;

@SuppressWarnings("serial")
public class Internal extends ers.Issue {
    public Internal(final String errorDesc, final Exception exCause) {
        super(errorDesc, exCause);
    }

    public Internal(final String errorDesc) {
        super(errorDesc);
    }
}
