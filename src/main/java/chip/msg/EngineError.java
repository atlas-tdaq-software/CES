package chip.msg;

import ers.Issue;


@SuppressWarnings("serial")
public class EngineError extends Issue {
    private final String statementName;
    private final String event;
    private final String epl;

    public EngineError(final String statementName, final String event, final String epl, final Throwable cause) {
        super("Unexpected exception processing EPL statement \"" + statementName + "\": " + cause, statementName, event, epl, cause);

        this.statementName = statementName;
        this.event = event;
        this.epl = epl;
    }

    public String getStatementName() {
        return this.statementName;
    }

    public String getEvent() {
        return this.event;
    }

    public String getEpl() {
        return this.epl;
    }
}
