package chip.msg;

import chip.event.issue.Issue;


@SuppressWarnings("serial")
public class Recovery extends ers.Issue {
    private final String category;
    private final String type;
    private final String name;
    private final String status;
    private final String component;
    private final String detector;

    public Recovery(final String msg,
                    final String type,
                    final String name,
                    final String status,
                    final String component,
                    final String detector,
                    final Exception exCause)
    {
        super(msg, Issue.ISSUETYPE.RECOVERY.toString(), type, name, status, component, detector, exCause);

        this.category = Issue.ISSUETYPE.RECOVERY.toString();
        this.type = type;
        this.name = name;
        this.status = status;
        this.component = component;
        this.detector = detector;
    }

    public Recovery(final String msg,
                    final String type,
                    final String name,
                    final String status,
                    final String component,
                    final String detector)
    {
        super(msg, Issue.ISSUETYPE.RECOVERY.toString(), type, name, status, component, detector);

        this.category = Issue.ISSUETYPE.RECOVERY.toString();
        this.type = type;
        this.name = name;
        this.status = status;
        this.component = component;
        this.detector = detector;
    }

    public String getCategory() {
        return this.category;
    }

    public String getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

    public String getStatus() {
        return this.status;
    }

    public String getComponent() {
        return this.component;
    }

    public String getDetector() {
        return this.detector;
    }
}
