package chip;

import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class Main {
    public static void main(final String[] args) {

        /**
         * Spring context generation. Spring load and instanciates all Java beans as described by the xml file
         */
        try (final AbstractXmlApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml")) {
            /**
             * Getting the bean for the CHIP
             */
            final CHIP chip = context.getBean("chip", CHIP.class);

            /**
             * CHIP configuration false to flag the reading
             */
            chip.configure();

            /**
             * Starting CHIP
             */
            chip.start();
        }
    }
}
