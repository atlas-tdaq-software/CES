package chip.dao;

import java.util.ConcurrentModificationException;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import chip.CEPService;
import chip.event.en.FSM;
import chip.event.en.IFDIES;
import chip.event.en.IFERROR;
import chip.event.en.IFFAILED;
import chip.event.en.MEMBERSHIP;
import chip.event.en.RCCOMMAND;
import chip.event.en.STARTAT;
import chip.event.en.STATUS;
import chip.event.en.STOPAT;
import chip.event.en.TRCOMMAND;
import chip.event.meta.ErrorItem;
import chip.event.meta.RCApplication;

import com.espertech.esper.common.client.EPException;
import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.common.client.fireandforget.EPFireAndForgetQueryResult;


/**
 * This class provides facilities to access Esper working memory as a normal DAO does for a DB.
 * <p>
 * Relies on Esper On-Demand Snapshot Query Execution (Esper doc. 10.4.3)
 * 
 * @author lmagnoni
 */
// NOTE:: When an on-demand query is added, please update the test in EPLLoadingTest
public class EsperDAO {
    private final CEPService cep;

    private final static Log log = LogFactory.getLog(EsperDAO.class);

    public EsperDAO(final CEPService cep) {
        this.cep = cep;
    }

    /**
     * Retrieve all Application from Esper working memory
     * <p>
     * Relies on the 'RCApplicationTable' stream define by the core.epl module which provide a 'unique by name' view on the Application event
     * set.
     * 
     * @return
     */
    public List<RCApplication> getAllRCApplication() {
        final List<RCApplication> apps = new LinkedList<RCApplication>();
        
        final String epl = "select * from RCApplicationTable()";
        final EPFireAndForgetQueryResult result = this.cep.executeQuery(epl);
        for(final com.espertech.esper.common.client.EventBean e : result.getArray()) {
            apps.add(this.createRCApplicationFromEventBean(e));
        }

        return apps;
    }

    public List<RCApplication> getChildrenOfController(final String controller) {
        final List<RCApplication> apps = new LinkedList<RCApplication>();
        
        final String epl = "select * from RCApplicationTable as app where app.controller = '" + controller + "'";
        final EPFireAndForgetQueryResult result = this.cep.executeQuery(epl);
        for(final EventBean e : result.getArray()) {
            apps.add(this.createRCApplicationFromEventBean(e));
        }

        return apps;
    }
    
    public RCApplication getRCApplication(final String name) {
        final String epl = "select * from RCApplicationTable() where name = '" + name + "'";
        
        final EPFireAndForgetQueryResult result = this.cep.executeQuery(epl);
        if(result.getArray().length > 0) {
            return this.createRCApplicationFromEventBean(result.getArray()[0]);
        }
        
        return null; // TODO exception here
    }

    public List<chip.event.meta.ErrorItem> getAllErrorsOfApplication(final String application) {
        final List<chip.event.meta.ErrorItem> errorItems = new LinkedList<chip.event.meta.ErrorItem>();
        final String epl = "select err.* from ErrorTable as err" + " where err.application = '" + application + "'";

        // TODO: remove when no more needed
        // This special handling is done because the query from time to time
        // throws an exception caused by a concurrent modification exception
        // That is a bug in the ESPER engine: retrying the query is an easy 
        // work-around.
        boolean done = false;
        do {
            try {
                final EPFireAndForgetQueryResult result = this.cep.executeQuery(epl);
                if((result != null) && (result.getArray() != null)) {
                    for(final EventBean e : result.getArray()) {
                        errorItems.add(this.createErrorItemFromEventBean(e));
                    }
                }

                done = true;
            }
            catch(final EPException ex) {
                if(ConcurrentModificationException.class.isInstance(ex.getCause()) == false) {
                    throw ex;
                }

                EsperDAO.log.warn("Got known exception, retrying...");
            }
        }
        while(done == false);

        return errorItems;
    }
    
    public boolean hasHostBeenDisabled(final String hostName) {
        final String epl = "select * from DisabledComputerTable as h" + " where h.hostName = '" + hostName + "'";

        final EPFireAndForgetQueryResult result = this.cep.executeQuery(epl);
        if((result != null) && (result.getArray() != null)) {
            return true;
        }
        
        return false;
    }
    
    public List<chip.event.meta.ErrorItem> getAllErrorsOfController(final String controller) {
        final List<chip.event.meta.ErrorItem> errorItems = new LinkedList<chip.event.meta.ErrorItem>();
        final String epl = "select err.* from ErrorTable as err" + " where err.controller = '" + controller + "'";

        // TODO: remove when no more needed
        // This special handling is done because the query from time to time
        // throws an exception caused by a concurrent modification exception
        // That is a bug in the ESPER engine: retrying the query is an easy 
        // work-around.
        boolean done = false;
        do {
            try {
                final EPFireAndForgetQueryResult result = this.cep.executeQuery(epl);
                if((result != null) && (result.getArray() != null)) {
                    for(final EventBean e : result.getArray()) {
                        errorItems.add(this.createErrorItemFromEventBean(e));
                    }
                }
                
                done = true;
            }
            catch(final EPException ex) {
                if(ConcurrentModificationException.class.isInstance(ex.getCause()) == false) {
                    throw ex;
                }
                
                EsperDAO.log.warn("Got known exception, retrying...");
            }
        }
        while(done == false);

        return errorItems;
    }

    private ErrorItem createErrorItemFromEventBean(final EventBean e) {
        final chip.event.meta.ErrorItem errorItem = new chip.event.meta.ErrorItem((String) e.get("controller"),
                                                                                  (String) e.get("application"),
                                                                                  (String) e.get("errorType"),
                                                                                  (String) e.get("errorDescription"));
        return errorItem;
    }

    public RCApplication createRCApplicationFromEventBean(final EventBean e) {
        @SuppressWarnings("unchecked")
        final RCApplication rcapp = new RCApplication((String) e.get("name"),
                                                      (String) e.get("oksClassName"),
                                                      (Boolean) e.get("restartableDuringRun"),
                                                      (IFFAILED) e.get("ifFailed"),
                                                      (IFDIES) e.get("ifDies"),
                                                      (STARTAT) e.get("startAt"),
                                                      (STOPAT) e.get("stopAt"),
                                                      (FSM) e.get("startAtState"),
                                                      (FSM) e.get("stopAtState"),
                                                      (Boolean) e.get("allowSpontaneousExit"),
                                                      (Set<String>) e.get("backupHosts"),
                                                      (Boolean) e.get("controlsTTCPartitions"),
                                                      (IFERROR) e.get("ifError"),
                                                      (Boolean) e.get("isController"),
                                                      (String) e.get("runningHost"),
                                                      (STATUS) e.get("status"),
                                                      (Boolean) e.get("notResponding"),
                                                      (MEMBERSHIP) e.get("membership"),
                                                      (Boolean) e.get("restarting"),
                                                      (Long) e.get("exitCode"),
                                                      (Long) e.get("exitSignal"),
                                                      (String) e.get("controller"),
                                                      (String) e.get("segment"),
                                                      (FSM) e.get("state"),
                                                      (Boolean) e.get("isTransitioning"),
                                                      (TRCOMMAND) e.get("currentTransitionCommand"),
                                                      (String) e.get("currentTransitionCommandUID"),
                                                      (TRCOMMAND) e.get("previousTransitionCommand"),
                                                      (Long) e.get("transitionTime"),
                                                      (Boolean) e.get("internalError"),
                                                      (RCCOMMAND) e.get("rccommand"),
                                                      (String[]) e.get("rccommandArgs"),
                                                      (String) e.get("rccommandUID"),
                                                      (Boolean) e.get("busy"),
                                                      (Long) e.get("timestamp"),
                                                      (Long) e.get("eventTimestamp"),
                                                      (FSM) e.get("parentState"),
                                                      (Boolean) e.get("parentIsTransitioning"),
                                                      (TRCOMMAND) e.get("parentCurrentTransitionCommand"),
                                                      (TRCOMMAND) e.get("parentPreviousTransitionCommand"),
                                                      (Boolean) e.get("parentIsExiting"),
                                                      (Boolean) e.get("parentIsInitializing"));
        return rcapp;
    }
}
