package chip;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;

import com.espertech.esper.common.client.EPCompiled;
import com.espertech.esper.common.client.EPException;
import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.common.client.EventType;
import com.espertech.esper.common.client.configuration.Configuration;
import com.espertech.esper.common.client.fireandforget.EPFireAndForgetPreparedQuery;
import com.espertech.esper.common.client.fireandforget.EPFireAndForgetQueryResult;
import com.espertech.esper.common.client.hook.exception.ExceptionHandler;
import com.espertech.esper.common.client.hook.exception.ExceptionHandlerContext;
import com.espertech.esper.common.client.hook.exception.ExceptionHandlerFactory;
import com.espertech.esper.common.client.hook.exception.ExceptionHandlerFactoryContext;
import com.espertech.esper.common.client.meta.EventTypeApplicationType;
import com.espertech.esper.common.client.meta.EventTypeMetadata;
import com.espertech.esper.common.client.meta.EventTypeTypeClass;
import com.espertech.esper.common.client.module.Module;
import com.espertech.esper.common.client.module.ModuleOrderException;
import com.espertech.esper.common.client.module.ModuleOrderUtil;
import com.espertech.esper.common.client.module.ParseException;
import com.espertech.esper.common.client.util.EventTypeBusModifier;
import com.espertech.esper.compiler.client.CompilerArguments;
import com.espertech.esper.compiler.client.EPCompileException;
import com.espertech.esper.compiler.client.EPCompiler;
import com.espertech.esper.compiler.client.EPCompilerProvider;
import com.espertech.esper.compiler.client.util.EPCompiledIOUtil;
import com.espertech.esper.runtime.client.EPDeployException;
import com.espertech.esper.runtime.client.EPDeployment;
import com.espertech.esper.runtime.client.EPRuntime;
import com.espertech.esper.runtime.client.EPRuntimeDestroyedException;
import com.espertech.esper.runtime.client.EPRuntimeProvider;
import com.espertech.esper.runtime.client.EPStatement;
import com.espertech.esper.runtime.client.EPUndeployException;
import com.espertech.esper.runtime.client.UpdateListener;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import chip.CHIPException.CEPConfigurationError;
import chip.annotation.Listeners;
import chip.annotation.Subscriber;
import chip.msg.EngineError;
import chip.utils.ExecutorFactory;


/**
 * Facade to mask interactions with the CEP engine from exposing implementation details.
 * 
 * @author lucamag
 */

public class CEPService {

    /**
     * Logger
     */
    private static final Log log = LogFactory.getLog(CEPService.class);

    @Autowired
    private ApplicationContext ctx;

    private final EPRuntime esper;

    private final String name;

    private final AtomicReference<Path> cacheDir = new AtomicReference<>();
    
    // Using SpEL to get properties list of files
    @Value("#{configuration['epl.files'].split(',')}")
    private List<String> modules;

    // The ESPER's engine processing goes here
    private final AtomicReference<ThreadPoolExecutor> esperProcessingPool = new AtomicReference<>();
        
    private final boolean enableMetrics;
    
    private final ConcurrentHashMap<String, EPFireAndForgetPreparedQuery> compiledQueries = new ConcurrentHashMap<>();
    private final ReadWriteLock destroyGuard = new ReentrantReadWriteLock();
    
    // This maps a Java class full name to the event it represents in EPL
    private final ConcurrentHashMap<String, String> eventNames = new ConcurrentHashMap<>();
    
    // This map holds all the EPL modules as they are (without parameter replacement) in text format 
    private final ConcurrentHashMap<String, String> rawEPLModules = new ConcurrentHashMap<>();
    
    // This map holds all the EPL modules in text format after parameter replacement 
    private final ConcurrentHashMap<String, String> interpolatedEPLModules = new ConcurrentHashMap<>();
    
    // Exception handler
    public static class CEPExceptionHandlerFactory implements ExceptionHandlerFactory {
        @Override
        public ExceptionHandler getHandler(final ExceptionHandlerFactoryContext factoryContext) {
            return new ExceptionHandler() {            
                private final Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
                
                @Override
                public void handle(final ExceptionHandlerContext context) {
                    final String eventString;

                    final EventBean event = context.getCurrentEvent();
                    if(event != null) {
                        eventString = "\"" + event.getEventType().getName() + "\":\n" + this.gson.toJson(event.getUnderlying());
                    } else {
                        eventString = "not available";
                    }
                    
                    final EngineError issue = new EngineError(context.getStatementName(), 
                                                              eventString, 
                                                              context.getEpl(), 
                                                              context.getThrowable());
                    ers.Logger.error(issue);
                }
            };
        }        
    }
    
    private final ConcurrentHashMap<EPLReplacementString, String> eplParameters = new ConcurrentHashMap<>();
    
    public enum EPLReplacementString {
        ROOT_CONTROLLER_NAME,
        DO_RECOVERY,
        DO_AUTO_PROCEDURE,
        MAX_EVENT_IS_SERVER_NAME,
        MAX_EVENT_IS_INFO_NAME,
        MAX_EVENT_IS_ATTRIBUTE_NAME;

        @Override
        public String toString() {
            return "::" + this.name() + "::";
        }                
    }
        
    /**
     * Ctor.
     * 
     * @param name the name of this instance of the cep engine
     * @param enableMetrics flag to enable the ESPER built-in metrics reporting
     */
    public CEPService(final String name,
                      final Boolean useInternalTimer,
                      final Boolean enableMetrics,
                      final ThreadPoolExecutor processingPool)
    {
        this.esperProcessingPool.set(processingPool);
                
        final Configuration esperConfig = new Configuration();
        esperConfig.getCommon().addAnnotationImport("chip.annotation.*");
        esperConfig.getCommon().addImport("org.apache.commons.lang.StringUtils");

        // In case of internal timer disabled, time events have to be injected by external threads,
        // no internal timer thread exists then.
        esperConfig.getRuntime().getThreading().setInternalTimerEnabled(useInternalTimer.booleanValue());
        esperConfig.getRuntime().getThreading().setThreadPoolTimerExec(true);
        esperConfig.getRuntime().getThreading().setThreadPoolTimerExecNumThreads(10);
        esperConfig.getRuntime().getThreading().setInternalTimerMsecResolution(10);

        esperConfig.getRuntime().getThreading().setThreadPoolInbound(false);
        esperConfig.getRuntime().getThreading().setThreadPoolOutbound(false);

        // Enable Esper metrics (at initialization); it allows to enable/disable it at runtime
        esperConfig.getRuntime().getMetricsReporting().setEnableMetricsReporting(true);
        esperConfig.getRuntime().getMetricsReporting().setRuntimeInterval(10000);
        esperConfig.getRuntime().getMetricsReporting().setStatementInterval(10000);
        esperConfig.getRuntime().getMetricsReporting().setThreading(true);
                    
        // Enable priority
        esperConfig.getRuntime().getExecution().setPrioritized(true);

        // Set the exception handler
        esperConfig.getRuntime().getExceptionHandling().addClass(CEPExceptionHandlerFactory.class);
        
        // Allow subscribers (false by default since ESPER 8)
        esperConfig.getCompiler().getByteCode().setAllowSubscriber(true);
        
        // Not all streams can be injected with "sendEvent" (allowed streams are marked in the EPL)
        esperConfig.getCompiler().getByteCode().setBusModifierEventType(EventTypeBusModifier.NONBUS);       
                       
        this.esper = EPRuntimeProvider.getRuntime(name, esperConfig);

        this.enableMetrics = enableMetrics.booleanValue();
                
        this.name = name;
        
        try {
            Path tmpPath;
            final EnumSet<PosixFilePermission> perms = EnumSet.of(PosixFilePermission.OWNER_READ, PosixFilePermission.OWNER_WRITE, PosixFilePermission.OWNER_EXECUTE,
                                                                  PosixFilePermission.GROUP_READ, PosixFilePermission.GROUP_WRITE, PosixFilePermission.GROUP_EXECUTE,
                                                                  PosixFilePermission.OTHERS_READ, PosixFilePermission.OTHERS_WRITE, PosixFilePermission.OTHERS_EXECUTE);
            try {
                // First try in the defined cache dir (if any)
                tmpPath = Files.createTempDirectory(FileSystems.getDefault().getPath((System.getenv("TDAQ_CES_CACHE_DIR"))), "CHIP-", PosixFilePermissions.asFileAttribute(perms));
            }
            catch(final IllegalArgumentException | IOException | NullPointerException ex) {
                // Then try in the system default temporary directory 
                tmpPath = Files.createTempDirectory("CHIP-", PosixFilePermissions.asFileAttribute(perms));                
            }
            
            // Be sure the directory is removed when the application exits
            tmpPath.toFile().deleteOnExit();
                        
            CEPService.log.debug("Created temporary directory \"" + tmpPath.toString() + "\" for caching compiled EPL modules");
        
            this.cacheDir.set(tmpPath);
        }
        catch(final IOException ex) {
            CEPService.log.error("Failed to create temporary directory for caching compiled EPL modules", ex);
        }
    }

    public EPRuntime getEsper() {
        return this.esper;
    }

    /**
     * This method prepares the CEP engine to receive events from injectors.
     * <p>
     * Statements must be created and deployed and listener/subscriber in place.
     * 
     * @throws ConfigurationException
     */
    public void configure() throws CHIPException.CEPConfigurationError {
        this.resetProcessingExecutor();
        
        // Better to clear this so that any attempt to map twice the same
        // class to different events will be detected
        this.eventNames.clear();

        // Read and deploy modules
        this.deployModules();
        
        if(this.enableMetrics == true) {
            this.esper.getMetricsService().setMetricsReportingEnabled();
            CEPService.log.info("Enabled metrics reporting");
        } else {
            this.esper.getMetricsService().setMetricsReportingDisabled();
            CEPService.log.info("Disabled metrics reporting");
        }
                        
        CEPService.log.info("CEP engine configured.");
    }

    public void initAndConfigure() throws CHIPException.CEPConfigurationError {
        this.esper.initialize();
        this.configure();
    }

    // This is used to be able to understand whether 'route' or 'sendEvent' should be used
    private final ThreadLocal<Boolean> processing = new ThreadLocal<Boolean>() {
        @Override
        protected Boolean initialValue() {
            return Boolean.FALSE;
        }
    };

    public void injectEventSynch(final Object obj) {
        final String className = obj.getClass().getName();
        final String eventName = this.eventNames.get(className);
        if(eventName != null) {
            if(this.processing.get().equals(Boolean.TRUE) == true) {
                try {
                    this.esper.getEventService().getEventSender(eventName).routeEvent(obj);
                }
                catch(final EPRuntimeDestroyedException a) {
                    CEPService.log.info("Cannot inject events into the engine because it has been destroyed: " + a);
                }
                catch(final Exception a) {
                    CEPService.log.error("Cannot inject events into the engine: " + a);
                }
            } else {
                this.processing.set(Boolean.TRUE);
                try {
                    this.esper.getEventService().getEventSender(eventName).sendEvent(obj);
                }
                catch(final EPRuntimeDestroyedException a) {
                    CEPService.log.info("Cannot inject events into the engine because it has been destroyed: " + a);
                }
                catch(final Exception a) {
                    CEPService.log.error("Cannot inject events into the engine: " + a);
                }
                finally {
                    this.processing.set(Boolean.FALSE);
                }
            }            
        } else {
            CEPService.log.error("Cannot inject events into the engine: cannot find the corresponding stream for event of class \""
                + className + "\"");
        }
    }

    public Future<?> injectEvent(final Object event) {
        try {
            return this.esperProcessingPool.get().submit(() -> { this.injectEventSynch(event); });
        }
        catch(final RejectedExecutionException e) {
            CEPService.log.error("Error submitting task: " + e);
            
            final CompletableFuture<?> fut = new CompletableFuture<>();
            fut.completeExceptionally(e);
            return fut;
        }
    }
    
    private List<Module> orderModules() throws IOException, ParseException, ModuleOrderException {
        final EPCompiler compiler = EPCompilerProvider.getCompiler();
        final List<Module> esperModules = new ArrayList<>();
               
        for(final String moduleName : this.modules) {      
            final String modText = this.rawEPLModules.computeIfAbsent(moduleName, k -> {
                try {
                    return new String(Files.readAllBytes(new ClassPathResource(k).getFile().toPath()));
                }
                catch(final IOException e) {
                    throw new RuntimeException(e);
                }
            });
            
            final Module module = compiler.parseModule(this.replaceEPLParameters(modText));
            esperModules.add(module);
        }
        
        return ModuleOrderUtil.getModuleOrder(esperModules, Collections.<String>emptySet(), null).getOrdered();
    }
    
    private void deployModules() throws CHIPException.CEPConfigurationError {
        // See http://www.espertech.com/2018/10/23/esper-8-migrating-step-by-step-for-existing-applications/

        CEPService.log.debug("Loading modules...");
        
        // This excludes any event processing while modules are deployed
        this.esper.getRuntimeInstanceWideLock().writeLock().lock();
        
        try {
            final EPCompiler compiler = EPCompilerProvider.getCompiler();
            final CompilerArguments compilerArgs = new CompilerArguments(this.esper.getConfigurationDeepCopy());
            compilerArgs.getPath().add(this.esper.getRuntimePath());                       
            
            final List<Module> ordered = this.orderModules();
                        
            for(final Module mod : ordered) {                
                final String modName = mod.getName();
                final String modText = mod.getModuleText();
                
                CEPService.log.info("Loading module " + modName + "...");
                
                // Jar file to save the compiled EPL modules
                File moduleJar = null;
                
                EPCompiled compiled = null;
                
                // To load compiled modules, the temporary directory should exists
                final Path cacheDirPath = this.cacheDir.get();
                if(cacheDirPath != null) {
                    moduleJar = new File(cacheDirPath.toString() + "/" + modName + ".jar");
                    moduleJar.deleteOnExit(); // Be sure the files are removed when the application exits
                                        
                    // The first time (i.e., when the application starts) the modules will not be loaded from the cache
                    // All the other times (i.e., after a DB reload), modules should be loaded from jar files
                    final boolean recompile = !modText.equals(this.interpolatedEPLModules.put(modName, modText));
                    if(recompile == false) {
                        CEPService.log.debug("Trying to load compiled module " + modName + " from cache");
                        
                        try {
                            compiled = EPCompiledIOUtil.read(moduleJar);
                            CEPService.log.debug("Module " + modName + " loaded from cache");
                        }
                        catch(final IOException ex) {
                            CEPService.log.warn("Could not load compiled module " + modName + " from cache: " + ex);
                        }
                    } 
                }

                // If the loading from jar files fails, then go back to the standard compilation at runtime
                if(compiled == null) {
                    CEPService.log.debug("Compiling module " + modName + "...");
                    
                    compiled = compiler.compile(mod, compilerArgs);
                    
                    if(moduleJar != null) {
                        try {
                            // Pedantic: eventually remove any previous file
                            if((moduleJar.exists() == true) && (moduleJar.delete() == true)) {
                                CEPService.log.debug("Deleted old " + moduleJar.getName() + " file");
                            }
                            
                            // Save the compiled module
                            EPCompiledIOUtil.write(compiled, moduleJar);
                            CEPService.log.debug("Compiled module " + modName + " saved to cache");
                        }
                        catch(final IOException ex) {
                            CEPService.log.warn("Could not save compiled module " + modName + " to cache: " + ex);
                        }
                    }
                }
                
                compilerArgs.getPath().add(compiled);
                final EPDeployment deployment = this.esper.getDeploymentService().deploy(compiled);
                for(final EPStatement stm : deployment.getStatements()) {
                    this.processStatement(stm);
                }
                
                CEPService.log.info("Module " + modName + " fully loaded");                
            }
        }
        catch(final EPRuntimeDestroyedException e) {
            CEPService.log.trace(e);
            e.printStackTrace();
            throw new CHIPException.CEPConfigurationError("EPS destroyed \n", e);
        }
        catch(final BeansException | ClassNotFoundException | EPDeployException | EPCompileException | IOException | ParseException | ModuleOrderException e) {
            CEPService.log.trace(e);
            e.printStackTrace();
            throw new CHIPException.CEPConfigurationError("Problem reading EPL modules \n", e);
        }
        finally {
            this.esper.getRuntimeInstanceWideLock().writeLock().unlock();
        }
    }

    /**
     * Process @Subscriber and @Listeners custom annotations and maps java classe to corresponding streams.
     * 
     * @param statement
     * @throws ClassNotFoundException 
     * @throws BeansException 
     * @throws CEPConfigurationError 
     */
    private void processStatement(final EPStatement statement) throws BeansException, ClassNotFoundException, CEPConfigurationError {
        // Inspect the EPL statement
        // If a stream has a Java class as its underlying event, create a map to be used in sendEvent (or routeEvent) 
        final EventType type = statement.getEventType();
        final EventTypeMetadata metaData = type.getMetadata();
                
        if(metaData.getTypeClass().equals(EventTypeTypeClass.STREAM)
           && metaData.getApplicationType().equals(EventTypeApplicationType.CLASS))
        {
            final String streamName = metaData.getName();
            final String classFullName = type.getUnderlyingType().getName();
            final String previous = this.eventNames.putIfAbsent(classFullName, streamName);
            if((previous != null) && (previous.equals(streamName) == false)) {
                throw new CHIPException.CEPConfigurationError("EPL inconsistency: the Java class \"" + classFullName
                                                              + "\" is the underlying event for at least two streams (\"" + streamName
                                                              + "\" and \"" + previous + "\")");
            } else if(previous == null) {            
                CEPService.log.debug("***** \"" + classFullName + "\" mapped to stream \"" + streamName + "\"");
            }
        }

        // Processing custom annotations: Subscriber and Listeners
        final Annotation[] annotations = statement.getAnnotations();
        for(final Annotation annotation : annotations) {
            if(annotation instanceof Subscriber) {
                final Subscriber subscriber = (Subscriber) annotation;
                final Object obj = this.getBeanWithSpring(subscriber.className());
                statement.setSubscriber(obj);

                CEPService.log.debug("Subscriber " + obj.getClass().getCanonicalName() + " attached to statement " + statement.getName());
            } else if(annotation instanceof Listeners) {
                final Listeners listeners = (Listeners) annotation;
                for(final String className : listeners.classNames()) {
                    final Object obj = this.getBeanWithSpring(className);
                    statement.addListener((UpdateListener) obj);

                    CEPService.log.debug("Listener " + obj.getClass().getCanonicalName() + " attached to statement " + statement.getName());
                }
            }
        }
    }

    /**
     * Get the subscriber instance from the Spring context configuration, allowing for multiple implementation for production and testing.
     * 
     * @param fqdn
     * @return
     * @throws ClassNotFoundException 
     * @throws BeansException 
     */
    public Object getBeanWithSpring(final String fqdn) throws BeansException, ClassNotFoundException {
        return this.ctx.getBean(Class.forName(fqdn));
    }

    public <T> T getBeanWithSpring(final Class<T> c) throws BeansException {
        return this.ctx.getBean(c);
    }
    
    public EPFireAndForgetQueryResult executeQuery(final String epl) {
        try {
            // Protect with a read lock
            // Better to not access already compiled queries when the engine is destroyed
            this.destroyGuard.readLock().lock();

            final EPFireAndForgetPreparedQuery preparedQuery = this.compiledQueries.computeIfAbsent(epl, (final String eplStatement) -> {
                try {
                    // Compilation should not happen concurrently in different threads
                    final EPCompiler compiler = EPCompilerProvider.getCompiler();
                    synchronized(compiler) {
                        final CompilerArguments compilerArgs = new CompilerArguments(this.esper.getConfigurationDeepCopy());
                        compilerArgs.getPath().add(this.esper.getRuntimePath());

                        final EPCompiled compiled = compiler.compileQuery(epl, compilerArgs);
                        return this.esper.getFireAndForgetService().prepareQuery(compiled);
                    }
                }
                catch(final EPCompileException ex) {
                    // This method is called in a controlled way by the EsperDAO class
                    // The EPL code MUST be correct, otherwise it is a bug
                    // Convert the exception in a runtime one
                    throw new EPException(ex);
                }
            });

            return preparedQuery.execute();
        }
        finally {
            this.destroyGuard.readLock().unlock();
        }
    }
    
    public void stop() {
        CEPService.log.debug("Shutting down callback thread pool...");
        this.stopProcessingExecutor();
        
        CEPService.log.debug("Shutting down Esper...");
        try {
            this.destroyGuard.writeLock().lock();
            
            // Clear all the already compiled queries just before destroying the engine
            this.compiledQueries.clear();
            
            try {
                this.esper.getDeploymentService().undeployAll();
            }
            catch(final EPRuntimeDestroyedException | EPUndeployException ex) {
                ex.printStackTrace();
            }
            
            this.esper.destroy();
        }
        finally {
            this.destroyGuard.writeLock().unlock();
        }
    }

    public String getName() {
        return this.name;
    }

    public void setEPLReplacementValue(final EPLReplacementString key, final String value) {
        this.eplParameters.put(key, value);
    }
    
    private String replaceEPLParameters(final String modTxt) {
        String s = modTxt;
        for(final Map.Entry<EPLReplacementString, String> entry : this.eplParameters.entrySet()) {
            s = s.replace(entry.getKey().toString(), entry.getValue());            
        }
        return s;
    }
    
    // Never cache the result of this call in client code
    ThreadPoolExecutor getProcessingExecutor() {
        return this.esperProcessingPool.get();
    }    
    
    void stopProcessingExecutor() {
        final ThreadPoolExecutor tp = this.esperProcessingPool.get();
        tp.shutdownNow();
        try {
            tp.awaitTermination(5, TimeUnit.SECONDS);
        }
        catch(final InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
    
    void resetProcessingExecutor() {
        ThreadPoolExecutor tp = this.esperProcessingPool.get();
        final ThreadPoolExecutor ntp = ExecutorFactory.newThreadPool("Processing",
                                                                     Integer.getInteger("chip.processing.threads",
                                                                                        tp.getMaximumPoolSize()).intValue(),
                                                                     false);

        boolean updated = false;
        do {
            updated = this.esperProcessingPool.compareAndSet(tp, ntp);
            if(updated == false) {
                tp = this.esperProcessingPool.get();
            } else if(tp.isShutdown() == false) {
                tp.shutdownNow(); // For extra safety
            }
        }
        while(updated == false);
    }
}
