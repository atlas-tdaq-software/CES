#!/usr/bin/env tdaq_python

# Script to enable or disable writing metrics information from CHIP to IS and set the interval

from optparse import OptionParser
from ipc import IPCPartition, getPartitions
from ispy import *
import sys

if __name__ == '__main__':

    parser = OptionParser(usage='enable_metrics_is.py -p partition [-s server] -e enabled -i interval')

    parser.add_option('-p', action='store', dest='partition', type='string',
                      help='Partition name to work in.')
    parser.add_option('-s', action='store', dest='server', type='string',
                      help='(Optional) IS Server name, default: RunParams.')
    parser.add_option('-e', action='store', dest='enabled', type='int',
                      help='1 enables and 0 disables metrics reporting (engine and statement metrics).')
    parser.add_option('-i', action='store', dest='interval', type='long',
                      help='Interval in milliseconds for the statement metrics reporting (non-negative value), 0 will disable  statement metrics reporting while keeping engine metrics reporting enabled.')

    parser.set_defaults(server='RunParams')
    (options, args) = parser.parse_args()

    if not options.partition:
        print("You must provide a partition name as parameter.")
        print("The available partitions are:")
        print("")
        for partition in getPartitions():
            print((partition.name()))
        sys.exit(1)

    p = IPCPartition(options.partition)

    print("You have chosen partition:", p.name())
    if not p.isValid():
        print("However, this is not a valid partition.")
        print("The available partitions are:")
        print("")
        for partition in getPartitions():
            print((partition.name()))
        print("\nExiting...")
        sys.exit(1)

    server_valid = False

    svr_it = ISServerIterator(p)
    while svr_it.next():
        if svr_it.name() == options.server:
            server_valid = True
    if server_valid is False:
        print("The IS server name is not valid for the given partition.")
        print("There are the following IS servers in the partition:")
        print("")
        svr_it = ISServerIterator(p)
        while svr_it.next():
            print((svr_it.name()))
        print("\nExiting...")
        sys.exit(1)

    if options.enabled is None or options.enabled > 1 or options.enabled < 0 or options.interval is None or options.interval < 0:
        parser.print_help()
        sys.exit(0)

    obj = ISObject(p, options.server + '.EnableCHIPMetricsIs', 'EnableCHIPMetricsIs')
    obj.setAttributeValue('enabled', options.enabled)
    obj.setAttributeValue('interval', options.interval)
    obj.checkin()
